# Game Master Client

The Game Master Client is a user interface for administering the Arena Tactics server.

To be fully functional the server engine and API must both be running.

## Development

This is a work in progress, incomplete and prone to inconceivable levels of refactoring.

```bash
cp .env.example .env
source .env
yarn install
quasar dev
```

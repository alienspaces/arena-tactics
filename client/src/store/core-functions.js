import Vue from 'vue'

export function actionFunctions (store) {
  var actions = {
    clearRecords: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.clearRecords(context, payload)
    },
    getRecords: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.getRecords(context, payload)
    },
    getRecord: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.getRecord(context, payload)
    },
    createRecord: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.createRecord(context, payload)
    },
    updateRecord: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.updateRecord(context, payload)
    },
    deleteRecord: function (context, payload) {
      Vue.$log.info('(actionFunctions) payload', payload)
      store.deleteRecord(context, payload)
    }
  }
  return actions
}

export function mutationFunctions (store) {
  var mutations = {
    clearStoreRecords: function (state, payload) {
      Vue.$log.info('(mutationFunctions) state', state)
      Vue.$log.info('(mutationFunctions) params', payload.params)
      store.clearStoreRecords(state, payload.params)
    },
    addStoreRecords: function (state, payload) {
      Vue.$log.info('(mutationFunctions) state', state)
      Vue.$log.info('(mutationFunctions) params', payload.params)
      Vue.$log.info('(mutationFunctions) data', payload.data)
      store.addStoreRecords(state, payload.params, payload.data)
    },
    addStoreRecord: function (state, payload) {
      Vue.$log.info('(mutationFunctions) state', state)
      Vue.$log.info('(mutationFunctions) params', payload.params)
      Vue.$log.info('(mutationFunctions) data', payload.data)
      store.addStoreRecord(state, payload.params, payload.data)
    },
    updateStoreRecord: function (state, payload) {
      Vue.$log.info('(mutationFunctions) state', state)
      Vue.$log.info('(mutationFunctions) params', payload.params)
      Vue.$log.info('(mutationFunctions) data', payload.data)
      store.updateStoreRecord(state, payload.params, payload.data)
    },
    deleteStoreRecord: function (state, payload) {
      Vue.$log.info('(mutationFunctions) state', state)
      Vue.$log.info('(mutationFunctions) params', payload.params)
      Vue.$log.info('(mutationFunctions) data', payload.data)
      store.deleteStoreRecord(state, payload.params, payload.data)
    }
  }
  return mutations
}

export function getterFunctions (store) {
  var getters = {
    getRecords: (state, getters) => (params) => {
      Vue.$log.info('(getterFunctions) state', state)
      Vue.$log.info('(getterFunctions) getters', getters)
      Vue.$log.info('(getterFunctions) params', params)
      return store.getStoreRecords(state, getters, params)
    },
    getRecord: (state, getters) => (params, recId) => {
      Vue.$log.info('(getterFunctions) state', state)
      Vue.$log.info('(getterFunctions) rec', getters)
      Vue.$log.info('(getterFunctions) recId', recId)
      return store.getStoreRecord(state, getters, params, recId)
    }
  }
  return getters
}

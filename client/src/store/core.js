import Vue from 'vue'

// NOTE: Data will be indexed by `params` defined
// in the resource API, be sure to order that
// array correctly when instantiating the API.

export default class Store {
  constructor (resourceName, resourceApi) {
    this.resourceName = resourceName
    this.resourceApi = resourceApi
    this.resourceParams = resourceApi.params
    this.state = {}
    Vue.set(this.state, this.resourceName, {})
    Vue.set(this.state[this.resourceName], 'records', {})
  }

  // actions

  // failure function
  failureFunc (context, payload, response) {
    Vue.$log.warn('(action) Context', context)
    Vue.$log.warn('(action) Payload', payload)
    Vue.$log.warn('(action) Response', response)

    payload.failureFunc(response)
  }

  // success function
  successFunc (context, payload, response, mutation) {
    Vue.$log.info('(action) Context', context)
    Vue.$log.info('(action) Payload', payload)
    Vue.$log.info('(action) Response', response)
    if (response.data) {
      context.commit(mutation, { params: payload.params, data: response.data.data })
    }
    payload.successFunc(response)
  }

  // clearRecords
  clearRecords (context, payload) {
    //
    Vue.$log.info('(action) payload', payload)

    Vue.$log.info('(action) resource params', this.resourceParams)

    // params
    var params = {}
    if (payload.params) {
      for (var idx in this.resourceParams) {
        var param = this.resourceParams[idx]
        if (payload.params[param] === null) {
          Vue.$log.info('(action) payload missing param', param)
          continue
        }
        params[param] = payload.params[param]
      }
    }

    Vue.$log.info('(action) payload params', payload.params)
    Vue.$log.info('(action) resulting params', params)

    context.commit('clearStoreRecords', { params: params, data: {} })

    return true
  }

  // getRecords
  getRecords (context, payload) {
    //
    Vue.$log.info('(action) payload', payload)

    // success function
    var successFunc = response => {
      return this.successFunc(context, payload, response, 'addStoreRecords')
    }

    // failure function
    var failureFunc = response => {
      Vue.$log.warn('(failureFunc) response', response)
      return this.failureFunc(context, payload, response)
    }

    Vue.$log.info('(action) resource params', this.resourceParams)

    // params
    var params = {}
    if (payload.params) {
      for (var idx in this.resourceParams) {
        var param = this.resourceParams[idx]
        if (payload.params[param] === null) {
          Vue.$log.info('(action) payload missing param', param)
          continue
        }
        params[param] = payload.params[param]
      }
    }

    Vue.$log.info('(action) payload params', payload.params)
    Vue.$log.info('(action) resulting params', params)

    this.resourceApi.getRecords(params, successFunc, failureFunc)

    return true
  }

  // getRecord
  getRecord (context, payload) {
    //
    Vue.$log.info('(action) payload', payload)

    // success function
    var successFunc = response => {
      return this.successFunc(context, payload, response, 'addStoreRecord')
    }

    // failure function
    var failureFunc = response => {
      return this.failureFunc(context, payload, response)
    }

    // params
    var params = {}
    if (payload.params) {
      for (var idx in this.resourceParams) {
        var param = this.resourceParams[idx]
        if (payload.params[param] === null) {
          Vue.$log.info('(action) payload missing param', param)
          continue
        }
        params[param] = payload.params[param]
      }
    }

    Vue.$log.info('(action) payload params', payload.params)
    Vue.$log.info('(action) resulting params', params)

    this.resourceApi.getRecord(params, successFunc, failureFunc)
  }

  // create record
  createRecord (context, payload) {
    //
    Vue.$log.info('(action) payload', payload)

    // check required
    if (payload.successFunc === 'undefined') {
      Vue.$log.error('(action) payload missing successFunc, cannot create record')
      return
    }

    if (payload.failureFunc === 'undefined') {
      Vue.$log.error('(action) payload missing failureFunc, cannot create record')
      return
    }

    // success function
    var successFunc = response => {
      return this.successFunc(context, payload, response, 'addStoreRecord')
    }

    // failure function
    var failureFunc = response => {
      return this.failureFunc(context, payload, response)
    }

    // params
    var params = {}
    if (payload.params) {
      for (var idx in this.resourceParams) {
        var param = this.resourceParams[idx]
        if (payload.params[param] === null) {
          Vue.$log.info('(action) payload missing param', param)
          continue
        }
        params[param] = payload.params[param]
      }
    }

    Vue.$log.info('(action) payload params', payload.params)
    Vue.$log.info('(action) resulting params', params)

    this.resourceApi.createRecord(params, payload.data, successFunc, failureFunc)
  }

  // update record
  updateRecord (context, payload) {
    //
    Vue.$log.info('payload', payload)

    // check required
    if (payload.successFunc === 'undefined') {
      Vue.$log.error('(action) payload missing successFunc, cannot update record')
      return
    }

    if (payload.failureFunc === 'undefined') {
      Vue.$log.error('(action) payload missing failureFunc, cannot update record')
      return
    }

    // success function
    var successFunc = response => {
      return this.successFunc(context, payload, response, 'updateStoreRecord')
    }

    // failure function
    var failureFunc = response => {
      return this.failureFunc(context, payload, response)
    }

    // params
    var params = {}
    if (payload.params) {
      for (var idx in this.resourceParams) {
        var param = this.resourceParams[idx]
        if (payload.params[param] === null) {
          Vue.$log.info('(action) payload missing param', param)
          continue
        }
        params[param] = payload.params[param]
      }
    }

    Vue.$log.info('(action) payload params', payload.params)
    Vue.$log.info('(action) resulting params', params)

    this.resourceApi.updateRecord(
      params,
      payload.data,
      successFunc,
      failureFunc
    )

    return true
  }

  // deleteRecord
  deleteRecord (context, payload) {
    //
    Vue.$log.info('payload', payload)

    // check required
    if (payload.successFunc === 'undefined') {
      Vue.$log.error('(action) payload missing successFunc, cannot delete record')
      return
    }

    if (payload.failureFunc === 'undefined') {
      Vue.$log.error('(action) payload missing failureFunc, cannot delete record')
      return
    }

    // success function
    var successFunc = response => {
      Vue.$log.info('(action) success Response', response)
      return this.successFunc(context, payload, response, 'deleteStoreRecord')
    }

    // failure function
    var failureFunc = response => {
      Vue.$log.error('(action) failure Response', response)
      return this.failureFunc(context, payload, response)
    }

    // params
    var params = {}
    for (var idx in this.resourceParams) {
      var param = this.resourceParams[idx]
      if (payload.params[param] === null) {
        Vue.$log.info('(action) payload missing param', param)
        continue
      }
      params[param] = payload.params[param]
    }

    Vue.$log.info('(action) params', params)

    this.resourceApi.deleteRecord(
      params,
      successFunc,
      failureFunc
    )

    return true
  }

  // mutations

  // clearStoreRecords
  clearStoreRecords (state, params) {
    //
    Vue.$log.info('(getter) state', state)
    Vue.$log.info('(getter) params', params)
    Vue.$log.info('(getter) resourceParams', this.resourceParams)

    var key = ''
    var stateRef = {}

    // build key / verify params
    for (var idx = 0; idx < this.resourceParams.length; idx++) {
      var param = this.resourceParams[idx]
      if (param === 'id') {
        continue
      }

      if (params[param] === 'undefined') {
        Vue.$log.error('(getter) missing params key', param)
      }

      Vue.$log.info('(getter) current key', key)

      key = key + param + '-' + params[param]
    }

    if (key === '') {
      stateRef = state[this.resourceName]
      key = 'records'
    } else {
      stateRef = state[this.resourceName]['records']
    }

    Vue.$log.info('(mutation) clearing records indexed by key >' + key + '<')

    Vue.delete(stateRef, key)

    return stateRef
  }

  // addStoreRecords
  addStoreRecords (state, params, recs) {
    //
    Vue.$log.info('(mutation) state', state)
    Vue.$log.info('(mutation) params', params)
    if (recs) {
      Vue.$log.info('(mutation) record length', recs.length)

      if (typeof (params) !== 'object') {
        params = {}
      }

      recs.forEach(rec => {
        // add/update params 'id' from record for indexing
        if (rec.id) {
          params['id'] = rec.id
        }
        this.addStoreRecord(state, params, rec)
      })
    }
  }

  // addStoreRecord
  addStoreRecord (state, params, rec) {
    //
    Vue.$log.info('(mutation) state', state)
    Vue.$log.info('(mutation) params', params)
    Vue.$log.info('(mutation) rec', rec)
    Vue.$log.info('(mutation) resourceParams', this.resourceParams)

    this.updateStoreRecord(state, params, rec)
  }

  // updateStoreRecord
  updateStoreRecord (state, params, rec) {
    //
    Vue.$log.info('(mutation) state', state)
    Vue.$log.info('(mutation) params', params)
    Vue.$log.info('(mutation) resourceParams', this.resourceParams)
    Vue.$log.info('(mutation) rec', rec)

    var key = ''
    if (typeof (state[this.resourceName].records) !== 'object') {
      Vue.set(state[this.resourceName], 'records', {})
    }

    var stateRef = state[this.resourceName]['records']

    // build key / verify params
    for (var idx = 0; idx < this.resourceParams.length; idx++) {
      var param = this.resourceParams[idx]
      if (param === 'id') {
        continue
      }

      if (params[param] === 'undefined') {
        Vue.$log.error('(mutation) missing params key', param)
      }

      key = key + param + '-' + params[param]

      Vue.$log.info('(mutation) current key', key)
    }

    Vue.$log.info('(mutation) updating record indexed by key >' + key + '<')

    if (key !== '') {
      Vue.$log.info('(mutation) using key', key)
      if (typeof (stateRef[key]) !== 'object') {
        Vue.set(stateRef, key, {})
      }
      stateRef = stateRef[key]
    }

    Vue.set(stateRef, rec.id, rec)

    Vue.$log.info('(mutation) resulting state', state)
  }

  // deleteStoreRecord
  deleteStoreRecord (state, params, rec) {
    //
    Vue.$log.info('(mutation) state', state)
    Vue.$log.info('(mutation) params', params)
    Vue.$log.info('(mutation) rec', rec)

    var key = ''
    var stateRef = state[this.resourceName]['records']

    // build key / verify params
    for (var idx = 0; idx < this.resourceParams.length; idx++) {
      var param = this.resourceParams[idx]
      if (param === 'id') {
        continue
      }

      if (params[param] === 'undefined') {
        Vue.$log.error('(mutation) missing params key', param)
      }

      key = key + param + '-' + params[param]

      Vue.$log.info('(mutation) current key', key)
    }

    if (key !== '') {
      Vue.$log.info('(mutation) using key', key)
      stateRef = stateRef[key]
    }

    Vue.delete(stateRef, rec.id)
  }

  // getters
  getStoreRecords (state, getters, params) {
    //
    Vue.$log.info('(getter) state', state)
    Vue.$log.info('(getter) params', params)
    Vue.$log.info('(getter) resourceParams', this.resourceParams)

    var key = ''
    var returnRecs = []
    var stateRef = state[this.resourceName]['records']

    // build key / verify params
    for (var idx = 0; idx < this.resourceParams.length; idx++) {
      var param = this.resourceParams[idx]
      if (param === 'id') {
        continue
      }

      if (params[param] === 'undefined') {
        Vue.$log.error('(getter) missing params key', param)
      }

      Vue.$log.info('(getter) current key', key)

      key = key + param + '-' + params[param]
    }

    Vue.$log.info('(mutation) returning records indexed by key >' + key + '<')

    if (key !== '') {
      stateRef = stateRef[key]
    }

    for (var id in stateRef) {
      Vue.$log.info('(mutation) Adding prop ID >' + id + '<')
      var rec = stateRef[id]
      Vue.$log.info('(mutation) Adding ID >' + rec.id + '<')
      returnRecs.push(rec)
    }

    Vue.$log.info('(mutation) returning records', returnRecs)

    return returnRecs
  }

  getStoreRecord (state, getters, params, recId) {
    // return null when no recID
    if (typeof (recId) === 'undefined') {
      return null
    }

    Vue.$log.info('(getter) state', state[this.resourceName])
    Vue.$log.info('(getter) recId', recId)

    var key = ''
    var stateRef = state[this.resourceName]['records']

    Vue.$log.info('(mutation) stateRef', stateRef)

    for (var param in params) {
      Vue.$log.info('(mutation) current key', key)
      if (param === 'id') {
        break
      }
      key = key + param + '-' + params[param]
    }

    Vue.$log.info('(mutation) returning records indexed by key', key)
    if (key !== '') {
      if (typeof (stateRef[key]) === 'undefined') {
        Vue.$log.warn('(mutation) error key does not exist', key)
        return
      }
      stateRef = stateRef[key]
    }

    return stateRef[recId]
  }

  // state
  getState () {
    //
    console.log('(state) resourceName', this.resourceName)

    if (!this.state[this.resourceName]) {
      //
      console.log('(state) initialising')

      this.state[this.resourceName] = {}

      var stateRef = this.state

      for (var param in this.resourceParams) {
        console.log('(state) resource param', this.resourceParams[param])
        stateRef[this.resourceParams[param]] = {}
        stateRef = stateRef[this.resourceParams[param]]
      }
    }

    console.log('(state) built empty state', this.state)

    return this.state
  }
}

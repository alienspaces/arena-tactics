import Vue from 'vue'
import Vuex from 'vuex'

// application resources
import app from './app'
import appAttribute from './appattribute'
import appEffect from './appeffect'
import appEffectAttribute from './appeffectattribute'
import appItem from './appitem'
import appSkill from './appskill'
import appItemEffect from './appitemeffect'
import appSkillEffect from './appskilleffect'
import appEntity from './appentity'
import appEntityGroup from './appentitygroup'
import appEntityGroupMember from './appentitygroupmember'
import appEntityAttribute from './appentityattribute'
import appEntityItem from './appentityitem'
import appEntitySkill from './appentityskill'
import appEntityTactic from './appentitytactic'
import appFight from './appfight'
import appFightEntityGroup from './appfightentitygroup'
import appFightInstance from './appfightinstance'
import appFightInstanceTurn from './appfightinstanceturn'

// application context
import context from './context'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // resources
      app,
      appAttribute,
      appEffect,
      appEffectAttribute,
      appItem,
      appSkill,
      appItemEffect,
      appSkillEffect,
      appEntity,
      appEntityGroup,
      appEntityGroupMember,
      appEntityAttribute,
      appEntityItem,
      appEntitySkill,
      appEntityTactic,
      appFight,
      appFightEntityGroup,
      appFightInstance,
      appFightInstanceTurn,
      // context
      context
    }
  })

  return Store
}

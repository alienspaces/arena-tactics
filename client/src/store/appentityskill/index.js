// app store
import Api from '../../api'
import Store from '../core'
import { actionFunctions, mutationFunctions, getterFunctions } from '../core-functions'

var store = new Store('appentityskill', Api.appEntitySkill)
var actions = actionFunctions(store)
var mutations = mutationFunctions(store)
var getters = getterFunctions(store)
var state = store.getState()

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}

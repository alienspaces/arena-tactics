// mutations
export function updateAppId (state, appId) {
  state.appId = appId
}

export function updateFightId (state, fightId) {
  state.fightId = fightId
}

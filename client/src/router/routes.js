const routes = [
  {
    path: '/',
    component: () => import('layouts/Admin.vue'),
    children: [
      {
        // - /
        path: '',
        name: 'admin-dashboard',
        component: () => import('pages/admin/Dashboard.vue')
      },
      // {
      //   // - /apps
      //   path: '/apps',
      //   name: 'admin-apps',
      //   component: () => import('pages/admin/Apps.vue')
      // },
      {
        // - /apps
        path: '/apps',
        name: 'admin-app-search',
        component: () => import('pages/admin/app/AppSearch.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0
        path: '/apps/:appId',
        name: 'admin-app',
        component: () => import('pages/admin/app/App.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/effects
        path: '/apps/:appId/attributes',
        name: 'admin-app-attributes',
        component: () => import('pages/admin/app/Attributes.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/effects
        path: '/apps/:appId/effects',
        name: 'admin-app-effects',
        component: () => import('pages/admin/app/Effects.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/items
        path: '/apps/:appId/items',
        name: 'admin-app-items',
        component: () => import('pages/admin/app/Items.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/skills
        path: '/apps/:appId/skills',
        name: 'admin-app-skills',
        component: () => import('pages/admin/app/Skills.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/entities
        path: '/apps/:appId/entities',
        name: 'admin-app-entities',
        component: () => import('pages/admin/app/Entities.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/entitygroups
        path: '/apps/:appId/entitygroups',
        name: 'admin-app-entity-groups',
        component: () => import('pages/admin/app/EntityGroups.vue')
      },
      // {
      //   // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/fights
      //   path: '/apps/:appId/fights',
      //   name: 'admin-app-fights',
      //   component: () => import('pages/admin/app/Fights.vue')
      // },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/fights
        path: '/apps/:appId/fights',
        name: 'admin-app-fight-search',
        component: () => import('pages/admin/fight/FightSearch.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/fights/6c630d2a-0bf9-11e9-8ce4-8c859074c8b1
        path: '/apps/:appId/fights/:fightId',
        name: 'admin-app-fight',
        component: () => import('pages/admin/fight/Fight.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/fights/6c630d2a-0bf9-11e9-8ce4-8c859074c8b1/entity-groups
        path: '/apps/:appId/fights/:fightId/entity-groups',
        name: 'admin-app-fight-entity-groups',
        component: () => import('pages/admin/fight/FightEntityGroups.vue')
      },
      {
        // - /apps/5a630b2a-0af9-11e9-8ce4-8c859074c8b0/fights/6c630d2a-0bf9-11e9-8ce4-8c859074c8b1/instances
        path: '/apps/:appId/fights/:fightId/instances',
        name: 'admin-app-fight-instances',
        component: () => import('pages/admin/fight/FightInstances.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes

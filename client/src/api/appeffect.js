import Api from './core'

const url =
  process.env.APP_API_HOST +
  ':' +
  process.env.APP_API_PORT +
  '/api/apps/{app_id}/effects'

// NOTE: Attribute `params` should be in the order that the store
// should index the results

var api = new Api(url, ['app_id', 'id'], process.env.APP_API_CLIENT_KEY)

export { api }

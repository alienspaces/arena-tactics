import * as appApi from './app'
import * as appAttributeApi from './appattribute'
import * as appEffectApi from './appeffect'
import * as appEffectAttributeApi from './appeffectattribute'
import * as appSkillApi from './appskill'
import * as appItemApi from './appitem'
import * as appItemEffectApi from './appitemeffect'
import * as appSkillEffectApi from './appskilleffect'
import * as appEntityApi from './appentity'
import * as appEntityGroupApi from './appentitygroup'
import * as appEntityGroupMemberApi from './appentitygroupmember'
import * as appEntityAttributeApi from './appentityattribute'
import * as appEntityItemApi from './appentityitem'
import * as appEntitySkillApi from './appentityskill'
import * as appEntityTacticApi from './appentitytactic'
import * as appFightApi from './appfight'
import * as appFightEntityGroupApi from './appfightentitygroup'
import * as appFightInstanceApi from './appfightinstance'
import * as appFightInstanceTurnApi from './appfightinstanceturn'

var app = appApi.api
var appAttribute = appAttributeApi.api
var appEffect = appEffectApi.api
var appEffectAttribute = appEffectAttributeApi.api
var appSkill = appSkillApi.api
var appItem = appItemApi.api
var appItemEffect = appItemEffectApi.api
var appSkillEffect = appSkillEffectApi.api
var appEntity = appEntityApi.api
var appEntityGroup = appEntityGroupApi.api
var appEntityGroupMember = appEntityGroupMemberApi.api
var appEntityAttribute = appEntityAttributeApi.api
var appEntityItem = appEntityItemApi.api
var appEntitySkill = appEntitySkillApi.api
var appEntityTactic = appEntityTacticApi.api
var appFight = appFightApi.api
var appFightEntityGroup = appFightEntityGroupApi.api
var appFightInstance = appFightInstanceApi.api
var appFightInstanceTurn = appFightInstanceTurnApi.api

export default {
  app,
  appAttribute,
  appEffect,
  appEffectAttribute,
  appSkill,
  appItem,
  appItemEffect,
  appSkillEffect,
  appEntity,
  appEntityGroup,
  appEntityGroupMember,
  appEntityAttribute,
  appEntityItem,
  appEntitySkill,
  appEntityTactic,
  appFight,
  appFightEntityGroup,
  appFightInstance,
  appFightInstanceTurn
}

import Vue from 'vue'

// NOTE: Attribute `params` should be in the order that the store
// should index the results

export default class Api {
  constructor (url, params, authKey) {
    this.url = url
    this.params = params
    this.authKey = authKey

    this.options = {
      headers: {
        'X-Authorization': this.AuthKey
      }
    }
  }

  // getUrl
  getUrl (url, params) {
    Vue.$log.info('(api) Url', url)
    if (params !== null) {
      for (var idx = 0; idx < this.params.length; idx++) {
        var value = params[this.params[idx]]
        Vue.$log.info(
          `(api) Replacing param >${this.params[idx]}< with value >${value}<`
        )
        if (value) {
          url = url.replace('{' + this.params[idx] + '}', value)
        }
      }
    }
    return url
  }

  // getRecords
  getRecords (params, successFunc, failureFunc) {
    Vue.$log.info('(api) Params - ', params)

    var url = this.getUrl(this.url, params)

    Vue.$log.info('(api) Request Url - ' + url)

    return Vue.axios
      .get(url, this.options)
      .then(successFunc, ({ response }) => failureFunc(response))
  }

  // get
  getRecord (params, successFunc, failureFunc) {
    Vue.$log.info('(api) Params - ', params)

    var url = this.getUrl(this.url + '/{id}', params)

    Vue.$log.info('(api) Request Url - ' + url)

    return Vue.axios
      .get(url, this.options)
      .then(successFunc, ({ response }) => failureFunc(response))
  }

  // create
  async createRecord (params, data, successFunc, failureFunc) {
    Vue.$log.info('(api) Params - ', params)

    var url = this.getUrl(this.url, params)

    Vue.$log.info('(api) Request Url - ' + url)

    var nestedData = {
      data: data
    }

    await Vue.axios
      .post(url, nestedData, this.options)
      .then(successFunc, ({ response }) => failureFunc(response))
  }

  // update
  async updateRecord (params, data, successFunc, failureFunc) {
    Vue.$log.info('(api) Params - ', params)

    var url = this.getUrl(this.url + '/{id}', params)

    Vue.$log.info('(api) Request Url', url)

    var nestedData = {
      data: data
    }

    Vue.$log.info('(api) Nested data', nestedData)

    await Vue.axios
      .put(url, nestedData, this.options)
      .then(successFunc, ({ response }) => failureFunc(response))
  }

  // delete
  async deleteRecord (params, successFunc, failureFunc) {
    Vue.$log.info('(api) Params - ', params)

    var url = this.getUrl(this.url + '/{id}', params)

    Vue.$log.info('(api) Request Url - ' + url)

    await Vue.axios
      .delete(url, this.options)
      .then(successFunc, ({ response }) => failureFunc(response))
  }
}

import Api from './core'

const url =
  process.env.APP_API_HOST +
  ':' +
  process.env.APP_API_PORT +
  '/api/apps/{app_id}/entitygroups/{app_entity_group_id}/members'

// NOTE: Attribute `params` should be in the order that the store
// should index the results

var api = new Api(url, ['app_id', 'app_entity_group_id', 'id'], process.env.APP_API_CLIENT_KEY)

export { api }

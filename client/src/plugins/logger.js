// logger

// https://github.com/justinkames/vuejs-logger#usage
import VueLogger from 'vuejs-logger'

const isProduction = process.env.NODE_ENV === 'production'

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  // something to do

  const options = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
  }

  Vue.use(VueLogger, options)
}

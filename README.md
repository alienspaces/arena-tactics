# Arena Tactics

❗ Project on hold ~ August 9th 2021 ❗

A tactical combat engine, API server and administrator UI.

This software is intended to be used as backend components for an RPG style game.

## Alpha 0.1

- Majority of engine features are complete.
- REST API is mostly stable.
- Approaching a Beta release.

## Server

- REST API
- Engine

[README](./server/README.md)

## Client

- Game Master UI

[README](./client/README.md)

## TODO

NOTE: Active development has ceased on this project. It was a great exploration!

### Now

- Client:
  - Nothing at the moment
- Server:
  - Refactor CI and test suites for better integration into Enterprise application

### Next

- Server:
  - Validate all entity tactics whenever:
    - Tactics are modified.
    - Items are added or removed from an entity.
    - Skills are added or removed from an entity.
  - Exclude tactics with a status of invalid when computing tactic outcomes during a fight.
  - Add default skills that are automatically assigned when new entities are created.
    For example a basic mechanic of the game skill design might be that all skills
    require a certain amount of a power attribute to be used. It would then make sense
    for every entity to have a power regeneration skill by default.
  - Reassess entity tactics if a tactic cannot be applied because the targets already
    have all tactic item/skill effects applied (tough..)

### Later

- Client:
  - Improve the UI for replaying a fight.
  - Validate and show application formula warnings as a dialogue on appropriate screens.
- Server:
  - Add a cost to active skills that are applied as a permanent effect. For
    example using the Life Drain skill requires 5 points in the mana attribute of
    the caster. If the caster does not have 5 points in mana to begin with then
    the skill cannot be used. It would then make sense for all entities to have
    a passive mana regeneration skill.
  - Add item usage limits to support items like healing potions and other attribute
    charge ups that can be used a limited number of times before they are empty.
  - Add item location restrictions to prevent entities from wearing two pairs of pants
    or four shields.
  - Add "damage types" to effects along with damage type "resistances" to items and skills.
    For example a skill applies fire type damage adjusting the health attribute by minus 10
    points however the target entity wears a cloak that has 4 points of fire resistance
    modifying the health attribute adjustment to minus 6 points.
  - Implement targeting only tactics
  - Allow a fight to be executed for a single turn through the API
    - This will allow turn based games to be developed where a player may select the next
      combat action they would like to perform. This would be implemented by replacing all
      existing tactics with a single tactic which would be their next combat action for the
      next turn. The tactics engine decision process essentially being a single choice.
  - Consider removing 'type' from skills and simply dynamically determining whether a skill
    can be 'used' as a tactic action based on whether it has any 'active' effects.
  - Consider allowing any item to be 'used' as a tactic action based on whether it has any
    'active' effects.
- Example Game:
  - Basic multi-player arena with fight pairing.
- Documentation:
  - Engine Server
  - API Server

## NOTES

### API Server

- The API server provides REST endpoints for managing application data

### Engine Server

- The Engine server manages running a application instance which in turn executes application fights.
- Fight and entity state is stored at the end of each fight turn.
- Fight and entity state is restored at the start of each fight turn.
- A history of what occurred during each fight turn must be stored so a fight may be analysed, printed or
  animated in all its gory detail

### Entities

- An entity is player or non-player character in a fight.
- An entity has attributes, items, skills and tactics.

### Items

Items are equipped by entities.

- Items that are of type 'weapon' or 'consumable' are able to be used as a tactic action.
- Items can have both passive and active effects however only active effects on items of type 'weapon'
  or 'consumable' will ever be applied as only items of these types may be used in tactics.
- Items are assigned to a location such as head, face, hands, arms, legs, torso, feet, shoudlers, waist etc.
  Locations have multiple slots such as head(1), hands(2), feet(2), belt(3). An items may only be equiped to
  a location where there are enough slots. For example gloves are equipped to the hands location and require
  2 empty slots. A helmet is equipped to the head location and requires 1 empty slot. A healing potion is
  equipped to the belt location and requires 1 empty slot.

### Skills

Skills are assigned to entities.

- Skills are able to be used as a tactic action.
- Skills can either be 'active' or 'passive' however only 'active' skills may be used in tactics.
- Skills that are 'active' may have active or passive effects assigned.
- Skills that are 'passive' may only have passive effects assigned.

### Effects

Effects are assigned to items and skills.

- Effects may be 'active' or 'passive', recurring or not, permanent or not and have a duration.
- Effects may adjust one or more entity attributes. Each effect attribute adjustment has an assigned target.
  For example when an effect is triggered it may lower the strength attribute of the entities current foe and
  increase the entities own strength.
- Each item and skill can have multiple 'active' and 'passive' effects.
- Passive effects are automatically applied to their targets without the item or skill needing to be 'used'.
- Active effects are applied to their targets when the item or skill is 'used' as a tactic action.

### Tactics

Tactics are how decisions are made by an entity when fighting.

- Tactic are ordered by the player in priority
- Tactics are tested in order to see whether they can be fulfilled
- A tactic can be a "targeting" tactic or an "action" tactic
- If a targeting tactic is fulfilled additional action tactics will continue to be tested
- If a targeting tactic is fulfilled no additional targeting tactics will be tested
- If an action action is fulfilled then no further tactics are tested

## Contributing

Merge requests welcome!

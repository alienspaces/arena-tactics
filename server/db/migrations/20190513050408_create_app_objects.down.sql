--
-- drop template objects
--
DROP TABLE IF EXISTS "template";

--
-- drop app objects
--

DROP TABLE IF EXISTS "app_entity_group_instance";
DROP TABLE IF EXISTS "app_entity_instance";
DROP TABLE IF EXISTS "app_instance";

DROP TABLE IF EXISTS "app_skill_effect";
DROP TABLE IF EXISTS "app_item_effect";

DROP TABLE IF EXISTS "app_entity_attribute";
DROP TABLE IF EXISTS "app_effect_attribute";
DROP TABLE IF EXISTS "app_attribute";


DROP TABLE IF EXISTS "app_entity_group_member";
DROP TABLE IF EXISTS "app_entity_group";

DROP TABLE IF EXISTS "app_effect";
DROP TYPE IF EXISTS "e_app_effect_apply_type";

DROP TABLE IF EXISTS "app_entity_skill";

DROP TABLE IF EXISTS "app_skill";
DROP TYPE IF EXISTS "e_app_skill_skill_type";

DROP TABLE IF EXISTS "app_entity_item";

DROP TABLE IF EXISTS "app_item";
DROP TYPE IF EXISTS "e_app_item_item_type";
DROP TYPE IF EXISTS "e_app_item_item_location";

DROP TABLE IF EXISTS "app_entity";

DROP TABLE IF EXISTS "app";
DROP TYPE IF EXISTS "e_app_status";

--
-- create template objects
--
CREATE TABLE "template" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  "name" TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT template_pk PRIMARY KEY (id)
);

--
-- create app tables
--
CREATE TYPE e_app_status AS ENUM (
  'stopped',   -- not running
  'starting',  -- waiting to start running
  'running',   -- running
  'stopping'   -- waiting to be shut down
);

CREATE TABLE "app" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  "name" TEXT NOT NULL,
  "description" TEXT,
  "initiative_formula" TEXT NOT NULL,
  "death_formula" TEXT NOT NULL,
  "status" e_app_status NOT NULL DEFAULT 'stopped',
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_pk PRIMARY KEY (id)
);

CREATE TABLE "app_entity" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_attribute" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  value_formula TEXT NOT NULL,
  max_value_formula TEXT NOT NULL,
  min_value_formula TEXT NOT NULL,
  assignable BOOL NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_attribute_pk PRIMARY KEY (id),
  CONSTRAINT app_attribute_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_entity_attribute" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_id UUID NOT NULL,
  app_attribute_id UUID NOT NULL,
  "value" INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_attribute_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_attribute_app_entity_id_fk
    FOREIGN KEY (app_entity_id)
    REFERENCES app_entity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_attribute_app_attribute_id_fk
    FOREIGN KEY (app_attribute_id)
    REFERENCES app_attribute (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TYPE e_app_item_item_type AS ENUM (
  'weapon',
  'armour',
  'jewellery',
  'consumable'
);

CREATE TYPE e_app_item_item_location AS ENUM (
  'head',
  'ear',
  'neck',
  'torso',
  'hand',
  'leg',
  'foot',
  'finger',
  'waist',
  'belt'
);

CREATE TABLE "app_item" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  narrative_text TEXT NOT NULL,
  item_type e_app_item_item_type NOT NULL,
  item_location e_app_item_item_location NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_item_pk PRIMARY KEY (id),
  CONSTRAINT app_item_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_entity_item" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_id UUID NOT NULL,
  app_item_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_item_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_item_app_entity_id_fk
    FOREIGN KEY (app_entity_id)
    REFERENCES app_entity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_item_app_item_id_fk
    FOREIGN KEY (app_item_id)
    REFERENCES app_item (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TYPE e_app_skill_skill_type AS ENUM (
  'passive',
  'active'
);

CREATE TABLE "app_skill" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  narrative_text TEXT NOT NULL,
  skill_type e_app_skill_skill_type NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_skill_pk PRIMARY KEY (id),
  CONSTRAINT app_skill_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_entity_skill" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_id UUID NOT NULL,
  app_skill_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_skill_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_skill_app_entity_id_fk
    FOREIGN KEY (app_entity_id)
    REFERENCES app_entity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_skill_app_skill_id_fk
    FOREIGN KEY (app_skill_id)
    REFERENCES app_skill (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TYPE e_app_entity_target AS ENUM (
  'self',           --
  'any-friend',     --
  'current-friend', --
  'any-foe',        --
  'current-foe'     --
);

CREATE TYPE e_app_entity_tactic_comparator AS ENUM (
  'is-less-than',    --
  'is-greater-than', --
  'is-greatest',     --
  'is-least'         --
);

CREATE TYPE e_app_entity_tactic_action AS ENUM (
  'switch-target',           --
  'use-skill',               --
  'use-item'                 --
);

CREATE TYPE e_app_entity_tactic_status AS ENUM (
  'valid',    --
  'invalid'   --
);

CREATE TABLE "app_entity_tactic" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_id UUID NOT NULL,
  "target" e_app_entity_target NOT NULL,
  app_attribute_id UUID NOT NULL,
  comparator e_app_entity_tactic_comparator NOT NULL,
  "value" INT NOT NULL,
  "action" e_app_entity_tactic_action NOT NULL,
  app_skill_id UUID,
  app_item_id UUID,
  "order" INT NOT NULL DEFAULT 1,
  "status" e_app_entity_tactic_status NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_tactic_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_tactic_app_entity_id_fk
    FOREIGN KEY (app_entity_id)
    REFERENCES app_entity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_tactic_app_attribute_id_fk
    FOREIGN KEY (app_attribute_id)
    REFERENCES app_attribute (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_tactic_app_skill_id_fk
    FOREIGN KEY (app_skill_id)
    REFERENCES app_skill (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_tactic_app_item_id_fk
    FOREIGN KEY (app_item_id)
    REFERENCES app_item (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_entity_group" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_group_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_group_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_entity_group_member" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_group_id UUID NOT NULL,
  app_entity_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_group_member_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_group_member_app_entity_group_id_fk
    FOREIGN KEY (app_entity_group_id)
    REFERENCES app_entity_group (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_entity_group_member_app_entity_id_fk
    FOREIGN KEY (app_entity_id)
    REFERENCES app_entity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TYPE e_app_effect_type AS ENUM (
  'passive', -- effect permanent
  'active' -- effect temporary
);

-- NOTE:
-- Effect type `passive`
-- The following attributes are ignored
--   duration  = ignored, forever
--   recurring = ignored, false
--   permanent = ignored
-- Effect type `active`
-- No attribute values are enforced so the following rules apply
--   duration  = the number of turns the effect remains applied
--   recurring = whether the effect value accumulates every turn
--   permanent = whether the total effect value is removed after duration

CREATE TABLE "app_effect" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "name" TEXT NOT NULL,
  narrative_text TEXT NOT NULL,
  effect_type e_app_effect_type NOT NULL,
  duration INT NOT NULL,
  recurring BOOL NOT NULL DEFAULT FALSE,
  permanent BOOL NOT NULL DEFAULT FALSE,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_effect_pk PRIMARY KEY (id),
  CONSTRAINT app_effect_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- constrain effect attribute application by
CREATE TYPE e_app_effect_attribute_apply_constraint_type AS ENUM (
  'maximum', -- maximum allowed value of attribute
  'calculated' -- calculated value of attribute
);

CREATE TABLE "app_effect_attribute" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_effect_id UUID NOT NULL,
  app_attribute_id UUID NOT NULL,
  "target" e_app_entity_target NOT NULL,
  apply_constraint e_app_effect_attribute_apply_constraint_type NOT NULL DEFAULT 'maximum',
  value_formula TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_effect_attribute_pk PRIMARY KEY (id),
  CONSTRAINT app_effect_attribute_app_effect_id_fk
    FOREIGN KEY (app_effect_id)
    REFERENCES app_effect (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_effect_attribute_app_attribute_id_fk
    FOREIGN KEY (app_attribute_id)
    REFERENCES app_attribute (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_item_effect" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_item_id UUID NOT NULL,
  app_effect_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_item_effect_pk PRIMARY KEY (id),
  CONSTRAINT app_item_effect_app_item_id_fk
    FOREIGN KEY (app_item_id)
    REFERENCES app_item (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_item_effect_app_effect_id_fk
    FOREIGN KEY (app_effect_id)
    REFERENCES app_effect (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_skill_effect" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_skill_id UUID NOT NULL,
  app_effect_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_skill_effect_pk PRIMARY KEY (id),
  CONSTRAINT app_skill_effect_app_item_id_fk
    FOREIGN KEY (app_skill_id)
    REFERENCES app_skill (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_skill_effect_app_effect_id_fk
    FOREIGN KEY (app_effect_id)
    REFERENCES app_effect (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

--
-- create app instance objects
--
CREATE TABLE "app_instance" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "data" JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_instance_pk PRIMARY KEY (id),
  CONSTRAINT app_instance_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- NOTE: May not need this table at all
CREATE TABLE "app_entity_group_instance" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_entity_group_id UUID NOT NULL,
  "data" JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_entity_group_instance_pk PRIMARY KEY (id),
  CONSTRAINT app_entity_group_instance_app_entity_group_id_fk
    FOREIGN KEY (app_entity_group_id)
    REFERENCES app_entity_group (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- fight
CREATE TYPE e_app_fight_status AS ENUM (
  'waiting',   -- waiting for setup
  'starting',  -- waiting to start running
  'fighting',  -- fighting
  'done'       -- done
);

CREATE TABLE "app_fight" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_id UUID NOT NULL,
  "status" e_app_fight_status NOT NULL DEFAULT 'waiting',
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_fight_pk PRIMARY KEY (id),
  CONSTRAINT app_fight_app_id_fk
    FOREIGN KEY (app_id)
    REFERENCES app (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TYPE e_app_fight_instance_status AS ENUM (
  'fighting',  -- fighting
  'done'       -- done
);

CREATE TABLE "app_fight_instance" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_fight_id UUID NOT NULL,
  "status" e_app_fight_instance_status NOT NULL DEFAULT 'fighting',
  turn INT NOT NULL,
  "data" JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_fight_instance_pk PRIMARY KEY (id),
  CONSTRAINT app_fight_instance_app_fight_id_fk
    FOREIGN KEY (app_fight_id)
    REFERENCES app_fight (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_fight_instance_turn" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_fight_id UUID NOT NULL,
  app_fight_instance_id UUID NOT NULL,
  turn INT NOT NULL,
  "data" JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_fight_instance_turn_pk PRIMARY KEY (id),
  CONSTRAINT app_fight_instance_turn_app_fight_id_fk
    FOREIGN KEY (app_fight_id)
    REFERENCES app_fight (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_fight_instance_turn_app_fight_instance_id_fk
    FOREIGN KEY (app_fight_instance_id)
    REFERENCES app_fight_instance (id)
    -- remove all fight instance turn records if fight instance removed
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE "app_fight_entity_group" (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  app_fight_id UUID NOT NULL,
  app_entity_group_id UUID NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  updated_at TIMESTAMP NULL DEFAULT NULL,
  deleted_at TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT app_fight_entity_group_pk PRIMARY KEY (id),
  CONSTRAINT app_fight_entity_group_app_fight_id_fk
    FOREIGN KEY (app_fight_id)
    REFERENCES app_fight (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT app_fight_entity_group_app_entity_group_id_fk
    FOREIGN KEY (app_entity_group_id)
    REFERENCES app_entity_group (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

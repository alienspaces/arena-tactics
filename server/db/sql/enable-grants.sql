create user arena_server_user unencrypted password 'arena_server_user_123';
create user arena_api_user unencrypted password 'arena_api_user_123';

grant usage on schema public to arena_server_user;
grant usage on schema public to arena_api_user;

grant select, insert, update, delete on all tables in schema public to arena_server_user;
grant select, insert, update, delete on all tables in schema public to arena_api_user;


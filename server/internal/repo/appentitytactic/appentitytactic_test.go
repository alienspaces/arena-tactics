package appentitytactic

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityRecord,
	*record.AppAttributeRecord,
	*record.AppSkillRecord,
	*record.AppEntityTacticRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	arp, _ := app.NewRepo(e, l, tx)
	ar, _ := arp.CreateTestRecord()

	// AppEntity record
	aerp, _ := appentity.NewRepo(e, l, tx)
	aer, _ := aerp.CreateTestRecord(ar.ID, "")

	// AppAttribute record
	aarp, _ := appattribute.NewRepo(e, l, tx)
	aar, _ := aarp.CreateTestRecord(
		ar.ID,
		fake.CharactersN(10),
		"1",
		"0",
		"10",
		true,
	)

	// AppSkill record
	asrp, _ := appskill.NewRepo(e, l, tx)
	asr, _ := asrp.CreateTestRecord(ar.ID, "", "", "")

	// AppEntityTactic record
	etrp, _ := NewRepo(e, l, tx)
	etr, _ := etrp.CreateTestRecord(
		aer.ID,
		record.TargetAnyFoe,
		aar.ID,
		record.EntityTacticComparatorGreaterThan,
		50,
		record.EntityTacticActionUseSkill,
		asr.ID,
		"",
		1,
	)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEntityTactic record
		etrp, _ := NewRepo(e, l, tx)
		if err := etrp.Remove(etr.ID); err != nil {
			t.Fatalf("Failed to remove app entity tactic record >%v<", err)
		}

		// remove AppSkill
		asrp, _ := appskill.NewRepo(e, l, tx)
		if err := asrp.Remove(asr.ID); err != nil {
			t.Fatalf("Failed to remove app skill record >%v<", err)
		}

		// AppAttribute record
		aarp, _ := appattribute.NewRepo(e, l, tx)
		if err := aarp.Remove(aar.ID); err != nil {
			t.Fatalf("Failed to remove app attribute record >%v<", err)
		}

		// AppEntity record
		aerp, _ := appentity.NewRepo(e, l, tx)
		if err := aerp.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app entity record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, aar, asr, etr, teardown
}

func TestCreate(t *testing.T) {

	_, aer, aar, _, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppEntityID = aer.ID
	rec.Target = record.TargetAnyFoe
	rec.AppAttributeID = aar.ID
	rec.Comparator = record.EntityTacticComparatorLeast
	rec.Value = 50
	rec.Action = record.EntityTacticActionSwitchTarget
	rec.Status = record.EntityTacticStatusValid

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEntityTactic record created without error")

	assert.NotNil(t, rec.ID, "AppEntityTactic record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityTactic record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityTactic record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityTactic record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppEntityTactic returns without error")

	assert.NotNil(t, rec.ID, "AppEntityTactic record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityTactic record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityTactic record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityTactic record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntityTactic record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntityTactic record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntityTactic records fetched without error")

	assert.NotNil(t, nrecs, "AppEntityTactic records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppEntityTactic record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntityTactic record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppEntityTactic record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppEntityTactic record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, aer, aar, asr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityTactic repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityTactic repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(
		aer.ID,
		record.TargetAnyFoe,
		aar.ID,
		record.EntityTacticComparatorGreaterThan,
		50,
		record.EntityTacticActionUseSkill,
		asr.ID,
		"",
		1,
	)

	assert.Nil(t, err, "AppEntityTactic record created without error")

	assert.NotEmpty(t, r.ID, "AppEntityTactic test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntityTactic test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

package appentity

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_entity
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_entity
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_entity
	(id, app_id, name, created_at)
VALUES
	(:id, :app_id, :name, :created_at)
RETURNING id, app_id, name, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_entity SET
	name = :name,
	updated_at = :updated_at
WHERE id 		   = :id
AND   app_id 	   = :app_id
AND   deleted_at IS NULL
RETURNING id, app_id, name, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_entity SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, app_id, name, created_at, updated_at, deleted_at
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_entity
WHERE id = :id
`
}

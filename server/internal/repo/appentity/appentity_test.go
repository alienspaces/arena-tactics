package appentity

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEntity record
	aem, _ := NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "")

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEntity record
		aem, _ := NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app entity record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, teardown
}

func TestCreate(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntity repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppID = ar.ID
	rec.Name = fake.CharactersN(20)

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New app entity record created without error")

	assert.NotNil(t, rec.Name, "AppEntity record Name is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntity record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntity record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntity record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, aer, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntity repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	rec, err := m.GetByID(aer.ID)
	assert.Nil(t, err, "Fetching AppEntity returns without error")

	assert.NotNil(t, rec.Name, "AppEntity record Name is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntity record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntity record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntity record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, aer, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntity repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	rec, _ := m.GetByID(aer.ID)
	rec.Name = fake.ProductName()

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntity record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntity record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, aer, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntity repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = aer.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntity records fetched without error")

	assert.NotNil(t, nrecs, "AppEntity records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, aer, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	// delete
	err = m.Delete(aer.ID)
	assert.Nil(t, err, "AppEntity record deleted without error")

	_, err = m.GetByID(aer.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntity record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, aer, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	// remove
	err = m.Remove(aer.ID)
	assert.Nil(t, err, "AppEntity record removed without error")

	// get by id
	_, err = m.GetByID(aer.ID)
	assert.NotNil(t, err, "Fetching removed AppEntity record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntity repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(ar.ID, "")
	assert.Nil(t, err, "AppEntity record created without error")

	assert.NotEmpty(t, r.ID, "AppEntity test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntity test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

package appentityitem

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityRecord,
	*record.AppItemRecord,
	*record.AppEntityItemRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEntity record
	aem, _ := appentity.NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "")

	// AppItem record
	aim, _ := appitem.NewRepo(e, l, tx)
	air, _ := aim.CreateTestRecord(ar.ID, "", "", "", "")

	// AppEntityItem record
	aeim, _ := NewRepo(e, l, tx)
	aeir, _ := aeim.CreateTestRecord(aer.ID, air.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transactiob %v", err)
		}

		// remove AppEntityItem record
		aeim, _ := NewRepo(e, l, tx)
		if err := aeim.Remove(aeir.ID); err != nil {
			t.Fatalf("Failed to remove app entity item record >%v<", err)
		}

		// remove AppItem record
		aim, _ := appitem.NewRepo(e, l, tx)
		if err := aim.Remove(air.ID); err != nil {
			t.Fatalf("Failed to remove app item record >%v<", err)
		}

		// remove AppEntity record
		aem, _ := appentity.NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app entity record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, air, aeir, teardown
}

func TestCreate(t *testing.T) {

	_, aer, air, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transaction %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppEntityID = aer.ID
	rec.AppItemID = air.ID

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEntityItem record created without error")

	assert.NotNil(t, rec.AppEntityID, "AppEntityItem record AppEntityID is not nil")
	assert.NotNil(t, rec.AppItemID, "AppItemItem record AppItemID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityItem record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityItem record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityItem record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, aeir, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transaction %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	rec, err := m.GetByID(aeir.ID)
	assert.Nil(t, err, "Fetching AppEntityItem returns without error")

	assert.NotNil(t, rec.ID, "AppEntityItem record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityItem record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityItem record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityItem record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	ar, _, _, aeir, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	// appitem
	aim, _ := appitem.NewRepo(e, l, tx)
	air, _ := aim.CreateTestRecord(ar.ID, "", "", "", "")

	// update appentityitem
	rec, _ := m.GetByID(aeir.ID)
	rec.AppItemID = air.ID

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntityItem record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntityItem record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, aeir, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = aeir.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntityItem records fetched without error")

	assert.NotNil(t, nrecs, "AppEntityItem records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, aeir, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	// delete
	err = m.Delete(aeir.ID)
	assert.Nil(t, err, "AppEntityItem record deleted without error")

	_, err = m.GetByID(aeir.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntityItem record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, aeir, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// AppEntityItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	// remove
	err = m.Remove(aeir.ID)
	assert.Nil(t, err, "AppEntityItem record removed without error")

	// get by id
	_, err = m.GetByID(aeir.ID)
	assert.NotNil(t, err, "Fetching removed AppEntityItem record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, aer, air, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// appentityitem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityItem repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(aer.ID, air.ID)
	assert.Nil(t, err, "AppEntityItem record created without error")

	assert.NotEmpty(t, r.ID, "AppEntityItem test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntityItem test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

package appentitygroup

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityGroupRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEntityGroup record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(ar.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEntityGroup record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app entity group record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, tr, teardown
}

func TestCreate(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppID = ar.ID

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEntityGroup record created without error")

	assert.NotNil(t, rec.ID, "AppEntityGroup record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityGroup record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityGroup record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityGroup record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppEntityGroup returns without error")

	assert.NotNil(t, rec.ID, "AppEntityGroup record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityGroup record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityGroup record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityGroup record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntityGroup record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntityGroup record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntityGroup records fetched without error")

	assert.NotNil(t, nrecs, "AppEntityGroup records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppEntityGroup record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntityGroup record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppEntityGroup record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppEntityGroup record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityGroup repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(ar.ID)
	assert.Nil(t, err, "AppEntityGroup record created without error")

	assert.NotEmpty(t, r.ID, "AppEntityGroup test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntityGroup test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

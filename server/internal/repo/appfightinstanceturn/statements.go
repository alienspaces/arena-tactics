package appfightinstanceturn

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_fight_instance_turn
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_fight_instance_turn
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_fight_instance_turn
	(id, app_fight_id, app_fight_instance_id, turn, data, created_at)
VALUES
	(:id, :app_fight_id, :app_fight_instance_id, :turn, :data, :created_at)
RETURNING id, app_fight_id, app_fight_instance_id, turn, data, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_fight_instance_turn SET
	app_fight_id          = :app_fight_id,
	app_fight_instance_id = :app_fight_instance_id,
	turn                  = :turn,
	data                  = :data,
	updated_at            = :updated_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, app_fight_id, app_fight_instance_id, turn, data, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_fight_instance_turn SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_fight_instance_turn
WHERE id = :id
`
}

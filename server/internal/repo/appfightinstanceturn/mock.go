package appfightinstanceturn

import (
	"database/sql"
	"fmt"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo"

	// util
	"gitlab.com/alienspaces/arena-tactics/server/internal/util"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// MockRepo - interface implementation
type MockRepo struct {
	// export so can assign test records for mock purposes
	Records map[string]*record.AppFightInstanceTurnRecord

	repo.Base
}

// NewMockRepo - returns an implementation of a Repo
func NewMockRepo() (m *MockRepo, err error) {

	m = &MockRepo{
		Records: make(map[string]*record.AppFightInstanceTurnRecord),
	}

	return m, err
}

// NewRecord -
func (m *MockRepo) NewRecord() *record.AppFightInstanceTurnRecord {
	return &record.AppFightInstanceTurnRecord{}
}

// NewRecordArray -
func (m *MockRepo) NewRecordArray() []*record.AppFightInstanceTurnRecord {
	return []*record.AppFightInstanceTurnRecord{}
}

// GetByID -
func (m *MockRepo) GetByID(id string) (*record.AppFightInstanceTurnRecord, error) {

	if _, ok := m.Records[id]; !ok {
		return nil, fmt.Errorf("GetByID record id >%s< not found", id)
	}

	rec := m.Records[id]

	return rec, nil
}

// GetByParam -
func (m *MockRepo) GetByParam(params map[string]interface{}) ([]*record.AppFightInstanceTurnRecord, error) {

	// records
	var recs []*record.AppFightInstanceTurnRecord

	for _, rec := range m.Records {

		// no params, add every record
		if len(params) == 0 {
			recs = append(recs, rec)
			continue
		}

		// compare params
		addRec, err := m.CompareParams(params, rec)
		if err != nil {
			m.Logger.Warn().Msgf("GetByParam failed compare params >%v<", err)
			return nil, err
		}

		if addRec == true {
			recs = append(recs, rec)
		}
	}

	return recs, nil
}

// Create -
func (m *MockRepo) Create(rec *record.AppFightInstanceTurnRecord) error {

	// id
	rec.ID = util.GetUUID()
	rec.CreatedAt = util.GetTime()

	m.Records[rec.ID] = rec

	return nil
}

// Update -
func (m *MockRepo) Update(rec *record.AppFightInstanceTurnRecord) error {

	if _, ok := m.Records[rec.ID]; !ok {
		return fmt.Errorf("Update record id >%s< not found", rec.ID)
	}

	rec.UpdatedAt = sql.NullString{
		String: util.GetTime(),
		Valid:  true,
	}

	m.Records[rec.ID] = rec

	return nil
}

// Delete -
func (m *MockRepo) Delete(id string) error {

	if _, ok := m.Records[id]; !ok {
		return fmt.Errorf("Delete record id >%s< not found", id)
	}

	delete(m.Records, id)

	return nil
}

// Remove -
func (m *MockRepo) Remove(id string) error {

	if _, ok := m.Records[id]; !ok {
		return fmt.Errorf("Delete record id >%s< not found", id)
	}

	delete(m.Records, id)

	return nil
}

// CreateTestRecord - returns a record for testing
func (m *MockRepo) CreateTestRecord(appFightID, appFightInstanceID string, turn int) (*record.AppFightInstanceTurnRecord, error) {

	r := m.NewRecord()
	r.AppFightID = appFightID
	r.AppFightInstanceID = appFightInstanceID
	r.Turn = turn

	err := m.Create(r)
	return r, err
}

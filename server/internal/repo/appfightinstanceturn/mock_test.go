package appfightinstanceturn

import (
	"testing"

	"github.com/stretchr/testify/assert"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

func mockSetup(t *testing.T) (
	*record.AppRecord,
	*app.MockRepo,
	*record.AppFightInstanceTurnRecord,
	*MockRepo, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewMockRepo()
	ar, _ := am.CreateTestRecord()

	// AppFightInstanceTurn record
	tm, _ := NewMockRepo()
	tr, _ := tm.CreateTestRecord(util.GetUUID(), util.GetUUID(), 1)

	tx.Commit()

	teardown := func() {

		// remove AppFightInstanceTurn record
		tm.Remove(tr.ID)

		// remove App record
		am.Remove(ar.ID)
	}

	return ar, am, tr, tm, teardown
}

func TestMockNewRepo(t *testing.T) {

	_, _, _, _, teardown := mockSetup(t)
	defer teardown()

	// mock repo
	m, err := NewMockRepo()

	assert.NoError(t, err, "New mock repo created without error")
	assert.NotNil(t, m, "New mock repo returns a non nil object")
}

func TestMockCreate(t *testing.T) {

	_, _, _, tm, teardown := mockSetup(t)
	defer teardown()

	rec := tm.NewRecord()
	rec.AppFightID = util.GetUUID()
	rec.AppFightInstanceID = util.GetUUID()
	rec.Turn = 1

	// create
	err := tm.Create(rec)

	if assert.Nil(t, err, "New AppFightInstanceTurn record created without error") {
		assert.NotNil(t, rec.ID, "AppFightInstanceTurn record ID is not nil")
		assert.NotNil(t, rec.CreatedAt, "AppFightInstanceTurn record CreatedAt is not nil")
		assert.Empty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is empty")
		assert.Empty(t, rec.DeletedAt.String, "AppFightInstanceTurn record DeletedAt is empty")
	}
}

func TestMockGetByID(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	rec, err := tm.GetByID(tr.ID)

	if assert.Nil(t, err, "Fetching AppFightInstanceTurn returns without error") {
		assert.NotNil(t, rec.ID, "AppFightInstanceTurn record ID is not nil")
		assert.NotNil(t, rec.CreatedAt, "AppFightInstanceTurn record CreatedAt is not nil")
		assert.Empty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is empty")
		assert.Empty(t, rec.DeletedAt.String, "AppFightInstanceTurn record DeletedAt is empty")
	}
}

func TestMockUpdate(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	rec, _ := tm.GetByID(tr.ID)
	rec.AppFightID = util.GetUUID()
	rec.AppFightInstanceID = util.GetUUID()
	rec.Turn = 2

	err := tm.Update(rec)
	if assert.Nil(t, err, "AppFightInstanceTurn record updated without error") {
		assert.NotEmpty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is not empty")
	}
}

func TestMockGetByParam(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := tm.GetByParam(params)

	assert.Nil(t, err, "AppFightInstanceTurn records fetched without error")
	assert.NotNil(t, nrecs, "AppFightInstanceTurn records is not empty")
}

func TestMockDelete(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// delete
	err := tm.Delete(tr.ID)
	assert.Nil(t, err, "AppFightInstanceTurn record deleted without error")

	_, err = tm.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppFightInstanceTurn record returns error")
}

func TestMockRemove(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// remove
	err := tm.Remove(tr.ID)
	assert.Nil(t, err, "AppFightInstanceTurn record removed without error")

	// get by id
	_, err = tm.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppFightInstanceTurn record returns error")
}

func TestMockCreateTestRecord(t *testing.T) {

	_, _, _, _, teardown := mockSetup(t)
	defer teardown()

	// mock repo
	m, _ := NewMockRepo()

	// create test record
	r, err := m.CreateTestRecord(util.GetUUID(), util.GetUUID(), 1)

	if assert.Nil(t, err, "AppFightInstanceTurn record created without error") {
		assert.NotEmpty(t, r.ID, "AppFightInstanceTurn test record ID is not empty")
		assert.NotEmpty(t, r.CreatedAt, "AppFightInstanceTurn test record CreatedAt is not empty")
	}
}

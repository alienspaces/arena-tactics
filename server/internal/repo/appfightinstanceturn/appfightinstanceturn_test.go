package appfightinstanceturn

import (
	"encoding/json"
	"testing"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightinstance"

	"github.com/stretchr/testify/assert"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"

	// repos
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

// test records
type TestRecs struct {
	AppRecord            *record.AppRecord
	AppFight             *record.AppFightRecord
	AppFightInstance     *record.AppFightInstanceRecord
	AppFightInstanceTurn *record.AppFightInstanceTurnRecord
}

func setup(t *testing.T) (
	*TestRecs, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// test records
	testRecs := &TestRecs{}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	testRecs.AppRecord, _ = am.CreateTestRecord()

	// AppFight record
	afm, _ := appfight.NewRepo(e, l, tx)
	testRecs.AppFight, _ = afm.CreateTestRecord(testRecs.AppRecord.ID)

	// AppFightInstance record
	afim, _ := appfightinstance.NewRepo(e, l, tx)
	testRecs.AppFightInstance, _ = afim.CreateTestRecord(testRecs.AppFight.ID)

	// AppFightInstanceTurn record
	afihm, _ := NewRepo(e, l, tx)
	testRecs.AppFightInstanceTurn, _ = afihm.CreateTestRecord(
		testRecs.AppFight.ID,
		testRecs.AppFightInstance.ID,
		1,
	)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppFightInstanceTurn record
		afihm, _ := NewRepo(e, l, tx)
		if err := afihm.Remove(testRecs.AppFightInstanceTurn.ID); err != nil {
			t.Fatalf("Failed to remove app fight instance history record >%v<", err)
		}

		// AppFightInstance record
		afim, _ := appfightinstance.NewRepo(e, l, tx)
		if err := afim.Remove(testRecs.AppFightInstance.ID); err != nil {
			t.Fatalf("Failed to remove app fight instance record >%v<", err)
		}

		// AppFight record
		afm, _ := appfight.NewRepo(e, l, tx)
		if err := afm.Remove(testRecs.AppFight.ID); err != nil {
			t.Fatalf("Failed to remove app fight record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(testRecs.AppRecord.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return testRecs, teardown
}

func TestCreate(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// data
	data := instance.FightData{
		AppFightID: testRecs.AppFight.ID,
	}

	jsonData, err := json.Marshal(data)
	assert.Nil(t, err, "Marshal AppInstanceHistory Data without error")

	rec := m.NewRecord()
	rec.AppFightID = testRecs.AppFight.ID
	rec.AppFightInstanceID = testRecs.AppFightInstance.ID
	rec.Turn = 1
	rec.Data = jsonData

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppFightInstanceTurn record created without error")

	assert.NotNil(t, rec.ID, "AppFightInstanceTurn record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightInstanceTurn record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightInstanceTurn record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	rec, err := m.GetByID(testRecs.AppFightInstanceTurn.ID)
	assert.Nil(t, err, "Fetching AppFightInstanceTurn returns without error")

	assert.NotNil(t, rec.ID, "AppFightInstanceTurn record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightInstanceTurn record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightInstanceTurn record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// data
	data := instance.FightData{
		AppFightID: testRecs.AppFight.ID,
	}

	jsonData, err := json.Marshal(data)
	assert.Nil(t, err, "Marshal AppInstanceHistory Data without error")

	rec, _ := m.GetByID(testRecs.AppFightInstanceTurn.ID)
	rec.Turn = 2
	rec.Data = jsonData

	err = m.Update(rec)
	assert.Nil(t, err, "AppFightInstanceTurn record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppFightInstanceTurn record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = testRecs.AppFightInstanceTurn.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppFightInstanceTurn records fetched without error")

	assert.NotNil(t, nrecs, "AppFightInstanceTurn records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// delete
	err = m.Delete(testRecs.AppFightInstanceTurn.ID)
	if assert.Nil(t, err, "AppFightInstanceTurn record deleted without error") {

		_, err = m.GetByID(testRecs.AppFightInstanceTurn.ID)
		assert.NotNil(t, err, "Fetching deleted AppFightInstanceTurn record returns error")
	}

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// remove
	err = m.Remove(testRecs.AppFightInstanceTurn.ID)
	if assert.Nil(t, err, "AppFightInstanceTurn record removed without error") {

		// get by id
		_, err = m.GetByID(testRecs.AppFightInstanceTurn.ID)
		assert.NotNil(t, err, "Fetching removed AppFightInstanceTurn record returns error")
	}

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstanceTurn repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstanceTurn repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(
		testRecs.AppFight.ID,
		testRecs.AppFightInstance.ID,
		1,
	)
	if assert.Nil(t, err, "AppFightInstanceTurn record created without error") {

		assert.NotEmpty(t, r.ID, "AppFightInstanceTurn test record ID is not empty")
		assert.NotEmpty(t, r.CreatedAt, "AppFightInstanceTurn test record CreatedAt is not empty")
	}

	// rollback
	tx.Rollback()
}

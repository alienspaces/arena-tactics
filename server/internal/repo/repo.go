// Package repo provides methods for interacting with the database
package repo

import (
	"database/sql"
	"errors"
	"reflect"
	"strconv"

	"github.com/davecgh/go-spew/spew"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repopreparer"
)

// Base -
type Base struct {
	Name         string
	Env          env.Env
	Logger       zerolog.Logger
	Tx           *sqlx.Tx
	Preparer     *repopreparer.Preparer
	RecordParams map[string]*RecordParam
}

// RecordParam -
type RecordParam struct {
	TypeInt        bool
	TypeString     bool
	TypeNullString bool
}

// Init -
func (m *Base) Init() error {

	// log
	log := m.Logger

	log.Debug().Msg("Initialising repo")

	if m.Env == nil {
		return errors.New("Env is nil, cannot initialise")
	}
	if m.Tx == nil {
		return errors.New("Database is nil, cannot initialise")
	}
	if m.Name == "" {
		return errors.New("Name is empty, cannot initialise")
	}

	return nil
}

// GetName - returns name property
func (m *Base) GetName() string {
	return m.Name
}

// DebugStruct -
func (m *Base) DebugStruct(msg string, rec interface{}) {

	// log
	log := m.Logger

	log.Debug().Msgf(msg + " " + spew.Sdump(rec))
}

// GetRecordByID - returns record for id
func (m *Base) GetRecordByID(id string, rec interface{}) error {

	// log
	log := m.Logger

	// preparer
	p := m.Preparer

	// stmt
	stmt := p.GetByIDStmt(m)

	log.Debug().Msgf("Get record ID >%s<", id)

	err := stmt.QueryRowx(id).StructScan(rec)
	if err != nil {
		log.Error().Msgf("Error querying row ID >%s< error >%v<", id, err)
		return err
	}

	m.DebugStruct("Fetched", rec)

	return nil
}

// CompareParams -
func (m *Base) CompareParams(params map[string]interface{}, rec interface{}) (bool, error) {

	recElem := reflect.ValueOf(rec).Elem()

	for paramName, paramValue := range params {

		for i := 0; i < recElem.NumField(); i++ {

			// Get the tag name
			valueField := recElem.Field(i)
			typeField := recElem.Type().Field(i)
			tag := typeField.Tag

			var stringValue string
			if paramName == tag.Get("db") {

				switch valueField.Kind() {
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					stringValue = strconv.FormatInt(valueField.Int(), 10)
				case reflect.String:
					stringValue = valueField.String()
				case reflect.Struct:
					// NOTE: Assuming sql.NullString for now
					iface := valueField.Interface()
					ifaceValue := iface.(sql.NullString).String
					stringValue = ifaceValue
				default:
					// no-op
				}

				if paramValue != stringValue {
					return false, nil
				}
			}
		}
	}

	return true, nil
}

// GetByIDSQL - Returns SQL
func (m *Base) GetByIDSQL() string {
	return ``
}

// GetByParamSQL -
func (m *Base) GetByParamSQL() string {
	return ``
}

// CreateSQL -
func (m *Base) CreateSQL() string {
	return ``
}

// UpdateSQL -
func (m *Base) UpdateSQL() string {
	return ``
}

// DeleteSQL -
func (m *Base) DeleteSQL() string {
	return ``
}

// RemoveSQL -
func (m *Base) RemoveSQL() string {
	return ``
}

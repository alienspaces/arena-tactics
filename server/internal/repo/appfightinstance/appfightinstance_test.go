package appfightinstance

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppFightRecord,
	*record.AppFightInstanceRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppFight
	afm, _ := appfight.NewRepo(e, l, tx)
	afr, _ := afm.CreateTestRecord(ar.ID)

	// AppFightInstance record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(afr.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppFightInstance record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app fight instance record >%v<", err)
		}

		// AppFight
		afm, _ := appfight.NewRepo(e, l, tx)
		if err := afm.Remove(afr.ID); err != nil {
			t.Fatalf("Failed to remove app fight record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, afr, tr, teardown
}

func TestCreate(t *testing.T) {

	_, afr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	// data
	data := instance.FightData{
		AppFightID: afr.ID,
	}

	jsonData, err := json.Marshal(data)
	assert.Nil(t, err, "Marshal AppInstance Data without error")

	rec := m.NewRecord()
	rec.AppFightID = afr.ID
	rec.Data = jsonData
	rec.Status = record.AppFightInstanceStatusFighting

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppFightInstance record created without error")

	assert.NotNil(t, rec.ID, "AppFightInstance record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightInstance record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightInstance record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightInstance record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppFightInstance returns without error")

	assert.NotNil(t, rec.ID, "AppFightInstance record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightInstance record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightInstance record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightInstance record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppFightInstance record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppFightInstance record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppFightInstance records fetched without error")

	assert.NotNil(t, nrecs, "AppFightInstance records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppFightInstance record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppFightInstance record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppFightInstance record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppFightInstance record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, afr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightInstance repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(afr.ID)
	assert.Nil(t, err, "AppFightInstance record created without error")

	assert.NotEmpty(t, r.ID, "AppFightInstance test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppFightInstance test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

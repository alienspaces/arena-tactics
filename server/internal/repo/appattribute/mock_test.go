package appattribute

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

func mockSetup(t *testing.T) (
	*record.AppRecord,
	*app.MockRepo,
	*record.AppAttributeRecord,
	*MockRepo, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewMockRepo()
	ar, _ := am.CreateTestRecord()

	// App attribute record
	aam, _ := NewMockRepo()
	aar, _ := aam.CreateTestRecord(ar.ID, "", "", true, 0)

	tx.Commit()

	teardown := func() {

		// remove AppAttribute record
		aam.Remove(aar.ID)

		// remove App record
		am.Remove(ar.ID)
	}

	return ar, am, aar, aam, teardown
}

func TestMockNewRepo(t *testing.T) {

	_, _, _, _, teardown := mockSetup(t)
	defer teardown()

	// mock repo
	m, err := NewMockRepo()

	assert.NoError(t, err, "New mock repo created without error")
	assert.NotNil(t, m, "New mock repo returns a non nil object")
}

func TestMockCreate(t *testing.T) {

	ar, _, _, tm, teardown := mockSetup(t)
	defer teardown()

	rec := tm.NewRecord()
	rec.AppID = ar.ID
	rec.Name = fake.CharactersN(10)

	// create
	err := tm.Create(rec)

	if assert.Nil(t, err, "New AppAttributeFormula record created without error") {
		assert.NotNil(t, rec.ID, "AppAttributeFormula record ID is not nil")
		assert.NotNil(t, rec.CreatedAt, "AppAttributeFormula record CreatedAt is not nil")
		assert.Empty(t, rec.UpdatedAt.String, "AppAttributeFormula record UpdatedAt is empty")
		assert.Empty(t, rec.DeletedAt.String, "AppAttributeFormula record DeletedAt is empty")
	}
}

func TestMockGetByID(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	rec, err := tm.GetByID(tr.ID)

	if assert.Nil(t, err, "Fetching AppAttributeFormula returns without error") {
		assert.NotNil(t, rec.ID, "AppAttributeFormula record ID is not nil")
		assert.NotNil(t, rec.CreatedAt, "AppAttributeFormula record CreatedAt is not nil")
		assert.Empty(t, rec.UpdatedAt.String, "AppAttributeFormula record UpdatedAt is empty")
		assert.Empty(t, rec.DeletedAt.String, "AppAttributeFormula record DeletedAt is empty")
	}
}

func TestMockUpdate(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	rec, _ := tm.GetByID(tr.ID)
	rec.Name = fake.CharactersN(10)

	err := tm.Update(rec)
	if assert.Nil(t, err, "AppAttributeFormula record updated without error") {
		assert.NotEmpty(t, rec.UpdatedAt.String, "AppAttributeFormula record UpdatedAt is not empty")
	}
}

func TestMockGetByParam(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := tm.GetByParam(params)

	assert.Nil(t, err, "AppAttributeFormula records fetched without error")
	assert.NotNil(t, nrecs, "AppAttributeFormula records is not empty")
}

func TestMockDelete(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// delete
	err := tm.Delete(tr.ID)
	assert.Nil(t, err, "AppAttributeFormula record deleted without error")

	_, err = tm.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppAttributeFormula record returns error")
}

func TestMockRemove(t *testing.T) {

	_, _, tr, tm, teardown := mockSetup(t)
	defer teardown()

	// remove
	err := tm.Remove(tr.ID)
	assert.Nil(t, err, "AppAttributeFormula record removed without error")

	// get by id
	_, err = tm.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppAttributeFormula record returns error")
}

func TestMockCreateTestRecord(t *testing.T) {

	ar, _, _, _, teardown := mockSetup(t)
	defer teardown()

	// mock repo
	m, _ := NewMockRepo()

	// create test record
	r, err := m.CreateTestRecord(ar.ID, fake.CharactersN(10), "", true, 0)

	if assert.Nil(t, err, "AppAttributeFormula record created without error") {
		assert.NotEmpty(t, r.ID, "AppAttributeFormula test record ID is not empty")
		assert.NotEmpty(t, r.CreatedAt, "AppAttributeFormula test record CreatedAt is not empty")
	}
}

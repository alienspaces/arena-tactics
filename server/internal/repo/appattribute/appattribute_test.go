package appattribute

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger

var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppAttributeRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppAttribute record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(ar.ID, "", "", "", "", true)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppAttribute record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app attribute record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, tr, teardown
}

func TestCreate(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppID = ar.ID
	rec.Name = fake.ProductName()

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppAttribute record created without error")

	assert.NotNil(t, rec.ID, "AppAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppAttribute returns without error")

	assert.NotNil(t, rec.ID, "AppAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)
	rec.Name = fake.CharactersN(10)
	rec.ValueFormula = "10 * 2"
	rec.MaxValueFormula = rec.ValueFormula
	rec.MinValueFormula = "0"

	err = m.Update(rec)
	assert.Nil(t, err, "AppAttribute record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppAttribute record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppAttribute records fetched without error")

	assert.NotNil(t, nrecs, "AppAttribute records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppAttribute record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppAttribute record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppAttribute record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppAttribute record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppAttribute repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(ar.ID, "", "", "", "", true)
	assert.Nil(t, err, "AppAttribute record created without error")

	assert.NotEmpty(t, r.ID, "AppAttribute test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppAttribute test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

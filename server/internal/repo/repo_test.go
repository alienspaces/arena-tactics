package repo

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

func TestRepo(t *testing.T) {

	// set test
	os.Setenv("APP_ENV", "TEST")

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, _ := env.NewEnv()

		// logger
		l, _ := logger.NewLogger(e)

		// database
		db, _ := database.NewDatabase(e, l)

		// tx
		tx, _ := db.Beginx()

		// repo
		m := Base{
			Env:    e,
			Logger: l,
			Tx:     tx,
		}

		assert.NotNil(t, m, "Repo is not nil")
	}
}

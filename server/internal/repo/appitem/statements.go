package appitem

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_item
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_item
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_item
	(id, app_id, name, narrative_text, item_type, item_location, created_at)
VALUES
	(:id, :app_id, :name, :narrative_text, :item_type, :item_location, :created_at)
RETURNING id, name, narrative_text, item_type, item_location, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_item SET
	app_id         = :app_id,
	name           = :name,
	narrative_text = :narrative_text,
	item_type      = :item_type,
	item_location  = :item_location,
	updated_at     = :updated_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, name, narrative_text, item_type, item_location, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_item SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_item
WHERE id = :id
`
}

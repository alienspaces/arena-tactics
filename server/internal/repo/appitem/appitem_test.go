// Package appitem provides access to the database resource appitems
package appitem

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppItemRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppItem record
	aim, _ := NewRepo(e, l, tx)
	air, _ := aim.CreateTestRecord(ar.ID, "", "", "", "")

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new trx %v", err)
		}

		// remove AppItem record
		aim, _ := NewRepo(e, l, tx)
		if err := aim.Remove(air.ID); err != nil {
			t.Fatalf("Failed to remove app item record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, air, teardown
}

func TestCreate(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppID = ar.ID
	rec.ItemType = record.ItemTypeJewellery
	rec.ItemLocation = record.ItemLocationEar

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppItem record created without error")

	assert.NotNil(t, rec.ID, "AppItem record ID is not nil")
	assert.NotNil(t, rec.AppID, "AppItemItem record AppID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppItem record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppItem record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppItem record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, air, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	rec, err := m.GetByID(air.ID)
	assert.Nil(t, err, "Fetching AppItem returns without error")

	assert.NotNil(t, rec.ID, "AppItem record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppItem record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppItem record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppItem record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, air, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	// update AppItem
	rec, _ := m.GetByID(air.ID)
	rec.Name = "Test Update Name"

	err = m.Update(rec)
	assert.Nil(t, err, "AppItem record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppItem record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, air, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = air.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppItem records fetched without error")

	assert.NotNil(t, nrecs, "AppItem records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, air, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	// delete
	err = m.Delete(air.ID)
	assert.Nil(t, err, "AppItem record deleted without error")

	_, err = m.GetByID(air.ID)
	assert.NotNil(t, err, "Fetching deleted AppItem record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, air, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	// remove
	err = m.Remove(air.ID)
	assert.Nil(t, err, "AppItem record removed without error")

	// get by id
	_, err = m.GetByID(air.ID)
	assert.NotNil(t, err, "Fetching removed AppItem record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItem repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItem repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(ar.ID, "", "", "", "")
	assert.Nil(t, err, "AppItem record created without error")

	assert.NotEmpty(t, r.ID, "AppItem test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppItem test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

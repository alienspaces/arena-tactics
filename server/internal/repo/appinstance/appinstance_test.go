package appinstance

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppInstanceRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppInstance record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(ar.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppInstance record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app instance record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, tr, teardown
}

func TestCreate(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	// data
	data := record.AppInstanceData{
		AppID: ar.ID,
	}

	jsonData, err := json.Marshal(data)
	assert.Nil(t, err, "Marshal AppInstance Data without error")

	rec := m.NewRecord()
	rec.AppID = ar.ID
	rec.Data = jsonData

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppInstance record created without error")

	assert.NotNil(t, rec.ID, "AppInstance record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppInstance record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppInstance record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppInstance record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppInstance returns without error")

	assert.NotNil(t, rec.ID, "AppInstance record ID is not nil")
	assert.NotEmpty(t, rec.Data, "AppInstance record Data is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppInstance record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppInstance record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppInstance record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppInstance record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppInstance record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppInstance records fetched without error")

	assert.NotNil(t, nrecs, "AppInstance records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppInstance record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppInstance record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppInstance record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppInstance record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	ar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppInstance repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppInstance repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(ar.ID)
	assert.Nil(t, err, "AppInstance record created without error")

	assert.NotEmpty(t, r.ID, "AppInstance test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppInstance test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

package app

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
	INSERT INTO app
		(id, name, description, initiative_formula, death_formula, status, created_at)
	VALUES
		(:id, :name, :description, :initiative_formula, :death_formula, :status, :created_at)
	RETURNING
		id, name, description, initiative_formula, death_formula, status, created_at, updated_at, deleted_at
	`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
	UPDATE app SET
		name 		       = :name,
		description        = :description,
		initiative_formula = :initiative_formula,
		death_formula     = :death_formula,
		status 		       = :status,
		updated_at         = :updated_at
	WHERE id = :id
	AND deleted_at IS NULL
	RETURNING
		id, name, description, initiative_formula, death_formula, status, created_at, updated_at, deleted_at
	`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
	UPDATE app SET
		deleted_at = :deleted_at
	WHERE id = :id
	AND deleted_at IS NULL
	`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
	DELETE FROM app
	WHERE id = :id
	`
}

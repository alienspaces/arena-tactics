package app

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app record
	am, _ := NewRepo(e, l, tx)
	ar, err := am.CreateTestRecord()
	if err != nil {
		t.Fatalf("Failed to create test app record %v", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transaction >%v<", err)
		}

		// remove app record
		am, _ := NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, teardown
}

func TestCreate(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	rec := m.NewRecord()
	rec.Name = fake.ProductName()
	rec.Status = record.AppStatusStopped

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New app record created without error")

	assert.NotNil(t, rec.Name, "app record Name is not nil")
	assert.NotNil(t, rec.CreatedAt, "app record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "app record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "app record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	ar, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	rec, err := m.GetByID(ar.ID)
	assert.Nil(t, err, "Fetching app returns without error")

	assert.NotNil(t, rec.Name, "app record Name is not nil")
	assert.NotNil(t, rec.CreatedAt, "app record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "app record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "app record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	ar, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	rec, _ := m.GetByID(ar.ID)
	rec.Name = fake.ProductName()

	err = m.Update(rec)
	assert.Nil(t, err, "app record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "app record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	ar, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = ar.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "app records fetched without error")

	assert.NotNil(t, nrecs, "app records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	ar, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	// delete
	err = m.Delete(ar.ID)
	assert.Nil(t, err, "app record deleted without error")

	_, err = m.GetByID(ar.ID)
	assert.NotNil(t, err, "Fetching deleted app record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	ar, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	// remove
	err = m.Remove(ar.ID)
	assert.Nil(t, err, "app record removed without error")

	// get by id
	_, err = m.GetByID(ar.ID)
	assert.NotNil(t, err, "Fetching removed app record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// app repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new app repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord()
	assert.Nil(t, err, "app record created without error")

	assert.NotEmpty(t, r.ID, "app test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "app test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

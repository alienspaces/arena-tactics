package appitemeffect

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppItemRecord,
	*record.AppEffectRecord,
	*record.AppItemEffectRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppItem record
	aim, _ := appitem.NewRepo(e, l, tx)
	air, _ := aim.CreateTestRecord(ar.ID, "", "", "", "")

	// AppEffect record
	aem, _ := appeffect.NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "", "", "", 0, false, false)

	// AppItemEffect record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(air.ID, aer.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppItemEffect record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app item effect record >%v<", err)
		}

		// remove AppEffect record
		aem, _ := appeffect.NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app effect record >%v<", err)
		}

		// remove AppItem record
		aim, _ := appitem.NewRepo(e, l, tx)
		if err := aim.Remove(air.ID); err != nil {
			t.Fatalf("Failed to remove app item record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, air, aer, tr, teardown
}

func TestCreate(t *testing.T) {

	_, air, aer, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppItemID = air.ID
	rec.AppEffectID = aer.ID

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppItemEffect record created without error")

	assert.NotNil(t, rec.ID, "AppItemEffect record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppItemEffect record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppItemEffect record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppItemEffect record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppItemEffect returns without error")

	assert.NotNil(t, rec.ID, "AppItemEffect record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppItemEffect record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppItemEffect record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppItemEffect record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppItemEffect record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppItemEffect record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppItemEffect records fetched without error")

	assert.NotNil(t, nrecs, "AppItemEffect records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppItemEffect record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppItemEffect record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppItemEffect record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppItemEffect record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, air, aer, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppItemEffect repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppItemEffect repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(air.ID, aer.ID)
	assert.Nil(t, err, "AppItemEffect record created without error")

	assert.NotEmpty(t, r.ID, "AppItemEffect test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppItemEffect test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

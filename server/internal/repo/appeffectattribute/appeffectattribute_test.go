package appeffectattribute

import (
	"testing"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEffectRecord,
	*record.AppAttributeRecord,
	*record.AppEffectAttributeRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEffect record
	aem, _ := appeffect.NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "", "", "", 0, false, false)

	// AppAttribute record
	aam, _ := appattribute.NewRepo(e, l, tx)
	aar, _ := aam.CreateTestRecord(ar.ID, "", "", "", "", true)

	// AppEffectAttribute record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(aer.ID, aar.ID, "", "", "")

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEffectAttribute record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app effect attribute record >%v<", err)
		}

		// remove AppEffect record
		aem, _ := appeffect.NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app effect record >%v<", err)
		}

		// remove AppAttribute record
		aam, _ := appattribute.NewRepo(e, l, tx)
		if err := aam.Remove(aar.ID); err != nil {
			t.Fatalf("Failed to remove app attribute record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, aar, tr, teardown
}

func TestCreate(t *testing.T) {

	_, aer, aar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppEffectID = aer.ID
	rec.AppAttributeID = aar.ID
	rec.Target = record.TargetAnyFoe
	rec.ApplyConstraint = record.ApplyConstraintCalculated
	rec.ValueFormula = "1"

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEffectAttribute record created without error")

	assert.NotNil(t, rec.ID, "AppEffectAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEffectAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEffectAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEffectAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppEffectAttribute returns without error")

	assert.NotNil(t, rec.ID, "AppEffectAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEffectAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEffectAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEffectAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppEffectAttribute record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEffectAttribute record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEffectAttribute records fetched without error")

	assert.NotNil(t, nrecs, "AppEffectAttribute records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppEffectAttribute record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppEffectAttribute record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppEffectAttribute record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppEffectAttribute record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, aer, aar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEffectAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEffectAttribute repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(aer.ID, aar.ID, "", "", "")
	assert.Nil(t, err, "AppEffectAttribute record created without error")

	assert.NotEmpty(t, r.ID, "AppEffectAttribute test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEffectAttribute test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

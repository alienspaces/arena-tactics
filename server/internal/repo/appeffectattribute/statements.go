package appeffectattribute

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_effect_attribute
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_effect_attribute
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_effect_attribute (
	id,
	app_effect_id,
	app_attribute_id,
	target,
	apply_constraint,
	value_formula,
	created_at
) VALUES (
	:id,
	:app_effect_id,
	:app_attribute_id,
	:target,
	:apply_constraint,
	:value_formula,
	:created_at
) RETURNING id, app_effect_id, app_attribute_id, target, apply_constraint, value_formula, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_effect_attribute SET
	app_effect_id 	              = :app_effect_id,
	app_attribute_id              = :app_attribute_id,
	target 		                  = :target,
	apply_constraint              = :apply_constraint,
	value_formula                 = :value_formula,
	updated_at 		              = :updated_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, app_effect_id, app_attribute_id, target, apply_constraint, value_formula, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_effect_attribute SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_effect_attribute
WHERE id = :id
`
}

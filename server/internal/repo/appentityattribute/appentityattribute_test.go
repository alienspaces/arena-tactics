package appentityattribute

import (
	"testing"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityRecord,
	*record.AppAttributeRecord,
	*record.AppEntityAttributeRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEntity record
	aem, _ := appentity.NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "")

	// AppAttribute record
	aam, _ := appattribute.NewRepo(e, l, tx)
	aar, _ := aam.CreateTestRecord(ar.ID, "", "", "", "", true)

	// AppEntityAttribute record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(aer.ID, aar.ID, nil)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEntityAttribute record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app entity attribute record >%v<", err)
		}

		// remove AppAttribute record
		aam, _ := appattribute.NewRepo(e, l, tx)
		if err := aam.Remove(aar.ID); err != nil {
			t.Fatalf("Failed to remove app attribute record >%v<", err)
		}

		// remove AppEntity record
		aem, _ := appentity.NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app entity record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, aar, tr, teardown
}

func TestCreate(t *testing.T) {

	_, aer, aar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppEntityID = aer.ID
	rec.AppAttributeID = aar.ID
	rec.Value = 10

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEntityAttribute record created without error")

	assert.NotNil(t, rec.ID, "AppEntityAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppEntityAttribute returns without error")

	assert.NotNil(t, rec.ID, "AppEntityAttribute record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntityAttribute record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntityAttribute record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntityAttribute record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntityAttribute record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntityAttribute record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntityAttribute records fetched without error")

	assert.NotNil(t, nrecs, "AppEntityAttribute records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppEntityAttribute record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntityAttribute record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppEntityAttribute record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppEntityAttribute record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, aer, aar, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntityAttribute repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntityAttribute repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(aer.ID, aar.ID, nil)
	assert.Nil(t, err, "AppEntityAttribute record created without error")

	assert.NotEmpty(t, r.ID, "AppEntityAttribute test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntityAttribute test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

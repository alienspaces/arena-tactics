package appentityskill

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_entity_skill
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_entity_skill
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_entity_skill
	(id, app_entity_id, app_skill_id, created_at)
VALUES
	(:id, :app_entity_id, :app_skill_id, :created_at)
RETURNING id, app_entity_id, app_skill_id, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_entity_skill SET
	app_entity_id = :app_entity_id,
	app_skill_id  = :app_skill_id,
	updated_at    = :updated_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, app_entity_id, app_skill_id, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_entity_skill SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_entity_skill
WHERE id = :id
`
}

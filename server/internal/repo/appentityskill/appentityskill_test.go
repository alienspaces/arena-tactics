package appentityskill

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppEntityRecord,
	*record.AppSkillRecord,
	*record.AppEntitySkillRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppEntity record
	aem, _ := appentity.NewRepo(e, l, tx)
	aer, _ := aem.CreateTestRecord(ar.ID, "")

	// AppSkill record
	asm, _ := appskill.NewRepo(e, l, tx)
	asr, _ := asm.CreateTestRecord(ar.ID, "", "", "")

	// AppEntitySkill record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(aer.ID, asr.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppEntitySkill record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app entity skill record >%v<", err)
		}

		// remove AppSkill record
		asm, _ := appskill.NewRepo(e, l, tx)
		if err := asm.Remove(asr.ID); err != nil {
			t.Fatalf("Failed to remove app skill record >%v<", err)
		}

		// AppEntity record
		aem, _ := appentity.NewRepo(e, l, tx)
		if err := aem.Remove(aer.ID); err != nil {
			t.Fatalf("Failed to remove app entity record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, aer, asr, tr, teardown
}

func TestCreate(t *testing.T) {

	_, aer, asr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppEntityID = aer.ID
	rec.AppSkillID = asr.ID

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppEntitySkill record created without error")

	assert.NotNil(t, rec.ID, "AppEntitySkill record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntitySkill record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntitySkill record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntitySkill record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppEntitySkill returns without error")

	assert.NotNil(t, rec.ID, "AppEntitySkill record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppEntitySkill record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppEntitySkill record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppEntitySkill record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, aer, asr, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)
	rec.AppEntityID = aer.ID
	rec.AppSkillID = asr.ID

	err = m.Update(rec)
	assert.Nil(t, err, "AppEntitySkill record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppEntitySkill record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppEntitySkill records fetched without error")

	assert.NotNil(t, nrecs, "AppEntitySkill records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppEntitySkill record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppEntitySkill record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppEntitySkill record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppEntitySkill record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, aer, asr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppEntitySkill repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppEntitySkill repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(aer.ID, asr.ID)
	assert.Nil(t, err, "AppEntitySkill record created without error")

	assert.NotEmpty(t, r.ID, "AppEntitySkill test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppEntitySkill test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

package appfight

// GetByIDSQL - Returns SQL
func (m *Repo) GetByIDSQL() string {
	return `
SELECT *
FROM app_fight
WHERE id = $1
AND deleted_at IS NULL
`
}

// GetByParamSQL -
func (m *Repo) GetByParamSQL() string {
	return `
SELECT *
FROM app_fight
WHERE deleted_at IS NULL
`
}

// CreateSQL -
func (m *Repo) CreateSQL() string {
	return `
INSERT INTO app_fight
	(id, app_id, status, created_at)
VALUES
	(:id, :app_id, :status, :created_at)
RETURNING id, app_id, status, created_at, updated_at, deleted_at
`
}

// UpdateSQL -
func (m *Repo) UpdateSQL() string {
	return `
UPDATE app_fight SET
	app_id     = :app_id,
	status     = :status,
	updated_at = :updated_at
WHERE id = :id
AND deleted_at IS NULL
RETURNING id, app_id, status, created_at, updated_at, deleted_at
`
}

// DeleteSQL -
func (m *Repo) DeleteSQL() string {
	return `
UPDATE app_fight SET
	deleted_at = :deleted_at
WHERE id = :id
AND deleted_at IS NULL
`
}

// RemoveSQL -
func (m *Repo) RemoveSQL() string {
	return `
DELETE FROM app_fight
WHERE id = :id
`
}

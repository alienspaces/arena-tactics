package template

import (
	"database/sql"
	"fmt"

	"github.com/icrowley/fake"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo"

	// util
	"gitlab.com/alienspaces/arena-tactics/server/internal/util"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// MockRepo - interface implementation
type MockRepo struct {
	// export so can assign test records for mock purposes
	Records map[string]*record.TemplateRecord

	repo.Base
}

// NewMockRepo - returns an implementation of a Repo
func NewMockRepo() (m *MockRepo, err error) {

	m = &MockRepo{
		Records: make(map[string]*record.TemplateRecord),
	}

	return m, err
}

// NewRecord -
func (m *MockRepo) NewRecord() *record.TemplateRecord {
	return &record.TemplateRecord{}
}

// NewRecordArray -
func (m *MockRepo) NewRecordArray() []*record.TemplateRecord {
	return []*record.TemplateRecord{}
}

// GetByID -
func (m *MockRepo) GetByID(id string) (*record.TemplateRecord, error) {

	if _, ok := m.Records[id]; !ok {
		return nil, fmt.Errorf("GetByID record id >%s< not found", id)
	}

	rec := m.Records[id]

	return rec, nil
}

// GetByParam -
func (m *MockRepo) GetByParam(params map[string]interface{}) ([]*record.TemplateRecord, error) {

	// records
	var recs []*record.TemplateRecord

	for _, rec := range m.Records {

		// no params, add every record
		if len(params) == 0 {
			recs = append(recs, rec)
			continue
		}

		// compare params
		addRec, err := m.CompareParams(params, rec)
		if err != nil {
			m.Logger.Warn().Msgf("GetByParam failed compare params >%v<", err)
			return nil, err
		}

		if addRec == true {
			recs = append(recs, rec)
		}
	}

	return recs, nil
}

// Create -
func (m *MockRepo) Create(rec *record.TemplateRecord) error {

	// id
	rec.ID = util.GetUUID()
	rec.CreatedAt = util.GetTime()

	m.Records[rec.ID] = rec

	return nil
}

// Update -
func (m *MockRepo) Update(rec *record.TemplateRecord) error {

	if _, ok := m.Records[rec.ID]; !ok {
		return fmt.Errorf("Update record id >%s< not found", rec.ID)
	}

	rec.UpdatedAt = sql.NullString{
		String: util.GetTime(),
		Valid:  true,
	}

	m.Records[rec.ID] = rec

	return nil
}

// Delete -
func (m *MockRepo) Delete(id string) error {

	if _, ok := m.Records[id]; !ok {
		return fmt.Errorf("Delete record id >%s< not found", id)
	}

	delete(m.Records, id)

	return nil
}

// Remove -
func (m *MockRepo) Remove(id string) error {

	if _, ok := m.Records[id]; !ok {
		return fmt.Errorf("Delete record id >%s< not found", id)
	}

	delete(m.Records, id)

	return nil
}

// CreateTestRecord - returns a record for testing
func (m *MockRepo) CreateTestRecord() (*record.TemplateRecord, error) {

	r := m.NewRecord()
	r.Name = fake.CharactersN(10)

	err := m.Create(r)
	return r, err
}

// Package template -
package template

import (
	"fmt"

	"github.com/icrowley/fake"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repopreparer"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo"

	// util
	"gitlab.com/alienspaces/arena-tactics/server/internal/util"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// RepoName -
const RepoName string = "template"

// Repo -
type Repo struct {
	repo.Base
}

// NewRepo returns a new repo
func NewRepo(e env.Env, l zerolog.Logger, tx *sqlx.Tx) (*Repo, error) {

	// repo preparer
	p, err := repopreparer.NewPreparer(e, l, tx)
	if err != nil {
		return nil, err
	}

	// logger
	l = l.With().Str("package", "repo/"+RepoName).Logger()

	// repo
	m := &Repo{
		repo.Base{
			Name:     RepoName,
			Tx:       tx,
			Env:      e,
			Logger:   l,
			Preparer: p,
		},
	}

	// init repo
	err = m.Init()
	if err != nil {
		return nil, err
	}

	// prepare statements
	p.Prepare(m)

	return m, nil
}

// CreateTestRecord - returns a record for testing
func (m *Repo) CreateTestRecord() (*record.TemplateRecord, error) {

	r := m.NewRecord()
	r.Name = fake.CharactersN(10)

	err := m.Create(r)
	return r, err
}

// ApplyDefaults -
func (m *Repo) ApplyDefaults(rec *record.TemplateRecord) error {

	return nil
}

// NewRecord - returns a new template record
func (m *Repo) NewRecord() *record.TemplateRecord {
	return &record.TemplateRecord{}
}

// NewRecordArray - returns a new array of template records
func (m *Repo) NewRecordArray() []*record.TemplateRecord {
	return []*record.TemplateRecord{}
}

// GetByID - returns record for id
func (m *Repo) GetByID(id string) (*record.TemplateRecord, error) {

	// logger
	l := m.Logger.With().Str("function", "GetByID").Logger()

	l.Debug().Msgf("Get >%s< record ID >%s<", RepoName, id)

	// new record
	rec := m.NewRecord()

	// get record by id
	err := m.GetRecordByID(id, rec)

	return rec, err
}

// GetByParam - accepts a map of parameters and returns an array of records
func (m *Repo) GetByParam(params map[string]interface{}) ([]*record.TemplateRecord, error) {

	// logger
	l := m.Logger.With().Str("function", "GetByParam").Logger()

	// records
	recs := m.NewRecordArray()

	// tx
	tx := m.Tx

	// sql
	sql := m.GetByParamSQL()

	// params
	for k := range params {
		sql = sql + fmt.Sprintf("AND %s = :%s\n", k, k)
	}

	stmt, err := tx.PrepareNamed(sql)
	if err != nil {
		l.Warn().Msgf("Error preparing statement >%v<", err)
		return nil, err
	}

	rows, err := stmt.Queryx(params)
	if err != nil {
		l.Warn().Msgf("Error querying row >%v<", err)
		return nil, err
	}

	for rows.Next() {
		e := m.NewRecord()
		err = rows.StructScan(&e)
		recs = append(recs, e)
	}

	m.DebugStruct("Fetched", recs)

	return recs, nil
}

// Create - creates a record
func (m *Repo) Create(rec *record.TemplateRecord) error {

	// logger
	l := m.Logger.With().Str("function", "Create").Logger()

	// preparer
	p := m.Preparer

	// stmt
	stmt := p.CreateStmt(m)

	// id
	rec.ID = util.GetUUID()

	// created at
	rec.CreatedAt = util.GetTime()

	// defaults
	err := m.ApplyDefaults(rec)
	if err != nil {
		l.Warn().Msgf("Error applying defaults >%v<", err)
		return err
	}

	m.DebugStruct("Create", rec)

	res, err := stmt.Exec(rec)
	if err != nil {
		l.Warn().Msgf("Error executing insert >%v<", err)
		return err
	}

	l.Debug().Msgf("Created record >%v<", rec)

	// last insert id / rows affected
	lid, err := res.LastInsertId()
	raf, err := res.RowsAffected()

	l.Debug().Msgf("Result LastInsertID >%d< RowsAffected >%d<", lid, raf)

	// get created record
	nrec, err := m.GetByID(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return err
	}

	// copy struct values
	*rec = *nrec

	m.DebugStruct("Created", rec)

	return nil
}

// Update - updates record
func (m *Repo) Update(rec *record.TemplateRecord) error {

	// logger
	l := m.Logger.With().Str("function", "Update").Logger()

	// preparer
	p := m.Preparer

	// stmt
	stmt := p.UpdateStmt(m)

	// updated at
	rec.UpdatedAt.String = util.GetTime()
	rec.UpdatedAt.Valid = true

	res, err := stmt.Exec(rec)
	if err != nil {
		l.Warn().Msgf("Error executing update >%v<", err)
		return err
	}

	// last insert id / rows affected
	lid, err := res.LastInsertId()
	raf, err := res.RowsAffected()

	l.Debug().Msgf("Result LastInsertID >%d< RowsAffected >%d<", lid, raf)

	// get updated record
	//  Database level triggers etc may modify record values
	nrec, err := m.GetByID(rec.ID)
	if err != nil {
		return err
	}

	// copy struct values
	*rec = *nrec

	m.DebugStruct("Updated", rec)

	return nil
}

// Delete - deletes record with id
func (m *Repo) Delete(id string) error {

	// logger
	l := m.Logger.With().Str("function", "Delete").Logger()

	l.Debug().Msgf("Delete >%s< record ID >%s<", RepoName, id)

	// preparer
	p := m.Preparer

	// stmt
	stmt := p.DeleteStmt(m)

	rec := m.NewRecord()
	rec.ID = id

	// updated at
	rec.DeletedAt.String = util.GetTime()
	rec.DeletedAt.Valid = true

	res, err := stmt.Exec(rec)
	if err != nil {
		l.Warn().Msgf("Error executing delete >%v<", err)
		return err
	}

	// last insert id / rows affected
	lid, err := res.LastInsertId()
	raf, err := res.RowsAffected()

	l.Debug().Msgf("Result LastInsertID >%d< RowsAffected >%d<", lid, raf)

	m.DebugStruct("Deleted", rec)

	return nil
}

// Remove - removes record with id
func (m *Repo) Remove(id string) error {

	// logger
	l := m.Logger.With().Str("function", "Remove").Logger()

	l.Debug().Msgf("Remove >%s< record ID >%s<", RepoName, id)

	// preparer
	p := m.Preparer

	// stmt
	stmt := p.RemoveStmt(m)

	rec := m.NewRecord()
	rec.ID = id

	res, err := stmt.Exec(rec)
	if err != nil {
		l.Warn().Msgf("Error executing delete >%v<", err)
		return err
	}

	// last insert id / rows affected
	lid, err := res.LastInsertId()
	raf, err := res.RowsAffected()

	l.Debug().Msgf("Result LastInsertID >%d< RowsAffected >%d<", lid, raf)

	m.DebugStruct("Removed", rec)

	return nil
}

// Count - return count of records in database
func (m *Repo) Count() (int, error) {

	// logger
	l := m.Logger.With().Str("function", "Count").Logger()

	params := make(map[string]interface{})
	recs, err := m.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed get by param >%v<", err)
		return 0, err
	}
	l.Debug().Msgf("Have record count >%d<", len(recs))

	return len(recs), nil
}

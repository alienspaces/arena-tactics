package template

import (
	"testing"

	"github.com/icrowley/fake"
	"github.com/stretchr/testify/assert"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"

	// repos
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

// test records
type TestRecs struct {
	App      *record.AppRecord
	Template *record.TemplateRecord
}

func setup(t *testing.T) (
	*TestRecs, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// test records
	testRecs := &TestRecs{}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	testRecs.App, _ = am.CreateTestRecord()

	// Template record
	tm, _ := NewRepo(e, l, tx)
	testRecs.Template, _ = tm.CreateTestRecord()

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove Template record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(testRecs.Template.ID); err != nil {
			t.Fatalf("Failed to remove template record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(testRecs.App.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return testRecs, teardown
}

func TestCreate(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	rec := m.NewRecord()
	rec.Name = fake.CharactersN(10)

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New Template record created without error")

	assert.NotNil(t, rec.ID, "Template record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "Template record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "Template record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "Template record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	rec, err := m.GetByID(testRecs.Template.ID)
	assert.Nil(t, err, "Fetching Template returns without error")

	assert.NotNil(t, rec.ID, "Template record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "Template record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "Template record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "Template record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	rec, _ := m.GetByID(testRecs.Template.ID)
	rec.Name = fake.CharactersN(10)

	err = m.Update(rec)
	assert.Nil(t, err, "Template record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "Template record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = testRecs.Template.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "Template records fetched without error")

	assert.NotNil(t, nrecs, "Template records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	// delete
	err = m.Delete(testRecs.Template.ID)
	if assert.Nil(t, err, "Template record deleted without error") {

		_, err = m.GetByID(testRecs.Template.ID)
		assert.NotNil(t, err, "Fetching deleted Template record returns error")
	}

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	testRecs, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	// remove
	err = m.Remove(testRecs.Template.ID)
	if assert.Nil(t, err, "Template record removed without error") {

		// get by id
		_, err = m.GetByID(testRecs.Template.ID)
		assert.NotNil(t, err, "Fetching removed Template record returns error")
	}

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// Template repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new Template repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord()
	assert.Nil(t, err, "Template record created without error")

	assert.NotEmpty(t, r.ID, "Template test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "Template test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

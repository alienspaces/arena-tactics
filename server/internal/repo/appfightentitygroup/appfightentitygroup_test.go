package appfightentitygroup

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*record.AppRecord,
	*record.AppFightRecord,
	*record.AppEntityGroupRecord,
	*record.AppFightEntityGroupRecord, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// App record
	am, _ := app.NewRepo(e, l, tx)
	ar, _ := am.CreateTestRecord()

	// AppFight
	afm, _ := appfight.NewRepo(e, l, tx)
	afr, _ := afm.CreateTestRecord(ar.ID)

	// AppEntityGroup
	aegm, _ := appentitygroup.NewRepo(e, l, tx)
	aegr, _ := aegm.CreateTestRecord(ar.ID)

	// AppFightEntityGroup record
	tm, _ := NewRepo(e, l, tx)
	tr, _ := tm.CreateTestRecord(afr.ID, aegr.ID)

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove AppFightEntityGroup record
		tm, _ := NewRepo(e, l, tx)
		if err := tm.Remove(tr.ID); err != nil {
			t.Fatalf("Failed to remove app fight entity group record >%v<", err)
		}

		// AppEntityGroup
		aegm, _ := appentitygroup.NewRepo(e, l, tx)
		if err := aegm.Remove(aegr.ID); err != nil {
			t.Fatalf("Failed to remove app entity group record >%v<", err)
		}

		// AppFight
		afm, _ := appfight.NewRepo(e, l, tx)
		if err := afm.Remove(afr.ID); err != nil {
			t.Fatalf("Failed to remove app fight record >%v<", err)
		}

		// remove App record
		am, _ := app.NewRepo(e, l, tx)
		if err := am.Remove(ar.ID); err != nil {
			t.Fatalf("Failed to remove app record >%v<", err)
		}

		tx.Commit()
	}

	return ar, afr, aegr, tr, teardown
}

func TestCreate(t *testing.T) {

	_, afr, aegr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	rec := m.NewRecord()
	rec.AppFightID = afr.ID
	rec.AppEntityGroupID = aegr.ID

	// create
	err = m.Create(rec)
	assert.Nil(t, err, "New AppFightEntityGroup record created without error")

	assert.NotNil(t, rec.ID, "AppFightEntityGroup record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightEntityGroup record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightEntityGroup record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightEntityGroup record DeletedAt is empty")

	tx.Rollback()
}

func TestGetByID(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	rec, err := m.GetByID(tr.ID)
	assert.Nil(t, err, "Fetching AppFightEntityGroup returns without error")

	assert.NotNil(t, rec.ID, "AppFightEntityGroup record ID is not nil")
	assert.NotNil(t, rec.CreatedAt, "AppFightEntityGroup record CreatedAt is not nil")
	assert.Empty(t, rec.UpdatedAt.String, "AppFightEntityGroup record UpdatedAt is empty")
	assert.Empty(t, rec.DeletedAt.String, "AppFightEntityGroup record DeletedAt is empty")

	tx.Rollback()
}

func TestUpdate(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	rec, _ := m.GetByID(tr.ID)

	err = m.Update(rec)
	assert.Nil(t, err, "AppFightEntityGroup record updated without error")

	assert.NotEmpty(t, rec.UpdatedAt.String, "AppFightEntityGroup record UpdatedAt is not empty")

	tx.Rollback()
}

func TestGetByParam(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	// get by param
	params := make(map[string]interface{})
	params["id"] = tr.ID

	nrecs, err := m.GetByParam(params)
	assert.Nil(t, err, "AppFightEntityGroup records fetched without error")

	assert.NotNil(t, nrecs, "AppFightEntityGroup records is not empty")

	tx.Rollback()
}

func TestDelete(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	// delete
	err = m.Delete(tr.ID)
	assert.Nil(t, err, "AppFightEntityGroup record deleted without error")

	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching deleted AppFightEntityGroup record returns error")

	tx.Rollback()
}

func TestRemove(t *testing.T) {

	_, _, _, tr, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	// remove
	err = m.Remove(tr.ID)
	assert.Nil(t, err, "AppFightEntityGroup record removed without error")

	// get by id
	_, err = m.GetByID(tr.ID)
	assert.NotNil(t, err, "Fetching removed AppFightEntityGroup record returns error")

	// rollback
	tx.Rollback()
}

func TestCreateTestRecord(t *testing.T) {

	_, afr, aegr, _, teardown := setup(t)
	defer teardown()

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// AppFightEntityGroup repo
	m, err := NewRepo(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create new AppFightEntityGroup repo %v", err)
	}

	// create test record
	r, err := m.CreateTestRecord(afr.ID, aegr.ID)
	assert.Nil(t, err, "AppFightEntityGroup record created without error")

	assert.NotEmpty(t, r.ID, "AppFightEntityGroup test record ID is not empty")
	assert.NotEmpty(t, r.CreatedAt, "AppFightEntityGroup test record CreatedAt is not empty")

	// rollback
	tx.Rollback()
}

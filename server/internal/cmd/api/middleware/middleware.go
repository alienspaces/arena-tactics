package middleware

import (
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/middleware/authen"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/middleware/authz"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/middleware/cors"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Middleware - interface definition
type Middleware interface {
	Apply(h handler.Handler, hf http.HandlerFunc) http.Handler
}

// Middlewarer - interface implementation
type Middlewarer struct {
	Env      env.Env
	Logger   zerolog.Logger
	Database *sqlx.DB
}

// NewMiddleware - Returns a new middleware instance
func NewMiddleware(e env.Env, l zerolog.Logger, db *sqlx.DB) *Middlewarer {

	// logger
	l = l.With().Str("package", "middleware").Logger()

	return &Middlewarer{
		Env:      e,
		Logger:   l,
		Database: db,
	}
}

// Apply - Applies selected middleware to handler chain
func (mw *Middlewarer) Apply(h handler.Handler, hf http.HandlerFunc) http.Handler {

	log := mw.Logger

	var nh http.HandlerFunc
	var err error

	// Requires Authorization
	nh, err = authz.NewAuthz(mw.Env, mw.Logger, mw.Database, h, hf)
	if err != nil {
		log.Error().Msgf("Failed to create authz middleware: %v", err)
		return nil
	}

	// Requires Authentication
	nh, err = authen.NewAuthen(mw.Env, mw.Logger, mw.Database, h, nh)
	if err != nil {
		log.Error().Msgf("Failed to create authz middleware: %v", err)
		return nil
	}

	// Cross Origin Request
	nh, err = cors.NewCors(mw.Env, mw.Logger, mw.Database, h, nh)
	if err != nil {
		log.Error().Msgf("Failed to create authz middleware: %v", err)
		return nil
	}

	return nh
}

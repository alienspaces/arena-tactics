package authz

// NOTE:
// - going to replace local user registration with only supporting Oauth
//   registration with Google or some other provider
// - those changes will certainly impact this middleware

import (
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Authz -
type Authz struct {
	Env      env.Env
	Logger   zerolog.Logger
	Database *sqlx.DB
}

// NewAuthz -
func NewAuthz(e env.Env, l zerolog.Logger, d *sqlx.DB, h handler.Handler, hf http.HandlerFunc) (http.HandlerFunc, error) {

	// logger
	l = l.With().Str("package", "middleware/authz").Logger()

	a := &Authz{
		Env:      e,
		Logger:   l,
		Database: d,
	}

	return a.Apply(h, hf)
}

// Apply -
func (a *Authz) Apply(h handler.Handler, hf http.HandlerFunc) (http.HandlerFunc, error) {

	// logger
	l := a.Logger.With().Str("function", "Apply").Logger()

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		l.Info().Msgf("Authz middleware request >%s<", r.URL)

		hf.ServeHTTP(w, r)
	}), nil
}

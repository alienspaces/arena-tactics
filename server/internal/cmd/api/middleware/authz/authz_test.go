package authz

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	// cmd
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"

	// util
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// lger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transaction %v", err)
	}

	// testing data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to new testing data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transaction %v", err)
		}

		tx.Commit()
	}

	return td, teardown
}

func TestMiddleware(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// handler
	h := &handler.Base{
		Name:            "Templates",
		Unauthenticated: false,
		Unauthorized:    false,
		Versioned:       true,
		Env:             e,
		Logger:          l,
		Database:        db,
	}

	// handler function will raise a server error without a valid user
	hf := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		l.Debug().Msgf("Handler request")
		w.WriteHeader(http.StatusOK)
	})

	// authz middleware
	mw, err := NewAuthz(e, l, db, h, hf)
	assert.Nil(t, err, "Authz initialised without error")
	assert.NotNil(t, mw, "Authz is not nil")

	// url
	url := "/api/templates"

	// recorder
	rr := httptest.NewRecorder()

	// request without user in context
	l.Debug().Msgf("Test without user in context")

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	mw.ServeHTTP(rr, req)

	// Status OK
	assert.Equal(t, 200, rr.Code, "Expect status code to be 200 Ok")
}

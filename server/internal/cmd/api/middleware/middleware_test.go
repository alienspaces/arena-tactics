package middleware

import (
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	// cmd
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

func TestMiddleware(t *testing.T) {

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, _ := env.NewEnv()

		// logger
		l, _ := logger.NewLogger(e)

		// database
		db, _ := database.NewDatabase(e, l)

		// handler
		h := &handler.Base{
			Name:            "Templates",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        db,
		}

		hf := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			l.Debug().Msgf("Handler request %s", r.URL)
		})

		// middleware
		mw := NewMiddleware(e, l, db)
		assert.NotNil(t, mw, "Middleware is not nil")

		mwh := mw.Apply(h, hf)
		assert.NotNil(t, mwh, "Middleware handler is not nil")
	}
}

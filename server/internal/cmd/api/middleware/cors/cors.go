package cors

import (
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/cors"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Cors -
type Cors struct {
	Env      env.Env
	Logger   zerolog.Logger
	Database *sqlx.DB
}

// NewCors -
func NewCors(e env.Env, l zerolog.Logger, d *sqlx.DB, h handler.Handler, hf http.HandlerFunc) (http.HandlerFunc, error) {

	// logger
	l = l.With().Str("package", "middleware/cors").Logger()

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		l.Info().Msgf("CORS request URL >%s<", r.URL)

		c := cors.New(cors.Options{
			Debug:          true,
			AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
			AllowedHeaders: []string{"Content-Type", "X-Authorization"},
		})

		c.Handler(hf).ServeHTTP(w, r)
	}), nil
}

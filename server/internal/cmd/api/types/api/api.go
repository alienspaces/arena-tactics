package api

import (
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
)

// Template -
type Template struct {
	ID        string `json:"id"`
	Name      string `json:"name" validate:"required"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// App -
type App struct {
	ID                string `json:"id"`
	Name              string `json:"name" validate:"required"`
	Description       string `json:"description" validate:"required"`
	InitiativeFormula string `json:"initiative_formula" validate:"required"`
	DeathFormula      string `json:"death_formula" validate:"required"`
	Status            string `json:"status"`
	CreatedAt         string `json:"created_at"`
	UpdatedAt         string `json:"updated_at"`
}

// AppAttribute -
type AppAttribute struct {
	ID              string `json:"id"`
	AppID           string `json:"app_id"`
	Name            string `json:"name" validate:"required"`
	ValueFormula    string `json:"value_formula" validate:"required"`
	MaxValueFormula string `json:"max_value_formula" validate:"required"`
	MinValueFormula string `json:"min_value_formula" validate:"required"`
	Assignable      *bool  `json:"assignable" validate:"required"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
}

// AppEffect -
type AppEffect struct {
	ID         string `json:"id"`
	AppID      string `json:"app_id"`
	Name       string `json:"name" validate:"required"`
	EffectType string `json:"type" validate:"required"`
	Recurring  *bool  `json:"recurring" validate:"required"`
	Permanent  *bool  `json:"permanent" validate:"required"`
	Duration   int    `json:"duration" validate:"required"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

// AppEffectAttribute -
type AppEffectAttribute struct {
	ID              string `json:"id"`
	AppEffectID     string `json:"app_effect_id"`
	AppAttributeID  string `json:"app_attribute_id" validate:"required"`
	Target          string `json:"target" validate:"required"`
	ApplyConstraint string `json:"apply_constraint" validate:"required"`
	ValueFormula    string `json:"value_formula" validate:"required"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
}

// AppItem -
type AppItem struct {
	ID           string `json:"id"`
	AppID        string `json:"app_id"`
	Name         string `json:"name" validate:"required"`
	ItemType     string `json:"item_type" validate:"required"`
	ItemLocation string `json:"item_location" validate:"required"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
}

// AppItemEffect -
type AppItemEffect struct {
	ID          string `json:"id"`
	AppItemID   string `json:"app_item_id"`
	AppEffectID string `json:"app_effect_id" validate:"required"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

// AppSkill -
type AppSkill struct {
	ID        string `json:"id"`
	AppID     string `json:"app_id"`
	Name      string `json:"name" validate:"required"`
	SkillType string `json:"skill_type" validate:"required"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// AppSkillEffect -
type AppSkillEffect struct {
	ID          string `json:"id"`
	AppSkillID  string `json:"app_skill_id"`
	AppEffectID string `json:"app_effect_id" validate:"required"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

// AppEntity -
type AppEntity struct {
	ID              string            `json:"id"`
	AppID           string            `json:"app_id"`
	Name            string            `json:"name" validate:"required"`
	AppEntityGroup  *AppEntityGroup   `json:"entity_group,omitempty"`
	AppEntitySkills []*AppEntitySkill `json:"entity_skills,omitempty"`
	AppEntityItems  []*AppEntityItem  `json:"entity_items,omitempty"`
	CreatedAt       string            `json:"created_at"`
	UpdatedAt       string            `json:"updated_at"`
}

// AppEntityGroup -
type AppEntityGroup struct {
	ID                    string                  `json:"id"`
	AppID                 string                  `json:"app_id"`
	Name                  string                  `json:"name" validate:"required"`
	AppEntityGroupMembers []*AppEntityGroupMember `json:"entity_group_members,omitempty"`
	AppFight              *AppFight               `json:"fight,omitempty"`
	CreatedAt             string                  `json:"created_at"`
	UpdatedAt             string                  `json:"updated_at"`
}

// AppEntityGroupMember -
type AppEntityGroupMember struct {
	ID               string `json:"id"`
	AppEntityGroupID string `json:"app_entity_group_id"`
	AppEntityID      string `json:"app_entity_id" validate:"required"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
}

// AppEntityAttribute -
type AppEntityAttribute struct {
	ID              string `json:"id"`
	AppEntityID     string `json:"app_entity_id"`
	AppAttributeID  string `json:"app_attribute_id" validate:"required"`
	Value           int    `json:"value" validate:"required"`
	CalculatedValue int    `json:"calculated_value"`
	AdjustedValue   int    `json:"adjusted_value"`
	Assignable      *bool  `json:"assignable"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
}

// AppEntityItem -
type AppEntityItem struct {
	ID          string `json:"id"`
	AppEntityID string `json:"app_entity_id"`
	AppItemID   string `json:"app_item_id" validate:"required"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

// AppEntitySkill -
type AppEntitySkill struct {
	ID          string `json:"id"`
	AppEntityID string `json:"app_entity_id"`
	AppSkillID  string `json:"app_skill_id" validate:"required"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

// AppEntityTactic -
type AppEntityTactic struct {
	ID             string `json:"id"`
	AppEntityID    string `json:"app_entity_id"`
	AppAttributeID string `json:"app_attribute_id" validate:"required"`
	Target         string `json:"target" validate:"required"`
	Comparator     string `json:"comparator" validate:"required"`
	Value          int    `json:"value"`
	Action         string `json:"action" validate:"required"`
	AppSkillID     string `json:"app_skill_id"`
	AppItemID      string `json:"app_item_id"`
	Order          int    `json:"order"`
	Status         string `json:"status"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
}

// AppFight -
type AppFight struct {
	ID                   string                 `json:"id"`
	AppID                string                 `json:"app_id"`
	Status               string                 `json:"status"`
	AppFightEntityGroups []*AppFightEntityGroup `json:"fight_entity_groups,omitempty"`
	CreatedAt            string                 `json:"created_at"`
	UpdatedAt            string                 `json:"updated_at"`
}

// AppFightInstance -
type AppFightInstance struct {
	ID         string `json:"id"`
	AppFightID string `json:"app_fight_id"`
	Status     string `json:"status"`
	Turn       int    `json:"turn"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

// AppFightInstanceTurn -
type AppFightInstanceTurn struct {
	ID                 string `json:"id"`
	AppFightID         string `json:"app_fight_id"`
	AppFightInstanceID string `json:"app_fight_instance_id"`
	Turn               int    `json:"turn"`

	// NOTE: using FightData struct here directly instead of
	// replicating which would allow us to customise for API output.
	// Until that is required this is fine as FightData isn't used for
	// anything else.
	Data      *instance.FightData `json:"data"`
	CreatedAt string              `json:"created_at"`
	UpdatedAt string              `json:"updated_at"`
}

// AppFightEntityGroup -
type AppFightEntityGroup struct {
	ID               string `json:"id"`
	AppFightID       string `json:"app_fight_id"`
	AppEntityGroupID string `json:"app_entity_group_id" validate:"required"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
}

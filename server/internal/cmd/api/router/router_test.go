package router

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

func TestRoutes(t *testing.T) {

	// set test
	os.Setenv("APP_ENV", "TEST")

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, _ := env.NewEnv()

		// logger
		l, _ := logger.NewLogger(e)

		// database
		db, _ := database.NewDatabase(e, l)

		// router
		r, err := NewRouter(e, l, db)
		assert.Nil(t, err, "Routes initialised without error")
		assert.NotNil(t, r, "Routes is not nil")
	}
}

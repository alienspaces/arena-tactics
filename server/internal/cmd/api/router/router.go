// Package router maps HTTP methods to handler functions
package router

import (
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appeffectattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentityattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentitygroupmember"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentityitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentityskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appentitytactic"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appfightentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appfightinstance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appfightinstanceturn"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appitemeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/appskilleffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/doc"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler/template"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/middleware"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// handlermapContextKey
var handlermapContextKey = "handlermap"

// Router -
type Router struct {
	Env      env.Env
	Logger   zerolog.Logger
	Database *sqlx.DB
	handler  http.Handler
	basePath string
}

// NewRouter -
func NewRouter(e env.Env, l zerolog.Logger, d *sqlx.DB) (http.Handler, error) {

	r := Router{
		Env:      e,
		Logger:   l,
		Database: d,
	}
	err := r.init()

	return r.handler, err
}

func (rt *Router) init() error {

	// logger
	l := rt.Logger.With().Str("function", "init").Logger()

	l.Info().Msgf("Initializing routes")

	m := mux.NewRouter()

	mw := middleware.NewMiddleware(rt.Env, rt.Logger, rt.Database)

	// template
	th, err := template.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new template handler >%v<", err)
		return err
	}

	m.Handle(th.GetCollectionPath(), mw.Apply(th, th.Post)).Methods(http.MethodPost)
	m.Handle(th.GetCollectionPath(), mw.Apply(th, th.GetCollection)).Methods(http.MethodGet)
	m.Handle(th.GetPath(), mw.Apply(th, th.Get)).Methods(http.MethodGet)
	m.Handle(th.GetPath(), mw.Apply(th, th.Put)).Methods(http.MethodPut)
	m.Handle(th.GetPath(), mw.Apply(th, th.Delete)).Methods(http.MethodDelete)

	m.Handle(th.GetCollectionPath(), mw.Apply(th, th.Options)).Methods(http.MethodOptions)
	m.Handle(th.GetPath(), mw.Apply(th, th.Options)).Methods(http.MethodOptions)

	// app
	ah, err := app.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app handler >%v<", err)
		return err
	}

	m.Handle(ah.GetCollectionPath(), mw.Apply(ah, ah.Post)).Methods(http.MethodPost)
	m.Handle(ah.GetCollectionPath(), mw.Apply(ah, ah.GetCollection)).Methods(http.MethodGet)
	m.Handle(ah.GetPath(), mw.Apply(ah, ah.Get)).Methods(http.MethodGet)
	m.Handle(ah.GetPath(), mw.Apply(ah, ah.Put)).Methods(http.MethodPut)
	m.Handle(ah.GetPath(), mw.Apply(ah, ah.Delete)).Methods(http.MethodDelete)

	m.Handle(ah.GetCollectionPath(), mw.Apply(ah, ah.Options)).Methods(http.MethodOptions)
	m.Handle(ah.GetPath(), mw.Apply(ah, ah.Options)).Methods(http.MethodOptions)

	// app attribute
	aah, err := appattribute.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app attribute handler >%v<", err)
		return err
	}

	m.Handle(aah.GetCollectionPath(), mw.Apply(aah, aah.Post)).Methods(http.MethodPost)
	m.Handle(aah.GetCollectionPath(), mw.Apply(aah, aah.GetCollection)).Methods(http.MethodGet)
	m.Handle(aah.GetPath(), mw.Apply(aah, aah.Get)).Methods(http.MethodGet)
	m.Handle(aah.GetPath(), mw.Apply(aah, aah.Put)).Methods(http.MethodPut)
	m.Handle(aah.GetPath(), mw.Apply(aah, aah.Delete)).Methods(http.MethodDelete)

	m.Handle(aah.GetCollectionPath(), mw.Apply(aah, aah.Options)).Methods(http.MethodOptions)
	m.Handle(aah.GetPath(), mw.Apply(aah, aah.Options)).Methods(http.MethodOptions)

	// app effect
	aeffh, err := appeffect.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app effect handler >%v<", err)
		return err
	}

	m.Handle(aeffh.GetCollectionPath(), mw.Apply(aeffh, aeffh.Post)).Methods(http.MethodPost)
	m.Handle(aeffh.GetCollectionPath(), mw.Apply(aeffh, aeffh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeffh.GetPath(), mw.Apply(aeffh, aeffh.Get)).Methods(http.MethodGet)
	m.Handle(aeffh.GetPath(), mw.Apply(aeffh, aeffh.Put)).Methods(http.MethodPut)
	m.Handle(aeffh.GetPath(), mw.Apply(aeffh, aeffh.Delete)).Methods(http.MethodDelete)

	m.Handle(aeffh.GetCollectionPath(), mw.Apply(aeffh, aeffh.Options)).Methods(http.MethodOptions)
	m.Handle(aeffh.GetPath(), mw.Apply(aeffh, aeffh.Options)).Methods(http.MethodOptions)

	// app effect attribute
	aeffah, err := appeffectattribute.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app effect handler >%v<", err)
		return err
	}

	m.Handle(aeffah.GetCollectionPath(), mw.Apply(aeffah, aeffah.Post)).Methods(http.MethodPost)
	m.Handle(aeffah.GetCollectionPath(), mw.Apply(aeffah, aeffah.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeffah.GetPath(), mw.Apply(aeffah, aeffah.Get)).Methods(http.MethodGet)
	m.Handle(aeffah.GetPath(), mw.Apply(aeffah, aeffah.Put)).Methods(http.MethodPut)
	m.Handle(aeffah.GetPath(), mw.Apply(aeffah, aeffah.Delete)).Methods(http.MethodDelete)

	m.Handle(aeffah.GetCollectionPath(), mw.Apply(aeffah, aeffah.Options)).Methods(http.MethodOptions)
	m.Handle(aeffah.GetPath(), mw.Apply(aeffah, aeffah.Options)).Methods(http.MethodOptions)

	// app item
	aih, err := appitem.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app item handler >%v<", err)
		return err
	}

	m.Handle(aih.GetCollectionPath(), mw.Apply(aih, aih.Post)).Methods(http.MethodPost)
	m.Handle(aih.GetCollectionPath(), mw.Apply(aih, aih.GetCollection)).Methods(http.MethodGet)
	m.Handle(aih.GetPath(), mw.Apply(aih, aih.Get)).Methods(http.MethodGet)
	m.Handle(aih.GetPath(), mw.Apply(aih, aih.Put)).Methods(http.MethodPut)
	m.Handle(aih.GetPath(), mw.Apply(aih, aih.Delete)).Methods(http.MethodDelete)

	m.Handle(aih.GetCollectionPath(), mw.Apply(aih, aih.Options)).Methods(http.MethodOptions)
	m.Handle(aih.GetPath(), mw.Apply(aih, aih.Options)).Methods(http.MethodOptions)

	// app item effect
	aieh, err := appitemeffect.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app item effect handler >%v<", err)
		return err
	}

	m.Handle(aieh.GetCollectionPath(), mw.Apply(aieh, aieh.Post)).Methods(http.MethodPost)
	m.Handle(aieh.GetCollectionPath(), mw.Apply(aieh, aieh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aieh.GetPath(), mw.Apply(aieh, aieh.Get)).Methods(http.MethodGet)
	m.Handle(aieh.GetPath(), mw.Apply(aieh, aieh.Put)).Methods(http.MethodPut)
	m.Handle(aieh.GetPath(), mw.Apply(aieh, aieh.Delete)).Methods(http.MethodDelete)

	m.Handle(aieh.GetCollectionPath(), mw.Apply(aieh, aieh.Options)).Methods(http.MethodOptions)
	m.Handle(aieh.GetPath(), mw.Apply(aieh, aieh.Options)).Methods(http.MethodOptions)

	// app skill
	ash, err := appskill.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app skill handler >%v<", err)
		return err
	}

	m.Handle(ash.GetCollectionPath(), mw.Apply(ash, ash.Post)).Methods(http.MethodPost)
	m.Handle(ash.GetCollectionPath(), mw.Apply(ash, ash.GetCollection)).Methods(http.MethodGet)
	m.Handle(ash.GetPath(), mw.Apply(ash, ash.Get)).Methods(http.MethodGet)
	m.Handle(ash.GetPath(), mw.Apply(ash, ash.Put)).Methods(http.MethodPut)
	m.Handle(ash.GetPath(), mw.Apply(ash, ash.Delete)).Methods(http.MethodDelete)

	m.Handle(ash.GetCollectionPath(), mw.Apply(ash, ash.Options)).Methods(http.MethodOptions)
	m.Handle(ash.GetPath(), mw.Apply(ash, ash.Options)).Methods(http.MethodOptions)

	// app skill effect
	aseh, err := appskilleffect.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app skill effect handler >%v<", err)
		return err
	}

	m.Handle(aseh.GetCollectionPath(), mw.Apply(aseh, aseh.Post)).Methods(http.MethodPost)
	m.Handle(aseh.GetCollectionPath(), mw.Apply(aseh, aseh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aseh.GetPath(), mw.Apply(aseh, aseh.Get)).Methods(http.MethodGet)
	m.Handle(aseh.GetPath(), mw.Apply(aseh, aseh.Put)).Methods(http.MethodPut)
	m.Handle(aseh.GetPath(), mw.Apply(aseh, aseh.Delete)).Methods(http.MethodDelete)

	m.Handle(aseh.GetCollectionPath(), mw.Apply(aseh, aseh.Options)).Methods(http.MethodOptions)
	m.Handle(aseh.GetPath(), mw.Apply(aseh, aseh.Options)).Methods(http.MethodOptions)

	// app fight
	afh, err := appfight.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app fight handler >%v<", err)
		return err
	}

	m.Handle(afh.GetCollectionPath(), mw.Apply(afh, afh.Post)).Methods(http.MethodPost)
	m.Handle(afh.GetCollectionPath(), mw.Apply(afh, afh.GetCollection)).Methods(http.MethodGet)
	m.Handle(afh.GetPath(), mw.Apply(afh, afh.Get)).Methods(http.MethodGet)
	m.Handle(afh.GetPath(), mw.Apply(afh, afh.Put)).Methods(http.MethodPut)
	m.Handle(afh.GetPath(), mw.Apply(afh, afh.Delete)).Methods(http.MethodDelete)

	m.Handle(afh.GetCollectionPath(), mw.Apply(afh, afh.Options)).Methods(http.MethodOptions)
	m.Handle(afh.GetPath(), mw.Apply(afh, afh.Options)).Methods(http.MethodOptions)

	// app fight entity group
	afegh, err := appfightentitygroup.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app fight entity group handler >%v<", err)
		return err
	}

	m.Handle(afegh.GetCollectionPath(), mw.Apply(afegh, afegh.Post)).Methods(http.MethodPost)
	m.Handle(afegh.GetCollectionPath(), mw.Apply(afegh, afegh.GetCollection)).Methods(http.MethodGet)
	m.Handle(afegh.GetPath(), mw.Apply(afegh, afegh.Get)).Methods(http.MethodGet)
	m.Handle(afegh.GetPath(), mw.Apply(afegh, afegh.Put)).Methods(http.MethodPut)
	m.Handle(afegh.GetPath(), mw.Apply(afegh, afegh.Delete)).Methods(http.MethodDelete)

	m.Handle(afegh.GetCollectionPath(), mw.Apply(afegh, afegh.Options)).Methods(http.MethodOptions)
	m.Handle(afegh.GetPath(), mw.Apply(afegh, afegh.Options)).Methods(http.MethodOptions)

	// app fight instance
	afih, err := appfightinstance.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app fight instance handler >%v<", err)
		return err
	}

	m.Handle(afih.GetCollectionPath(), mw.Apply(afih, afih.GetCollection)).Methods(http.MethodGet)
	m.Handle(afih.GetPath(), mw.Apply(afih, afih.Get)).Methods(http.MethodGet)

	m.Handle(afih.GetCollectionPath(), mw.Apply(afih, afih.Options)).Methods(http.MethodOptions)
	m.Handle(afih.GetPath(), mw.Apply(afih, afih.Options)).Methods(http.MethodOptions)

	// app fight instance turn
	afith, err := appfightinstanceturn.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app fight instance turn handler >%v<", err)
		return err
	}

	m.Handle(afith.GetCollectionPath(), mw.Apply(afith, afith.GetCollection)).Methods(http.MethodGet)
	m.Handle(afith.GetPath(), mw.Apply(afith, afith.Get)).Methods(http.MethodGet)

	m.Handle(afith.GetCollectionPath(), mw.Apply(afith, afith.Options)).Methods(http.MethodOptions)
	m.Handle(afith.GetPath(), mw.Apply(afith, afith.Options)).Methods(http.MethodOptions)

	// app entity
	aeh, err := appentity.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity handler >%v<", err)
		return err
	}

	m.Handle(aeh.GetCollectionPath(), mw.Apply(aeh, aeh.Post)).Methods(http.MethodPost)
	m.Handle(aeh.GetCollectionPath(), mw.Apply(aeh, aeh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeh.GetPath(), mw.Apply(aeh, aeh.Get)).Methods(http.MethodGet)
	m.Handle(aeh.GetPath(), mw.Apply(aeh, aeh.Put)).Methods(http.MethodPut)
	m.Handle(aeh.GetPath(), mw.Apply(aeh, aeh.Delete)).Methods(http.MethodDelete)

	m.Handle(aeh.GetCollectionPath(), mw.Apply(aeh, aeh.Options)).Methods(http.MethodOptions)
	m.Handle(aeh.GetPath(), mw.Apply(aeh, aeh.Options)).Methods(http.MethodOptions)

	// app entity skill
	aesh, err := appentityskill.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity skill handler >%v<", err)
		return err
	}

	m.Handle(aesh.GetCollectionPath(), mw.Apply(aesh, aesh.Post)).Methods(http.MethodPost)
	m.Handle(aesh.GetCollectionPath(), mw.Apply(aesh, aesh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aesh.GetPath(), mw.Apply(aesh, aesh.Get)).Methods(http.MethodGet)
	m.Handle(aesh.GetPath(), mw.Apply(aesh, aesh.Put)).Methods(http.MethodPut)
	m.Handle(aesh.GetPath(), mw.Apply(aesh, aesh.Delete)).Methods(http.MethodDelete)

	m.Handle(aesh.GetCollectionPath(), mw.Apply(aesh, aesh.Options)).Methods(http.MethodOptions)
	m.Handle(aesh.GetPath(), mw.Apply(aesh, aesh.Options)).Methods(http.MethodOptions)

	// app entity item
	aeih, err := appentityitem.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity skill handler >%v<", err)
		return err
	}

	m.Handle(aeih.GetCollectionPath(), mw.Apply(aeih, aeih.Post)).Methods(http.MethodPost)
	m.Handle(aeih.GetCollectionPath(), mw.Apply(aeih, aeih.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeih.GetPath(), mw.Apply(aeih, aeih.Get)).Methods(http.MethodGet)
	m.Handle(aeih.GetPath(), mw.Apply(aeih, aeih.Put)).Methods(http.MethodPut)
	m.Handle(aeih.GetPath(), mw.Apply(aeih, aeih.Delete)).Methods(http.MethodDelete)

	m.Handle(aeih.GetCollectionPath(), mw.Apply(aeih, aeih.Options)).Methods(http.MethodOptions)
	m.Handle(aeih.GetPath(), mw.Apply(aeih, aeih.Options)).Methods(http.MethodOptions)

	// app entity attribute
	aeah, err := appentityattribute.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity skill handler >%v<", err)
		return err
	}

	m.Handle(aeah.GetCollectionPath(), mw.Apply(aeah, aeah.Post)).Methods(http.MethodPost)
	m.Handle(aeah.GetCollectionPath(), mw.Apply(aeah, aeah.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeah.GetPath(), mw.Apply(aeah, aeah.Get)).Methods(http.MethodGet)
	m.Handle(aeah.GetPath(), mw.Apply(aeah, aeah.Put)).Methods(http.MethodPut)
	m.Handle(aeah.GetPath(), mw.Apply(aeah, aeah.Delete)).Methods(http.MethodDelete)

	m.Handle(aeah.GetCollectionPath(), mw.Apply(aeah, aeah.Options)).Methods(http.MethodOptions)
	m.Handle(aeah.GetPath(), mw.Apply(aeah, aeah.Options)).Methods(http.MethodOptions)

	// app entity tactic
	aeth, err := appentitytactic.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity skill handler >%v<", err)
		return err
	}

	m.Handle(aeth.GetCollectionPath(), mw.Apply(aeth, aeth.Post)).Methods(http.MethodPost)
	m.Handle(aeth.GetCollectionPath(), mw.Apply(aeth, aeth.GetCollection)).Methods(http.MethodGet)
	m.Handle(aeth.GetPath(), mw.Apply(aeth, aeth.Get)).Methods(http.MethodGet)
	m.Handle(aeth.GetPath(), mw.Apply(aeth, aeth.Put)).Methods(http.MethodPut)
	m.Handle(aeth.GetPath(), mw.Apply(aeth, aeth.Delete)).Methods(http.MethodDelete)

	m.Handle(aeth.GetCollectionPath(), mw.Apply(aeth, aeth.Options)).Methods(http.MethodOptions)
	m.Handle(aeth.GetPath(), mw.Apply(aeth, aeth.Options)).Methods(http.MethodOptions)

	// app entity group
	aegh, err := appentitygroup.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity group handler >%v<", err)
		return err
	}

	m.Handle(aegh.GetCollectionPath(), mw.Apply(aegh, aegh.Post)).Methods(http.MethodPost)
	m.Handle(aegh.GetCollectionPath(), mw.Apply(aegh, aegh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aegh.GetPath(), mw.Apply(aegh, aegh.Get)).Methods(http.MethodGet)
	m.Handle(aegh.GetPath(), mw.Apply(aegh, aegh.Put)).Methods(http.MethodPut)
	m.Handle(aegh.GetPath(), mw.Apply(aegh, aegh.Delete)).Methods(http.MethodDelete)

	m.Handle(aegh.GetCollectionPath(), mw.Apply(aegh, aegh.Options)).Methods(http.MethodOptions)
	m.Handle(aegh.GetPath(), mw.Apply(aegh, aegh.Options)).Methods(http.MethodOptions)

	// app entity group member
	aegmh, err := appentitygroupmember.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new app entity group member handler >%v<", err)
		return err
	}

	m.Handle(aegmh.GetCollectionPath(), mw.Apply(aegmh, aegmh.Post)).Methods(http.MethodPost)
	m.Handle(aegmh.GetCollectionPath(), mw.Apply(aegmh, aegmh.GetCollection)).Methods(http.MethodGet)
	m.Handle(aegmh.GetPath(), mw.Apply(aegmh, aegmh.Get)).Methods(http.MethodGet)
	m.Handle(aegmh.GetPath(), mw.Apply(aegmh, aegmh.Put)).Methods(http.MethodPut)
	m.Handle(aegmh.GetPath(), mw.Apply(aegmh, aegmh.Delete)).Methods(http.MethodDelete)

	m.Handle(aegmh.GetCollectionPath(), mw.Apply(aegmh, aegmh.Options)).Methods(http.MethodOptions)
	m.Handle(aegmh.GetPath(), mw.Apply(aegmh, aegmh.Options)).Methods(http.MethodOptions)

	// doc
	dh, err := doc.NewHandler(rt.Env, rt.Logger, rt.Database)
	if err != nil {
		l.Warn().Msgf("Failed new doc handler >%v<", err)
		return err
	}

	m.PathPrefix("/").Handler(mw.Apply(dh, dh.GetCollection)).Methods(http.MethodGet)

	// request logging
	lr := handlers.LoggingHandler(os.Stdout, m)

	rt.handler = lr

	return nil
}

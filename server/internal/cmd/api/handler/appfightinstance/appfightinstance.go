// Package appfightinstance - REST API handlers
package appfightinstance

import (
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppFightInstance `json:"data"`
	Errors []handler.ErrorData  `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppFightInstance `json:"data"`
	Errors []handler.ErrorData    `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppFightInstance `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appfight"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger hook
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/fights/{fight_id}/instances/{fight_instance_id}",
			CollectionPath:  "/api/apps/{app_id}/fights/{fight_id}/instances",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	rec, _ := m.GetFightInstanceRec(params["fight_instance_id"].(string))
	if rec != nil {

		res := ResponseData{
			Data: api.AppFightInstance{
				ID:         rec.ID,
				AppFightID: rec.AppFightID,
				Status:     rec.Status,
				Turn:       rec.Turn,
				CreatedAt:  rec.CreatedAt,
				UpdatedAt:  rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	qParams := make(map[string]interface{})
	qParams["app_fight_id"] = params["fight_id"].(string)

	recs, _ := m.GetFightInstanceRecs(qParams)
	if recs != nil {

		var ed []api.AppFightInstance

		for _, rec := range recs {

			fight := api.AppFightInstance{
				ID:         rec.ID,
				AppFightID: rec.AppFightID,
				Status:     rec.Status,
				Turn:       rec.Turn,
				CreatedAt:  rec.CreatedAt,
				UpdatedAt:  rec.UpdatedAt.String,
			}

			ed = append(ed, fight)
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

package appfightinstance

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

type TestData struct {
	TestRecordData *testingdata.Data
	FightInstance  *instance.Fight
}

func setup(t *testing.T) (*TestData, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transaction >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	// run fight
	rs, err := repostore.NewRepoStore(e, l, tx)
	if err != nil {
		t.Fatalf("Failed to create fight instance >%v<", err)
	}

	fi, err := instance.NewFight(e, l, rs, td.AppFightRecs[0].ID)
	if err != nil {
		t.Fatalf("Failed to create fight instance >%v<", err)
	}

	tx.Commit()

	// remove record
	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transaction >%v<", err)
		}

		err = td.RemoveAppData(tx)
		if err != nil {
			t.Fatalf("Failed to remove test data >%v<", err)
		}

		tx.Commit()
	}

	return &TestData{
		TestRecordData: td,
		FightInstance:  fi,
	}, teardown
}

func TestGet(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tr := td.TestRecordData
	fi := td.FightInstance

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Get).Methods(http.MethodGet)

	// recorder
	w := httptest.NewRecorder()

	// request
	url := h.GetPath()
	url = strings.Replace(url, "{app_id}", tr.AppRec.ID, 1)
	url = strings.Replace(url, "{fight_id}", tr.AppFightRecs[0].ID, 1)
	url = strings.Replace(url, "{fight_instance_id}", fi.FightInstanceID, 1)

	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Get response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.NotEmpty(t, res.Data.CreatedAt, "Fight CreatedAt is not empty")
		assert.Empty(t, res.Data.UpdatedAt, "Fight UpdatedAt is empty")
	}
}

func TestGetCollection(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tr := td.TestRecordData
	fi := td.FightInstance

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.GetCollection).Methods(http.MethodGet)

	// request
	url := h.GetCollectionPath()
	url = strings.Replace(url, "{app_id}", tr.AppRec.ID, 1)
	url = strings.Replace(url, "{fight_id}", tr.AppFightRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Get collection response status is OK") {

		// test body
		res := CollectionResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.Equal(t, fi.FightInstanceID, res.Data[0].ID, "Fight instance ID equals expected")
	}
}

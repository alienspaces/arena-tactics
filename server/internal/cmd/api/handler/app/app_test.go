package app

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

// test record to remove
var recID string

func setup(t *testing.T) (*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transactiob %v", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data %v", err)
	}

	// app test data
	err = td.AddApp(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data %v", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transaction %v", err)
		}

		// remove app record
		if recID != "" {
			m, _ := app.NewRepo(e, l, tx)
			err = m.Remove(recID)
			if err != nil {
				t.Fatalf("Failed to remove app record %v", err)
			}
		}

		// remove app test data
		td.RemoveApp(tx)

		tx.Commit()
	}

	return td, teardown
}

func TestPost(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.Post).Methods(http.MethodPost)

	ts := httptest.NewServer(m)
	defer ts.Close()

	// data
	er := RequestData{
		Data: api.App{
			Name:              "Test App",
			Description:       "Test App Description",
			InitiativeFormula: "dexterity",
			DeathFormula:      "health < 1",
		},
	}

	erj, _ := json.Marshal(er)

	url := ts.URL + h.GetCollectionPath()

	r, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(erj))

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Create response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.NotEmpty(t, res.Data.ID, "App ID is not nil")
		assert.Equal(t, er.Data.Name, res.Data.Name, "App Name equals expected")
		assert.Equal(t, er.Data.Description, res.Data.Description, "App Description equals expected")
		assert.Equal(t, er.Data.InitiativeFormula, res.Data.InitiativeFormula, "App InitiativeFormula equals expected")
		assert.Equal(t, er.Data.DeathFormula, res.Data.DeathFormula, "App DeathFormula equals expected")
		assert.NotEmpty(t, res.Data.CreatedAt, "App CreatedAt is not nil")

		// remove created
		if res.Data.ID != "" {
			t.Logf("Removing app record ID >%s<", res.Data.ID)
			tx, _ := db.Beginx()
			r, _ := app.NewRepo(e, l, tx)
			err := r.Remove(res.Data.ID)
			assert.NoError(t, err, "Remove does not result in error")
			tx.Commit()
		}
	}
}

func TestGet(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Get).Methods(http.MethodGet)

	ts := httptest.NewServer(m)
	defer ts.Close()

	// request
	url := ts.URL + h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)

	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Get response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.Equal(t, td.AppRec.Name, res.Data.Name, "App Name equals expected")
		assert.Equal(t, td.AppRec.Description, res.Data.Description, "App Description equals expected")
		assert.Equal(t, td.AppRec.InitiativeFormula, res.Data.InitiativeFormula, "App InitiativeFormula equals expected")
		assert.Equal(t, td.AppRec.DeathFormula, res.Data.DeathFormula, "App DeathFormula equals expected")
		assert.NotEmpty(t, res.Data.CreatedAt, "App CreatedAt is not empty")
		assert.Empty(t, res.Data.UpdatedAt, "App UpdatedAt is empty")
	}
}

func TestUpdate(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Put).Methods(http.MethodPut)

	ts := httptest.NewServer(m)
	defer ts.Close()

	// data
	er := RequestData{
		Data: api.App{
			ID:                td.AppRec.ID,
			Name:              "Test App Update",
			Description:       "Test App Description Update",
			InitiativeFormula: "dexterity + 1",
			DeathFormula:      "health < 1",
			Status:            record.AppStatusStopped,
		},
	}

	erj, _ := json.Marshal(er)

	url := ts.URL + h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)

	r, _ := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(erj))

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Update response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.Equal(t, er.Data.Name, res.Data.Name, "App Name equals expected")
		assert.Equal(t, er.Data.Status, res.Data.Status, "App Status equals expected")
		assert.Equal(t, er.Data.Description, res.Data.Description, "App Description equals expected")
		assert.Equal(t, er.Data.InitiativeFormula, res.Data.InitiativeFormula, "App InitiativeFormula equals expected")
		assert.Equal(t, er.Data.DeathFormula, res.Data.DeathFormula, "App DeathFormula equals expected")
		assert.NotEmpty(t, res.Data.CreatedAt, "App CreatedAt is not empty")
		assert.NotEmpty(t, res.Data.UpdatedAt, "App UpdatedAt is not empty")
	}
}

func TestDelete(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Delete).Methods(http.MethodDelete)

	ts := httptest.NewServer(m)
	defer ts.Close()

	url := ts.URL + h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)

	r, _ := http.NewRequest(http.MethodDelete, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	assert.Equal(t, http.StatusOK, w.Code, "Delete response status code is OK")
}

func TestValidate(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.Post).Methods(http.MethodPost)

	ts := httptest.NewServer(m)
	defer ts.Close()

	// data
	er := RequestData{
		Data: api.App{},
	}

	erj, _ := json.Marshal(er)

	url := ts.URL + h.GetCollectionPath()

	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(erj))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	// test status
	assert.Equal(t, resp.StatusCode, http.StatusBadRequest, "Create response status code is Bad Request")

	// test body
	res := ResponseData{}
	json.NewDecoder(resp.Body).Decode(&res)

	errors := res.Errors
	assert.NotNil(t, errors, "Validation errors are not nil")

	for _, err := range errors {
		assert.NotNil(t, err.Type, "Error Title is not nil")
		assert.NotNil(t, err.Detail, "Error Detail is not nil")
	}
}

func TestGetCollection(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	// app handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.GetCollection).Methods(http.MethodGet)

	ts := httptest.NewServer(m)
	defer ts.Close()

	// request
	url := ts.URL + h.GetCollectionPath()
	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	assert.Equal(t, http.StatusOK, w.Code, "Get collection response status is OK")
}

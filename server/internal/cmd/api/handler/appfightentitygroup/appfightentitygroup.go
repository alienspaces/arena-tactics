// Package appfightentitygroup provides handlers for REST methods allowing web clients to manage appfightentitygroups
package appfightentitygroup

import (
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppFightEntityGroup `json:"data"`
	Errors []handler.ErrorData     `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppFightEntityGroup `json:"data"`
	Errors []handler.ErrorData       `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppFightEntityGroup `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appfightentitygroup"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/fights/{fight_id}/entitygroups/{fight_entity_group_id}",
			CollectionPath:  "/api/apps/{app_id}/fights/{fight_id}/entitygroups",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	rec, _ := m.GetFightEntityGroupRec(params["fight_entity_group_id"].(string))
	if rec != nil {
		res := ResponseData{
			Data: api.AppFightEntityGroup{
				ID:               rec.ID,
				AppFightID:       rec.AppFightID,
				AppEntityGroupID: rec.AppEntityGroupID,
				CreatedAt:        rec.CreatedAt,
				UpdatedAt:        rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	recs, _ := m.GetFightEntityGroupRecs(params["fight_id"].(string))
	if recs != nil {

		var ed []api.AppFightEntityGroup

		for _, rec := range recs {
			ed = append(ed, api.AppFightEntityGroup{
				ID:               rec.ID,
				AppFightID:       rec.AppFightID,
				AppEntityGroupID: rec.AppEntityGroupID,
				CreatedAt:        rec.CreatedAt,
				UpdatedAt:        rec.UpdatedAt.String,
			})
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppFightModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppFightEntityGroupRecord{}
	rec.AppFightID = params["fight_id"].(string)
	rec.AppEntityGroupID = data.AppEntityGroupID

	// create
	err = m.CreateFightEntityGroupRec(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create fight entity group >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppFightEntityGroup{
				ID:               rec.ID,
				AppFightID:       rec.AppFightID,
				AppEntityGroupID: rec.AppEntityGroupID,
				CreatedAt:        rec.CreatedAt,
				UpdatedAt:        rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppFightModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppFightEntityGroupRecord{}
	rec.ID = params["fight_entity_group_id"].(string)
	rec.AppFightID = params["fight_id"].(string)
	rec.AppEntityGroupID = data.AppEntityGroupID

	// update
	err = m.UpdateFightEntityGroupRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppFightEntityGroup{
				ID:               rec.ID,
				AppFightID:       rec.AppFightID,
				AppEntityGroupID: rec.AppEntityGroupID,
				CreatedAt:        rec.CreatedAt,
				UpdatedAt:        rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// delete
	err = m.DeleteFightEntityGroupRec(params["fight_entity_group_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppFightEntityGroup{
			ID: params["fight_entity_group_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

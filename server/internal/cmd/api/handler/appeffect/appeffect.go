// Package appeffect provides handlers for REST methods allowing web clients to manage appeffects
package appeffect

import (
	"fmt"
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   *api.AppEffect      `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []*api.AppEffect    `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data *api.AppEffect `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appeffect"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/effects/{effect_id}",
			CollectionPath:  "/api/apps/{app_id}/effects",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// get resource
	effectData, err := h.getEffectData(params["effect_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed get effect data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEffectModel

	// get
	recs, err := m.GetEffectRecs(params)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	var effectData []*api.AppEffect

	if recs != nil {
		for _, rec := range recs {

			// get resource
			data, err := h.getEffectData(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed get resource data >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}
			effectData = append(effectData, data)
		}
	}

	// response
	res := CollectionResponseData{
		Data: effectData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEffectModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEffectRecord{}
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name
	rec.EffectType = data.EffectType
	rec.Recurring = *data.Recurring
	rec.Permanent = *data.Permanent
	rec.Duration = data.Duration

	// create
	err = m.CreateEffectRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get effect data
	effectData, err := h.getEffectData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEffectModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEffectRecord{}
	rec.ID = params["effect_id"].(string)
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name
	rec.EffectType = data.EffectType
	rec.Recurring = *data.Recurring
	rec.Permanent = *data.Permanent
	rec.Duration = data.Duration

	// update
	err = m.UpdateEffectRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get effect data
	effectData, err := h.getEffectData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEffectModel

	// delete
	err = m.DeleteEffectRec(params["effect_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: &api.AppEffect{
			ID: params["effect_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

func (h *Handler) getEffectData(appEffectID string) (*api.AppEffect, error) {

	// logger
	l := h.Logger.With().Str("function", "getEffectData").Logger()

	// models
	m := h.AppEffectModel

	// get
	rec, err := m.GetEffectRec(appEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect record >%v<", err)
		return nil, err
	}

	if rec == nil {
		l.Warn().Msgf("Resource not found")
		return nil, fmt.Errorf("Resource not found")
	}

	data := &api.AppEffect{
		ID:         rec.ID,
		AppID:      rec.AppID,
		Name:       rec.Name,
		EffectType: rec.EffectType,
		Recurring:  &rec.Recurring,
		Permanent:  &rec.Permanent,
		Duration:   rec.Duration,
		CreatedAt:  rec.CreatedAt,
		UpdatedAt:  rec.UpdatedAt.String,
	}

	return data, nil
}

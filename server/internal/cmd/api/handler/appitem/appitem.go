// Package appitem provides handlers for REST methods allowing web clients to manage appitems
package appitem

import (
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppItem         `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppItem       `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppItem `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appitem"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/items/{item_id}",
			CollectionPath:  "/api/apps/{app_id}/items",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppItemModel

	// get
	rec, _ := m.GetItemRec(params["item_id"].(string))
	if rec != nil {
		res := ResponseData{
			Data: api.AppItem{
				ID:           rec.ID,
				AppID:        rec.AppID,
				Name:         rec.Name,
				ItemType:     rec.ItemType,
				ItemLocation: rec.ItemLocation,
				CreatedAt:    rec.CreatedAt,
				UpdatedAt:    rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppItemModel

	// get
	recs, _ := m.GetItemRecs(params)
	if recs != nil {

		var ed []api.AppItem

		for _, rec := range recs {
			ed = append(ed, api.AppItem{
				ID:           rec.ID,
				AppID:        rec.AppID,
				Name:         rec.Name,
				ItemType:     rec.ItemType,
				ItemLocation: rec.ItemLocation,
				CreatedAt:    rec.CreatedAt,
				UpdatedAt:    rec.UpdatedAt.String,
			})
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppItemModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppItemRecord{}
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name
	rec.ItemType = data.ItemType
	rec.ItemLocation = data.ItemLocation

	// create
	err = m.CreateItemRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppItem{
				ID:           rec.ID,
				AppID:        rec.AppID,
				Name:         rec.Name,
				ItemType:     rec.ItemType,
				ItemLocation: rec.ItemLocation,
				CreatedAt:    rec.CreatedAt,
				UpdatedAt:    rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppItemModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppItemRecord{}
	rec.ID = params["item_id"].(string)
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name
	rec.ItemType = data.ItemType
	rec.ItemLocation = data.ItemLocation

	// update
	err = m.UpdateItemRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppItem{
				ID:           rec.ID,
				AppID:        rec.AppID,
				Name:         rec.Name,
				ItemType:     rec.ItemType,
				ItemLocation: rec.ItemLocation,
				CreatedAt:    rec.CreatedAt,
				UpdatedAt:    rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppItemModel

	// delete
	err = m.DeleteItemRec(params["item_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppItem{
			ID: params["item_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

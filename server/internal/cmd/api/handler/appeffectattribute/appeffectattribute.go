// Package appeffectattribute provides handlers for REST methods allowing web clients to manage appentityitems
package appeffectattribute

import (
	"fmt"
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   *api.AppEffectAttribute `json:"data"`
	Errors []handler.ErrorData     `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []*api.AppEffectAttribute `json:"data"`
	Errors []handler.ErrorData       `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data *api.AppEffectAttribute `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentityitem"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/effects/{effect_id}/attributes/{effect_attribute_id}",
			CollectionPath:  "/api/apps/{app_id}/effects/{effect_id}/attributes",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// get resource
	effectAttributeData, err := h.getEffectAttributeData(params["effect_attribute_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed get effect attribute data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectAttributeData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEffectModel

	// get
	recs, err := m.GetEffectAttributeRecs(params["effect_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	var effectAttributeData []*api.AppEffectAttribute

	if recs != nil {
		for _, rec := range recs {

			// get resource
			data, err := h.getEffectAttributeData(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed get resource data >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}
			effectAttributeData = append(effectAttributeData, data)
		}
	}

	// response
	res := CollectionResponseData{
		Data: effectAttributeData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEffectModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEffectAttributeRecord{}
	rec.AppEffectID = params["effect_id"].(string)
	rec.AppAttributeID = data.AppAttributeID
	rec.Target = data.Target
	rec.ApplyConstraint = data.ApplyConstraint
	rec.ValueFormula = data.ValueFormula

	// create
	err = m.CreateEffectAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get effect attribute data
	effectAttributeData, err := h.getEffectAttributeData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectAttributeData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEffectModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEffectAttributeRecord{}
	rec.ID = params["effect_attribute_id"].(string)
	rec.AppEffectID = params["effect_id"].(string)
	rec.AppAttributeID = data.AppAttributeID
	rec.Target = data.Target
	rec.ApplyConstraint = data.ApplyConstraint
	rec.ValueFormula = data.ValueFormula

	// update
	err = m.UpdateEffectAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get effect attribute data
	effectAttributeData, err := h.getEffectAttributeData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: effectAttributeData,
	}

	h.DebugStruct("Put Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	effectAttributeID := params["effect_attribute_id"].(string)

	// models
	m := h.AppEffectModel

	// delete
	err = m.DeleteEffectAttributeRec(effectAttributeID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: &api.AppEffectAttribute{
			ID: effectAttributeID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

// getEffectAttributeData - return single resource Data
func (h *Handler) getEffectAttributeData(effectAttributeID string) (*api.AppEffectAttribute, error) {

	// logger
	l := h.Logger.With().Str("function", "getEffectAttributeData").Logger()

	// models
	m := h.AppEffectModel

	rec, err := m.GetEffectAttributeRec(effectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute record >%v<", err)
		return nil, err
	}

	if rec == nil {
		l.Warn().Msgf("Resource not found")
		return nil, fmt.Errorf("Resource not found")
	}

	data := &api.AppEffectAttribute{
		ID:              rec.ID,
		AppEffectID:     rec.AppEffectID,
		AppAttributeID:  rec.AppAttributeID,
		Target:          rec.Target,
		ApplyConstraint: rec.ApplyConstraint,
		ValueFormula:    rec.ValueFormula,
		CreatedAt:       rec.CreatedAt,
		UpdatedAt:       rec.UpdatedAt.String,
	}

	return data, nil
}

// Package appentitygroup -
package appentitygroup

import (
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppEntityGroup  `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppEntityGroup `json:"data"`
	Errors []handler.ErrorData  `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppEntityGroup `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentitygroup"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Path:            "/api/apps/{app_id}/entitygroups/{entity_group_id}",
			CollectionPath:  "/api/apps/{app_id}/entitygroups",
			Unauthenticated: false,
			Unauthorized:    false,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityGroupModel

	// get
	rec, _ := m.GetEntityGroupRec(params["entity_group_id"].(string))
	if rec != nil {

		// fight
		fightRec, err := m.GetEntityGroupFightRec(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group fight record >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		// group members
		memberRecs, err := m.GetEntityGroupMemberRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group member records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppEntityGroup{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Name:      rec.Name,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if memberRecs != nil {
			members := []*api.AppEntityGroupMember{}

			for _, memberRec := range memberRecs {
				members = append(members, &api.AppEntityGroupMember{
					ID:          memberRec.ID,
					AppEntityID: memberRec.AppEntityID,
					CreatedAt:   memberRec.CreatedAt,
					UpdatedAt:   memberRec.UpdatedAt.String,
				})
			}
			res.Data.AppEntityGroupMembers = members
		}

		if fightRec != nil {
			res.Data.AppFight = &api.AppFight{
				ID:        fightRec.ID,
				AppID:     fightRec.AppID,
				Status:    fightRec.Status,
				CreatedAt: fightRec.CreatedAt,
				UpdatedAt: fightRec.UpdatedAt.String,
			}
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityGroupModel

	// get
	recs, _ := m.GetEntityGroupRecs(params)
	if recs != nil {

		var ed []api.AppEntityGroup

		for _, rec := range recs {

			// fight
			fightRec, err := m.GetEntityGroupFightRec(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed to get entity group fight record >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}

			// group members
			memberRecs, err := m.GetEntityGroupMemberRecs(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed to get entity group member records >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}

			// group
			entityGroup := api.AppEntityGroup{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Name:      rec.Name,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			}

			if memberRecs != nil {
				members := []*api.AppEntityGroupMember{}

				for _, memberRec := range memberRecs {
					members = append(members, &api.AppEntityGroupMember{
						ID:          memberRec.ID,
						AppEntityID: memberRec.AppEntityID,
						CreatedAt:   memberRec.CreatedAt,
						UpdatedAt:   memberRec.UpdatedAt.String,
					})
				}
				entityGroup.AppEntityGroupMembers = members
			}

			if fightRec != nil {
				entityGroup.AppFight = &api.AppFight{
					ID:        fightRec.ID,
					AppID:     fightRec.AppID,
					Status:    fightRec.Status,
					CreatedAt: fightRec.CreatedAt,
					UpdatedAt: fightRec.UpdatedAt.String,
				}
			}

			ed = append(ed, entityGroup)
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityGroupModel

	// record
	// example: rec.RecordID = params["xxx_id"].(string)
	rec := &record.AppEntityGroupRecord{}
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name

	l.Info().Msgf("Record is >%v<", rec)

	// create
	err = m.CreateEntityGroupRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {

		// fight
		fightRec, err := m.GetEntityGroupFightRec(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group fight record >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		// group members
		memberRecs, err := m.GetEntityGroupMemberRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group member records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppEntityGroup{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Name:      rec.Name,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if memberRecs != nil {
			members := []*api.AppEntityGroupMember{}

			for _, memberRec := range memberRecs {
				members = append(members, &api.AppEntityGroupMember{
					ID:          memberRec.ID,
					AppEntityID: memberRec.AppEntityID,
					CreatedAt:   memberRec.CreatedAt,
					UpdatedAt:   memberRec.UpdatedAt.String,
				})
			}
			res.Data.AppEntityGroupMembers = members
		}

		if fightRec != nil {
			res.Data.AppFight = &api.AppFight{
				ID:        fightRec.ID,
				AppID:     fightRec.AppID,
				Status:    fightRec.Status,
				CreatedAt: fightRec.CreatedAt,
				UpdatedAt: fightRec.UpdatedAt.String,
			}
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityGroupModel

	// record
	// example: rec.RecordID = params["xxx_id"].(string)
	rec := &record.AppEntityGroupRecord{}
	rec.ID = params["entity_group_id"].(string)
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name

	// update
	err = m.UpdateEntityGroupRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {

		// fight
		fightRec, err := m.GetEntityGroupFightRec(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group fight record >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		// group members
		memberRecs, err := m.GetEntityGroupMemberRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity group member records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppEntityGroup{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Name:      rec.Name,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if memberRecs != nil {
			members := []*api.AppEntityGroupMember{}

			for _, memberRec := range memberRecs {
				members = append(members, &api.AppEntityGroupMember{
					ID:          memberRec.ID,
					AppEntityID: memberRec.AppEntityID,
					CreatedAt:   memberRec.CreatedAt,
					UpdatedAt:   memberRec.UpdatedAt.String,
				})
			}
			res.Data.AppEntityGroupMembers = members
		}

		if fightRec != nil {
			res.Data.AppFight = &api.AppFight{
				ID:        fightRec.ID,
				AppID:     fightRec.AppID,
				Status:    fightRec.Status,
				CreatedAt: fightRec.CreatedAt,
				UpdatedAt: fightRec.UpdatedAt.String,
			}
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityGroupModel

	// TODO: Add app_id to args
	err = m.DeleteEntityGroupRec(params["entity_group_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppEntityGroup{
			ID: params["entity_group_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

package appentitygroupmember

// appentitygroupmember (used only as example and testing)

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroupmember"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

// func setup(t *testing.T) (*testingdata.Data, func()) {
func setup(t *testing.T) (*testingdata.Data, func()) {

	// add test data
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed new test data >%v<", err)
	}

	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	tx.Commit()

	// remove test data
	teardown := func() {

		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		err = td.RemoveAppData(tx)
		if err != nil {
			t.Fatalf("Failed to remove test data >%v<", err)
		}

		tx.Commit()
	}

	return td, teardown
}

func TestPost(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// application handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.Post).Methods(http.MethodPost)

	// data
	er := RequestData{
		Data: api.AppEntityGroupMember{
			AppEntityGroupID: td.AppEntityGroupRecs[0].ID,
			AppEntityID:      td.AppEntityRecs[0].ID,
		},
	}

	erj, _ := json.Marshal(er)

	// request
	// - example replace nested path parameters
	// url := strings.Replace(h.GetCollectionPath(), "{app_id}", td.AppRec.ID, 1)
	url := h.GetCollectionPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url = strings.Replace(url, "{entity_group_id}", td.AppEntityGroupRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(erj))

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Create response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.NotEmpty(t, res.Data.ID, "ID is not nil")
		assert.NotEmpty(t, res.Data.CreatedAt, "CreatedAt is not nil")

		// remove created
		if res.Data.ID != "" {
			tx, _ := db.Beginx()
			r, _ := appentitygroupmember.NewRepo(e, l, tx)
			err := r.Remove(res.Data.ID)
			assert.NoError(t, err, "Remove does not result in error")
			tx.Commit()
		}
	}
}

func TestGet(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Get).Methods(http.MethodGet)

	// recorder
	w := httptest.NewRecorder()

	// request
	// - example replace nested path parameters
	// url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url := h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url = strings.Replace(url, "{entity_group_id}", td.AppEntityGroupRecs[0].ID, 1)
	url = strings.Replace(url, "{entity_group_member_id}", td.AppEntityGroupMemberRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Get response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.Equal(t, td.AppEntityGroupMemberRecs[0].ID, res.Data.ID, "ID equals expected")
		assert.NotEmpty(t, res.Data.CreatedAt, "CreatedAt is not empty")
		assert.Empty(t, res.Data.UpdatedAt, "UpdatedAt is empty")
	}
}

func TestPut(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Put).Methods(http.MethodPut)

	// data
	er := RequestData{
		Data: api.AppEntityGroupMember{
			AppEntityGroupID: td.AppEntityGroupRecs[0].ID,
			AppEntityID:      td.AppEntityRecs[1].ID,
		},
	}

	erj, _ := json.Marshal(er)

	// request
	// - example replace nested path parameters
	// url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url := h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url = strings.Replace(url, "{entity_group_id}", td.AppEntityGroupRecs[0].ID, 1)
	url = strings.Replace(url, "{entity_group_member_id}", td.AppEntityGroupMemberRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(erj))

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	if assert.Equal(t, http.StatusOK, w.Code, "Update response status code is OK") {

		// test body
		res := ResponseData{}
		json.NewDecoder(w.Body).Decode(&res)

		assert.Equal(t, td.AppEntityGroupMemberRecs[0].ID, res.Data.ID, "ID equals expected")
		assert.Equal(t, er.Data.AppEntityID, res.Data.AppEntityID, "AppEntityID equals expected")
		assert.Equal(t, er.Data.AppEntityGroupID, res.Data.AppEntityGroupID, "AppEntityGroupID equals expected")
		assert.NotEmpty(t, res.Data.CreatedAt, "CreatedAt is not empty")
		assert.NotEmpty(t, res.Data.UpdatedAt, "UpdatedAt is not empty")
	}
}

func TestDelete(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetPath(), h.Delete).Methods(http.MethodDelete)

	// request
	// - example replace nested path parameters
	// url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url := h.GetPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url = strings.Replace(url, "{entity_group_id}", td.AppEntityGroupRecs[0].ID, 1)
	url = strings.Replace(url, "{entity_group_member_id}", td.AppEntityGroupMemberRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodDelete, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	assert.Equal(t, http.StatusOK, w.Code, "Delete response status code is OK")
}

func TestGetCollection(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// router
	m := mux.NewRouter()
	m.HandleFunc(h.GetCollectionPath(), h.GetCollection).Methods(http.MethodGet)

	// request
	// - example replace nested path parameters
	// url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url := h.GetCollectionPath()
	url = strings.Replace(url, "{app_id}", td.AppRec.ID, 1)
	url = strings.Replace(url, "{entity_group_id}", td.AppEntityGroupRecs[0].ID, 1)

	r, _ := http.NewRequest(http.MethodGet, url, nil)

	// recorder
	w := httptest.NewRecorder()

	// serve
	m.ServeHTTP(w, r)

	// test status
	assert.Equal(t, http.StatusOK, w.Code, "Get collection response status is OK")
}

func TestValidate(t *testing.T) {

	// handler
	h, err := NewHandler(e, l, db)
	if assert.NoError(t, err, "NewHandler does not return error") == false {
		t.Fatalf("Failed new handler >%v<", err)
	}

	// data
	rd := RequestData{
		Data: api.AppEntityGroupMember{},
	}

	// validate
	errs := h.Validate(rd)
	if assert.NotEmpty(t, errs, "Validate returns expected errors") {
		for _, err := range errs {
			assert.Error(t, err, "Errors array contains errors")
		}
	}
}

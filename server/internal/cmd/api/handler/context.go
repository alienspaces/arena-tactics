package handler

import (
	"net/http"

	"github.com/jmoiron/sqlx"
)

// ContextData -
type ContextData struct {
	Tx *sqlx.Tx
}

// GetContextData -
func (h *Base) GetContextData(r *http.Request) (*ContextData, error) {

	// logger
	log := h.Logger

	ctx := &ContextData{}

	log.Info().Msgf("Returninq context data %v", ctx)

	return ctx, nil
}

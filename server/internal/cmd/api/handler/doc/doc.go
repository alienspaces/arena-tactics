// Package doc serves content from the ./www/doc directory
package doc

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "doc"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger hook
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Path:            "/",
			CollectionPath:  "/",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	log := h.Logger
	env := h.Env

	ah := env.Get("APP_HOME")
	if ah == "" {
		log.Error().Msgf("APP_HOME is not defined, cannot return content")
		return
	}

	path := r.URL.Path
	path = strings.Trim(path, "/ ")
	if path == "" {
		path = "index.html"
	}

	log.Debug().Msgf("Get %s", path)

	// file name
	fn := ah + "/www/doc/" + path

	log.Debug().Msgf("File name %s", fn)

	// file extension
	fe := filepath.Ext(fn)

	log.Debug().Msgf("File extension %s", fe)

	b, err := ioutil.ReadFile(fn)
	if err != nil {
		log.Error().Msgf("Failed reading filename %s: %v", fn, err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// content type
	ct := "text/plain"
	switch fe {
	case ".html":
		ct = "text/html"
	case ".css":
		ct = "text/css"
	case ".js":
		ct = "text/javascript"
	case ".ico":
		ct = "image/x-icon"
	case ".jpg":
		ct = "image/jpeg"
	case ".jpeg":
		ct = "image/jpeg"
	case ".png":
		ct = "image/png"
	}

	ct = ct + "; charset=utf-8"
	w.Header().Set("Content-Type", ct)
	w.WriteHeader(http.StatusOK)

	fmt.Fprintf(w, "%s", b)
}

// Package appentitytactic provides handlers for REST methods allowing web clients to manage appentitytactics
package appentitytactic

import (
	"database/sql"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"
	"gitlab.com/alienspaces/arena-tactics/server/internal/fault"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppEntityTactic `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppEntityTactic `json:"data"`
	Errors []handler.ErrorData   `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppEntityTactic `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentitytactic"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/entities/{entity_id}/tactics/{entity_tactic_id}",
			CollectionPath:  "/api/apps/{app_id}/entities/{entity_id}/tactics",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	rec, _ := m.GetEntityTacticRec(params["entity_tactic_id"].(string))
	if rec != nil {
		res := ResponseData{
			Data: api.AppEntityTactic{
				ID:             rec.ID,
				AppEntityID:    rec.AppEntityID,
				AppAttributeID: rec.AppAttributeID,
				Target:         rec.Target,
				Comparator:     rec.Comparator,
				Value:          rec.Value,
				Action:         rec.Action,
				AppSkillID:     rec.AppSkillID.String,
				AppItemID:      rec.AppItemID.String,
				Status:         rec.Status,
				Order:          rec.Order,
				CreatedAt:      rec.CreatedAt,
				UpdatedAt:      rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	// NOTE: returns tactics already ordered
	recs, _ := m.GetEntityTacticRecs(params["entity_id"].(string))
	if recs != nil {

		var ed []api.AppEntityTactic

		for _, rec := range recs {
			ed = append(ed, api.AppEntityTactic{
				ID:             rec.ID,
				AppEntityID:    rec.AppEntityID,
				AppAttributeID: rec.AppAttributeID,
				Target:         rec.Target,
				Comparator:     rec.Comparator,
				Value:          rec.Value,
				Action:         rec.Action,
				AppSkillID:     rec.AppSkillID.String,
				AppItemID:      rec.AppItemID.String,
				Status:         rec.Status,
				Order:          rec.Order,
				CreatedAt:      rec.CreatedAt,
				UpdatedAt:      rec.UpdatedAt.String,
			})
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEntityTacticRecord{}
	rec.AppEntityID = params["entity_id"].(string)
	rec.AppAttributeID = data.AppAttributeID
	rec.Target = data.Target
	rec.Comparator = data.Comparator
	rec.Value = data.Value
	rec.Action = data.Action
	rec.Order = data.Order

	if data.AppSkillID != "" {
		rec.AppSkillID = sql.NullString{
			String: data.AppSkillID,
			Valid:  true,
		}
	}
	if data.AppItemID != "" {
		rec.AppItemID = sql.NullString{
			String: data.AppItemID,
			Valid:  true,
		}
	}

	// create
	err = m.CreateEntityTacticRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppEntityTactic{
				ID:             rec.ID,
				AppEntityID:    rec.AppEntityID,
				AppAttributeID: rec.AppAttributeID,
				Target:         rec.Target,
				Comparator:     rec.Comparator,
				Value:          rec.Value,
				Action:         rec.Action,
				AppSkillID:     rec.AppSkillID.String,
				AppItemID:      rec.AppItemID.String,
				Status:         rec.Status,
				CreatedAt:      rec.CreatedAt,
				UpdatedAt:      rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	/// get
	rec, err := m.GetEntityTacticRec(params["entity_tactic_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	if rec == nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, []error{fault.NewNotFoundError(params["entity_group_id"].(string))})
		return
	}

	rec.AppEntityID = params["entity_id"].(string)
	rec.AppAttributeID = data.AppAttributeID
	rec.Target = data.Target
	rec.Comparator = data.Comparator
	rec.Value = data.Value
	rec.Action = data.Action
	rec.Order = data.Order

	if data.AppSkillID != "" {
		rec.AppSkillID = sql.NullString{
			String: data.AppSkillID,
			Valid:  true,
		}
	}
	if data.AppItemID != "" {
		rec.AppItemID = sql.NullString{
			String: data.AppItemID,
			Valid:  true,
		}
	}

	// update
	err = m.UpdateEntityTacticRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppEntityTactic{
				ID:             rec.ID,
				AppEntityID:    rec.AppEntityID,
				AppAttributeID: rec.AppAttributeID,
				Target:         rec.Target,
				Comparator:     rec.Comparator,
				Value:          rec.Value,
				Action:         rec.Action,
				AppSkillID:     rec.AppSkillID.String,
				AppItemID:      rec.AppItemID.String,
				Status:         rec.Status,
				CreatedAt:      rec.CreatedAt,
				UpdatedAt:      rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	appentitytacticID := params["entity_tactic_id"].(string)

	// models
	m := h.AppEntityModel

	// delete
	err = m.DeleteEntityTacticRec(appentitytacticID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppEntityTactic{
			ID: appentitytacticID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

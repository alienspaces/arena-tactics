// Package appfight provides handlers for REST methods allowing web clients to manage
package appfight

import (
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppFight        `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppFight      `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppFight `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appfight"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger hook
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/fights/{fight_id}",
			CollectionPath:  "/api/apps/{app_id}/fights",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	rec, _ := m.GetFightRec(params["fight_id"].(string))
	if rec != nil {

		// fight groups
		entityGroupRecs, err := m.GetFightEntityGroupRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get fight group records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppFight{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Status:    rec.Status,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if entityGroupRecs != nil {

			groups := []*api.AppFightEntityGroup{}

			for _, groupRec := range entityGroupRecs {
				groups = append(groups, &api.AppFightEntityGroup{
					ID:               groupRec.ID,
					AppFightID:       groupRec.AppFightID,
					AppEntityGroupID: groupRec.AppEntityGroupID,
					CreatedAt:        groupRec.CreatedAt,
					UpdatedAt:        groupRec.UpdatedAt.String,
				})
			}
			res.Data.AppFightEntityGroups = groups
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// get
	recs, _ := m.GetFightRecs(params)
	if recs != nil {

		var ed []api.AppFight

		for _, rec := range recs {

			// fight groups
			entityGroupRecs, err := m.GetFightEntityGroupRecs(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed to get fight group records >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}

			fight := api.AppFight{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Status:    rec.Status,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			}

			if entityGroupRecs != nil {

				groups := []*api.AppFightEntityGroup{}

				for _, groupRec := range entityGroupRecs {
					groups = append(groups, &api.AppFightEntityGroup{
						ID:               groupRec.ID,
						AppFightID:       groupRec.AppFightID,
						AppEntityGroupID: groupRec.AppEntityGroupID,
						CreatedAt:        groupRec.CreatedAt,
						UpdatedAt:        groupRec.UpdatedAt.String,
					})
				}
				fight.AppFightEntityGroups = groups
			}

			ed = append(ed, fight)
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppFightModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppFightRecord{}
	rec.AppID = params["app_id"].(string)

	// create
	err = m.CreateFightRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {

		// fight groups
		entityGroupRecs, err := m.GetFightEntityGroupRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get fight group records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppFight{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Status:    rec.Status,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if entityGroupRecs != nil {

			groups := []*api.AppFightEntityGroup{}

			for _, groupRec := range entityGroupRecs {
				groups = append(groups, &api.AppFightEntityGroup{
					ID:               groupRec.ID,
					AppFightID:       groupRec.AppFightID,
					AppEntityGroupID: groupRec.AppEntityGroupID,
					CreatedAt:        groupRec.CreatedAt,
					UpdatedAt:        groupRec.UpdatedAt.String,
				})
			}
			res.Data.AppFightEntityGroups = groups
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppFightModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec, err := m.GetFightRec(params["fight_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if data.Status != "" {
		rec.Status = data.Status
	}

	// update
	err = m.UpdateFightRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {

		// fight groups
		entityGroupRecs, err := m.GetFightEntityGroupRecs(rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to get fight group records >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}

		res := ResponseData{
			Data: api.AppFight{
				ID:        rec.ID,
				AppID:     rec.AppID,
				Status:    rec.Status,
				CreatedAt: rec.CreatedAt,
				UpdatedAt: rec.UpdatedAt.String,
			},
		}

		if entityGroupRecs != nil {

			groups := []*api.AppFightEntityGroup{}

			for _, groupRec := range entityGroupRecs {
				groups = append(groups, &api.AppFightEntityGroup{
					ID:               groupRec.ID,
					AppFightID:       groupRec.AppFightID,
					AppEntityGroupID: groupRec.AppEntityGroupID,
					CreatedAt:        groupRec.CreatedAt,
					UpdatedAt:        groupRec.UpdatedAt.String,
				})
			}
			res.Data.AppFightEntityGroups = groups
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppFightModel

	// delete
	err = m.DeleteFightRec(params["fight_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppFight{
			ID: params["fight_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

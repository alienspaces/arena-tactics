package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

type TestRequest struct{}

func TestHandler(t *testing.T) {

	// set test
	os.Setenv("APP_ENV", "TEST")

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, _ := env.NewEnv()

		// logger
		l, _ := logger.NewLogger(e)

		h := Base{
			Name:            "Test Resource",
			Path:            "/testresources/{id}",
			CollectionPath:  "/testresources",
			Unauthenticated: false,
			Unauthorized:    false,
			Versioned:       true,
			Env:             e,
			Logger:          l,
		}

		assert.NotNil(t, h, "Base is not nil")
		assert.Equal(t, h.Name, h.GetName(), "GetName returns expected")
		assert.Equal(t, h.Path, h.GetPath(), "GetPath returns expected")
		assert.Equal(t, h.CollectionPath, h.GetCollectionPath(), "GetCollectionPath returns expected")

		// router
		m := mux.NewRouter()

		m.HandleFunc(h.GetCollectionPath(), h.Post).Methods(http.MethodPost)
		m.HandleFunc(h.GetPath(), h.Get).Methods(http.MethodGet)
		m.HandleFunc(h.GetPath(), h.Put).Methods(http.MethodPut)
		m.HandleFunc(h.GetPath(), h.Delete).Methods(http.MethodDelete)

		m.HandleFunc(h.GetCollectionPath(), h.GetCollection).Methods(http.MethodGet)
		m.HandleFunc(h.GetCollectionPath(), h.PutCollection).Methods(http.MethodPut)
		m.HandleFunc(h.GetCollectionPath(), h.DeleteCollection).Methods(http.MethodDelete)

		ts := httptest.NewServer(m)
		defer ts.Close()

		d := TestRequest{}

		j, _ := json.Marshal(d)

		url := ts.URL + h.GetPath()

		testGet(t, url, j)
		testPut(t, url, j)
		testDelete(t, url, j)

		url = ts.URL + h.GetCollectionPath()

		testPost(t, url, j)
		testGetCollection(t, url, j)
		testPutCollection(t, url, j)
		testDeleteCollection(t, url, j)
	}
}

func testPost(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Create response status code is NotFound")
}

func testGet(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodGet, url+"/someid", bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

func testPut(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodPut, url+"/someid", bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

func testDelete(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodDelete, url+"/someid", bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

func testPostCollection(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Create response status code is NotFound")
}

func testGetCollection(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

func testPutCollection(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

func testDeleteCollection(t *testing.T, url string, j []byte) {

	req, _ := http.NewRequest(http.MethodDelete, url, bytes.NewBuffer(j))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusNotFound, resp.StatusCode, "Get response status code is NotFound")
}

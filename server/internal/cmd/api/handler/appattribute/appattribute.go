// Package appattribute provides handlers for REST methods allowing web clients to manage appattributes
package appattribute

import (
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/fault"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppAttribute    `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppAttribute  `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppAttribute `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appattribute"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/attributes/{attribute_id}",
			CollectionPath:  "/api/apps/{app_id}/attributes",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// param vars
	appID := params["app_id"].(string)
	attributeID := params["attribute_id"].(string)

	// models
	m := h.AppAttributeModel

	// get
	rec, _ := m.GetAttributeRec(attributeID)
	if rec != nil {
		res := ResponseData{
			Data: api.AppAttribute{
				ID:              rec.ID,
				Name:            rec.Name,
				ValueFormula:    rec.ValueFormula,
				MaxValueFormula: rec.MaxValueFormula,
				MinValueFormula: rec.MinValueFormula,
				Assignable:      &rec.Assignable,
				CreatedAt:       rec.CreatedAt,
				UpdatedAt:       rec.UpdatedAt.String,
			},
		}

		// attribute validation errors
		resErr, err := h.getAttributeErrors(appID)
		if err != nil {
			l.Warn().Msgf("Failed getting attribute errors >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}
		if resErr != nil {
			res.Errors = resErr
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// param vars
	appID := params["app_id"].(string)

	// models
	m := h.AppAttributeModel

	// get
	recs, _ := m.GetAttributeRecs(params)
	if recs != nil {

		var ed []api.AppAttribute

		for _, rec := range recs {
			ed = append(ed, api.AppAttribute{
				ID:              rec.ID,
				Name:            rec.Name,
				ValueFormula:    rec.ValueFormula,
				MaxValueFormula: rec.MaxValueFormula,
				MinValueFormula: rec.MinValueFormula,
				Assignable:      &rec.Assignable,
				CreatedAt:       rec.CreatedAt,
				UpdatedAt:       rec.UpdatedAt.String,
			})
		}

		res := CollectionResponseData{
			Data: ed,
		}

		// attribute validation errors
		resErr, err := h.getAttributeErrors(appID)
		if err != nil {
			l.Warn().Msgf("Failed getting attribute errors >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}
		if resErr != nil {
			res.Errors = resErr
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	// param vars
	appID := params["app_id"].(string)

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppAttributeModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppAttributeRecord{}
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name
	rec.ValueFormula = data.ValueFormula
	rec.MaxValueFormula = data.MaxValueFormula
	rec.MinValueFormula = data.MinValueFormula
	rec.Assignable = *data.Assignable

	// create
	err = m.CreateAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppAttribute{
				ID:              rec.ID,
				Name:            rec.Name,
				ValueFormula:    rec.ValueFormula,
				MaxValueFormula: rec.MaxValueFormula,
				MinValueFormula: rec.MinValueFormula,
				Assignable:      &rec.Assignable,
				CreatedAt:       rec.CreatedAt,
				UpdatedAt:       rec.UpdatedAt.String,
			},
		}

		// attribute validation errors
		resErr, err := h.getAttributeErrors(appID)
		if err != nil {
			l.Warn().Msgf("Failed getting attribute errors >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}
		if resErr != nil {
			res.Errors = resErr
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	// param vars
	appID := params["app_id"].(string)
	attributeID := params["attribute_id"].(string)

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppAttributeModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppAttributeRecord{}
	rec.ID = attributeID
	rec.AppID = appID
	rec.Name = data.Name
	rec.ValueFormula = data.ValueFormula
	rec.MaxValueFormula = data.MaxValueFormula
	rec.MinValueFormula = data.MinValueFormula
	rec.Assignable = *data.Assignable

	// update
	err = m.UpdateAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppAttribute{
				ID:              rec.ID,
				Name:            rec.Name,
				ValueFormula:    rec.ValueFormula,
				MaxValueFormula: rec.MaxValueFormula,
				MinValueFormula: rec.MinValueFormula,
				Assignable:      &rec.Assignable,
				CreatedAt:       rec.CreatedAt,
				UpdatedAt:       rec.UpdatedAt.String,
			},
		}

		// attribute validation errors
		resErr, err := h.getAttributeErrors(appID)
		if err != nil {
			l.Warn().Msgf("Failed getting attribute errors >%v<", err)
			h.SendSystemErrorResponse(w, ctx, err)
			return
		}
		if resErr != nil {
			res.Errors = resErr
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	attributeID := params["attribute_id"].(string)

	// models
	m := h.AppAttributeModel

	// delete
	err = m.DeleteAttributeRec(attributeID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppAttribute{
			ID: attributeID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

// getAttributeErrors - return any attribute ordering or formula errors
func (h *Handler) getAttributeErrors(appID string) (errData []handler.ErrorData, err error) {

	// logger
	l := h.Logger.With().Str("function", "getAttributeErrors").Logger()

	l.Info().Msgf("Gatting attribute errors for appID >%s<", appID)

	// models
	m := h.AppAttributeModel

	err = m.ValidateAttributes(appID)
	if err != nil {
		errData = append(errData, handler.ErrorData{
			Type:   fault.FaultTypeValidation,
			Detail: err.Error(),
		})
	}

	return errData, nil
}

// Package appentity -
package appentity

import (
	"fmt"
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   *api.AppEntity      `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []*api.AppEntity    `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data *api.AppEntity `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentity"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Path:            "/api/apps/{app_id}/entities/{entity_id}",
			CollectionPath:  "/api/apps/{app_id}/entities",
			Unauthenticated: false,
			Unauthorized:    false,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// get resource
	entityData, err := h.getEntityData(params["entity_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	recs, err := m.GetEntityRecs(params)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	var entityData []*api.AppEntity

	if recs != nil {
		for _, rec := range recs {

			// get resource
			data, err := h.getEntityData(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed get resource data >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}
			entityData = append(entityData, data)
		}
	}

	// response
	res := CollectionResponseData{
		Data: entityData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["xxx_id"].(string)
	rec := &record.AppEntityRecord{}
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name

	l.Info().Msgf("Record is >%v<", rec)

	// create
	err = m.CreateEntityRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get entity data
	entityData, err := h.getEntityData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["xxx_id"].(string)
	rec := &record.AppEntityRecord{}
	rec.ID = params["entity_id"].(string)
	rec.AppID = params["app_id"].(string)
	rec.Name = data.Name

	// update
	err = m.UpdateEntityRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get entity data
	entityData, err := h.getEntityData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	err = m.DeleteEntityRec(params["entity_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: &api.AppEntity{
			ID: params["entity_id"].(string),
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

// TODO:
// - Use instance/entity to apply self targeted passive effects
// and subsequent calculated and adjusted attribute values

// getEntityData - return single resource Data
func (h *Handler) getEntityData(resourceID string) (*api.AppEntity, error) {

	// logger
	l := h.Logger.With().Str("function", "getEntityData").Logger()

	// models
	m := h.AppEntityModel

	rec, err := m.GetEntityRec(resourceID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity record >%v<", err)
		return nil, err
	}

	if rec == nil {
		l.Warn().Msgf("Resource not found")
		return nil, fmt.Errorf("Resource not found")
	}

	// entity group record
	entityGroupRec, err := m.GetEntityGroupRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group record >%v<", err)
		return nil, err
	}

	// entity item records
	entityItemRecs, err := m.GetEntityItemRecs(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity item records >%v<", err)
		return nil, err
	}

	// entity skill records
	entitySkillRecs, err := m.GetEntitySkillRecs(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity skill records >%v<", err)
		return nil, err
	}

	data := &api.AppEntity{
		ID:        rec.ID,
		AppID:     rec.AppID,
		Name:      rec.Name,
		CreatedAt: rec.CreatedAt,
		UpdatedAt: rec.UpdatedAt.String,
	}

	if entityGroupRec != nil {
		data.AppEntityGroup = &api.AppEntityGroup{
			ID:        entityGroupRec.ID,
			AppID:     entityGroupRec.AppID,
			Name:      entityGroupRec.Name,
			CreatedAt: entityGroupRec.CreatedAt,
			UpdatedAt: entityGroupRec.UpdatedAt.String,
		}
	}

	if entityItemRecs != nil {
		for _, entityItemRec := range entityItemRecs {
			data.AppEntityItems = append(data.AppEntityItems, &api.AppEntityItem{
				ID:          entityItemRec.ID,
				AppEntityID: entityItemRec.AppEntityID,
				AppItemID:   entityItemRec.AppItemID,
				CreatedAt:   entityItemRec.CreatedAt,
				UpdatedAt:   entityItemRec.UpdatedAt.String,
			})
		}
	}

	if entitySkillRecs != nil {
		for _, entitySkillRec := range entitySkillRecs {
			data.AppEntitySkills = append(data.AppEntitySkills, &api.AppEntitySkill{
				ID:          entitySkillRec.ID,
				AppEntityID: entitySkillRec.AppEntityID,
				AppSkillID:  entitySkillRec.AppSkillID,
				CreatedAt:   entitySkillRec.CreatedAt,
				UpdatedAt:   entitySkillRec.UpdatedAt.String,
			})
		}
	}

	return data, nil
}

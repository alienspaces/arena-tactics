package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"gopkg.in/go-playground/validator.v9"

	"gitlab.com/alienspaces/arena-tactics/server/internal/fault"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/attribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/effect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/fight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/item"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/skill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/template"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Handler -
type Handler interface {
	Get(w http.ResponseWriter, r *http.Request)
	Post(w http.ResponseWriter, r *http.Request)
	Put(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetCollection(w http.ResponseWriter, r *http.Request)
	PutCollection(w http.ResponseWriter, r *http.Request)
	DeleteCollection(w http.ResponseWriter, r *http.Request)
	GetName() string
	GetPath() string
	GetCollectionPath() string
	RequiresAuthentication() bool
	RequiresAuthorization() bool
	GetVersioned() bool
	GetLogger() zerolog.Logger
	DecodeRequest(r *http.Request, s interface{}) error
}

// ErrorData -
type ErrorData struct {
	Type   string `json:"type"`
	Detail string `json:"detail"`
}

// ErrorResponse -
type ErrorResponse struct {
	Errors []ErrorData `json:"errors"`
}

// Base -
type Base struct {
	Name            string
	Path            string
	CollectionPath  string
	Unauthenticated bool
	Unauthorized    bool
	Versioned       bool
	Env             env.Env
	Logger          zerolog.Logger
	Database        *sqlx.DB
	RepoStore       *repostore.RepoStore
	ValidationError error

	// models
	TemplateModel       *template.Model
	AppModel            *app.Model
	AppAttributeModel   *attribute.Model
	AppEffectModel      *effect.Model
	AppEntityModel      *entity.Model
	AppEntityGroupModel *entitygroup.Model
	AppFightModel       *fight.Model
	AppItemModel        *item.Model
	AppSkillModel       *skill.Model
}

// Params -
type Params map[string]interface{}

// Init -
func (h *Base) Init() error {

	l := h.Logger.With().Str("function", "Init").Logger()

	var err error

	// NOTE: Having repostore hanging off a global handler may be
	// dangerous. May need to instantiate each request..

	// TODO: Remove repostore altogether and have models instantiate
	// their own repo dependencies

	// repostore
	h.RepoStore, err = repostore.NewRepoStore(h.Env, h.Logger, nil)
	if err != nil {
		l.Warn().Msgf("Failed new repostore >%v<", err)
		return err
	}

	// models
	h.TemplateModel, err = template.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new template model >%v<", err)
		return err
	}

	h.AppModel, err = app.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new app model >%v<", err)
		return err
	}

	h.AppAttributeModel, err = attribute.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new attribute model >%v<", err)
		return err
	}

	h.AppEffectModel, err = effect.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new effect model >%v<", err)
		return err
	}

	h.AppEntityModel, err = entity.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity model >%v<", err)
		return err
	}

	h.AppEntityGroupModel, err = entitygroup.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity group model >%v<", err)
		return err
	}

	h.AppFightModel, err = fight.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new fight model >%v<", err)
		return err
	}

	h.AppItemModel, err = item.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new item model >%v<", err)
		return err
	}

	h.AppSkillModel, err = skill.NewModel(h.Env, h.Logger, h.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new skill model >%v<", err)
		return err
	}

	return nil
}

// NOTE: Handler functions *must* call BeginHandler for a new tx

// BeginHandler -
func (h *Base) BeginHandler(r *http.Request) (*ContextData, error) {

	// logger
	l := h.Logger.With().Str("function", "BeginHandler").Logger()

	// database
	db := h.Database

	l.Info().Msgf("Beginning handler")

	// tx
	tx, err := db.Beginx()
	if err != nil {
		l.Warn().Msgf("Failed to begin handler transaction >%v<", err)
		return nil, err
	}

	// repostore
	err = h.RepoStore.Init(tx)
	if err != nil {
		l.Warn().Msgf("Failed to init repostore >%v<", err)
		return nil, err
	}

	// context
	ctx, err := h.GetContextData(r)
	if err != nil {
		l.Warn().Msgf("Failed to get handler context >%v<", err)
		return nil, err
	}

	// assign context
	ctx.Tx = tx

	return ctx, nil
}

// EndHandler -
func (h *Base) EndHandler(ctx *ContextData, success bool) error {

	// logger
	l := h.Logger.With().Str("function", "EndHandler").Logger()

	// tx
	tx := ctx.Tx

	// rollback or commit tx
	if success != true {
		l.Info().Msgf("Ending handler with rollback")
		tx.Rollback()

	} else {
		l.Info().Msgf("Ending handler with commit")
		tx.Commit()
	}

	return nil
}

// Get -
func (h *Base) Get(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "Get").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// Post -
func (h *Base) Post(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "Post").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// Put -
func (h *Base) Put(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "Put").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// Delete -
func (h *Base) Delete(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "Delete").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// Options -
func (h *Base) Options(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "Options").Logger()

	l.Info().Msgf("Options method path %s", r.RequestURI)
}

// GetCollection -
func (h *Base) GetCollection(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// PutCollection -
func (h *Base) PutCollection(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "PutCollection").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// DeleteCollection -
func (h *Base) DeleteCollection(w http.ResponseWriter, r *http.Request) {

	// log
	l := h.Logger.With().Str("function", "DeleteCollection").Logger()

	l.Error().Msgf("Method not implemented for path %s", r.RequestURI)
	http.Error(w, http.StatusText(http.StatusNotFound),
		http.StatusNotFound)
}

// DebugStruct -
func (h *Base) DebugStruct(msg string, rec interface{}) {

	// logger
	l := h.Logger.With().Str("function", "DebugStruct").Logger()

	l.Debug().Msgf(msg + " " + spew.Sdump(rec))
}

// GetRequestData -
func (h *Base) GetRequestData(r *http.Request, rd interface{}) (Params, []error) {

	// logger
	l := h.Logger.With().Str("function", "getRequestData").Logger()

	// validate params
	params, errs := h.ValidateParams(r)
	if errs != nil {
		return Params{}, errs
	}

	l.Debug().Msgf("With params >%#v<", params)

	// validate data
	if r.Method == http.MethodPost || r.Method == http.MethodPut || r.Method == http.MethodPatch {

		err := h.DecodeRequest(r, rd)
		if err != nil {
			l.Warn().Msgf("Failed decode request >%v<", err)
			return Params{}, []error{err}
		}

		l.Debug().Msgf("With data >%#v<", rd)

		errs = h.Validate(rd)
		if errs != nil {
			l.Warn().Msgf("Failed validate >%v<", errs)
			return Params{}, errs
		}
	}

	return params, nil
}

// ValidateParams -
func (h *Base) ValidateParams(r *http.Request) (params Params, errs []error) {

	// logger
	l := h.Logger.With().Str("function", "ValidateParams").Logger()

	// validate vars
	vars := mux.Vars(r)

	l.Info().Msgf("Validate params with vars >%#v<", vars)

	params = Params{}

	for k, v := range vars {
		l.Info().Msgf("Validate param k [%s] v [%s]", k, v)

		if v == "" || v == "{"+k+"}" {
			l.Warn().Msgf("Validate error for param %s", k)

			errs = append(errs, fault.NewValidationError(fmt.Sprintf("Field %s failed 'required' check", k)))
			continue
		}
		params[k] = v
	}

	if len(errs) == 0 {
		return params, nil
	}

	return nil, errs
}

// Validate -
func (h *Base) Validate(str interface{}) (errs []error) {

	// log
	l := h.Logger.With().Str("function", "Name").Logger()

	validate := validator.New()
	err := validate.Struct(str)
	if err != nil {

		l.Info().Msgf("Validation error >%v<", err)

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			errs = append(errs, fault.NewValidationError("Unknown validation error"))
			return errs
		}

		for _, err := range err.(validator.ValidationErrors) {

			// log.Error().Msgf(err.Namespace())
			// log.Error().Msgf(err.Field())
			// log.Error().Msgf(err.StructNamespace()) // can differ when a custom TagNameFunc is registered or
			// log.Error().Msgf(err.StructField())     // by passing alt name to ReportError like below
			// log.Error().Msgf(err.Tag())
			// log.Error().Msgf(err.ActualTag())
			// log.Error().Msgf(err.Kind())
			// log.Error().Msgf(err.Type())
			// log.Error().Msgf(err.Value())
			// log.Error().Msgf(err.Param())

			l.Info().Msgf("Validation error, field %s failed '%s' check", err.Field(), err.Tag())

			errs = append(errs, fault.NewValidationError(
				fmt.Sprintf("Field %s failed '%s' check", err.Field(), err.Tag())))
		}

		return errs
	}

	return nil
}

// GetName - Returns configured resource name
func (h *Base) GetName() string {
	return h.Name
}

// GetPath - Returns configured resource path
func (h *Base) GetPath() string {
	return h.Path
}

// GetCollectionPath - Returns configured resource collection path
func (h *Base) GetCollectionPath() string {
	return h.CollectionPath
}

// RequiresAuthentication -
func (h *Base) RequiresAuthentication() bool {
	return h.Unauthenticated != true
}

// RequiresAuthorization -
func (h *Base) RequiresAuthorization() bool {
	return h.Unauthorized != true
}

// GetVersioned -
func (h *Base) GetVersioned() bool {
	return h.Versioned
}

// GetLogger -
func (h *Base) GetLogger() zerolog.Logger {
	return h.Logger
}

// DecodeRequest -
func (h *Base) DecodeRequest(r *http.Request, s interface{}) error {
	if r.Body != nil {
		return json.NewDecoder(r.Body).Decode(s)
	}
	return nil
}

// SendSystemErrorResponse -
func (h *Base) SendSystemErrorResponse(w http.ResponseWriter, ctx *ContextData, err error) {

	errs := []error{
		fault.NewSystemError(fmt.Sprintf("Error detail >%v<", err)),
	}

	h.SendErrorResponse(w, ctx, errs)
}

// SendErrorResponse -
func (h *Base) SendErrorResponse(w http.ResponseWriter, ctx *ContextData, errs []error) {

	// logger
	l := h.Logger.With().Str("function", "Name").Logger()

	l.Error().Msgf("Sending error response >%v<", errs)

	res := ErrorResponse{}

	responseHeader := http.StatusBadRequest

	for _, err := range errs {

		switch errType := err.(type) {
		case fault.ValidationError, fault.ModelError, fault.SystemError:
			res.Errors = append(res.Errors, ErrorData{
				Type:   fmt.Sprintf("%s", errType),
				Detail: err.Error(),
			})
		case fault.NotFoundError:
			res.Errors = append(res.Errors, ErrorData{
				Type:   fmt.Sprintf("%s", errType),
				Detail: err.Error(),
			})

			responseHeader = http.StatusNotFound

		default:
			res.Errors = append(res.Errors, ErrorData{
				Type:   "Unknown",
				Detail: err.Error(),
			})
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(responseHeader)
	h.SendResponse(w, ctx, &res)
}

// SendResponse -
func (h *Base) SendResponse(w http.ResponseWriter, ctx *ContextData, data interface{}) error {

	// logger
	l := h.Logger.With().Str("function", "SendResponse").Logger()

	switch rt := data.(type) {
	case *ErrorResponse:
		l.Warn().Msgf("Response type is error >%#v<", rt)

		// not a successful ending
		h.EndHandler(ctx, false)
	default:
		l.Info().Msgf("Response type is not an error >%#v<", rt)

		// a quite successful ending
		h.EndHandler(ctx, true)
	}

	// content type json
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	return json.NewEncoder(w).Encode(data)
}

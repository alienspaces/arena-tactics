// Package appentityattribute provides handlers for REST methods allowing web clients to manage appentityattributes
package appentityattribute

import (
	"fmt"
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   *api.AppEntityAttribute `json:"data"`
	Errors []handler.ErrorData     `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []*api.AppEntityAttribute `json:"data"`
	Errors []handler.ErrorData       `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data *api.AppEntityAttribute `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentityattribute"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/entities/{entity_id}/attributes/{entity_attribute_id}",
			CollectionPath:  "/api/apps/{app_id}/entities/{entity_id}/attributes",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// entity instance
	entInstance, err := h.getEntityInstance(params["entity_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed to get entity instance >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get resource
	entityData, err := h.getEntityAttributeData(params["entity_attribute_id"].(string), entInstance)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	recs, err := m.GetEntityAttributeRecs(params["entity_id"].(string))
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// entity instance
	entInstance, err := h.getEntityInstance(params["entity_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed to get entity instance >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	var entityAttributeData []*api.AppEntityAttribute

	if recs != nil {
		for _, rec := range recs {

			// get resource
			data, err := h.getEntityAttributeData(rec.ID, entInstance)
			if err != nil {
				l.Warn().Msgf("Failed get resource data >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}
			entityAttributeData = append(entityAttributeData, data)
		}
	}

	// response
	res := CollectionResponseData{
		Data: entityAttributeData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEntityAttributeRecord{}
	rec.AppEntityID = params["entity_id"].(string)
	rec.AppAttributeID = data.AppAttributeID
	rec.Value = data.Value

	// create
	err = m.CreateEntityAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// entity instance
	entInstance, err := h.getEntityInstance(params["entity_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed to get entity instance >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get resource
	entityAttributeData, err := h.getEntityAttributeData(rec.ID, entInstance)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityAttributeData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec, _ := m.GetEntityAttributeRec(params["entity_attribute_id"].(string))

	rec.Value = data.Value

	// update
	err = m.UpdateEntityAttributeRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// entity instance
	entInstance, err := h.getEntityInstance(params["entity_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed to get entity instance >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get resource
	entityAttributeData, err := h.getEntityAttributeData(rec.ID, entInstance)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: entityAttributeData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	entityAttributeID := params["entity_attribute_id"].(string)

	// models
	m := h.AppEntityModel

	// delete
	err = m.DeleteEntityAttributeRec(entityAttributeID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: &api.AppEntityAttribute{
			ID: entityAttributeID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

func (h *Handler) getEntityInstance(entityID string) (*instance.Entity, error) {

	// logger
	l := h.Logger.With().Str("function", "getEntityInstance").Logger()

	// entity instance
	entInstance, err := instance.NewEntity(h.Env, h.Logger, h.RepoStore, entityID)
	if err != nil {
		l.Warn().Msgf("Failed to create entity instance >%v<", err)
		return nil, err
	}

	// calculate attribute values
	err = entInstance.CalculateAttributeValues()
	if err != nil {
		l.Warn().Msgf("Failed to calculate entity instance attribute values >%v<", err)
		return nil, err
	}

	for _, entSkill := range entInstance.Skills {
		for _, skillEffect := range entSkill.Effects {
			_, err := entInstance.ApplySkillEffect(entInstance.AppEntityID, record.TargetSelf, skillEffect)
			if err != nil {
				l.Warn().Msgf("Failed to apply entity skill effect >%v<", err)
				return nil, err
			}
		}
	}

	for _, entItem := range entInstance.Items {
		for _, itemEffect := range entItem.Effects {
			_, err := entInstance.ApplyItemEffect(entInstance.AppEntityID, record.TargetSelf, itemEffect)
			if err != nil {
				l.Warn().Msgf("Failed to apply entity item effect >%v<", err)
				return nil, err
			}
		}
	}

	return entInstance, nil
}

// getEntityAttributeData - return single resource Data
func (h *Handler) getEntityAttributeData(entityAttributeID string, entityInstance *instance.Entity) (*api.AppEntityAttribute, error) {

	// logger
	l := h.Logger.With().Str("function", "getEntityAttributeData").Logger()

	// models
	m := h.AppEntityModel
	am := h.AppAttributeModel

	// get entity attribute rec
	rec, err := m.GetEntityAttributeRec(entityAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute record >%v<", err)
		return nil, err
	}

	if rec == nil {
		l.Warn().Msgf("Resource not found")
		return nil, fmt.Errorf("Resource not found")
	}

	// get attribute rec
	attrRec, _ := am.GetAttributeRec(rec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute record >%v<", err)
		return nil, err
	}

	// get attribute values
	entAttr, err := entityInstance.GetEntityAttribute(attrRec.Name)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute values >%v<", err)
		return nil, err
	}

	data := &api.AppEntityAttribute{
		ID:              rec.ID,
		AppEntityID:     rec.AppEntityID,
		AppAttributeID:  rec.AppAttributeID,
		Value:           entAttr.AssignedValue,
		CalculatedValue: entAttr.CalculatedValue,
		AdjustedValue:   entAttr.AdjustedValue,
		Assignable:      &attrRec.Assignable,
		CreatedAt:       rec.CreatedAt,
		UpdatedAt:       rec.UpdatedAt.String,
	}

	return data, nil
}

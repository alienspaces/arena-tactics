// Package template provides handlers for REST methods allowing web clients to manage templates
package template

import (
	"fmt"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   *api.Template       `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []*api.Template     `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data *api.Template `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "template"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/templates/{template_id}",
			CollectionPath:  "/api/templates",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// get resource
	templateData, err := h.getTemplateData(params["template_id"].(string))
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: templateData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.TemplateModel

	// get
	recs, err := m.GetTemplateRecs(params)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	var templateData []*api.Template

	if recs != nil {
		for _, rec := range recs {

			// get resource
			data, err := h.getTemplateData(rec.ID)
			if err != nil {
				l.Warn().Msgf("Failed get resource data >%v<", err)
				h.SendSystemErrorResponse(w, ctx, err)
				return
			}
			templateData = append(templateData, data)
		}
	}

	// response
	res := CollectionResponseData{
		Data: templateData,
	}

	h.DebugStruct("Get Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.TemplateModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.TemplateRecord{}
	rec.Name = data.Name

	// create
	err = m.CreateTemplateRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get template data
	templateData, err := h.getTemplateData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: templateData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.TemplateModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.TemplateRecord{}
	rec.ID = params["template_id"].(string)
	rec.Name = data.Name

	// update
	err = m.UpdateTemplateRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// get template data
	templateData, err := h.getTemplateData(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed get resource data >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// response
	res := ResponseData{
		Data: templateData,
	}

	h.DebugStruct("Post Response", res)

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	templateID := params["template_id"].(string)

	// models
	m := h.TemplateModel

	// delete
	err = m.DeleteTemplateRec(templateID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: &api.Template{
			ID: templateID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

// getTemplateData - return single resource Data
func (h *Handler) getTemplateData(resourceID string) (*api.Template, error) {

	// logger
	l := h.Logger.With().Str("function", "getTemplateData").Logger()

	// models
	m := h.TemplateModel

	// get
	rec, err := m.GetTemplateRec(resourceID)
	if err != nil {
		l.Warn().Msgf("Failed to get template record >%v<", err)
		return nil, err
	}

	if rec == nil {
		l.Warn().Msgf("Resource not found")
		return nil, fmt.Errorf("Resource not found")
	}

	data := &api.Template{
		ID:        rec.ID,
		Name:      rec.Name,
		CreatedAt: rec.CreatedAt,
		UpdatedAt: rec.UpdatedAt.String,
	}

	return data, nil
}

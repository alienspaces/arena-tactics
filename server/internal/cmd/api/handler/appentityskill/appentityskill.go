// Package appentityskill provides handlers for REST methods allowing web clients to manage appentityskills
package appentityskill

import (
	"net/http"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// internal
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/handler"
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/types/api"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// ResponseData -
type ResponseData struct {
	Data   api.AppEntitySkill  `json:"data"`
	Errors []handler.ErrorData `json:"errors"`
}

// CollectionResponseData -
type CollectionResponseData struct {
	Data   []api.AppEntitySkill `json:"data"`
	Errors []handler.ErrorData  `json:"errors"`
}

// RequestData -
type RequestData struct {
	Data api.AppEntitySkill `json:"data"`
}

// Handler -
type Handler struct {
	handler.Base
}

// HandlerName -
const HandlerName string = "appentityskill"

// NewHandler -
func NewHandler(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Handler, error) {

	// logger
	l = l.With().Str("package", "handler/"+HandlerName).Logger()

	h := Handler{
		handler.Base{
			Name:            HandlerName,
			Path:            "/api/apps/{app_id}/entities/{entity_id}/skills/{entity_skill_id}",
			CollectionPath:  "/api/apps/{app_id}/entities/{entity_id}/skills",
			Unauthenticated: true,
			Unauthorized:    true,
			Versioned:       true,
			Env:             e,
			Logger:          l,
			Database:        d,
		},
	}

	// init
	err := h.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return &h, nil
}

// Get -
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Get").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	rec, _ := m.GetEntitySkillRec(params["entity_skill_id"].(string))
	if rec != nil {
		res := ResponseData{
			Data: api.AppEntitySkill{
				ID:          rec.ID,
				AppEntityID: rec.AppEntityID,
				AppSkillID:  rec.AppSkillID,
				CreatedAt:   rec.CreatedAt,
				UpdatedAt:   rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched OK")
}

// GetCollection -
func (h *Handler) GetCollection(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "GetCollection").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	// models
	m := h.AppEntityModel

	// get
	recs, _ := m.GetEntitySkillRecs(params["entity_id"].(string))
	if recs != nil {

		var ed []api.AppEntitySkill

		for _, rec := range recs {
			ed = append(ed, api.AppEntitySkill{
				ID:          rec.ID,
				AppEntityID: rec.AppEntityID,
				AppSkillID:  rec.AppSkillID,
				CreatedAt:   rec.CreatedAt,
				UpdatedAt:   rec.UpdatedAt.String,
			})
		}

		res := CollectionResponseData{
			Data: ed,
		}

		h.DebugStruct("Get Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Fetched collection OK")
}

// Post -
func (h *Handler) Post(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Post").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEntitySkillRecord{}
	rec.AppEntityID = params["entity_id"].(string)
	rec.AppSkillID = data.AppSkillID

	// create
	err = m.CreateEntitySkillRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppEntitySkill{
				ID:          rec.ID,
				AppEntityID: rec.AppEntityID,
				AppSkillID:  rec.AppSkillID,
				CreatedAt:   rec.CreatedAt,
				UpdatedAt:   rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Post Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Created OK")
}

// Put -
func (h *Handler) Put(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Put").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, &rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	data := rd.Data

	l.Info().Msgf("With params >%#v<", params)
	l.Info().Msgf("With data >%#v<", data)

	// models
	m := h.AppEntityModel

	// record
	// example: rec.RecordID = params["record_id"].(string)
	rec := &record.AppEntitySkillRecord{}
	rec.ID = params["entity_skill_id"].(string)
	rec.AppEntityID = params["entity_id"].(string)
	rec.AppSkillID = data.AppSkillID

	// update
	err = m.UpdateEntitySkillRec(rec)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	if rec.ID != "" {
		res := ResponseData{
			Data: api.AppEntitySkill{
				ID:          rec.ID,
				AppEntityID: rec.AppEntityID,
				AppSkillID:  rec.AppSkillID,
				CreatedAt:   rec.CreatedAt,
				UpdatedAt:   rec.UpdatedAt.String,
			},
		}

		h.DebugStruct("Put Response", res)

		h.SendResponse(w, ctx, &res)
	}

	l.Info().Msgf("Updated OK")
}

// Delete -
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {

	// logger
	l := h.Logger.With().Str("function", "Delete").Logger()

	// begin handler
	ctx, err := h.BeginHandler(r)
	if err != nil {
		l.Warn().Msgf("Failed begin handler >%v<", err)
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	// request data
	rd := RequestData{}

	params, errs := h.GetRequestData(r, rd)
	if errs != nil {
		l.Warn().Msgf("Failed get request data >%v<", err)
		h.SendErrorResponse(w, ctx, errs)
		return
	}

	l.Info().Msgf("With params >%#v<", params)

	appentityskillID := params["entity_skill_id"].(string)

	// models
	m := h.AppEntityModel

	// delete
	err = m.DeleteEntitySkillRec(appentityskillID)
	if err != nil {
		h.SendSystemErrorResponse(w, ctx, err)
		return
	}

	res := ResponseData{
		Data: api.AppEntitySkill{
			ID: appentityskillID,
		},
	}

	h.SendResponse(w, ctx, &res)

	l.Info().Msgf("Deleted OK")
}

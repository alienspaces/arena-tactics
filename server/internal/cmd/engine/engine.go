package engine

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// cmd
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/engine/runner"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// model
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/app"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Engine -
type Engine struct {
	Env       env.Env
	Logger    zerolog.Logger
	Database  *sqlx.DB
	AppStates map[string]*App
}

// App -
type App struct {
	ID          string
	Status      string
	LastPing    time.Time
	ToAppChan   chan *runner.Message
	FromAppChan chan *runner.Message
}

// channel for interrupt / terminate signals
var sigChan chan os.Signal

// channel for engine results
var errChan chan error

const (
	// PackageName -
	PackageName string = "engine"
)

// NewEngine -
func NewEngine(e env.Env, l zerolog.Logger, d *sqlx.DB) (*Engine, error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	eng := Engine{
		Env:       e,
		Logger:    l,
		Database:  d,
		AppStates: make(map[string]*App),
	}

	// channel for interrupt / terminate signals
	sigChan = make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	return &eng, nil
}

// Run -
func (eng *Engine) Run(appID string, runTimes int, runSpeed int) error {

	l := eng.Logger.With().Str("function", "Run").Logger()

	l.Info().Msgf("Start polling")

	cycleCount := 0
loop:
	for {

		cycleCount++

		select {
		case <-sigChan:

			// break loop
			l.Debug().Msgf("Signal break, stop all")

			// stop all
			err := eng.stopApps(true)
			if err != nil {
				l.Debug().Msgf("Error stop all >%v<", err)
			}

			break loop

		default:
			// no-op
		}

		l.Debug().Msgf("=> Receiving app messages (pre) <=")

		// check app engine messages
		err := eng.receiveAppMessages()
		if err != nil {
			l.Warn().Msgf("Error checking engine messages >%v<", err)
			return err
		}

		l.Debug().Msgf("=> Starting apps <=")

		// start app engines
		err = eng.startApps(appID, runTimes, runSpeed)
		if err != nil {
			l.Warn().Msgf("Error starting engines >%v<", err)
			return err
		}

		l.Debug().Msgf("=> Stopping apps <=")

		// stop app engines
		err = eng.stopApps(false)
		if err != nil {
			l.Warn().Msgf("Error stopping engines >%v<", err)
			return err
		}

		l.Debug().Msgf("=> Sending app messages <=")

		// send app engine messages
		err = eng.sendAppMessages()
		if err != nil {
			l.Warn().Msgf("Error sending messages >%v<", err)
			return err
		}

		l.Debug().Msgf("=> Receiving app messages (post) <=")

		// check app engine messages
		err = eng.receiveAppMessages()
		if err != nil {
			l.Warn().Msgf("Error checking engine messages >%v<", err)
			return err
		}

		l.Debug().Msgf("=> Cycle >%d< <=", cycleCount)

		// run forever or when runTimes, check if all apps stopped
		if runTimes != 0 {
			allStopped, err := eng.allAppsStopped()
			if err != nil {
				l.Warn().Msgf("Failed checking all apps stopped >%v<", err)
				return err
			}
			if allStopped {
				l.Debug().Msgf("All apps stopped..")
				break loop
			}
		}

		time.Sleep(1000 * time.Millisecond)
	}

	// stop all app engines
	l.Info().Msgf("Loop end, stopping all")
	err := eng.stopApps(true)
	if err != nil {
		l.Warn().Msgf("Error stop all >%v<", err)
	}

	// cleanup here
	l.Info().Msgf("End polling")

	return nil
}

// startApps - starts apps running that have a status of starting
func (eng *Engine) startApps(appID string, runTimes int, runSpeed int) error {

	l := eng.Logger.With().Str("function", "startApps").Logger()

	if appID != "" {
		l.Debug().Msgf("Starting app ID >%s<", appID)
	} else {
		l.Debug().Msgf("Starting all apps")
	}

	// get starting apps
	recs, err := eng.getAppRecords(record.AppStatusStarting)
	if err != nil {
		l.Warn().Msgf("Error getting app records >%v<", err)
		return err
	}

	for _, rec := range recs {

		if appID != "" && rec.ID != appID {
			l.Debug().Msgf("Skipping app ID >%s<", rec.ID)
			continue
		}

		// already running
		if appState, ok := eng.AppStates[rec.ID]; ok && appState.Status == record.AppStatusRunning {

			l.Debug().Msgf("Have App ID >%s< state >%s<", rec.ID, appState.Status)
			continue
		}

		// not running
		if appState, ok := eng.AppStates[rec.ID]; !ok || appState.Status != record.AppStatusRunning {

			l.Info().Msgf("Starting App ID >%s<", rec.ID)

			eng.AppStates[rec.ID] = &App{
				ID:          rec.ID,
				Status:      rec.Status,
				ToAppChan:   make(chan *runner.Message, 3),
				FromAppChan: make(chan *runner.Message, 3),
			}

			go func(frec *record.AppRecord) {

				l.Debug().Msgf("Starting App >%#v<", eng.AppStates[rec.ID])

				// engine
				eng, err := runner.NewRunner(
					eng.Env,
					eng.Logger,
					eng.Database,
					rec.ID,
					eng.AppStates[rec.ID].ToAppChan,
					eng.AppStates[rec.ID].FromAppChan,
					runTimes,
					runSpeed,
				)
				if err != nil {
					l.Warn().Msgf("Failed App ID >%s< new engine >%v<", rec.ID, err)
				}

				err = eng.Run()
				if err != nil {
					l.Warn().Msgf("Failed App ID >%s< engine run >%v<", rec.ID, err)
				}

			}(rec)
		}

		// update status
		err := eng.updateAppStatus(rec.ID, record.AppStatusRunning)
		if err != nil {
			l.Warn().Msgf("Failed App ID >%s< updating status >%v<", rec.ID, err)
			return err
		}
	}

	l.Debug().Msgf("Done starting apps with >%d< apps states", len(eng.AppStates))

	return nil
}

// stopApps - stopAll true/false
// - when true will stop all applications regardless of whether
//   there was a request to stop the application
// - when false will only stop applications where there has been
//   a request to stop the application
func (eng *Engine) stopApps(stopAll bool) error {

	l := eng.Logger.With().Str("function", "stopApps").Logger()

	l.Debug().Msgf("Stopping apps")

	// get apps
	status := ""
	if stopAll == false {
		status = record.AppStatusStopping
	}

	recs, err := eng.getAppRecords(status)
	if err != nil {
		l.Warn().Msgf("Error getting app records >%v<", err)
		return err
	}

	// no apps to stop
	if len(recs) == 0 {
		l.Debug().Msgf("No apps to stop")
		return nil
	}

	// send request stop
	for _, rec := range recs {
		appState := eng.AppStates[rec.ID]
		if appState == nil {
			l.Debug().Msgf("App ID >%s< status >%s< not running", rec.ID, rec.Status)
			continue
		}

		if appState.Status == record.AppStatusRunning {
			l.Info().Msgf("Sending shutdown message to App ID >%s<", rec.ID)
			appState.ToAppChan <- &runner.Message{Message: runner.MessageRequestStop}
		}
	}

loop:
	for i := 0; i < 20; i++ {

		err := eng.receiveAppMessages()
		if err != nil {
			l.Warn().Msgf("Failed checking messages >%v<", err)
			return err
		}

		// all apps stopped?
		allStopped := true

		for _, rec := range recs {

			appState := eng.AppStates[rec.ID]
			if appState == nil {
				l.Debug().Msgf("App ID >%s< not running", rec.ID)
				continue
			}

			l.Debug().Msgf("Checking App ID >%s< has stopped", rec.ID)
			if appState.Status != record.AppStatusStopped && appState.Status != record.AppStatusStarting {
				allStopped = false
				break
			}
		}

		if allStopped {
			l.Debug().Msgf("All apps stopped")
			break loop
		}

		l.Debug().Msgf("Stop loop >%d<", i)

		time.Sleep(1000 * time.Millisecond)
	}

	l.Debug().Msgf("Stopping all done")

	return nil
}

func (eng *Engine) allAppsStopped() (bool, error) {

	l := eng.Logger.With().Str("function", "allAppsStopped").Logger()

	// get apps
	statuses := []string{
		record.AppStatusRunning, record.AppStatusStarting, record.AppStatusStopping,
	}

	for _, status := range statuses {

		recs, err := eng.getAppRecords(status)
		if err != nil {
			l.Warn().Msgf("Error getting app records >%v<", err)
			return false, err
		}

		if len(recs) != 0 {
			l.Debug().Msgf("Have >%d< apps with status >%s<", len(recs), status)
			return false, nil
		}
	}

	l.Debug().Msgf("All apps stopped")

	return true, nil
}

func (eng *Engine) sendAppMessages() error {

	l := eng.Logger.With().Str("function", "sendAppMessages").Logger()

	l.Debug().Msgf("Sending messages")

	// send messages message
	for recID, appState := range eng.AppStates {
		if appState.Status == record.AppStatusRunning {

			// send ping
			l.Debug().Msgf("Sending ping message to App ID >%s<", recID)
			appState.ToAppChan <- &runner.Message{Message: runner.MessageRequestPing}

			l.Debug().Msg("Ping sent")
		}
	}

	l.Debug().Msgf("Sending messages done")

	return nil
}

func (eng *Engine) receiveAppMessages() error {

	l := eng.Logger.With().Str("function", "receiveAppMessages").Logger()

	l.Debug().Msgf("Checking for engine messages")

	for recID, appState := range eng.AppStates {

		if appState.Status == record.AppStatusRunning {

			l.Debug().Msgf("Checking messages from App ID >%s<", appState.ID)

			// NOTE: Don't need to do this necessarily, just interesting to see whether
			// we ever end up with more than a single message
			for i := 0; i < 5; i = i + 1 {

				select {
				case msg, ok := <-appState.FromAppChan:

					l.Debug().Msgf("Received message >%s< ok >%t< from App ID >%s<", msg, ok, recID)

					// app stopping - update app status to stopped
					if msg.Message == runner.MessageResponseStopping {

						err := eng.updateAppStatus(recID, record.AppStatusStopped)
						if err != nil {
							l.Warn().Msgf("Error updating status App ID >%s< >%v<", recID, err)
							return err
						}
					}

					// app pausing - update app status to starting
					if msg.Message == runner.MessageResponsePausing {

						err := eng.updateAppStatus(recID, record.AppStatusStarting)
						if err != nil {
							l.Warn().Msgf("Error updating status App ID >%s< >%v<", recID, err)
							return err
						}
					}

					// app pong`ing
					if msg.Message == runner.MessageResponsePong {
						eng.AppStates[recID].LastPing = time.Now()
					}

				default:
					// no-op
				}
			}
		}
	}

	l.Debug().Msgf("All done")

	return nil
}

func (eng *Engine) updateAppStatus(recID string, status string) error {

	l := eng.Logger.With().Str("function", "updateAppStatus").Logger()

	// tx
	tx, err := eng.Database.Beginx()
	if err != nil {
		l.Warn().Msgf("Failed to start new transaction >%v<", err)
		return err
	}

	// repostore
	rs, err := repostore.NewRepoStore(eng.Env, eng.Logger, tx)
	if err != nil {
		l.Warn().Msgf("Failed new repo store >%v<", err)
		return err
	}

	// app model
	am, err := app.NewModel(eng.Env, eng.Logger, rs)
	if err != nil {
		l.Warn().Msgf("Failed new app repo >%v<", err)
		return err
	}

	rec, err := am.GetAppRec(recID)
	if err != nil {
		l.Warn().Msgf("Failed new app repo >%v<", err)
		return err
	}

	// update status
	rec.Status = status

	err = am.UpdateAppRec(rec)
	if err != nil {
		l.Warn().Msgf("Failed app record update >%v<", err)
		return err
	}

	// update app state
	eng.AppStates[recID].Status = rec.Status

	// tx commit
	tx.Commit()

	return nil
}

// getAppRecords - returns application records for specified status
func (eng *Engine) getAppRecords(status string) ([]*record.AppRecord, error) {

	l := eng.Logger.With().Str("function", "getAppRecords").Logger()

	// tx
	tx, err := eng.Database.Beginx()
	if err != nil {
		l.Warn().Msgf("Failed to start new transaction >%v<", err)
		return nil, err
	}

	// repostore
	rs, err := repostore.NewRepoStore(eng.Env, eng.Logger, tx)
	if err != nil {
		l.Warn().Msgf("Failed new repo store >%v<", err)
		return nil, err
	}

	// app model
	am, err := app.NewModel(eng.Env, eng.Logger, rs)

	// apps pending stop
	params := make(map[string]interface{})
	if status != "" {
		params["status"] = status
	}

	// run pending
	recs, err := am.GetAppRecs(params)
	if err != nil {
		l.Error().Msgf("Failed new app repo >%v<", err)
		return nil, err
	}

	// commit
	tx.Commit()

	return recs, nil
}

package app

import (
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// engine
	"gitlab.com/alienspaces/arena-tactics/server/internal/instance"

	// model
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/fight"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// App -
type App struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppID
	AppID string

	// records
	appRec         *record.AppRecord
	appInstanceRec *record.AppInstanceRecord

	// models
	appModel   *app.Model
	fightModel *fight.Model

	// fights
	Fights []*instance.Fight

	// Run cycle number
	RunCycleNum int
}

const (
	// PackageName -
	PackageName string = "engine/runner/app"
)

// NewApp -
func NewApp(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, appID string) (app *App, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	app = &App{
		AppID:     appID,
		Env:       e,
		Logger:    l,
		RepoStore: rs,
	}

	return app, nil
}

// Init - loads and initialises application data
func (a *App) Init(tx *sqlx.Tx) (err error) {

	l := a.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("Initialising")

	// repostore init
	err = a.RepoStore.Init(tx)
	if err != nil {
		l.Warn().Msgf("Init failed repostore init >%v<", err)
		return err
	}

	// init models
	a.appModel, err = app.NewModel(a.Env, a.Logger, a.RepoStore)
	a.fightModel, err = fight.NewModel(a.Env, a.Logger, a.RepoStore)

	// init app
	err = a.initApp()
	if err != nil {
		l.Warn().Msgf("Init failed init app >%v<", err)
		return err
	}

	// init app instance data
	err = a.initAppInstanceData()
	if err != nil {
		l.Warn().Msgf("Init failed instance data init >%v<", err)
		return err
	}

	// init app fights
	err = a.initAppFights()
	if err != nil {
		l.Warn().Msgf("Init failed init app fights >%v<", err)
		return err
	}

	return nil
}

// initApp -
func (a *App) initApp() (err error) {

	l := a.Logger.With().Str("function", "initApp").Logger()

	l.Debug().Msgf("<= Initialise app =>")

	// get app record
	appRec, err := a.appModel.GetAppRec(a.AppID)
	if err != nil {
		l.Warn().Msgf("Init failed to get app rec >%v<", err)
		return err
	}

	a.appRec = appRec

	// get app instance record
	appInstanceRec, err := a.appModel.GetAppInstanceRec(a.AppID)
	if err != nil {
		l.Warn().Msgf("Init failed to get app instance rec >%v<", err)
		return err
	}

	if appInstanceRec == nil {

		// create app instance record
		appInstanceRec, err = a.appModel.CreateAppInstanceRec(a.AppID)
		if err != nil {
			l.Warn().Msgf("Init failed to create app instance rec >%v<", err)
			return err
		}
	}

	a.appInstanceRec = appInstanceRec

	l.Debug().Msgf("<= Initialise app done =>")

	return nil
}

// initAppInstanceData -
func (a *App) initAppInstanceData() (err error) {

	l := a.Logger.With().Str("function", "initAppInstanceData").Logger()

	l.Debug().Msgf("<= Initialise app instance data =>")

	// TODO: Not entirely sure what we are storing in
	// application instance data at the moment..

	l.Debug().Msgf("<= Initialise app instance data done =>")

	return nil
}

// initAppFights -
func (a *App) initAppFights() (err error) {

	l := a.Logger.With().Str("function", "initAppFights").Logger()

	l.Debug().Msgf("<= Initiaising app fights =>")

	a.Fights = []*instance.Fight{}

	// starting fight records
	recs, err := a.fightModel.GetStartingFightRecs(a.AppID)
	if err != nil {
		l.Warn().Msgf("Failed get starting fight recs >%v<", err)
		return err
	}

	l.Debug().Msgf("Have >%d< fight records starting", len(recs))

	for _, rec := range recs {

		l.Debug().Msgf("Adding fight ID >%s< Status >%s", rec.ID, rec.Status)

		f, err := instance.NewFight(a.Env, a.Logger, a.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new starting fight >%v<", err)
			return err
		}

		// save fight so we have instance data set for turn 0
		err = f.Save()
		if err != nil {
			l.Warn().Msgf("Failed to save starting fight >%v<", err)
			return err
		}

		a.Fights = append(a.Fights, f)
	}

	// fighting fight records
	recs, err = a.fightModel.GetFightingFightRecs(a.AppID)
	if err != nil {
		l.Warn().Msgf("Failed get fighting fight recs >%v<", err)
		return err
	}

	l.Debug().Msgf("Have >%d< fight records fighting", len(recs))

	for _, rec := range recs {

		l.Debug().Msgf("Adding fight ID >%s< Status >%s", rec.ID, rec.Status)

		f, err := instance.NewFight(a.Env, a.Logger, a.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new fighting fight >%v<", err)
			return err
		}

		a.Fights = append(a.Fights, f)
	}

	l.Debug().Msgf("<= Initiaising app fights done =>")

	return nil
}

// Run - executes an app cycle, just the one
func (a *App) Run() (err error) {

	l := a.Logger.With().Str("function", "Run").Logger()

	// increment app run cycle num
	a.RunCycleNum++

	l.Debug().Msgf("=> Running cycle num >%d< <=", a.RunCycleNum)

	// cycle through fights
	for _, fight := range a.Fights {

		l.Debug().Msgf("Checking fight ID >%s< Status >%s<", fight.FightID, fight.Status)

		if fight.Status == record.AppFightStatusStarting ||
			fight.Status == record.AppFightStatusFighting {

			// run fight
			err = fight.Run()
			if err != nil {
				a.Logger.Warn().Msgf("Run fail fight run >%v<", err)
				return err
			}
		}
	}

	l.Debug().Msgf("=> Running cycle num >%d< done <=", a.RunCycleNum)

	return nil
}

// Save - saves all mapper instance data
func (a *App) Save() (err error) {

	l := a.Logger.With().Str("function", "Save").Logger()

	l.Debug().Msgf("Save app saving")

	// fights
	for _, fight := range a.Fights {
		err := fight.Save()
		if err != nil {
			a.Logger.Warn().Msgf("Save failed fight save >%v<", err)
			return err
		}
	}

	// app
	err = a.appModel.UpdateAppInstanceRec(a.appInstanceRec)
	if err != nil {
		a.Logger.Warn().Msgf("Save failed to update app map instance data >%v<", err)
		return err
	}

	return nil
}

// Cleanup - Removes all instance data which isn't a good idea
// if there are fights that are not done etc. This function is
// used for testing purposes at this stage.
func (a *App) Cleanup(tx *sqlx.Tx) (err error) {

	l := a.Logger.With().Str("function", "Cleanup").Logger()

	l.Debug().Msgf("Cleanup app cleaning")

	// re-initialize the repostore
	if tx != nil {
		err = a.RepoStore.Init(tx)
		if err != nil {
			l.Warn().Msgf("Cleanup failed repostore init >%v<", err)
			return err
		}
	}

	// fights
	for _, fight := range a.Fights {
		err := fight.Cleanup()
		if err != nil {
			a.Logger.Warn().Msgf("Cleanup failed fight cleanup >%v<", err)
			return err
		}
	}

	// app instance
	if a.appInstanceRec != nil {
		err = a.appModel.RemoveAppInstanceRec(a.appInstanceRec.ID)
		if err != nil {
			a.Logger.Warn().Msgf("Cleanup failed to remove instance data >%v<", err)
			return err
		}

		a.appInstanceRec = nil
	}

	return nil
}

package app

import (
	"testing"

	"github.com/stretchr/testify/assert"

	fightmodel "gitlab.com/alienspaces/arena-tactics/server/internal/model/fight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		// remove app test data
		err = td.RemoveAppData(tx)
		if err != nil {
			t.Fatalf("Failed to remove app test data >%v<", err)
		}

		tx.Commit()
	}

	return td, teardown
}

func TestInit(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// update app fight records to starting
	afr := rs.AppFightRepo

	for _, appFightRec := range td.AppFightRecs {
		appFightRec.Status = record.AppFightStatusStarting
		err := afr.Update(appFightRec)
		if err != nil {
			t.Fatalf("Failed to update fight record >%v<", err)
		}
	}

	a, err := NewApp(e, l, rs, td.AppRec.ID)
	if assert.NoError(t, err, "NewApp does not result in error") {
		if assert.NotNil(t, a, "NewApp returns a new app") {
			// test init
			err := a.Init(tx)
			if assert.NoError(t, err, "Init does not result in error") {
				// test fights
				if assert.NotEmpty(t, a.Fights, "Fights is not empty") {
					// test fight instance data
					for _, fi := range a.Fights {
						t.Logf("Have fight instance ID >%s<", fi.FightInstanceID)

						fm, err := fightmodel.NewModel(e, l, rs)
						if err != nil {
							t.Fatalf("Failed new fight model >%v<", err)
						}

						rec, err := fm.GetFightInstanceRec(fi.FightInstanceID)
						if err != nil {
							t.Fatalf("Failed to get fight instance record >%v<", err)
						}

						data, err := fm.GetFightInstanceData(rec)
						if err != nil {
							t.Fatalf("Failed to get fight instance data >%v<", err)
						}

						// test entities
						if assert.NotEmpty(t, data.Entities, "Entities is not empty") {
							for _, ei := range data.Entities {
								t.Logf("Have entity name >%s<", ei.Name)
								// test entity attributes
								if assert.NotEmpty(t, ei.Attributes, "Attributes are not empty") {
									for _, ea := range ei.Attributes {
										t.Logf("Have attribute name >%s<", ea.Name)
										// AttributeHealth should not be zero
										if ea.Name == testingdata.AttributeHealth {
											assert.NotEqual(t, 0, ea.AdjustedValue, "Health AdjustedValue is not zero")
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	err = a.Cleanup(tx)
	assert.NoError(t, err, "Cleanup does not result in error")

	tx.Rollback()
}

func TestRun(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	a, err := NewApp(e, l, rs, td.AppRec.ID)
	if assert.NoError(t, err, "NewApp does not result in error") {
		// test init
		err := a.Init(tx)
		if assert.NoError(t, err, "Init does not result in error") {
			// test run
			err := a.Run()
			assert.NoError(t, err, "Run does not result in error")
		}
	}

	err = a.Cleanup(tx)
	assert.NoError(t, err, "Cleanup does not result in error")

	tx.Rollback()
}

func TestSave(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	a, err := NewApp(e, l, rs, td.AppRec.ID)
	if assert.NoError(t, err, "NewApp does not result in error") {

		err := a.Init(tx)
		if assert.NoError(t, err, "Init does not result in error") {

			err := a.Run()
			if assert.NoError(t, err, "Run does not result in error") {

				err := a.Save()
				assert.NoError(t, err, "Save does not result in error")
			}
		}
	}

	err = a.Cleanup(tx)
	assert.NoError(t, err, "Cleanup does not result in error")

	tx.Rollback()
}

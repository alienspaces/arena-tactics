package runner

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		// remove app test data
		err = td.RemoveAppData(tx)
		if err != nil {
			t.Fatalf("Failed to remove app test data >%v<", err)
		}

		tx.Commit()
	}

	return td, teardown
}

func TestNewRunner(t *testing.T) {

	td, teardown := setup(t)

	toRunnerChan := make(chan *Message, 3)
	fromRunnerChan := make(chan *Message, 3)

	rnr, err := NewRunner(e, l, db, td.AppRec.ID, toRunnerChan, fromRunnerChan, RunTimesOnce, RunSpeedFastest)
	if assert.NoError(t, err, "New engine created without error") {
		assert.NotNil(t, rnr, "New engine is not nil")
		assert.NotNil(t, rnr.App, "New engine App is not nil")

		err = rnr.Cleanup()
		assert.NoError(t, err, "Cleanup returns without error")
	}

	teardown()
}

func TestRun(t *testing.T) {

	td, teardown := setup(t)

	toRunnerChan := make(chan *Message, 3)
	fromRunnerChan := make(chan *Message, 3)

	rnr, _ := NewRunner(e, l, db, td.AppRec.ID, toRunnerChan, fromRunnerChan, RunTimesOnce, RunSpeedFastest)

	rnr.ToRunnerChan <- &Message{Message: MessageRequestPing}

	assert.NoError(t, rnr.Run())

	msg, ok := <-rnr.FromRunnerChan

	assert.True(t, ok, "Runner from message channel is empty")
	assert.Equal(t, &Message{Message: MessageResponsePong}, msg, "Runner responded with pong")

	err := rnr.Cleanup()
	assert.NoError(t, err, "Cleanup returns without error")

	teardown()
}

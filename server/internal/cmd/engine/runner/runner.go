package runner

import (
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// model
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/engine/runner/app"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Runner -
type Runner struct {
	AppID          string
	ToRunnerChan   chan *Message
	FromRunnerChan chan *Message
	RunTimes       int
	RunSpeed       int

	// services
	Env      env.Env
	Logger   zerolog.Logger
	Database *sqlx.DB

	// models
	App *app.App
}

const (
	// MessageRequestStop - engine will stop and set status to stopped
	MessageRequestStop = "request-stop"
	// MessageRequestPause - engine will stop and set status to starting
	MessageRequestPause = "request-pause"
	// MessageRequestPing -
	MessageRequestPing = "request-ping"
	// MessageResponseStopping -
	MessageResponseStopping = "response-stopping"
	// MessageResponsePausing -
	MessageResponsePausing = "response-pausing"
	// MessageResponsePong -
	MessageResponsePong = "response-pong"
	// MessageResponseError -
	MessageResponseError = "response-error"

	// RunSpeedDefault -
	RunSpeedDefault = 250
	// RunSpeedSlowest -
	RunSpeedSlowest = 10000
	// RunSpeedSlower -
	RunSpeedSlower = 5000
	// RunSpeedFaster -
	RunSpeedFaster = 1000
	// RunSpeedEvenFaster -
	RunSpeedEvenFaster = 500
	// RunSpeedFastest -
	RunSpeedFastest = 10

	// RunTimesOnce -
	RunTimesOnce = 1
	// RunTimesTen -
	RunTimesTen = 10
	// RunTimesForever -
	RunTimesForever = 0
)

// Message -
type Message struct {
	Message string
}

const (
	// PackageName -
	PackageName string = "engine/runner"
)

// NewRunner -
func NewRunner(
	e env.Env,
	l zerolog.Logger,
	d *sqlx.DB,
	appID string,
	toRunnerChan chan *Message,
	fromRunnerChan chan *Message,
	runTimes int,
	runSpeed int) (rnr *Runner, err error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	rnr = &Runner{
		AppID:          appID,
		Env:            e,
		Logger:         l,
		Database:       d,
		ToRunnerChan:   toRunnerChan,
		FromRunnerChan: fromRunnerChan,
		RunTimes:       runTimes,
		RunSpeed:       runSpeed,
	}

	err = rnr.Init()
	if err != nil {
		l.Warn().Msgf("NewRunner failed to init: %v", err)
		return nil, err
	}

	return rnr, nil
}

// Init -
func (rnr *Runner) Init() (err error) {

	l := rnr.Logger.With().Str("function", "Init").Logger()

	// default run speed
	if rnr.RunSpeed == 0 {
		rnr.RunSpeed = RunSpeedDefault
	}

	// init app
	err = rnr.InitApp()
	if err != nil {
		l.Warn().Msgf("Init failed init app >%v<", err)
		return err
	}

	return nil
}

// InitApp -
func (rnr *Runner) InitApp() (err error) {

	l := rnr.Logger.With().Str("function", "InitApp").Logger()

	// instantiate without a transaction
	rs, err := repostore.NewRepoStore(rnr.Env, rnr.Logger, nil)
	if err != nil {
		l.Warn().Msgf("InitApp failed new repo store >%v<", err)
		return err
	}

	app, err := app.NewApp(rnr.Env, rnr.Logger, rs, rnr.AppID)
	if err != nil {
		l.Warn().Msgf("InitApp failed new app >%v<", err)
		return err
	}

	rnr.App = app

	return nil
}

// Run -
func (rnr *Runner) Run() (err error) {

	l := rnr.Logger.With().Str("function", "Run").Logger()

	l.Debug().Msgf("App ID %s running", rnr.AppID)

	handleError := func(tx *sqlx.Tx, err error) error {

		l.Warn().Msgf("Failed with error >%v<", err)

		// rollback
		if tx != nil {
			tx.Rollback()
		}

		rnr.FromRunnerChan <- &Message{Message: MessageResponseError}

		return err
	}

	loopCount := 0

loop:
	for {

		l.Debug().Msgf("=> Running App ID >%s< loopCount >%d< <=", rnr.AppID, loopCount)

		select {
		case msg := <-rnr.ToRunnerChan:

			l.Debug().Msgf("App ID %s received message %v", rnr.AppID, msg)

			if msg.Message == MessageRequestStop {

				// reply with shutting down
				l.Debug().Msgf("App ID %s replying with stopping message", rnr.AppID)

				rnr.FromRunnerChan <- &Message{Message: MessageResponseStopping}

				l.Debug().Msgf("Reply stopping sent")

				break loop

			} else if msg.Message == MessageRequestPause {

				// reply with pausing
				l.Debug().Msgf("App ID %s replying with pausing message", rnr.AppID)

				rnr.FromRunnerChan <- &Message{Message: MessageResponsePausing}

				l.Debug().Msgf("Reply pausing sent")

				break loop

			} else if msg.Message == MessageRequestPing {

				// reply with pong
				l.Debug().Msgf("App ID %s replying with pong message", rnr.AppID)

				rnr.FromRunnerChan <- &Message{Message: MessageResponsePong}

				l.Debug().Msgf("Reply pong sent")

			} else {

				// unknown message
				l.Debug().Msgf("App ID %s received unknown message", rnr.AppID)
			}

		default:
			// no-op
		}

		// run forever or runTimes
		if rnr.RunTimes != 0 {
			if loopCount == rnr.RunTimes {

				// notify pausing
				l.Debug().Msgf("App ID >%s< RunTimes >%d< loopCount >%d< notifying stopping message", rnr.AppID, rnr.RunTimes, loopCount)

				rnr.FromRunnerChan <- &Message{Message: MessageResponseStopping}

				l.Debug().Msgf("Notify stop sent")
				break loop
			}
			loopCount++
		}

		// tx
		tx, err := rnr.Database.Beginx()
		if err != nil {
			return handleError(tx, err)
		}

		// NOTE: initialise with a new transaction per run
		//       loop so if there's an error we only lose a
		//       single turn
		err = rnr.App.Init(tx)
		if err != nil {
			return handleError(tx, err)
		}

		// app run
		err = rnr.App.Run()
		if err != nil {
			return handleError(tx, err)
		}

		// app save
		err = rnr.App.Save()
		if err != nil {
			return handleError(tx, err)
		}

		// commit
		err = tx.Commit()
		if err != nil {
			return handleError(tx, err)
		}

		l.Debug().Msgf("=> Running App ID >%s< loopCount >%d< done <=", rnr.AppID, loopCount)

		time.Sleep(time.Duration(rnr.RunSpeed) * time.Millisecond)
	}

	// done
	l.Debug().Msgf("App ID %s shutdown", rnr.AppID)

	return nil
}

// Cleanup - Removes all app instance data which isn't a good idea
// if there are fights that are not done etc. This function is
// used for testing purposes at this stage.
func (rnr *Runner) Cleanup() (err error) {

	l := rnr.Logger.With().Str("function", "Cleanup").Logger()

	l.Debug().Msgf("Cleanup engine cleaning")

	// tx
	tx, _ := rnr.Database.Beginx()

	err = rnr.App.Cleanup(tx)
	if err != nil {
		l.Warn().Msgf("Cleanup failed app cleanup >%v<", err)

		// rollback
		tx.Rollback()

		return err
	}

	tx.Commit()

	return nil
}

package engine

import (
	"testing"

	"github.com/stretchr/testify/assert"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/engine/runner"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func TestNewEngine(t *testing.T) {

	s, err := NewEngine(e, l, db)
	assert.NotNil(t, s, "New server is not nil")
	assert.NoError(t, err, "New server created without error")
}

func TestRun(t *testing.T) {

	s, err := NewEngine(e, l, db)
	assert.NotNil(t, s, "New server is not nil")
	assert.NoError(t, err, "New server created without error")

	err = s.Run("", 1, 1)
	assert.NoError(t, err, "Server ran once without error")
}

func TestStartApps(t *testing.T) {

	s, _ := NewEngine(e, l, db)
	assert.NoError(t, s.startApps("", runner.RunTimesOnce, runner.RunSpeedDefault), "Calling startApps does not result in error")
}

func TestStopApps(t *testing.T) {

	s, _ := NewEngine(e, l, db)
	assert.NoError(t, s.stopApps(true), "Calling stopApps does not result in error")
}

func TestSendAppMessages(t *testing.T) {

	s, _ := NewEngine(e, l, db)
	assert.NoError(t, s.sendAppMessages(), "Calling sendAppMessages does not result in error")
}

func TestReceiveAppMessages(t *testing.T) {

	s, _ := NewEngine(e, l, db)
	assert.NoError(t, s.receiveAppMessages(), "Calling receiveAppMessages does not result in error")
}

package fault

import "fmt"

// SystemError -
type SystemError struct {
	Type   string
	Detail string
}

// Error -
func (se SystemError) Error() string {
	return fmt.Sprintf("Error: %s", se.Detail)
}

// NewSystemError -
func NewSystemError(detail string) error {
	return SystemError{
		Type:   FaultTypeSystem,
		Detail: detail,
	}
}

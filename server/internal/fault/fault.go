package fault

const (
	// FaultTypeValidation -
	FaultTypeValidation string = "Validation"
	// FaultTypeModel -
	FaultTypeModel string = "Model"
	// FaultTypeSystem -
	FaultTypeSystem string = "System"
	// FaultTypeNotFound -
	FaultTypeNotFound string = "NotFound"
)

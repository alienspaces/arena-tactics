package fault

import "fmt"

// ValidationError -
type ValidationError struct {
	Type   string
	Detail string
}

// Error -
func (ve ValidationError) Error() string {
	return fmt.Sprintf("%s", ve.Detail)
}

// NewValidationError -
func NewValidationError(detail string) error {
	return ValidationError{
		Type:   FaultTypeValidation,
		Detail: detail,
	}
}

package fault

import "fmt"

// ModelError -
type ModelError struct {
	Type   string
	Detail string
}

// Error -
func (me ModelError) Error() string {
	return fmt.Sprintf("Error: %s", me.Detail)
}

// NewModelError -
func NewModelError(detail string) error {
	return ModelError{
		Type:   FaultTypeModel,
		Detail: detail,
	}
}

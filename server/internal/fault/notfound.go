package fault

import "fmt"

// NotFoundError -
type NotFoundError struct {
	Type   string
	Detail string
}

// Error -
func (se NotFoundError) Error() string {
	return fmt.Sprintf("Error: %s", se.Detail)
}

// NewNotFoundError -
func NewNotFoundError(detail string) error {
	return NotFoundError{
		Type:   FaultTypeNotFound,
		Detail: detail,
	}
}

package instance

// FightData - Instance json data
type FightData struct {
	AppFightID string `json:"app_fight_id"`

	// Entities -
	Entities map[string]*EntityData `json:"entities"`

	// TacticEvents - contains events that occurred this turn
	TacticEvents []*TacticEvent `json:"tactic_events"`

	// EffectEvents -
	EffectEvents []*EffectEvent `json:"effect_events"`
}

// EntityData - Instance json data
type EntityData struct {

	// ID
	AppEntityID string `json:"app_entity_id"`
	Name        string `json:"name"`

	// Attributes - contains current attributes state
	Attributes map[string]*EntityAttributeData `json:"attributes"`

	// Effects - contains current effects state
	Effects map[string]*EntityEffectData `json:"effects"`

	// Other properties
	Dead bool `json:"dead"`
}

// EntityAttributeData -
type EntityAttributeData struct {
	AppEntityAttributeID string `json:"app_entity_attribute_id"`
	Name                 string `json:"name"`
	AssignedValue        int    `json:"assigned_value"`
	CalculatedValue      int    `json:"calculated_value"`
	MinValue             int    `json:"min_value"`
	MaxValue             int    `json:"max_value"`
	AdjustedValue        int    `json:"adjusted_value"`
	AdjustmentTotal      int    `json:"adjustment_total"`
}

// EntityEffectData -
type EntityEffectData struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Type        string `json:"type"`
	Duration    int    `json:"duration"`
	Recurring   bool   `json:"recurring"`
	Permanent   bool   `json:"permanent"`
	Adjustments []*EntityEffectAdjustmentData
}

// EntityEffectAdjustmentData -
type EntityEffectAdjustmentData struct {
	AppEffectAttributeID string `json:"app_effect_attribute_id"`
	Value                int    `json:"value"`
	AppliedValue         int    `json:"applied_value"`
}

// TacticEvent - Describes a tactic event
type TacticEvent struct {

	// ID unique
	ID string `json:"id"`

	// source
	SourceAppEntityID string `json:"source_entity_id"`
	SourceName        string `json:"source_entity_name"`

	// target
	TargetAppEntityID string `json:"target_entity_id"`
	TargetName        string `json:"target_entity_name"`

	// action
	Action string `json:"action"`

	// item or skill effect that was applied
	AppSkillID string `json:"app_skill_id"`
	SkillName  string `json:"skill_name"`
	AppItemID  string `json:"app_item_id"`
	ItemName   string `json:"item_name"`

	// item or skill narrative text
	NarrativeText string `json:"narrative_text"`

	// effects events that occurred as a result of this tactic event
	EffectEvents []*EffectEvent `json:"effect_events"`
}

// EffectEvent - Describes an effect event
type EffectEvent struct {

	// target
	AppEntityID string `json:"target_entity_id"`
	Name        string `json:"target_entity_name"`

	// effect
	AppEffectID string `json:"app_effect_id"`
	EffectName  string `json:"effect_name"`

	// attribute
	AppAttributeID string `json:"app_attribute_id"`
	AttributeName  string `json:"attribute_name"`

	// adjustment value that was applied
	AdjustmentValue int `json:"adjustment_value"`

	// effect narrative text
	NarrativeText string `json:"narrative_text"`
}

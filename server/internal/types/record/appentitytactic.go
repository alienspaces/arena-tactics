package record

import (
	"database/sql"
)

const (
	// TargetSelf -
	TargetSelf string = "self"
	// TargetAnyFriend -
	TargetAnyFriend string = "any-friend"
	// TargetCurrentFriend -
	TargetCurrentFriend string = "current-friend"
	// TargetAnyFoe -
	TargetAnyFoe string = "any-foe"
	// TargetCurrentFoe -
	TargetCurrentFoe string = "current-foe"

	// EntityTacticComparatorLessThan -
	EntityTacticComparatorLessThan string = "is-less-than"
	// EntityTacticComparatorGreaterThan -
	EntityTacticComparatorGreaterThan string = "is-greater-than"
	// EntityTacticComparatorLeast -
	EntityTacticComparatorLeast string = "is-least"
	// EntityTacticComparatorGreatest -
	EntityTacticComparatorGreatest string = "is-greatest"

	// EntityTacticActionSwitchTarget -
	EntityTacticActionSwitchTarget string = "switch-target"
	// EntityTacticActionUseSkill -
	EntityTacticActionUseSkill string = "use-skill"
	// EntityTacticActionUseItem -
	EntityTacticActionUseItem string = "use-item"

	// EntityTacticActionDoNothing - not a configurable tactic action, just
	// something that occasionally happens
	EntityTacticActionDoNothing string = "do-nothing"

	// EntityTacticStatusValid -
	EntityTacticStatusValid string = "valid"
	// EntityTacticStatusInvalid -
	EntityTacticStatusInvalid string = "invalid"
)

// AppEntityTacticRecord -
type AppEntityTacticRecord struct {
	ID             string         `db:"id"`
	AppEntityID    string         `db:"app_entity_id"`
	AppAttributeID string         `db:"app_attribute_id"`
	Target         string         `db:"target"`     // Target
	Comparator     string         `db:"comparator"` // EntityTacticComparator
	Value          int            `db:"value"`
	Action         string         `db:"action"` // EntityTacticAction
	AppSkillID     sql.NullString `db:"app_skill_id"`
	AppItemID      sql.NullString `db:"app_item_id"`
	Order          int            `db:"order"`
	Status         string         `db:"status"` // EntityTacticStatus
	CreatedAt      string         `db:"created_at"`
	UpdatedAt      sql.NullString `db:"updated_at"`
	DeletedAt      sql.NullString `db:"deleted_at"`
}

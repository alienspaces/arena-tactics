package record

import (
	"database/sql"
)

// AppEntityAttributeRecord -
type AppEntityAttributeRecord struct {
	ID             string         `db:"id"`
	AppEntityID    string         `db:"app_entity_id"`
	AppAttributeID string         `db:"app_attribute_id"`
	Value          int            `db:"value"`
	CreatedAt      string         `db:"created_at"`
	UpdatedAt      sql.NullString `db:"updated_at"`
	DeletedAt      sql.NullString `db:"deleted_at"`
}

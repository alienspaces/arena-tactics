package record

import (
	"database/sql"
)

// AppFightInstanceTurnRecord -
type AppFightInstanceTurnRecord struct {
	ID                 string         `db:"id"`
	AppFightID         string         `db:"app_fight_id"`
	AppFightInstanceID string         `db:"app_fight_instance_id"`
	Turn               int            `db:"turn"`
	Data               []byte         `db:"data"`
	CreatedAt          string         `db:"created_at"`
	UpdatedAt          sql.NullString `db:"updated_at"`
	DeletedAt          sql.NullString `db:"deleted_at"`
}

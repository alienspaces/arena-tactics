package record

import (
	"database/sql"
)

// AppItemEffectRecord -
type AppItemEffectRecord struct {
	ID          string         `db:"id"`
	AppItemID   string         `db:"app_item_id"`
	AppEffectID string         `db:"app_effect_id"`
	CreatedAt   string         `db:"created_at"`
	UpdatedAt   sql.NullString `db:"updated_at"`
	DeletedAt   sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// AppEffectRecord -
type AppEffectRecord struct {
	ID            string         `db:"id"`
	AppID         string         `db:"app_id"`
	Name          string         `db:"name"`
	NarrativeText string         `db:"narrative_text"`
	EffectType    string         `db:"effect_type"` // Passive or active
	Duration      int            `db:"duration"`    // Duration in game turns for active effects
	Recurring     bool           `db:"recurring"`   // Should the active effect be re-applied each turn
	Permanent     bool           `db:"permanent"`   // Should the final total effect value be removed after duration
	CreatedAt     string         `db:"created_at"`
	UpdatedAt     sql.NullString `db:"updated_at"`
	DeletedAt     sql.NullString `db:"deleted_at"`
}

const (
	// EffectTypePassive `passive`
	// The following attributes are ignored
	//   duration  = ignored, forever
	//   recurring = ignored, false
	//   permanent = ignored
	EffectTypePassive string = "passive"
	// EffectTypeActive `active`
	// No attributes are ignored so the following rules apply
	//   duration  = the number of turns the effect remains applied
	//   recurring = whether the effect value accumulates every turn
	//   permanent = whether the total effect value is removed after duration
	EffectTypeActive string = "active"
)

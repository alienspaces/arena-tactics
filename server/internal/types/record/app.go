package record

import "database/sql"

// AppRecord -
type AppRecord struct {
	ID                string         `db:"id"`
	Name              string         `db:"name"`
	Description       string         `db:"description"`
	InitiativeFormula string         `db:"initiative_formula"` // Formula that should return an int
	DeathFormula      string         `db:"death_formula"`      // Formula that should return a bool
	Status            string         `db:"status"`
	CreatedAt         string         `db:"created_at"`
	UpdatedAt         sql.NullString `db:"updated_at"`
	DeletedAt         sql.NullString `db:"deleted_at"`
}

const (
	// AppStatusStopped -
	AppStatusStopped string = "stopped"
	// AppStatusStarting -
	AppStatusStarting string = "starting"
	// AppStatusStopping -
	AppStatusStopping string = "stopping"
	// AppStatusRunning -
	AppStatusRunning string = "running"
)

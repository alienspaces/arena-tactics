package record

import (
	"database/sql"
)

// AppEntityGroupMemberRecord -
type AppEntityGroupMemberRecord struct {
	ID               string         `db:"id"`
	AppEntityGroupID string         `db:"app_entity_group_id"`
	AppEntityID      string         `db:"app_entity_id"`
	CreatedAt        string         `db:"created_at"`
	UpdatedAt        sql.NullString `db:"updated_at"`
	DeletedAt        sql.NullString `db:"deleted_at"`
}

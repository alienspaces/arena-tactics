package record

import (
	"database/sql"
)

// AppFightInstanceRecord -
type AppFightInstanceRecord struct {
	ID         string         `db:"id"`
	AppFightID string         `db:"app_fight_id"`
	Status     string         `db:"status"`
	Turn       int            `db:"turn"`
	Data       []byte         `db:"data"`
	CreatedAt  string         `db:"created_at"`
	UpdatedAt  sql.NullString `db:"updated_at"`
	DeletedAt  sql.NullString `db:"deleted_at"`
}

const (
	// AppFightInstanceStatusFighting -
	AppFightInstanceStatusFighting string = "fighting"
	// AppFightInstanceStatusDone -
	AppFightInstanceStatusDone string = "done"
)

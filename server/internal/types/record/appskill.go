package record

import (
	"database/sql"
)

const (
	// SkillTypePassive -
	SkillTypePassive string = "passive"
	// SkillTypeActive -
	SkillTypeActive string = "active"
)

// AppSkillRecord -
type AppSkillRecord struct {
	ID            string         `db:"id"`
	AppID         string         `db:"app_id"`
	Name          string         `db:"name"`
	NarrativeText string         `db:"narrative_text"`
	SkillType     string         `db:"skill_type"`
	CreatedAt     string         `db:"created_at"`
	UpdatedAt     sql.NullString `db:"updated_at"`
	DeletedAt     sql.NullString `db:"deleted_at"`
}

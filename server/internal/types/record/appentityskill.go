package record

import (
	"database/sql"
)

// AppEntitySkillRecord -
type AppEntitySkillRecord struct {
	ID          string         `db:"id"`
	AppEntityID string         `db:"app_entity_id"`
	AppSkillID  string         `db:"app_skill_id"`
	CreatedAt   string         `db:"created_at"`
	UpdatedAt   sql.NullString `db:"updated_at"`
	DeletedAt   sql.NullString `db:"deleted_at"`
}

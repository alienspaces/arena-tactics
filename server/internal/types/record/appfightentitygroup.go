package record

import (
	"database/sql"
)

// AppFightEntityGroupRecord -
type AppFightEntityGroupRecord struct {
	ID               string         `db:"id"`
	AppFightID       string         `db:"app_fight_id"`
	AppEntityGroupID string         `db:"app_entity_group_id"`
	CreatedAt        string         `db:"created_at"`
	UpdatedAt        sql.NullString `db:"updated_at"`
	DeletedAt        sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// AppEntityItemRecord -
type AppEntityItemRecord struct {
	ID          string         `db:"id"`
	AppEntityID string         `db:"app_entity_id"`
	AppItemID   string         `db:"app_item_id"`
	CreatedAt   string         `db:"created_at"`
	UpdatedAt   sql.NullString `db:"updated_at"`
	DeletedAt   sql.NullString `db:"deleted_at"`
}

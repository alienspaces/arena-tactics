package record

import (
	"database/sql"
)

// AppSkillEffectRecord -
type AppSkillEffectRecord struct {
	ID          string         `db:"id"`
	AppSkillID  string         `db:"app_skill_id"`
	AppEffectID string         `db:"app_effect_id"`
	CreatedAt   string         `db:"created_at"`
	UpdatedAt   sql.NullString `db:"updated_at"`
	DeletedAt   sql.NullString `db:"deleted_at"`
}

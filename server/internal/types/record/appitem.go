package record

import (
	"database/sql"
)

const (
	// ItemTypeWeapon -
	ItemTypeWeapon string = "weapon"
	// ItemTypeArmour -
	ItemTypeArmour string = "armour"
	// ItemTypeJewellery -
	ItemTypeJewellery string = "jewellery"

	// ItemLocationHead -
	ItemLocationHead string = "head"
	// ItemLocationEar -
	ItemLocationEar string = "ear"
	// ItemLocationNeck -
	ItemLocationNeck string = "neck"
	// ItemLocationTorso -
	ItemLocationTorso string = "torso"
	// ItemLocationHand -
	ItemLocationHand string = "hand"
	// ItemLocationLeg -
	ItemLocationLeg string = "leg"
	// ItemLocationFoot -
	ItemLocationFoot string = "foot"
	// ItemLocationFinger -
	ItemLocationFinger string = "finger"
)

// AppItemRecord -
type AppItemRecord struct {
	ID            string         `db:"id"`
	AppID         string         `db:"app_id"`
	Name          string         `db:"name"`
	NarrativeText string         `db:"narrative_text"`
	ItemType      string         `db:"item_type"`
	ItemLocation  string         `db:"item_location"`
	CreatedAt     string         `db:"created_at"`
	UpdatedAt     sql.NullString `db:"updated_at"`
	DeletedAt     sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// TemplateRecord -
type TemplateRecord struct {
	ID        string         `db:"id"`
	Name      string         `db:"name"`
	CreatedAt string         `db:"created_at"`
	UpdatedAt sql.NullString `db:"updated_at"`
	DeletedAt sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// AppAttributeRecord -
type AppAttributeRecord struct {
	ID              string         `db:"id"`
	AppID           string         `db:"app_id"`
	Name            string         `db:"name"`
	ValueFormula    string         `db:"value_formula"`
	MaxValueFormula string         `db:"max_value_formula"`
	MinValueFormula string         `db:"min_value_formula"`
	Assignable      bool           `db:"assignable"`
	CreatedAt       string         `db:"created_at"`
	UpdatedAt       sql.NullString `db:"updated_at"`
	DeletedAt       sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// AppInstanceRecord -
type AppInstanceRecord struct {
	ID        string         `db:"id"`
	AppID     string         `db:"app_id"`
	Data      []byte         `db:"data"`
	CreatedAt string         `db:"created_at"`
	UpdatedAt sql.NullString `db:"updated_at"`
	DeletedAt sql.NullString `db:"deleted_at"`
}

// AppInstanceData - Instance json data
type AppInstanceData struct {
	AppID string `json:"id"`
}

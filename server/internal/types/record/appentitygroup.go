package record

import (
	"database/sql"
)

// AppEntityGroupRecord -
type AppEntityGroupRecord struct {
	ID        string         `db:"id"`
	AppID     string         `db:"app_id"`
	Name      string         `db:"name"`
	CreatedAt string         `db:"created_at"`
	UpdatedAt sql.NullString `db:"updated_at"`
	DeletedAt sql.NullString `db:"deleted_at"`
}

package record

import (
	"database/sql"
)

// AppEffectAttributeRecord -
type AppEffectAttributeRecord struct {
	ID              string         `db:"id"`
	AppEffectID     string         `db:"app_effect_id"`
	AppAttributeID  string         `db:"app_attribute_id"`
	Target          string         `db:"target"`           // self, any-friend, current-friend, any-foe, current-foe
	ApplyConstraint string         `db:"apply_constraint"` // maximum, calculated
	ValueFormula    string         `db:"value_formula"`
	CreatedAt       string         `db:"created_at"`
	UpdatedAt       sql.NullString `db:"updated_at"`
	DeletedAt       sql.NullString `db:"deleted_at"`
}

const (
	// ApplyConstraintMaximum - cannot apply if current attribute adjusted amount >= attribute maximum value
	ApplyConstraintMaximum string = "maximum"
	// ApplyConstraintCalculated - cannot apply if current attribute adjusted amount >= attribute calculated value
	ApplyConstraintCalculated string = "calculated"
)

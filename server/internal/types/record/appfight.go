package record

import (
	"database/sql"
)

// AppFightRecord -
type AppFightRecord struct {
	ID        string         `db:"id"`
	AppID     string         `db:"app_id"`
	Status    string         `db:"status"`
	CreatedAt string         `db:"created_at"`
	UpdatedAt sql.NullString `db:"updated_at"`
	DeletedAt sql.NullString `db:"deleted_at"`
}

const (
	// AppFightStatusWaiting - waiting for groups
	AppFightStatusWaiting string = "waiting"
	// AppFightStatusStarting -
	AppFightStatusStarting string = "starting"
	// AppFightStatusFighting -
	AppFightStatusFighting string = "fighting"
	// AppFightStatusDone -
	AppFightStatusDone string = "done"
)

package repostore

import (
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// TemplateRepo -
type TemplateRepo interface {
	NewRecord() *record.TemplateRecord
	NewRecordArray() []*record.TemplateRecord
	GetByID(id string) (*record.TemplateRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.TemplateRecord, error)
	Create(rec *record.TemplateRecord) error
	Update(rec *record.TemplateRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord() (*record.TemplateRecord, error)
}

// AppRepo -
type AppRepo interface {
	NewRecord() *record.AppRecord
	NewRecordArray() []*record.AppRecord
	GetByID(id string) (*record.AppRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppRecord, error)
	Create(rec *record.AppRecord) error
	Update(rec *record.AppRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord() (*record.AppRecord, error)
}

// AppEntityRepo -
type AppEntityRepo interface {
	NewRecord() *record.AppEntityRecord
	NewRecordArray() []*record.AppEntityRecord
	GetByID(id string) (*record.AppEntityRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityRecord, error)
	Create(rec *record.AppEntityRecord) error
	Update(rec *record.AppEntityRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID, entityName string) (*record.AppEntityRecord, error)
}

// AppAttributeRepo -
type AppAttributeRepo interface {
	NewRecord() *record.AppAttributeRecord
	NewRecordArray() []*record.AppAttributeRecord
	GetByID(id string) (*record.AppAttributeRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppAttributeRecord, error)
	Create(rec *record.AppAttributeRecord) error
	Update(rec *record.AppAttributeRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID, name, valueFormula, minValueFormula, maxValueFormula string, assignable bool) (*record.AppAttributeRecord, error)
}

// AppEffectRepo - interface definition
type AppEffectRepo interface {
	NewRecord() *record.AppEffectRecord
	NewRecordArray() []*record.AppEffectRecord
	GetByID(id string) (*record.AppEffectRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEffectRecord, error)
	Create(rec *record.AppEffectRecord) error
	Update(rec *record.AppEffectRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID, effectName, narrativeText, effectType string, effectDuration int, effectRecurring, effectPermanent bool) (*record.AppEffectRecord, error)
}

// AppEffectAttributeRepo - interface definition
type AppEffectAttributeRepo interface {
	NewRecord() *record.AppEffectAttributeRecord
	NewRecordArray() []*record.AppEffectAttributeRecord
	GetByID(id string) (*record.AppEffectAttributeRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEffectAttributeRecord, error)
	Create(rec *record.AppEffectAttributeRecord) error
	Update(rec *record.AppEffectAttributeRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appEffectID, appAttributeID, effectTarget, applyConstraint, effectFormula string) (*record.AppEffectAttributeRecord, error)
}

// AppEntityAttributeRepo -
type AppEntityAttributeRepo interface {
	NewRecord() *record.AppEntityAttributeRecord
	NewRecordArray() []*record.AppEntityAttributeRecord
	GetByID(id string) (*record.AppEntityAttributeRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityAttributeRecord, error)
	Create(rec *record.AppEntityAttributeRecord) error
	Update(rec *record.AppEntityAttributeRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appEntityID, appAttributeID string, value *int) (*record.AppEntityAttributeRecord, error)
}

// AppEntityGroupRepo -
type AppEntityGroupRepo interface {
	NewRecord() *record.AppEntityGroupRecord
	NewRecordArray() []*record.AppEntityGroupRecord
	GetByID(id string) (*record.AppEntityGroupRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityGroupRecord, error)
	Create(rec *record.AppEntityGroupRecord) error
	Update(rec *record.AppEntityGroupRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID string) (*record.AppEntityGroupRecord, error)
}

// AppEntityGroupMemberRepo -
type AppEntityGroupMemberRepo interface {
	NewRecord() *record.AppEntityGroupMemberRecord
	NewRecordArray() []*record.AppEntityGroupMemberRecord
	GetByID(id string) (*record.AppEntityGroupMemberRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityGroupMemberRecord, error)
	Create(rec *record.AppEntityGroupMemberRecord) error
	Update(rec *record.AppEntityGroupMemberRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appEntityGroupID, appEntityID string) (*record.AppEntityGroupMemberRecord, error)
}

// AppEntityItemRepo -
type AppEntityItemRepo interface {
	NewRecord() *record.AppEntityItemRecord
	NewRecordArray() []*record.AppEntityItemRecord
	GetByID(id string) (*record.AppEntityItemRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityItemRecord, error)
	Create(rec *record.AppEntityItemRecord) error
	Update(rec *record.AppEntityItemRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appEntityID string, appItemID string) (*record.AppEntityItemRecord, error)
}

// AppEntitySkillRepo -
type AppEntitySkillRepo interface {
	NewRecord() *record.AppEntitySkillRecord
	NewRecordArray() []*record.AppEntitySkillRecord
	GetByID(id string) (*record.AppEntitySkillRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntitySkillRecord, error)
	Create(rec *record.AppEntitySkillRecord) error
	Update(rec *record.AppEntitySkillRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appEntityID, appSkillID string) (*record.AppEntitySkillRecord, error)
}

// AppEntityTacticRepo -
type AppEntityTacticRepo interface {
	NewRecord() *record.AppEntityTacticRecord
	NewRecordArray() []*record.AppEntityTacticRecord
	GetByID(id string) (*record.AppEntityTacticRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppEntityTacticRecord, error)
	Create(rec *record.AppEntityTacticRecord) error
	Update(rec *record.AppEntityTacticRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(
		appEntityID, target, appAttributeID, comparator string,
		value int,
		action, appSkillID, appItemID string,
		order int) (*record.AppEntityTacticRecord, error)
}

// AppFightRepo -
type AppFightRepo interface {
	NewRecord() *record.AppFightRecord
	NewRecordArray() []*record.AppFightRecord
	GetByID(id string) (*record.AppFightRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppFightRecord, error)
	Create(rec *record.AppFightRecord) error
	Update(rec *record.AppFightRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID string) (*record.AppFightRecord, error)
}

// AppFightInstanceRepo -
type AppFightInstanceRepo interface {
	NewRecord() *record.AppFightInstanceRecord
	NewRecordArray() []*record.AppFightInstanceRecord
	GetByID(id string) (*record.AppFightInstanceRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppFightInstanceRecord, error)
	Create(rec *record.AppFightInstanceRecord) error
	Update(rec *record.AppFightInstanceRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appFightID string) (*record.AppFightInstanceRecord, error)
}

// AppFightInstanceTurnRepo -
type AppFightInstanceTurnRepo interface {
	NewRecord() *record.AppFightInstanceTurnRecord
	NewRecordArray() []*record.AppFightInstanceTurnRecord
	GetByID(id string) (*record.AppFightInstanceTurnRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppFightInstanceTurnRecord, error)
	Create(rec *record.AppFightInstanceTurnRecord) error
	Update(rec *record.AppFightInstanceTurnRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appFightID, appFightInstanceID string, turn int) (*record.AppFightInstanceTurnRecord, error)
}

// AppInstanceRepo -
type AppInstanceRepo interface {
	NewRecord() *record.AppInstanceRecord
	NewRecordArray() []*record.AppInstanceRecord
	GetByID(id string) (*record.AppInstanceRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppInstanceRecord, error)
	Create(rec *record.AppInstanceRecord) error
	Update(rec *record.AppInstanceRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID string) (*record.AppInstanceRecord, error)
}

// AppItemRepo -
type AppItemRepo interface {
	NewRecord() *record.AppItemRecord
	NewRecordArray() []*record.AppItemRecord
	GetByID(id string) (*record.AppItemRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppItemRecord, error)
	Create(rec *record.AppItemRecord) error
	Update(rec *record.AppItemRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID, itemName, narrativeText, itemType, itemLocation string) (*record.AppItemRecord, error)
}

// AppSkillRepo -
type AppSkillRepo interface {
	NewRecord() *record.AppSkillRecord
	NewRecordArray() []*record.AppSkillRecord
	GetByID(id string) (*record.AppSkillRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppSkillRecord, error)
	Create(rec *record.AppSkillRecord) error
	Update(rec *record.AppSkillRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appID, skillName, narrativeText, skillType string) (*record.AppSkillRecord, error)
}

// AppFightEntityGroupRepo -
type AppFightEntityGroupRepo interface {
	NewRecord() *record.AppFightEntityGroupRecord
	NewRecordArray() []*record.AppFightEntityGroupRecord
	GetByID(id string) (*record.AppFightEntityGroupRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppFightEntityGroupRecord, error)
	Create(rec *record.AppFightEntityGroupRecord) error
	Update(rec *record.AppFightEntityGroupRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appFightID, appEntityGroupID string) (*record.AppFightEntityGroupRecord, error)
}

// AppSkillEffectRepo -
type AppSkillEffectRepo interface {
	NewRecord() *record.AppSkillEffectRecord
	NewRecordArray() []*record.AppSkillEffectRecord
	GetByID(id string) (*record.AppSkillEffectRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppSkillEffectRecord, error)
	Create(rec *record.AppSkillEffectRecord) error
	Update(rec *record.AppSkillEffectRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appSkillID, appEffectID string) (*record.AppSkillEffectRecord, error)
}

// AppItemEffectRepo -
type AppItemEffectRepo interface {
	NewRecord() *record.AppItemEffectRecord
	NewRecordArray() []*record.AppItemEffectRecord
	GetByID(id string) (*record.AppItemEffectRecord, error)
	GetByParam(params map[string]interface{}) ([]*record.AppItemEffectRecord, error)
	Create(rec *record.AppItemEffectRecord) error
	Update(rec *record.AppItemEffectRecord) error
	Delete(id string) error
	Remove(id string) error

	// test record
	CreateTestRecord(appItemID, appEffectID string) (*record.AppItemEffectRecord, error)
}

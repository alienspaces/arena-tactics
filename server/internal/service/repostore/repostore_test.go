package repostore

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

func TestRepoMap(t *testing.T) {

	// set test
	os.Setenv("APP_ENV", "TEST")

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, _ := env.NewEnv()

		// logger
		l, _ := logger.NewLogger(e)

		// database
		db, _ := database.NewDatabase(e, l)

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new transactiob %v", err)
		}

		// repostore
		ms, err := NewRepoStore(e, l, tx)
		assert.Nil(t, err, "RepoStore initialised without error")
		assert.NotNil(t, ms, "RepoStore is not nil")

		// get template repo
		tm := ms.TemplateRepo
		assert.NotNil(t, tm, "TemplateRepo is not nil")

		tx.Rollback()
	}
}

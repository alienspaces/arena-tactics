// Package repostore provides methods for interacting with the database
package repostore

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffectattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroupmember"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitytactic"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightinstance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightinstanceturn"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appinstance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitemeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskilleffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/template"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// RepoStore - contains a map of repo structures
type RepoStore struct {
	Env    env.Env
	Logger zerolog.Logger
	Tx     *sqlx.Tx

	// repos
	TemplateRepo TemplateRepo

	AppAttributeRepo            AppAttributeRepo
	AppEffectAttributeRepo      AppEffectAttributeRepo
	AppEffectRepo               AppEffectRepo
	AppEntityAttributeRepo      AppEntityAttributeRepo
	AppEntityGroupMemberRepo    AppEntityGroupMemberRepo
	AppEntityGroupRepo          AppEntityGroupRepo
	AppEntityItemRepo           AppEntityItemRepo
	AppEntityRepo               AppEntityRepo
	AppEntitySkillRepo          AppEntitySkillRepo
	AppEntityTacticRepo         AppEntityTacticRepo
	AppFightEntityGroupRepo     AppFightEntityGroupRepo
	AppFightInstanceRepo        AppFightInstanceRepo
	AppFightInstanceTurnRepo AppFightInstanceTurnRepo
	AppFightRepo                AppFightRepo
	AppInstanceRepo             AppInstanceRepo
	AppItemEffectRepo           AppItemEffectRepo
	AppItemRepo                 AppItemRepo
	AppRepo                     AppRepo
	AppSkillEffectRepo          AppSkillEffectRepo
	AppSkillRepo                AppSkillRepo
}

// NewRepoStore -
func NewRepoStore(e env.Env, l zerolog.Logger, tx *sqlx.Tx) (ms *RepoStore, err error) {

	ms = &RepoStore{
		Env:    e,
		Logger: l,
		Tx:     tx,
	}

	// if given a transaction initialize immediately
	if tx != nil {
		err = ms.Init(tx)
	}

	return ms, err
}

// Init - initialises all repos with a new transaction
func (m *RepoStore) Init(tx *sqlx.Tx) (err error) {

	m.Logger.Debug().Msg("Initializing repos")

	if tx == nil {
		tx = m.Tx
	}

	if tx == nil {
		return fmt.Errorf("Init no active transaction, cannot initialise")
	}

	// TODO: Add a way to init this repostore with a new tx without
	// creating new repo objects every time, may be required to refactor
	// repos slightly as well..

	m.TemplateRepo, err = template.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new template repo >%v<", err)
		return err
	}

	m.AppRepo, err = app.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new app repo >%v<", err)
		return err
	}

	m.AppAttributeRepo, err = appattribute.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appattribute repo >%v<", err)
		return err
	}

	m.AppEffectRepo, err = appeffect.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appeffect repo >%v<", err)
		return err
	}

	m.AppEffectAttributeRepo, err = appeffectattribute.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appeffectattribute repo >%v<", err)
		return err
	}

	m.AppInstanceRepo, err = appinstance.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appinstance repo >%v<", err)
		return err
	}

	m.AppSkillRepo, err = appskill.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appskill repo >%v<", err)
		return err
	}

	m.AppSkillEffectRepo, err = appskilleffect.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appskilleffect repo >%v<", err)
		return err
	}

	m.AppItemRepo, err = appitem.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appitem repo >%v<", err)
		return err
	}

	m.AppItemEffectRepo, err = appitemeffect.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appitemeffect repo >%v<", err)
		return err
	}

	m.AppEntityRepo, err = appentity.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentity repo >%v<", err)
		return err
	}

	m.AppEntityAttributeRepo, err = appentityattribute.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentityattribute repo >%v<", err)
		return err
	}

	m.AppEntityItemRepo, err = appentityitem.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentityitem repo >%v<", err)
		return err
	}

	m.AppEntitySkillRepo, err = appentityskill.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentityskill repo >%v<", err)
		return err
	}

	m.AppEntityTacticRepo, err = appentitytactic.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentitytactic repo >%v<", err)
		return err
	}

	m.AppEntityGroupRepo, err = appentitygroup.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentitygroup repo >%v<", err)
		return err
	}

	m.AppEntityGroupMemberRepo, err = appentitygroupmember.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appentitygroupmember repo >%v<", err)
		return err
	}

	m.AppFightRepo, err = appfight.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appfight repo >%v<", err)
		return err
	}

	m.AppFightInstanceRepo, err = appfightinstance.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appfightinstance repo >%v<", err)
		return err
	}

	m.AppFightInstanceTurnRepo, err = appfightinstanceturn.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appfightinstanceturn repo >%v<", err)
		return err
	}

	m.AppFightEntityGroupRepo, err = appfightentitygroup.NewRepo(m.Env, m.Logger, tx)
	if err != nil {
		m.Logger.Warn().Msgf("Init failed new appfightentitygroup repo >%v<", err)
		return err
	}

	return err
}

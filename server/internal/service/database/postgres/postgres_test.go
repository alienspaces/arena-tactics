package postgres

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

func TestMysql(t *testing.T) {

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, err := env.NewEnv()
		assert.Nil(t, err, "Env initialized without error")

		// logger
		l, err := logger.NewLogger(e)
		assert.NotNil(t, l, "Logger initialized without error")

		// database
		db, err := NewDatabase(e, l)
		assert.Nil(t, err, "Database initialized without error")
		assert.NotNil(t, db, "Database connected")
	}
}

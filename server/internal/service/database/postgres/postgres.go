package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // blank import intended
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Database -
type Database struct {
	Conn   *sqlx.DB
	Env    env.Env
	Logger zerolog.Logger
}

// NewDatabase -
func NewDatabase(e env.Env, l zerolog.Logger) (*sqlx.DB, error) {

	d := &Database{
		Env:    e,
		Logger: l,
	}

	err := d.Init()
	if err != nil {
		l.Error().Msgf("Failed to initialize database %v", err)
		return nil, err
	}

	return d.Conn, err
}

// Init -
func (d *Database) Init() error {

	// env
	env := d.Env

	// log
	log := d.Logger

	var dbUser, dbPass, dbHost, dbPort, dbName string
	var err error

	if dbUser = env.Get("APP_DATABASE_USER"); dbUser == "" {
		return err
	}

	if dbPass = env.Get("APP_DATABASE_PASSWORD"); dbPass == "" {
		return err
	}

	if dbHost = env.Get("APP_DATABASE_HOST"); dbHost == "" {
		return err
	}

	if dbPort = env.Get("APP_DATABASE_PORT"); dbPort == "" {
		return err
	}

	if dbName = env.Get("APP_DATABASE_NAME"); dbName == "" {
		return err
	}

	cs := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", dbUser, dbPass, dbName, dbHost, dbPort)

	log.Info().Msgf("Connect string %s", cs)

	c, err := sqlx.Connect("postgres", cs)
	if err != nil {
		log.Error().Msgf("Failed to open db: %v", err)
		return err
	}

	d.Conn = c

	return nil
}

package database

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database/postgres"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Database provides methods to get database connection
type Database interface {
	GetConnection() *sqlx.DB
	CloseConnection()
	BeginTrx() *sqlx.Tx
}

// NewDatabase - Establishes a new database connection
func NewDatabase(e env.Env, l zerolog.Logger) (*sqlx.DB, error) {

	dt := e.Get("APP_DATABASE")
	if dt == "" {
		l.Info().Msg("Defaulting to postgres")
		dt = "postgres"
	}

	if dt == "postgres" {
		l.Info().Msg("Using postgres")
		db, err := postgres.NewDatabase(e, l)
		return db, err
	}

	return nil, fmt.Errorf("Unsuported database type %s", dt)
}

package env

import (
	"fmt"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

// Env - interface definition
type Env interface {
	CheckRequired(reqd []string) error
	Get(key string) (value string)
	Set(key string, value string)
}

// Enver - interface implementation
type Enver struct {
	env map[string]string
}

// NewEnv - Create a new environment
func NewEnv() (Env, error) {

	e := Enver{env: map[string]string{}}

	err := e.init()
	if err != nil {
		return nil, fmt.Errorf("NewEnv failed for %v", err)
	}

	return &e, nil
}

func (e *Enver) init() error {

	h := os.Getenv("APP_HOME")
	if h == "" {
		panic("APP HOME not set")
	}

	envFile := fmt.Sprintf("%s/%s", os.Getenv("APP_HOME"), ".env")
	err := godotenv.Load(envFile)
	if err != nil {
		// not fatal
		fmt.Printf("Missing local .env file %s, not loading\n", envFile)
	}

	// possible items
	envItems := []string{

		"APP_ENV",
		"APP_HOME",
		"APP_DATABASE",
		"APP_DATABASE_NAME",
		"APP_DATABASE_USER",
		"APP_DATABASE_PASSWORD",
		"APP_DATABASE_OWNER_USER",
		"APP_DATABASE_OWNER_PASSWORD",
		"APP_DATABASE_HOST",
		"APP_DATABASE_PORT",
		"APP_LOG_LEVEL",
		"APP_LOG_PRETTY",
		"APP_SERVER_PORT",
	}

	// required items
	// - Mostly everything is required to exists even if it
	//   has a bogus value at the moment
	reqItems := []string{

		"APP_ENV",
		"APP_HOME",
		"APP_LOG_LEVEL",
		"APP_SERVER_PORT",
		"APP_DATABASE_NAME",
		"APP_DATABASE_USER",
		"APP_DATABASE_PASSWORD",
		"APP_DATABASE_OWNER_USER",
		"APP_DATABASE_OWNER_PASSWORD",
		"APP_DATABASE_HOST",
		"APP_DATABASE_PORT",
	}

	for _, envItem := range envItems {
		osEnvValue := os.Getenv(envItem)

		// dont show passwords or keys
		if strings.Contains(envItem, "PASS") == false && strings.Contains(envItem, "KEY") == false {
			fmt.Printf("ENV Key %s Val %s\n", envItem, osEnvValue)
		}
		e.env[envItem] = os.Getenv(envItem)
	}

	err = e.CheckRequired(reqItems)
	if err != nil {
		panic(fmt.Errorf("Required environment variables missing %v", err))
	}

	return nil
}

// CheckRequired - Tests that the provided list of environment variabled
// have been set
func (e *Enver) CheckRequired(reqd []string) error {

	// required check
	for _, reqItem := range reqd {
		if e.env[reqItem] == "" {
			return fmt.Errorf("Could not find required env value for : %s", reqItem)
		}
	}

	return nil
}

// Get an env key
func (e *Enver) Get(key string) (value string) {
	value = e.env[key]
	return value
}

// Set an env key
func (e *Enver) Set(key string, value string) {
	e.env[key] = value
}

package env

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEnv(t *testing.T) {

	h := os.Getenv("APP_HOME")
	if assert.NotEqual(t, h, "", "APP_HOME IS not an empty string") {

		// environment
		e, err := NewEnv()

		assert.NotNil(t, e, "env is not nil")
		assert.Nil(t, err, "err is nil")

		v := e.Get("APP_DATABASE_NAME")
		assert.NotEqual(t, v, "", "Value is not an empty string")

		v = e.Get("SOME_UNKNOWN_KEY")
		assert.Equal(t, v, "", "Value is an empty string")
	}
}

package repopreparer

import (
	"sync"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

// Preparable -
type Preparable interface {
	GetName() string
	GetByIDSQL() string
	GetByParamSQL() string
	CreateSQL() string
	UpdateSQL() string
	DeleteSQL() string
	RemoveSQL() string
}

// Preparer - Methods for preparing and fetching repo statements
type Preparer struct {
	Env    env.Env
	Logger zerolog.Logger
	Tx     *sqlx.Tx
}

var getByIDSQLArr = make(map[string]string)
var getByParamSQLArr = make(map[string]string)
var createSQLArr = make(map[string]string)
var updateSQLArr = make(map[string]string)
var deleteSQLArr = make(map[string]string)
var removeSQLArr = make(map[string]string)

var getByIDStmtArr = make(map[string]*sqlx.Stmt)
var getByParamStmtArr = make(map[string]*sqlx.NamedStmt)
var createStmtArr = make(map[string]*sqlx.NamedStmt)
var updateStmtArr = make(map[string]*sqlx.NamedStmt)
var deleteStmtArr = make(map[string]*sqlx.NamedStmt)
var removeStmtArr = make(map[string]*sqlx.NamedStmt)

// prepared
var prepared = make(map[string]bool)

// mutext
var mutex = &sync.Mutex{}

const (
	// PackageName -
	PackageName string = "repopreparer"
)

// NewPreparer -
func NewPreparer(e env.Env, l zerolog.Logger, tx *sqlx.Tx) (*Preparer, error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	p := Preparer{
		Env:    e,
		Logger: l,
		Tx:     tx,
	}

	return &p, nil
}

// Prepare - Prepares all repo SQL statements for faster execution
func (p *Preparer) Prepare(m Preparable) error {

	l := p.Logger.With().Str("function", "Prepare").Logger()

	// lock/unlock
	mutex.Lock()
	defer mutex.Unlock()

	// already prepared
	if _, ok := prepared[m.GetName()]; ok {
		return nil
	}

	l.Debug().Msgf("Preparing statements >%s<", m.GetName())

	// get by id
	query := m.GetByIDSQL()

	getByIDStmt, err := p.Tx.Preparex(query)
	if err != nil {
		l.Warn().Msgf("Error preparing GetByIDSQL statement >%v<", err)
		return err
	}

	getByIDSQLArr[m.GetName()] = query
	getByIDStmtArr[m.GetName()] = getByIDStmt

	// get by param
	query = m.GetByParamSQL()

	getByParamStmt, err := p.Tx.PrepareNamed(m.GetByParamSQL())
	if err != nil {
		l.Warn().Msgf("Error preparing GetByParamSQL statement >%v<", err)
		return err
	}

	getByParamSQLArr[m.GetName()] = query
	getByParamStmtArr[m.GetName()] = getByParamStmt

	// create
	query = m.CreateSQL()

	createStmt, err := p.Tx.PrepareNamed(query)
	if err != nil {
		l.Warn().Msgf("Error preparing CreateSQL statement >%v<", err)
		return err
	}

	createSQLArr[m.GetName()] = query
	createStmtArr[m.GetName()] = createStmt

	// update
	query = m.UpdateSQL()

	updateStmt, err := p.Tx.PrepareNamed(query)
	if err != nil {
		l.Warn().Msgf("Error preparing UpdateSQL statement >%v<", err)
		return err
	}

	updateSQLArr[m.GetName()] = query
	updateStmtArr[m.GetName()] = updateStmt

	// delete
	query = m.DeleteSQL()

	deleteStmt, err := p.Tx.PrepareNamed(query)
	if err != nil {
		l.Warn().Msgf("Error preparing DeleteSQL statement >%v<", err)
		return err
	}

	deleteSQLArr[m.GetName()] = query
	deleteStmtArr[m.GetName()] = deleteStmt

	// remove
	query = m.RemoveSQL()

	removeStmt, err := p.Tx.PrepareNamed(query)
	if err != nil {
		l.Warn().Msgf("Error preparing RemoveSQL statement >%v<", err)
		return err
	}

	removeSQLArr[m.GetName()] = query
	removeStmtArr[m.GetName()] = removeStmt

	prepared[m.GetName()] = true

	return nil
}

// GetByIDStmt -
func (p *Preparer) GetByIDStmt(m Preparable) *sqlx.Stmt {

	stmt := getByIDStmtArr[m.GetName()]

	return p.Tx.Stmtx(stmt)
}

// GetByParamStmt -
func (p *Preparer) GetByParamStmt(m Preparable) *sqlx.NamedStmt {

	stmt := getByParamStmtArr[m.GetName()]

	return p.Tx.NamedStmt(stmt)
}

// CreateStmt -
func (p *Preparer) CreateStmt(m Preparable) *sqlx.NamedStmt {

	stmt := createStmtArr[m.GetName()]

	return p.Tx.NamedStmt(stmt)
}

// UpdateStmt -
func (p *Preparer) UpdateStmt(m Preparable) *sqlx.NamedStmt {

	stmt := updateStmtArr[m.GetName()]

	return p.Tx.NamedStmt(stmt)
}

// DeleteStmt -
func (p *Preparer) DeleteStmt(m Preparable) *sqlx.NamedStmt {

	stmt := deleteStmtArr[m.GetName()]

	return p.Tx.NamedStmt(stmt)
}

// RemoveStmt -
func (p *Preparer) RemoveStmt(m Preparable) *sqlx.NamedStmt {

	stmt := removeStmtArr[m.GetName()]

	return p.Tx.NamedStmt(stmt)
}

// GetByIDSQL -
func (p *Preparer) GetByIDSQL(m Preparable) string {

	query := getByIDSQLArr[m.GetName()]

	return query
}

// GetByParamSQL -
func (p *Preparer) GetByParamSQL(m Preparable) string {

	query := getByParamSQLArr[m.GetName()]

	return query
}

// CreateSQL -
func (p *Preparer) CreateSQL(m Preparable) string {

	query := createSQLArr[m.GetName()]

	return query
}

// UpdateSQL -
func (p *Preparer) UpdateSQL(m Preparable) string {

	query := updateSQLArr[m.GetName()]

	return query
}

// DeleteSQL -
func (p *Preparer) DeleteSQL(m Preparable) string {

	query := deleteSQLArr[m.GetName()]

	return query
}

// RemoveSQL -
func (p *Preparer) RemoveSQL(m Preparable) string {

	query := removeSQLArr[m.GetName()]

	return query
}

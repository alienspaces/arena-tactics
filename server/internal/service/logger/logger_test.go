package logger

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
)

func TestLogger(t *testing.T) {

	// environment
	e, _ := env.NewEnv()

	l, err := NewLogger(e)
	assert.NoError(t, err, "NewLogger returns without error")
	assert.NotNil(t, l, "NewLogger is not nil")

	l.Debug().Msg("Test debug")
	l.Info().Msg("Test info")
	l.Warn().Msg("Test warn")
	l.Error().Msg("Test error")
}

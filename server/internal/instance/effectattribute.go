// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/attribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/effect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EffectAttribute -
type EffectAttribute struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// ID's
	AppEffectID          string
	AppAttributeID       string
	AppEffectAttributeID string

	// models
	effectModel    *effect.Model
	attributeModel *attribute.Model

	// record
	effectAttributeRec *record.AppEffectAttributeRecord
	effectRec          *record.AppEffectRecord
	attributeRec       *record.AppAttributeRecord

	// properties
	EffectName    string
	AttributeName string
	Target        string

	// TODO:
	ApplyConstraint string // AttributeMaxValue or AttributeCalculatedValue
}

// NewEffectAttribute -
func NewEffectAttribute(
	e env.Env, l zerolog.Logger, rs *repostore.RepoStore,
	effectAttributeID string) (instance *EffectAttribute, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &EffectAttribute{
		Env:                  e,
		Logger:               l,
		RepoStore:            rs,
		AppEffectAttributeID: effectAttributeID,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EffectAttribute) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Effect Attribute ID >%s< =====>", e.AppEffectAttributeID)

	// models
	e.effectModel, err = effect.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new effect model >%v<", err)
		return err
	}

	e.attributeModel, err = attribute.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new attribute model >%v<", err)
		return err
	}

	// effect attribute record
	effectAttributeRec, err := e.effectModel.GetEffectAttributeRec(e.AppEffectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute rec >%v<", err)
		return err
	}

	// effect record
	effectRec, err := e.effectModel.GetEffectRec(effectAttributeRec.AppEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect rec >%v<", err)
		return err
	}

	// attribute record
	attributeRec, err := e.attributeModel.GetAttributeRec(effectAttributeRec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute rec >%v<", err)
		return err
	}

	// properties
	e.effectAttributeRec = effectAttributeRec
	e.attributeRec = attributeRec
	e.effectRec = effectRec

	// public properties
	e.AppEffectID = effectAttributeRec.AppEffectID
	e.EffectName = e.effectRec.Name
	e.AppAttributeID = effectAttributeRec.AppAttributeID
	e.AttributeName = e.attributeRec.Name
	e.Target = e.effectAttributeRec.Target
	e.ApplyConstraint = e.effectAttributeRec.ApplyConstraint

	l.Debug().Msgf("<===== Done Initialise Effect Attribute =====>")

	return nil
}

// GetValue - returns the calculated value of an effect attribute
func (e *EffectAttribute) GetValue(params map[string]int) (int, error) {

	l := e.Logger.With().Str("function", "GetValue").Logger()

	value, err := e.effectModel.GetAttributeValue(e.AppEffectAttributeID, params)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute value >%v<", err)
		return 0, err
	}

	l.Debug().Msgf("Calculated Attribute >%s< Value >%d<", e.attributeRec.Name, value)

	return value, nil
}

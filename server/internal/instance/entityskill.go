// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/effect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/skill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EntitySkill -
type EntitySkill struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntitySkillID
	AppEntitySkillID string

	// AppSkillID
	AppSkillID string

	// models
	skillModel  *skill.Model
	effectModel *effect.Model

	// records
	skillRec       *record.AppSkillRecord
	entitySkillRec *record.AppEntitySkillRecord

	// appliable skill effects
	Effects []*SkillEffect

	// properties
	Name string
}

// NewEntitySkill -
func NewEntitySkill(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, entitySkillID string) (instance *EntitySkill, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &EntitySkill{
		AppEntitySkillID: entitySkillID,
		Env:              e,
		Logger:           l,
		RepoStore:        rs,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EntitySkill) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Entity Skill ID >%s< =====>", e.AppEntitySkillID)

	// models
	e.skillModel, err = skill.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new skill model >%v<", err)
		return err
	}

	e.effectModel, err = effect.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new effect model >%v<", err)
		return err
	}

	// init entity skill
	err = e.initEntitySkill()
	if err != nil {
		l.Warn().Msgf("Failed to init entity skill >%v<", err)
		return err
	}

	// init skill effects
	err = e.initSkillEffects()
	if err != nil {
		l.Warn().Msgf("Failed to init skill effects >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Initialise Entity Skill =====>")

	return nil
}

// InitEntityAttribute -
func (e *EntitySkill) initEntitySkill() (err error) {

	l := e.Logger.With().Str("function", "initEntitySkill").Logger()

	// get entity skill record
	entitySkillRec, err := e.skillModel.GetEntitySkillRec(e.AppEntitySkillID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity skill rec >%v<", err)
		return err
	}

	// get skill record
	skillRec, err := e.skillModel.GetSkillRec(entitySkillRec.AppSkillID)
	if err != nil {
		l.Warn().Msgf("Failed to get skill rec >%v<", err)
		return err
	}

	// properties
	e.entitySkillRec = entitySkillRec
	e.skillRec = skillRec

	// public properties
	e.AppSkillID = skillRec.ID
	e.Name = skillRec.Name

	return nil
}

// initSkillEffects -
func (e *EntitySkill) initSkillEffects() (err error) {

	l := e.Logger.With().Str("function", "initSkillEffects").Logger()

	l.Debug().Msgf("<===== Initialise Entity Skill Effects =====>")

	// get skill effect records
	recs, err := e.skillModel.GetSkillEffectRecs(e.skillRec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get skill effect recs >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new entity skill
		skill, err := NewSkillEffect(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new skill >%v<", err)
			return err
		}

		e.Effects = append(e.Effects, skill)
	}

	l.Debug().Msgf("<===== Done Initialise Entity Skill Effects >%d< =====>", len(e.Effects))

	return nil
}

// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/attribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EntityTactic -
type EntityTactic struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntityTacticID
	AppEntityTacticID string

	// models
	entityModel    *entity.Model
	attributeModel *attribute.Model

	// records
	entityTacticRec *record.AppEntityTacticRecord
	attributeRec    *record.AppAttributeRecord

	// properties
	Attribute  string
	Target     string
	Comparator string
	Value      int
	Action     string
	AppSkillID string
	AppItemID  string
}

// NewEntityTactic -
func NewEntityTactic(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, entityTacticID string) (instance *EntityTactic, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &EntityTactic{
		AppEntityTacticID: entityTacticID,
		Env:               e,
		Logger:            l,
		RepoStore:         rs,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EntityTactic) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Entity Tactic ID >%s< =====>", e.AppEntityTacticID)

	// models
	e.entityModel, err = entity.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity model >%v<", err)
		return err
	}

	e.attributeModel, err = attribute.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new attribute model >%v<", err)
		return err
	}

	// init entity tactic
	err = e.initEntityTactic()
	if err != nil {
		l.Warn().Msgf("Failed to init entity tactic >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Initialise Entity Tactic =====>")

	return nil
}

// initEntityTactic -
func (e *EntityTactic) initEntityTactic() (err error) {

	l := e.Logger.With().Str("function", "initEntityTactic").Logger()

	// get entity tactic record
	entityTacticRec, err := e.entityModel.GetEntityTacticRec(e.AppEntityTacticID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity tactic rec >%v<", err)
		return err
	}

	// get attribute record
	attributeRec, err := e.attributeModel.GetAttributeRec(entityTacticRec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute rec >%v<", err)
		return err
	}

	// properties
	e.entityTacticRec = entityTacticRec
	e.attributeRec = attributeRec

	// tactic properties
	e.Attribute = attributeRec.Name
	e.Target = entityTacticRec.Target
	e.Comparator = entityTacticRec.Comparator
	e.Value = entityTacticRec.Value
	e.Action = entityTacticRec.Action

	if entityTacticRec.AppSkillID.Valid {
		e.AppSkillID = entityTacticRec.AppSkillID.String
	}
	if entityTacticRec.AppItemID.Valid {
		e.AppItemID = entityTacticRec.AppItemID.String
	}

	return nil
}

// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"fmt"

	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/attribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Entity -
type Entity struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntityID
	AppEntityID string

	// AppEntityGroupID
	AppEntityGroupID string

	// models
	entityModel    *entity.Model
	attributeModel *attribute.Model

	// records
	entityRec      *record.AppEntityRecord
	entityGroupRec *record.AppEntityGroupRecord

	// instances
	Attributes []*EntityAttribute
	Items      []*EntityItem
	Skills     []*EntitySkill
	Tactics    []*EntityTactic

	// applied entity effects - mapped by unique key
	Effects map[string]*EntityEffect

	// properties
	Name                 string
	FoeTargetEntityID    string
	FriendTargetEntityID string
	Dead                 bool
}

// NewEntity -
func NewEntity(e env.Env, l zerolog.Logger, ms *repostore.RepoStore, entityID string) (instance *Entity, err error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	instance = &Entity{
		AppEntityID: entityID,
		Env:         e,
		Logger:      l,
		RepoStore:   ms,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialise >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *Entity) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Entity ID >%s< =====>", e.AppEntityID)

	// models
	e.entityModel, err = entity.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity model >%v<", err)
		return err
	}

	e.attributeModel, err = attribute.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new attribute model >%v<", err)
		return err
	}

	// instance objects
	e.Attributes = []*EntityAttribute{}
	e.Items = []*EntityItem{}
	e.Skills = []*EntitySkill{}
	e.Tactics = []*EntityTactic{}
	e.Effects = make(map[string]*EntityEffect)

	err = e.initEntity()
	if err != nil {
		l.Warn().Msgf("Failed to init entity >%v<", err)
		return err
	}

	err = e.initAttributes()
	if err != nil {
		l.Warn().Msgf("Failed to init attributes >%v<", err)
		return err
	}

	err = e.initItems()
	if err != nil {
		l.Warn().Msgf("Failed to init items >%v<", err)
		return err
	}

	err = e.initSkills()
	if err != nil {
		l.Warn().Msgf("Failed to init skills >%v<", err)
		return err
	}

	err = e.initTactics()
	if err != nil {
		l.Warn().Msgf("Failed to init tactics >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Initialise Entity =====>")

	return nil
}

// initEntity -
func (e *Entity) initEntity() (err error) {

	l := e.Logger.With().Str("function", "InitEntity").Logger()

	// entity record
	entityRec, err := e.entityModel.GetEntityRec(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity rec >%v<", err)
		return err
	}

	l.Debug().Msgf("Init entity Name >%s< ID >%s<", entityRec.Name, entityRec.ID)

	e.entityRec = entityRec

	// get entity group record
	entityGroupRec, err := e.entityModel.GetEntityGroupRec(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group rec >%v<", err)
		return err
	}

	// entity group properties
	if entityGroupRec != nil {
		e.AppEntityGroupID = entityGroupRec.ID
		e.entityGroupRec = entityGroupRec
	}

	// public properties
	e.Name = entityRec.Name

	return nil
}

// initAttributes - Initialise entity attributes
func (e *Entity) initAttributes() (err error) {

	l := e.Logger.With().Str("function", "initAttributes").Logger()

	l.Debug().Msg("<= Initialise Attributes =>")

	// entity attribute records
	recs, err := e.entityModel.GetEntityAttributeRecs(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute recs >%v<", err)
		return err
	}

	// NOTE: we intentionally order and validate the attributes prior
	//	     to creating attribute instance objects

	// order attributes
	err = e.attributeModel.OrderEntityAttributeRecs(recs)
	if err != nil {
		l.Warn().Msgf("Failed order attributes >%v<", err)
		return err
	}

	// validate attributes
	err = e.attributeModel.ValidateEntityAttributeRecs(recs)
	if err != nil {
		l.Warn().Msgf("Failed validate attributes >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new attribute
		attr, err := NewEntityAttribute(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			e.Logger.Warn().Msgf("Failed new attribute >%v<", err)
			return err
		}

		l.Debug().Msgf("Attribute ID >%s< Base >%d<", rec.AppAttributeID, rec.Value)

		// NOTE: pre-ordered and validated array of attributes
		e.Attributes = append(e.Attributes, attr)
	}

	l.Debug().Msgf("<= Done Initialise Attributes >%d< =>", len(e.Attributes))

	return nil
}

// initItems - Initialise entity items
func (e *Entity) initItems() (err error) {

	l := e.Logger.With().Str("function", "initItems").Logger()

	l.Debug().Msg("<= Initialise Entity Items =>")

	// get entity item records
	recs, err := e.entityModel.GetEntityItemRecs(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity item recs >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new entity item
		item, err := NewEntityItem(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new item >%v<", err)
			return err
		}

		e.Items = append(e.Items, item)
	}

	l.Debug().Msgf("<= Done Initialise Entity Items >%d< =>", len(e.Items))

	return nil
}

// initSkills - Initialise entity skills
func (e *Entity) initSkills() (err error) {

	l := e.Logger.With().Str("function", "initSkills").Logger()

	l.Debug().Msg("<= Initialise Entity Skills =>")

	// get entity skill records
	recs, err := e.entityModel.GetEntitySkillRecs(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity skill recs >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new entity skill
		skill, err := NewEntitySkill(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new skill >%v<", err)
			return err
		}

		e.Skills = append(e.Skills, skill)
	}

	l.Debug().Msgf("<= Done Initialise Entity Skills >%d<", len(e.Skills))

	return nil
}

// initTactics - Initialise entity tactics
func (e *Entity) initTactics() (err error) {

	l := e.Logger.With().Str("function", "initTactics").Logger()

	l.Debug().Msg("<= Initialise Entity Tactics =>")

	// get entity tactics records
	// NOTE: returns tactics already ordered
	recs, err := e.entityModel.GetEntityTacticRecs(e.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity tactic recs >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new entity tactic
		tactic, err := NewEntityTactic(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new tactic >%v<", err)
			return err
		}

		e.Tactics = append(e.Tactics, tactic)
	}

	l.Debug().Msgf("<= Done Initialise Entity Tactics >%d< =>", len(e.Tactics))

	return nil
}

// CalculateAttributeValues - recalculates and sets attribute values
func (e *Entity) CalculateAttributeValues() error {

	l := e.Logger.With().Str("function", "CalculateAttributeValues").Logger()

	// calculate current attribute values
	attrValues := make(map[string]int)

	for _, attr := range e.Attributes {

		// calculate attribute values
		err := attr.CalculateValues(attrValues)
		if err != nil {
			l.Warn().Msgf("Failed get set attribute calculated value >%v<", err)
			return err
		}

		// NOTE: attributes values are always calculated using other unadjusted attribute values
		attrValues[attr.Name] = attr.CalculatedValue

		l.Debug().Msgf("Attribute Name >%s< Calculated >%d<", attr.Name, attr.CalculatedValue)
	}

	return nil
}

// GetEntityAttribute - returns an entity attribute
func (e *Entity) GetEntityAttribute(attrName string) (*EntityAttribute, error) {

	l := e.Logger.With().Str("function", "GetEntityAttribute").Logger()

	for _, attr := range e.Attributes {
		if attr.Name == attrName {
			return attr, nil
		}
	}

	l.Warn().Msgf("Entity has no Attribute >%s<, failed get attribute value", attrName)

	return nil, fmt.Errorf("Entity has no Attribute >%s<, failed get attribute value", attrName)
}

// HasEffect - tests whether this entity already has a specific item
// or skill effect applied
func (e *Entity) HasEffect(entityID, effectAttributeID string) bool {

	l := e.Logger.With().Str("function", "HasEffect").Logger()

	l.Debug().Msgf("Check effect entityID >%s< effectAttributeID >%s<", entityID, effectAttributeID)

	// NOTE: An entity cannot have an effect from the same entity +
	// item or skill applied twice so the key combination used is
	// of entity ID + effectAttributeID

	key := fmt.Sprintf("%s-%s", entityID, effectAttributeID)

	if _, ok := e.Effects[key]; ok {
		l.Debug().Msgf("Entity has effect")
		return true
	}

	l.Debug().Msgf("Entity does not have effect")

	return false
}

// ApplyItemEffect - applies an item effect
func (e *Entity) ApplyItemEffect(entityID, target string, itemEffect *ItemEffect) (effectEvents []*instance.EffectEvent, err error) {

	l := e.Logger.With().Str("function", "ApplyItemEffect").Logger()

	l.Debug().Msgf("Applying item effect entityID >%s<", entityID)

	// skip if already applied
	if e.HasEffect(
		entityID,
		itemEffect.AppItemEffectID) == true {
		l.Debug().Msgf("Entity already has item effect applied")
		return effectEvents, nil
	}

	// entity effect
	entityEffect, err := NewEntityEffect(
		itemEffect.AppItemEffectID,
		itemEffect.EffectName,
		itemEffect.EffectType,
		itemEffect.EffectDuration,
		itemEffect.EffectRecurring,
		itemEffect.EffectPermanent,
	)
	if err != nil {
		l.Warn().Msgf("Failed new entity effect >%v<", err)
		return effectEvents, err
	}

	// applied count
	appliedCount := 0

	// effect attributes for target
	effectAttrs, err := itemEffect.GetEffectAttributes(target)

	for _, effectAttr := range effectAttrs {

		// skip if not requested target
		if effectAttr.Target != target {
			l.Debug().Msgf("Skipping effect attribute >%+v<, target not >%s<", effectAttr, target)
			continue
		}

		l.Debug().Msgf("Applying adjustment effect >%s< attribute >%s<", effectAttr.EffectName, effectAttr.AttributeName)

		// apply effect attribute adjustments
		applied, adjustment, err := e.ApplyEffectAttributeAdjustment(entityEffect, effectAttr)
		if err != nil {
			l.Warn().Msgf("Failed to add attribute adjustment >%v<", err)
			return effectEvents, err
		}

		if applied != true {
			l.Debug().Msgf("Adjustment not applied")
			continue
		}

		// duration
		if entityEffect.Type == record.EffectTypeActive && entityEffect.Duration > 0 {
			entityEffect.Duration = entityEffect.Duration - 1
		}

		// effect event
		effectEvent := &instance.EffectEvent{
			AppEntityID:     e.AppEntityID,
			Name:            e.Name,
			AppEffectID:     itemEffect.AppEffectID,
			EffectName:      itemEffect.EffectName,
			AppAttributeID:  effectAttr.AppAttributeID,
			AttributeName:   effectAttr.AttributeName,
			AdjustmentValue: adjustment,
		}
		effectEvents = append(effectEvents, effectEvent)

		l.Debug().Msgf("Applied item effect Attribute >%s< AdjustmentTotal >%d< Permanent >%t< remaining Duration >%d<", effectAttr.AttributeName, adjustment, entityEffect.Permanent, entityEffect.Duration)

		appliedCount++
	}

	if appliedCount != 0 {
		// entity effect key
		// NOTE - entity ID + item effect ID
		key := fmt.Sprintf("%s-%s", entityID, itemEffect.AppItemEffectID)
		e.Effects[key] = entityEffect
	}

	return effectEvents, nil
}

// ApplySkillEffect - applies a skill effect
func (e *Entity) ApplySkillEffect(entityID, target string, skillEffect *SkillEffect) (effectEvents []*instance.EffectEvent, err error) {

	l := e.Logger.With().Str("function", "ApplySkillEffect").Logger()

	l.Debug().Msgf("Applying Skill Effect >%s< Entity ID >%s< Target >%s<", skillEffect.EffectName, entityID, target)

	// skip if already applied
	if e.HasEffect(
		entityID,
		skillEffect.AppSkillEffectID) == true {
		l.Debug().Msgf("Entity already has skill effect applied")
		return effectEvents, nil
	}

	// entity effect
	entityEffect, err := NewEntityEffect(
		skillEffect.AppSkillEffectID,
		skillEffect.EffectName,
		skillEffect.EffectType,
		skillEffect.EffectDuration,
		skillEffect.EffectRecurring,
		skillEffect.EffectPermanent,
	)
	if err != nil {
		l.Warn().Msgf("Failed new entity effect >%v<", err)
		return effectEvents, err
	}

	// applied count
	appliedCount := 0

	// effect attributes for target
	effectAttrs, err := skillEffect.GetEffectAttributes(target)

	for _, effectAttr := range effectAttrs {

		// skip if not requested target
		if effectAttr.Target != target {
			l.Debug().Msgf("Skipping Effect Attribute >%s<, target not >%s<", effectAttr.AttributeName, target)
			continue
		}

		l.Debug().Msgf("Applying Effect Attribute >%s<", effectAttr.AttributeName)

		// apply effect attribute adjustments
		applied, adjustment, err := e.ApplyEffectAttributeAdjustment(entityEffect, effectAttr)
		if err != nil {
			l.Warn().Msgf("Failed to add attribute adjustment >%v<", err)
			return effectEvents, err
		}

		if applied != true {
			l.Debug().Msgf("Adjustment not applied")
			continue
		}

		// duration
		if entityEffect.Type == record.EffectTypeActive && entityEffect.Duration > 0 {
			entityEffect.Duration = entityEffect.Duration - 1
		}

		// effect event
		effectEvent := &instance.EffectEvent{
			AppEntityID:     e.AppEntityID,
			Name:            e.Name,
			AppEffectID:     skillEffect.AppEffectID,
			EffectName:      skillEffect.EffectName,
			AppAttributeID:  effectAttr.AppAttributeID,
			AttributeName:   effectAttr.AttributeName,
			AdjustmentValue: adjustment,
		}
		effectEvents = append(effectEvents, effectEvent)

		l.Debug().Msgf("Applied Effect Attribute >%s< AdjustmentTotal >%d< Permanent >%t< remaining Duration >%d<",
			effectAttr.AttributeName, adjustment, entityEffect.Permanent, entityEffect.Duration)

		appliedCount++
	}

	if appliedCount != 0 {
		// entity effect key
		// NOTE - entity ID + item effect ID
		key := fmt.Sprintf("%s-%s", entityID, skillEffect.AppSkillEffectID)
		e.Effects[key] = entityEffect
	}

	return effectEvents, nil
}

// GetAttribute -
func (e *Entity) GetAttribute(name string) (*EntityAttribute, error) {

	l := e.Logger.With().Str("function", "GetAttribute").Logger()

	l.Debug().Msgf("Getting entity Attribute >%s<", name)

	for _, attr := range e.Attributes {
		if attr.Name == name {
			return attr, nil
		}
	}

	return nil, fmt.Errorf("No such entity Attribute >%s<", name)
}

// GetEntityItem -
func (e *Entity) GetEntityItem(itemID string) (*EntityItem, error) {

	l := e.Logger.With().Str("function", "GetEntityItem").Logger()

	for _, entityItem := range e.Items {
		if entityItem.AppItemID == itemID {
			l.Debug().Msgf("Returning entity item Name >%s< ID >%s<", entityItem.Name, entityItem.AppEntityItemID)
			return entityItem, nil
		}
	}

	return nil, fmt.Errorf("Entity ID >%s< does not have item ID >%s<", e.AppEntityID, itemID)
}

// GetEntitySkill -
func (e *Entity) GetEntitySkill(skillID string) (*EntitySkill, error) {

	l := e.Logger.With().Str("function", "GetEntitySkill").Logger()

	for _, entitySkill := range e.Skills {
		if entitySkill.AppSkillID == skillID {
			l.Debug().Msgf("Returning entity skill Name >%s< ID >%s<", entitySkill.Name, entitySkill.AppEntitySkillID)
			return entitySkill, nil
		}
	}
	return nil, fmt.Errorf("Entity Name >%s< ID >%s< does not have skill ID >%s<", e.Name, e.AppEntityID, skillID)
}

// UpdateActiveEffects - updates active effects, decrements duration,
// adds or removes attribute adjustments as necessary
func (e *Entity) UpdateActiveEffects() ([]*instance.EffectEvent, error) {

	l := e.Logger.With().Str("function", "UpdateActiveEffects").Logger()

	l.Debug().Msgf("<==== Update Active Effects Entity Name >%s< ===>", e.Name)

	effectEvents := []*instance.EffectEvent{}

	for key, entityEffect := range e.Effects {

		// skip passive effects
		if entityEffect.Type == record.EffectTypePassive {
			continue
		}

		l.Debug().Msgf("Updating Effect Name >%s< Type >%s< Duration >%d< Permanent >%t<",
			entityEffect.Name, entityEffect.Type, entityEffect.Duration, entityEffect.Permanent)

		// NOTE: An active effect with a duration 1 will hang around until
		// the following turn where we remove it here. This all happens
		// before this turns tactics are executed so attributes will
		// be adjusted accordingly when the effect is removed before it
		// matters for combat..
		if entityEffect.Duration == 0 {

			// remove attribute adjustments if not permanent
			if entityEffect.Permanent != true {

				l.Debug().Msgf("Removing Effect >%+v<", entityEffect)

				for _, effectAdjustment := range entityEffect.Adjustments {

					l.Debug().Msgf("Removing Adjustment >%+v<", effectAdjustment)

					err := e.RemoveEffectAttributeAdjustment(entityEffect, effectAdjustment)
					if err != nil {
						l.Warn().Msgf("Failed to remove Adjustment >%v<", err)
						return nil, err
					}
				}

			} else {
				l.Debug().Msgf("Leaving permanent effect adjustments in place")
			}

			// remove entity effect
			delete(e.Effects, key)

			l.Debug().Msgf("Removed Effect >%+v<", entityEffect)
		}

		if entityEffect.Duration > 0 {

			// apply attribute adjustments for recurring
			if entityEffect.Recurring {

				l.Debug().Msgf("Adding Effect >%+v<", entityEffect)

				for _, effectAdjustment := range entityEffect.Adjustments {

					l.Debug().Msgf("Updating Adjustment >%+v<", effectAdjustment)

					applied, adjustment, err := e.UpdateEffectAttributeAdjustment(entityEffect, effectAdjustment)
					if err != nil {
						l.Warn().Msgf("Failed to update Adjustment >%v<", err)
						return nil, err
					}

					if applied != true {
						l.Debug().Msgf("Adjustment not applied")
						continue
					}

					// effect event
					effectEvent := &instance.EffectEvent{
						AppEntityID:     e.AppEntityID,
						Name:            e.Name,
						AppEffectID:     effectAdjustment.EffectAttribute.AppEffectID,
						EffectName:      effectAdjustment.EffectAttribute.EffectName,
						AppAttributeID:  effectAdjustment.EffectAttribute.AppAttributeID,
						AttributeName:   effectAdjustment.EffectAttribute.AttributeName,
						AdjustmentValue: adjustment,
					}
					effectEvents = append(effectEvents, effectEvent)

					l.Debug().Msgf("Adjustment applied >%d<", adjustment)
				}
			}

			// NOTE: Hitting duration 0 results in the effect being removed
			// next turn, not this turn. This turn it still applies!
			entityEffect.Duration = entityEffect.Duration - 1
		}
	}

	l.Debug().Msgf("Total remaining effects >%d<", len(e.Effects))

	l.Debug().Msgf("<==== Done Update Active Effects Entity Name >%s< ===>", e.Name)

	return effectEvents, nil
}

// RemovePassiveEffects - removes all passive effects
func (e *Entity) RemovePassiveEffects() error {

	l := e.Logger.With().Str("function", "RemovePassiveEffects").Logger()

	l.Debug().Msgf("<=== Remove Passive Effects Entity Name >%s< ===>", e.Name)

	for key, entityEffect := range e.Effects {

		if entityEffect.Type == record.EffectTypePassive {

			l.Debug().Msgf("Removing Effect Name >%s< Permanent >%t< Adjustments >%+v< ",
				entityEffect.Name, entityEffect.Permanent, entityEffect.Adjustments)

			if entityEffect.Permanent != true {

				for _, effectAdjustment := range entityEffect.Adjustments {

					l.Debug().Msgf("Removing Adjustment >%+v<", effectAdjustment)

					err := e.RemoveEffectAttributeAdjustment(entityEffect, effectAdjustment)
					if err != nil {
						l.Warn().Msgf("Failed to subtract attribute adjustment >%v<", err)
						return err
					}

					l.Debug().Msgf("Removed Adjustment >%+v<", effectAdjustment)
				}

			} else {
				l.Debug().Msgf("Leaving permanent effect adjustments in place")
			}

			// remove entity effect
			delete(e.Effects, key)

			l.Debug().Msgf("Removed Effect >%+v<", entityEffect)
		}
	}

	l.Debug().Msgf("<=== Done Remove Passive Effects Entity Name >%s< ===>", e.Name)

	return nil
}

// ApplyEffectAttributeAdjustment -
func (e *Entity) ApplyEffectAttributeAdjustment(entityEffect *EntityEffect, effectAttr *EffectAttribute) (bool, int, error) {

	l := e.Logger.With().Str("function", "ApplyEffectAttributeAdjustment").Logger()

	l.Debug().Msgf("Applying Effect >%s< Attribute >%s<",
		entityEffect.Name,
		effectAttr.AttributeName,
	)

	// TODO: Pass in the applying entity object so we can use their attributes
	// in the adjustment value formula

	// NOTE: effect adjustments use adjusted attribute values to calculate
	// the resulting adjusted amount
	params := map[string]int{}
	for _, attr := range e.Attributes {
		params[attr.Name] = attr.AdjustedValue
	}

	// get effect attribute adjustment value
	adjustment, err := effectAttr.GetValue(params)
	if err != nil {
		l.Warn().Msgf("Failed to get effect value >%v<", err)
		return false, 0, err
	}

	// can adjustment be applied
	applied := false

	entAttr, err := e.GetAttribute(effectAttr.attributeRec.Name)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute >%v<", err)
		return false, 0, err
	}

	// TODO: determine whether adjustment can be applied based on attribute min and max values
	//		effectAttr.ApplyConstraint string // AttributeMaxValue or AttributeCalculatedValue
	if effectAttr.ApplyConstraint == record.ApplyConstraintMaximum &&
		entAttr.AdjustedValue+adjustment >= entAttr.MaxValue {
		l.Debug().Msgf("Effect attribute constraint >%s< prevents application", effectAttr.ApplyConstraint)
		return false, 0, nil
	}

	if effectAttr.ApplyConstraint == record.ApplyConstraintCalculated &&
		entAttr.AdjustedValue+adjustment >= entAttr.CalculatedValue {
		l.Debug().Msgf("Effect attribute constraint >%s< prevents application", effectAttr.ApplyConstraint)
		return false, 0, nil
	}

	applied, err = entAttr.AddAdjustment(adjustment)
	if err != nil {
		l.Warn().Msgf("Failed to add adjustment >%v<", err)
		return false, 0, err
	}

	l.Debug().Msgf("Effect >%s< Attribute >%s< Applied >%t<",
		effectAttr.effectRec.Name,
		effectAttr.attributeRec.Name,
		applied,
	)

	if applied {

		// add the adjustment
		effectAdjustment, err := NewEntityEffectAdjustment(
			e.Env,
			e.Logger,
			e.RepoStore,
			effectAttr.AppEffectAttributeID,
			adjustment,
		)
		if err != nil {
			l.Warn().Msgf("Failed new entity effect adjustment >%v<", err)
			return false, 0, err
		}

		effectAdjustment.Apply()

		entityEffect.Adjustments = append(entityEffect.Adjustments, effectAdjustment)
	}

	return applied, adjustment, nil
}

// RemoveEffectAttributeAdjustment -
func (e *Entity) RemoveEffectAttributeAdjustment(entityEffect *EntityEffect, effectAdjustment *EntityEffectAdjustment) error {

	l := e.Logger.With().Str("function", "RemoveEffectAttributeAdjustment").Logger()

	l.Debug().Msgf("Removing effect >%+v< adjustment >%+v<", entityEffect, effectAdjustment)

	// effect attribute
	effectAttr := effectAdjustment.EffectAttribute

	entAttr, err := e.GetAttribute(effectAttr.AttributeName)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute >%v<", err)
		return err
	}

	adjustmentTotal := effectAdjustment.AppliedValue

	removed, err := entAttr.RemoveAdjustment(adjustmentTotal)
	if err != nil {
		l.Warn().Msgf("Failed to add adjustment >%v<", err)
		return err
	}

	l.Debug().Msgf("Effect >%s< Attribute >%s< Removed >%t<",
		effectAttr.effectRec.Name,
		effectAttr.attributeRec.Name,
		removed,
	)

	if removed {

		// TODO: Actually remove effect adjustment from entity effect
		effectAdjustment.Value = 0
		effectAdjustment.AppliedValue = 0
	}

	return nil
}

// UpdateEffectAttributeAdjustment -
func (e *Entity) UpdateEffectAttributeAdjustment(
	entityEffect *EntityEffect, effectAdjustment *EntityEffectAdjustment) (applied bool, adjustment int, err error) {

	l := e.Logger.With().Str("function", "UpdateEffectAttributeAdjustment").Logger()

	l.Debug().Msgf("Updating effect >%+v< adjustment >%+v<", entityEffect, effectAdjustment)

	// can adjustment be applied
	applied = false

	// effect attribute
	effectAttr := effectAdjustment.EffectAttribute

	entAttr, err := e.GetAttribute(effectAttr.AttributeName)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute >%v<", err)
		return false, 0, err
	}

	adjustment = effectAdjustment.Value

	// TODO: determine whether adjustment can be applied based on attribute min and max values
	//		effectAttr.ApplyConstraint string // AttributeMaxValue or AttributeCalculatedValue
	if effectAttr.ApplyConstraint == record.ApplyConstraintMaximum &&
		entAttr.AdjustedValue+adjustment >= entAttr.MaxValue {
		l.Warn().Msgf("Effect attribute constraint >%s< prevents application", effectAttr.ApplyConstraint)
		return false, 0, nil
	}

	if effectAttr.ApplyConstraint == record.ApplyConstraintCalculated &&
		entAttr.AdjustedValue+adjustment >= entAttr.CalculatedValue {
		l.Warn().Msgf("Effect attribute constraint >%s< prevents application", effectAttr.ApplyConstraint)
		return false, 0, nil
	}

	applied, err = entAttr.AddAdjustment(adjustment)
	if err != nil {
		l.Warn().Msgf("Failed to add adjustment >%v<", err)
		return false, 0, err
	}

	l.Debug().Msgf("Effect >%s< Attribute >%s< Applied >%t<",
		effectAttr.effectRec.Name,
		effectAttr.attributeRec.Name,
		applied,
	)

	if applied {
		effectAdjustment.Apply()
	}

	return applied, adjustment, err
}

// SetState - sets entity current state
func (e *Entity) SetState(data *instance.EntityData) error {

	l := e.Logger.With().Str("function", "SetState").Logger()

	// reinstate attribute adjustments
	for _, attr := range data.Attributes {
		entAttr, err := e.GetAttribute(attr.Name)
		if err != nil {
			l.Warn().Msgf("Failed to get attribute >%s< >%v<", attr.Name, err)
			return err
		}

		l.Debug().Msgf("Reinstate attribute Name >%s< Data >%+v<", attr.Name, attr)

		entAttr.Name = attr.Name
		entAttr.AssignedValue = attr.AssignedValue
		entAttr.CalculatedValue = attr.CalculatedValue
		entAttr.MinValue = attr.MinValue
		entAttr.MaxValue = attr.MaxValue
		entAttr.AdjustedValue = attr.AdjustedValue
		entAttr.AdjustmentTotal = attr.AdjustmentTotal
	}

	// reinstate applied effects
	for key, effect := range data.Effects {

		l.Debug().Msgf("Have instance stored effect key >%s<", key)

		// NOTE: Instance stored effect adjustments have already been applied so we
		// don't need to apply them, we just need to add the effects
		entityEffect, err := NewEntityEffect(
			effect.ID,
			effect.Name,
			effect.Type,
			effect.Duration,
			effect.Recurring,
			effect.Permanent,
		)
		if err != nil {
			l.Warn().Msgf("Failed new entity effect >%v<", err)
			return err
		}

		for _, adjustment := range effect.Adjustments {
			effectAdjustment, err := NewEntityEffectAdjustment(e.Env, e.Logger, e.RepoStore, adjustment.AppEffectAttributeID, adjustment.Value)
			if err != nil {
				l.Warn().Msgf("Failed new effect adjustment >%v<", err)
				return err
			}
			effectAdjustment.AppliedValue = adjustment.AppliedValue

			entityEffect.Adjustments = append(entityEffect.Adjustments, effectAdjustment)
		}

		e.Effects[key] = entityEffect
	}

	// reinstate dead
	e.Dead = data.Dead

	l.Debug().Msg("<= Done Initialise Entity Effects =>")

	return nil
}

// GetState - return entity current state
func (e *Entity) GetState() (*instance.EntityData, error) {

	l := e.Logger.With().Str("function", "GetState").Logger()

	data := &instance.EntityData{}

	data.AppEntityID = e.AppEntityID
	data.Name = e.Name

	data.Attributes = make(map[string]*instance.EntityAttributeData)

	for _, attr := range e.Attributes {

		l.Debug().Msgf("State data Attribute >%s< Values >%+v<", attr.Name, attr)

		data.Attributes[attr.AppEntityAttributeID] = &instance.EntityAttributeData{
			AppEntityAttributeID: attr.AppEntityAttributeID,
			Name:                 attr.Name,
			AssignedValue:        attr.AssignedValue,
			CalculatedValue:      attr.CalculatedValue,
			MinValue:             attr.MinValue,
			MaxValue:             attr.MaxValue,
			AdjustedValue:        attr.AdjustedValue,
			AdjustmentTotal:      attr.AdjustmentTotal,
		}
	}

	// entity effects
	data.Effects = make(map[string]*instance.EntityEffectData)

	for key, effect := range e.Effects {

		l.Debug().Msgf("State data effect Name >%s< Adjustments >%+v<", effect.Name, effect.Adjustments)

		entityEffect := &instance.EntityEffectData{
			ID:          effect.ID,
			Name:        effect.Name,
			Type:        effect.Type,
			Duration:    effect.Duration,
			Recurring:   effect.Recurring,
			Permanent:   effect.Permanent,
			Adjustments: []*instance.EntityEffectAdjustmentData{},
		}

		for _, adjustment := range effect.Adjustments {
			effectAdjustment := &instance.EntityEffectAdjustmentData{
				AppEffectAttributeID: adjustment.AppEffectAttributeID,
				Value:                adjustment.Value,
				AppliedValue:         adjustment.AppliedValue,
			}

			entityEffect.Adjustments = append(entityEffect.Adjustments, effectAdjustment)
		}

		data.Effects[key] = entityEffect
	}

	// other properties
	data.Dead = e.Dead

	return data, nil
}

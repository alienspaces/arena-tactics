package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

func TestNewFight(t *testing.T) {

	td, teardown := setup(t)

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	f, err := NewFight(e, l, rs, td.AppFightRecs[0].ID)
	if assert.NoError(t, err, "NewFight created without error") {

		assert.NotNil(t, f, "Fight is not nil")

		err = f.Cleanup()

		assert.NoError(t, err, "Cleanup returns without error")
	}

	tx.Rollback()

	teardown()
}

func TestFightRun(t *testing.T) {

	td, teardown := setup(t)

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// update app fight records to starting
	afr := rs.AppFightRepo

	for _, appFightRec := range td.AppFightRecs {
		appFightRec.Status = record.AppFightStatusStarting
		err := afr.Update(appFightRec)
		if err != nil {
			t.Fatalf("Failed to update fight record >%v<", err)
		}
	}

	f, err := NewFight(e, l, rs, td.AppFightRecs[0].ID)
	if assert.NoError(t, err, "NewFight created without error") {

		assert.NotNil(t, f, "NewFight is not nil")

		err := f.Run()
		if assert.NoError(t, err, "Run returns without error") {

			// inspect state of each entity after running
			for _, entGroup := range f.EntityGroups {
				for _, ent := range entGroup.Entities {
					assert.NotEmpty(t, ent.Effects, "Entity has effects applied")
				}
			}
		}

		err = f.Cleanup()
		assert.NoError(t, err, "Cleanup returns without error")
	}

	tx.Rollback()

	teardown()
}

func TestGetOrderedEntities(t *testing.T) {

	td, teardown := setup(t)

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	f, err := NewFight(e, l, rs, td.AppFightRecs[0].ID)

	if assert.NoError(t, err, "NewFight created without error") {

		assert.NotNil(t, f, "NewFight is not nil")

		// before sort
		t.Logf("Before sort")
		for _, entityGroup := range f.EntityGroups {
			for _, ent := range entityGroup.Entities {

				// TODO: Perhaps roll this loop into an entity instance function
				adjustedValues := make(map[string]int)
				for _, entityAttr := range ent.Attributes {
					adjustedValues[entityAttr.Name] = entityAttr.AdjustedValue
				}
				initValue, _ := f.appModel.GetInitiativeValue(td.AppRec, adjustedValues)
				t.Logf("Entity name >%s< initiative >%d<", ent.Name, initValue)
			}
		}

		// get ordered entities
		entities, err := f.GetOrderedEntities()
		if assert.NoError(t, err, "GetOrderedEntities returns without error") {

			assert.NotEmpty(t, entities, "GetOrderedEntities returns entities")

			t.Logf("After sort")
			prevValue := 100
			for _, ent := range entities {

				// TODO: Perhaps roll this loop into an entity instance function
				adjustedValues := make(map[string]int)
				for _, entityAttr := range ent.Attributes {
					adjustedValues[entityAttr.Name] = entityAttr.AdjustedValue
				}

				initValue, _ := f.appModel.GetInitiativeValue(td.AppRec, adjustedValues)

				t.Logf("Entity name >%s< initiative >%d<", ent.Name, initValue)

				// entity initiative is less than or equal to previous entity initiative
				assert.True(t, initValue <= prevValue, "Initiative value is less than previous entity")
				prevValue = initValue
			}
		}

		err = f.Cleanup()

		assert.NoError(t, err, "Cleanup returns without error")
	}

	tx.Rollback()

	teardown()
}

// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/attribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EntityAttribute -
type EntityAttribute struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntityAttributeID
	AppEntityAttributeID string

	// models
	entityModel    *entity.Model
	attributeModel *attribute.Model

	// records
	attributeRec       *record.AppAttributeRecord
	entityAttributeRec *record.AppEntityAttributeRecord

	// Name -
	Name string

	// AssignedValue - assigned value
	AssignedValue int

	// CalculatedValue - calculated value from formula
	CalculatedValue int

	// MinValue - minimum value
	MinValue int

	// MaxValue - maximum value
	MaxValue int

	// AdjustedValue - calculated value + adjustment
	AdjustedValue int

	// AdjustmentTotal - total adjustment value
	AdjustmentTotal int
}

// NewEntityAttribute -
func NewEntityAttribute(e env.Env, l zerolog.Logger, ms *repostore.RepoStore, entityAttributeID string) (instance *EntityAttribute, err error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	instance = &EntityAttribute{
		AppEntityAttributeID: entityAttributeID,
		Env:                  e,
		Logger:               l,
		RepoStore:            ms,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EntityAttribute) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Entity Attribute ID >%s< =====>", e.AppEntityAttributeID)

	// models
	e.entityModel, err = entity.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity model >%v<", err)
		return err
	}

	e.attributeModel, err = attribute.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new attribute model >%v<", err)
		return err
	}

	err = e.initEntityAttribute()
	if err != nil {
		l.Warn().Msgf("Failed to init entity attribute >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Initialise Entity Attribute =====>")

	return nil
}

// InitEntityAttribute -
func (e *EntityAttribute) initEntityAttribute() (err error) {

	l := e.Logger.With().Str("function", "InitEntityAttribute").Logger()

	// get entity attribute record
	entityAttributeRec, err := e.entityModel.GetEntityAttributeRec(e.AppEntityAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute rec >%v<", err)
		return err
	}

	// get attribute record
	attributeRec, err := e.attributeModel.GetAttributeRec(entityAttributeRec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute rec >%v<", err)
		return err
	}

	// properties
	e.entityAttributeRec = entityAttributeRec
	e.attributeRec = attributeRec

	e.Name = attributeRec.Name
	e.AssignedValue = entityAttributeRec.Value
	e.CalculatedValue = 0
	e.AdjustedValue = 0
	e.AdjustmentTotal = 0

	return nil
}

// CalculateValues - calculates the base, min and max values
func (e *EntityAttribute) CalculateValues(params map[string]int) error {

	l := e.Logger.With().Str("function", "CalculateValues").Logger()

	l.Debug().Msgf("Calculating value attribute Name >%s< Assigned >%d<", e.Name, e.AssignedValue)

	calculatedValue, minValue, maxValue, err := e.attributeModel.GetEntityAttributeValue(e.entityAttributeRec, params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute value >%v<", err)
		return err
	}

	l.Debug().Msgf("Attribute Name >%s< calculated >%d<", e.Name, calculatedValue)

	// calculated value
	e.CalculatedValue = calculatedValue

	// minimum value
	e.MinValue = minValue

	// maximum value
	e.MaxValue = maxValue

	// adjusted value
	e.AdjustedValue = e.CalculatedValue + e.AdjustmentTotal

	return nil
}

// AddAdjustment -
func (e *EntityAttribute) AddAdjustment(adjustment int) (bool, error) {

	l := e.Logger.With().Str("function", "AddAdjustment").Logger()

	adjustedTotal := e.CalculatedValue + e.AdjustmentTotal + adjustment
	if adjustedTotal > e.MaxValue {
		l.Debug().Msgf("Attribute Name >%s< adjusted total >%d< greater than maximum >%d<, not adding adjustment >%d<",
			e.Name, adjustedTotal, e.MaxValue, adjustment,
		)
		return false, nil
	}
	if adjustedTotal < e.MinValue {
		l.Debug().Msgf("Attribute Name >%s< adjusted total >%d< less than minimum >%d<, not adding adjustment >%d<",
			e.Name, adjustedTotal, e.MinValue, adjustment,
		)
		return false, nil
	}

	e.AdjustmentTotal = e.AdjustmentTotal + adjustment
	e.AdjustedValue = e.CalculatedValue + e.AdjustmentTotal

	l.Debug().Msgf("Attribute Name >%s< added adjustment >%d<", e.Name, adjustment)

	return true, nil
}

// RemoveAdjustment -
func (e *EntityAttribute) RemoveAdjustment(adjustment int) (bool, error) {

	l := e.Logger.With().Str("function", "RemoveAdjustment").Logger()

	adjustedTotal := e.CalculatedValue + e.AdjustmentTotal - adjustment
	if adjustedTotal > e.MaxValue {
		l.Debug().Msgf("Attribute Name >%s< adjusted total >%d< greater than maximum >%d<, not removing adjustment >%d<",
			e.Name, adjustedTotal, e.MaxValue, adjustment,
		)
		return false, nil
	}
	if adjustedTotal < e.MinValue {
		l.Debug().Msgf("Attribute Name >%s< adjusted total >%d< less than 0, not removing adjustment >%d<",
			e.Name, adjustedTotal, adjustment,
		)
		return false, nil
	}

	e.AdjustmentTotal = e.AdjustmentTotal - adjustment
	e.AdjustedValue = e.CalculatedValue + e.AdjustmentTotal

	return true, nil
}

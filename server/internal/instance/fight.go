package instance

import (
	"fmt"
	"sort"

	"github.com/rs/zerolog"

	// models
	appmodel "gitlab.com/alienspaces/arena-tactics/server/internal/model/app"
	entitygroupmodel "gitlab.com/alienspaces/arena-tactics/server/internal/model/entitygroup"
	fightmodel "gitlab.com/alienspaces/arena-tactics/server/internal/model/fight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Fight -
type Fight struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// FightID
	FightID         string
	FightInstanceID string

	// models
	appModel         *appmodel.Model
	fightModel       *fightmodel.Model
	entityGroupModel *entitygroupmodel.Model

	// records
	appRec           *record.AppRecord
	fightRec         *record.AppFightRecord
	fightInstanceRec *record.AppFightInstanceRecord

	// engine instances
	EntityGroups []*EntityGroup
	Tactic       *Tactic

	// fight properties
	Turn   int
	Status string
}

// InitiativeFunc - function to determine entity initiative score
type InitiativeFunc func(entityParams map[string]int) (int, error)

// NewFight -
func NewFight(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, fightID string) (instance *Fight, err error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	instance = &Fight{
		FightID:   fightID,
		Env:       e,
		Logger:    l,
		RepoStore: rs,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("NewFight failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init - Initializes all entity attributes, tactics, items and skills
func (f *Fight) Init() (err error) {

	l := f.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("Initialising")

	// init models
	f.appModel, err = appmodel.NewModel(f.Env, f.Logger, f.RepoStore)
	f.fightModel, err = fightmodel.NewModel(f.Env, f.Logger, f.RepoStore)
	f.entityGroupModel, err = entitygroupmodel.NewModel(f.Env, f.Logger, f.RepoStore)

	// init fight
	err = f.InitFight()
	if err != nil {
		l.Warn().Msgf("Failed init fight >%v<", err)
		return err
	}

	// init entity items
	err = f.InitEntityGroups()
	if err != nil {
		l.Warn().Msgf("Failed init entity groups >%v<", err)
		return err
	}

	// init tactic
	err = f.InitTactic()
	if err != nil {
		l.Warn().Msgf("Failed init tactic >%v<", err)
		return err
	}

	// NOTE: we save the initial fight instance when the fight is
	// first starting so that we have all entities saved at turn 0
	// with full stats
	err = f.SetEntityState()
	if err != nil {
		l.Warn().Msgf("Failed to set entity state >%v<", err)
		return err
	}

	// cycle through entities from all groups ordered by
	// an app level configured attribute
	entities, err := f.GetOrderedEntities()
	if err != nil {
		l.Warn().Msgf("Failed to get ordered entities >%v<", err)
		return err
	}

	// update entity passive effects
	err = f.UpdateEntityPassiveEffects(entities)
	if err != nil {
		l.Warn().Msgf("Failed to update passive effects >%v<", err)
		return err
	}

	return nil
}

// InitFight -
func (f *Fight) InitFight() (err error) {

	l := f.Logger.With().Str("function", "InitFight").Logger()

	// get fight record
	fightRec, err := f.fightModel.GetFightRec(f.FightID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight rec >%v<", err)
		return err
	}

	f.fightRec = fightRec

	// fight properties
	f.Status = fightRec.Status

	// get "fighting" fight instance record
	fightInstanceRec, err := f.fightModel.GetFightingFightInstanceRec(f.FightID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance rec >%v<", err)
		return err
	}

	if fightInstanceRec == nil {
		l.Debug().Msgf("Creating new fight instance for fight ID >%s<", f.FightID)

		// create fight instance record
		fightInstanceRec, err = f.fightModel.CreateFightInstanceRec(f.FightID)
		if err != nil {
			l.Warn().Msgf("Failed to create fight instance rec >%v<", err)
			return err
		}
	}

	// clear fight instance events
	err = f.fightModel.ClearFightInstanceEvents(fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to clear fight instance events >%v<", err)
		return err
	}

	// fight instance
	f.FightInstanceID = fightInstanceRec.ID
	f.fightInstanceRec = fightInstanceRec

	// properties
	f.Turn = fightInstanceRec.Turn

	// get app record
	appRec, err := f.appModel.GetAppRec(fightRec.AppID)
	if err != nil {
		l.Warn().Msgf("Failed to get app rec >%v<", err)
		return err
	}

	f.appRec = appRec

	return nil
}

// InitEntityGroups -
func (f *Fight) InitEntityGroups() (err error) {

	l := f.Logger.With().Str("function", "InitEntityGroups").Logger()

	l.Debug().Msgf("<= Initialise entity groups =>")

	recs, err := f.fightModel.GetFightEntityGroupRecs(f.FightID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity groups >%v<", err)
		return err
	}

	for _, rec := range recs {

		l.Debug().Msgf("Initialising entity group ID >%s<", rec.ID)

		// new entity group
		entityGroup, err := NewEntityGroup(f.Env, f.Logger, f.RepoStore, rec.AppEntityGroupID)
		if err != nil {
			l.Warn().Msgf("Failed new entity group >%v<", err)
			return err
		}

		f.EntityGroups = append(f.EntityGroups, entityGroup)
	}

	l.Debug().Msgf("<= Initialise entity groups done =>")

	return nil
}

// InitTactic -
func (f *Fight) InitTactic() (err error) {

	l := f.Logger.With().Str("function", "InitTactic").Logger()

	tactic, err := NewTactic(f.Env, f.Logger, f.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new tactic >%v<", err)
		return err
	}

	f.Tactic = tactic

	return nil
}

// SetEntityState -
func (f *Fight) SetEntityState() error {

	l := f.Logger.With().Str("function", "SetEntityState").Logger()

	l.Debug().Msgf("<= Setting entity state =>")

	// get instance data
	data, err := f.fightModel.GetFightInstanceData(f.fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance data >%v<", err)
		return err
	}

	l.Debug().Msgf("Have state data >%v< =>", data.Entities)

	// set each entities current state
	for _, entityGroup := range f.EntityGroups {

		for _, ent := range entityGroup.Entities {

			entState, ok := data.Entities[ent.AppEntityID]
			if ok {
				l.Debug().Msgf("Reinstating entity Name >%s< ID >%s< state", ent.Name, ent.AppEntityID)

				err := ent.SetState(entState)
				if err != nil {
					l.Warn().Msgf("Failed to set entity state >%v<", err)
					return err
				}
			} else {
				l.Debug().Msgf("No current state for entity Name >%s< ID >%s<, skipping..", ent.Name, ent.AppEntityID)
			}

			l.Debug().Msgf("Setting attribute values Name >%s< ID >%s<", ent.Name, ent.AppEntityID)

			err = ent.CalculateAttributeValues()
			if err != nil {
				l.Warn().Msgf("Failed to calculate attribute values >%v<", err)
				return err
			}
		}
	}

	l.Debug().Msgf("<= Setting entity state done =>")

	return nil
}

// TODO:
// - Need a way to indicate that there is a winning
// group as a result of run
// - Run should not run if the fight already has a
// winner

// Run -
func (f *Fight) Run() (err error) {

	l := f.Logger.With().Str("function", "Run").Logger()

	l.Debug().Msg("<===== Run =====>")

	// increment turn
	f.Turn++

	// set fight status to fighting
	if f.Status == record.AppFightStatusStarting {
		f.Status = record.AppFightStatusFighting
	}

	if f.Status != record.AppFightStatusStarting &&
		f.Status != record.AppFightStatusFighting {
		l.Warn().Msgf("Fight status >%s< is not starting and not fighting, cannot run", f.Status)
		return fmt.Errorf("Fight status >%s< is not starting and not fighting, cannot run", f.Status)
	}

	// set entity state
	err = f.SetEntityState()
	if err != nil {
		l.Warn().Msgf("Failed to set entity state >%v<", err)
		return err
	}

	// cycle through entities from all groups ordered by
	// an app level configured attribute
	entities, err := f.GetOrderedEntities()
	if err != nil {
		l.Warn().Msgf("Failed to get ordered entities >%v<", err)
		return err
	}

	// update entity passive effects
	err = f.UpdateEntityPassiveEffects(entities)
	if err != nil {
		l.Warn().Msgf("Failed to update passive effects >%v<", err)
		return err
	}

	// update entity active effects
	err = f.UpdateEntityActiveEffects(entities)
	if err != nil {
		l.Warn().Msgf("Failed to update active effects >%v<", err)
		return err
	}

	// process tactics
	for _, ent := range entities {

		// dead
		if ent.Dead == true {
			l.Debug().Msgf("Entity Name >%s< ID >%s< is dead, not apply passive effects", ent.Name, ent.AppEntityID)
			continue
		}

		l.Debug().Msgf("Process tactics Name >%s< ID >%s<", ent.Name, ent.AppEntityID)

		err := f.ProcessEntityTactics(ent)
		if err != nil {
			l.Warn().Msgf("Failed process tactics Name >%s< ID >%s< tactics >%v<", ent.Name, ent.AppEntityID, err)
			return err
		}

		// all foes dead?
		allFoesDead, err := f.checkAllFoesDead(ent)
		if err != nil {
			l.Warn().Msgf("Failed checking all foes dead >%v<", err)
			return err
		}

		// this entity struck the winning blow!
		if allFoesDead {
			f.Status = record.AppFightStatusDone
		}
	}

	l.Debug().Msg("<===== Done Run =====>")

	return nil
}

// UpdateEntityPassiveEffects -
func (f *Fight) UpdateEntityPassiveEffects(entities []*Entity) error {

	l := f.Logger.With().Str("function", "UpdateEntityPassiveEffects").Logger()

	l.Debug().Msg("<===== Update Entity Passive Effects =====>")

	// NOTE: From turn to turn an entities friend and foe target may change
	// which means any passive effect targeting a friend or foe last turn
	// should be removed first
	for _, ent := range entities {

		// dead
		if ent.Dead == true {
			l.Debug().Msgf("Entity ID >%s< Name >%s< is dead, not apply passive effects", ent.AppEntityID, ent.Name)
			continue
		}

		err := ent.RemovePassiveEffects()
		if err != nil {
			l.Warn().Msgf("Failed to remove passive effects >%v<", err)
			return err
		}
	}

	// check and apply passive item and skill effects to current friend
	// and foe targets
	for _, ent := range entities {

		// dead
		if ent.Dead == true {
			l.Debug().Msgf("Entity ID >%s< Name >%s< is dead, not apply passive effects", ent.AppEntityID, ent.Name)
			continue
		}

		err := f.ApplyEntityPassiveEffects(ent)
		if err != nil {
			l.Warn().Msgf("Failed to init effects >%v<", err)
			return err
		}
	}

	l.Debug().Msg("<===== Update Entity Passive Effects Done =====>")

	return nil
}

// UpdateEntityActiveEffects -
func (f *Fight) UpdateEntityActiveEffects(entities []*Entity) error {

	l := f.Logger.With().Str("function", "UpdateEntityActiveEffects").Logger()

	l.Debug().Msg("<===== Update Entity Active Effects =====>")

	// get instance data
	data, err := f.fightModel.GetFightInstanceData(f.fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance data >%v<", err)
		return err
	}

	effectEvents := []*instance.EffectEvent{}

	// update active duration based item and skill effects for
	// all entities before processing tactics
	for _, ent := range entities {

		// dead
		if ent.Dead == true {

			// NOTE: ordered entities should not contain currently dead
			// so this is really a stop-gap..
			l.Warn().Msgf("Entity Name >%s< ID >%s< is dead, not apply passive effects", ent.Name, ent.AppEntityID)
			continue
		}

		l.Debug().Msgf("Updating effects entity Name >%s< ID >%s<", ent.Name, ent.AppEntityID)

		entityEffectEvents, err := ent.UpdateActiveEffects()
		if err != nil {
			l.Warn().Msgf("Failed to update effects >%v<", err)
			return err
		}

		if len(entityEffectEvents) != 0 {
			l.Debug().Msgf("Entity Name >%s< had >%d< effect effects applied", ent.Name, len(entityEffectEvents))
			for _, effectEvent := range entityEffectEvents {
				effectEvents = append(effectEvents, effectEvent)
			}
		}

		// check dead
		dead, err := f.checkEntityDead(ent)
		if err != nil {
			l.Warn().Msgf("Failed to check entity dead >%v<", err)
			return err
		}

		if dead == true {
			l.Debug().Msgf("Entity Name >%s< ID >%s< is now dead", ent.Name, ent.AppEntityID)
			ent.Dead = true
		}
	}

	// append all effect events
	for _, effectEvent := range effectEvents {
		data.EffectEvents = append(data.EffectEvents, effectEvent)
	}

	// set instance data
	err = f.fightModel.SetFightInstanceData(f.fightInstanceRec, data)
	if err != nil {
		l.Warn().Msgf("Failed to set fight instance data >%v<", err)
		return err
	}

	l.Debug().Msg("<===== Update Entity Active Effects Done =====>")

	return nil
}

// ApplyEntityPassiveEffects -
func (f *Fight) ApplyEntityPassiveEffects(ent *Entity) error {

	l := f.Logger.With().Str("function", "ApplyEntityPassiveEffects").Logger()

	l.Debug().Msgf("<=== Apply Entity Passive Effects Entity Name >%s< ===>", ent.Name)

	for _, item := range ent.Items {

		_, err := f.ApplyEntityItemEffects(ent, item, true)
		if err != nil {
			l.Warn().Msgf("Failed applying entity item effects >%v<", err)
			return err
		}
	}

	for _, skill := range ent.Skills {

		_, err := f.ApplyEntitySkillEffects(ent, skill, true)
		if err != nil {
			l.Warn().Msgf("Failed applying entity skill effects >%v<", err)
			return err
		}
	}

	l.Debug().Msgf("<=== Done Apply Entity Passive Effects Entity Name >%s< ===>", ent.Name)

	return nil
}

// ApplyEntityItemEffects -
func (f *Fight) ApplyEntityItemEffects(
	ent *Entity,
	item *EntityItem,
	passive bool) ([]*instance.TacticEvent, error) {

	l := f.Logger.With().Str("function", "ApplyEntityItemEffects").Logger()

	l.Debug().Msgf("<= Apply Entity Item Effects source entity Name >%s< item Name >%s< Effects >%d< =>",
		ent.Name,
		item.Name,
		len(item.Effects),
	)

	tacticEvents := []*instance.TacticEvent{}

	for _, itemEffect := range item.Effects {

		// passive effects only
		if passive && itemEffect.EffectType == record.EffectTypeActive {
			l.Debug().Msg("Passive only, skipping active effect")
			continue
		}

		l.Debug().Msgf("Apply item effect Name >%s< Type >%s< Duration >%d< Recurring >%t< Permanent >%t<",
			itemEffect.EffectName,
			itemEffect.EffectType,
			itemEffect.EffectDuration,
			itemEffect.EffectRecurring,
			itemEffect.EffectPermanent,
		)

		// cycle through item effect attributes and apply to appropriate target
		for _, effectAttribute := range itemEffect.Attributes {

			var fightEvent *instance.TacticEvent

			switch target := effectAttribute.Target; target {
			case record.TargetSelf:

				l.Debug().Msgf("Apply item effects target entity Name >%s< ID >%s< Attribute >%s<",
					ent.Name, ent.AppEntityID, effectAttribute.AttributeName)

				// TODO: return effects as effect events or something so we can create effect events

				// apply item effect
				effectEvents, err := ent.ApplyItemEffect(ent.AppEntityID, record.TargetSelf, itemEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply item effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: ent.AppEntityID,
						TargetName:        ent.Name,
						Action:            record.EntityTacticActionUseItem,
						AppItemID:         item.AppItemID,
						ItemName:          item.Name,
						EffectEvents:      effectEvents,
					}
				}

			case record.TargetAnyFoe:
				// TODO: random foe target

			case record.TargetCurrentFoe:
				entityID := ent.FoeTargetEntityID
				if entityID == "" {
					l.Debug().Msgf("Entity has no current foe target, not applying attribute effect")
					continue
				}
				targetEnt, err := f.getEntity(entityID)
				if err != nil {
					l.Warn().Msgf("Failed to get entity >%v<", err)
					return nil, err
				}

				l.Debug().Msgf("Apply item effects target entity Name >%s< ID >%s< Attribute >%s<",
					targetEnt.Name, targetEnt.AppEntityID, effectAttribute.AttributeName)

				effectEvents, err := targetEnt.ApplyItemEffect(ent.AppEntityID, record.TargetCurrentFoe, itemEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply skill effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: targetEnt.AppEntityID,
						TargetName:        targetEnt.Name,
						Action:            record.EntityTacticActionUseItem,
						AppItemID:         item.AppItemID,
						ItemName:          item.Name,
						EffectEvents:      effectEvents,
					}
				}

			case record.TargetAnyFriend:
				// TODO: random friend target

			case record.TargetCurrentFriend:
				entityID := ent.FriendTargetEntityID
				if entityID == "" {
					l.Debug().Msgf("Entity has no current friend target, not applying attribute effect")
					continue
				}
				targetEnt, err := f.getEntity(entityID)
				if err != nil {
					l.Warn().Msgf("Failed to get entity >%v<", err)
					return nil, err
				}

				l.Debug().Msgf("Apply item effects target entity Name >%s< ID >%s< Attribute >%s<",
					targetEnt.Name, targetEnt.AppEntityID, effectAttribute.AttributeName)

				effectEvents, err := targetEnt.ApplyItemEffect(ent.AppEntityID, record.TargetCurrentFriend, itemEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply skill effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: targetEnt.AppEntityID,
						TargetName:        targetEnt.Name,
						Action:            record.EntityTacticActionUseItem,
						AppItemID:         item.AppItemID,
						ItemName:          item.Name,
						EffectEvents:      effectEvents,
					}
				}

			default:
				l.Warn().Msgf("Effect attribute target >%s< is not valid", effectAttribute.Target)
				return nil, fmt.Errorf("Effect attribute target >%s< is not valid", effectAttribute.Target)
			}

			if fightEvent != nil {
				l.Debug().Msgf("Adding fight event >%#v<", fightEvent)
				tacticEvents = append(tacticEvents, fightEvent)
			}
		}
	}

	l.Debug().Msgf("<= Done Apply Entity Item Effects =>")

	return tacticEvents, nil
}

// ApplyEntitySkillEffects -
func (f *Fight) ApplyEntitySkillEffects(
	ent *Entity,
	skill *EntitySkill,
	passive bool) ([]*instance.TacticEvent, error) {

	l := f.Logger.With().Str("function", "ApplyEntitySkillEffects").Logger()

	l.Debug().Msgf("<= Apply Entity Skill Effects source entity Name >%s< skill Name >%s< Effects >%d< =>",
		ent.Name, skill.Name, len(skill.Effects))

	tacticEvents := []*instance.TacticEvent{}

	for _, skillEffect := range skill.Effects {

		// passive effects only
		if passive && skillEffect.EffectType == record.EffectTypeActive {
			l.Debug().Msg("Passive only, skipping active effect")
			continue
		}

		l.Debug().Msgf("Apply skill effect Name >%s< Type >%s< Duration >%d< Recurring >%t< Permanent >%t<",
			skillEffect.EffectName,
			skillEffect.EffectType,
			skillEffect.EffectDuration,
			skillEffect.EffectRecurring,
			skillEffect.EffectPermanent,
		)

		// cycle through skill effect attributes and apply to appropriate target
		for _, effectAttribute := range skillEffect.Attributes {

			var fightEvent *instance.TacticEvent

			switch target := effectAttribute.Target; target {

			case record.TargetSelf:

				l.Debug().Msgf("Apply skill effects target entity Name >%s< ID >%s< Attribute >%s<",
					ent.Name, ent.AppEntityID, effectAttribute.AttributeName)

				// we have the entity
				effectEvents, err := ent.ApplySkillEffect(ent.AppEntityID, record.TargetSelf, skillEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply skill effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: ent.AppEntityID,
						TargetName:        ent.Name,
						Action:            record.EntityTacticActionUseSkill,
						AppSkillID:        skill.AppSkillID,
						SkillName:         skill.Name,
						EffectEvents:      effectEvents,
					}
				}

			case record.TargetAnyFoe:
				// TODO: random foe target

			case record.TargetCurrentFoe:
				entityID := ent.FoeTargetEntityID
				if entityID == "" {
					l.Debug().Msgf("Entity has no current foe target, not applying attribute effect")
					continue
				}
				targetEnt, err := f.getEntity(entityID)
				if err != nil {
					l.Warn().Msgf("Failed to get entity >%v<", err)
					return nil, err
				}

				l.Debug().Msgf("Apply skill effects target entity Name >%s< ID >%s< Attribute >%s<",
					targetEnt.Name, targetEnt.AppEntityID, effectAttribute.AttributeName)

				effectEvents, err := targetEnt.ApplySkillEffect(ent.AppEntityID, record.TargetCurrentFoe, skillEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply skill effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: targetEnt.AppEntityID,
						TargetName:        targetEnt.Name,
						Action:            record.EntityTacticActionUseSkill,
						AppSkillID:        skill.AppSkillID,
						SkillName:         skill.Name,
						EffectEvents:      effectEvents,
					}
				}

			case record.TargetAnyFriend:
				// TODO: random friend target

			case record.TargetCurrentFriend:
				entityID := ent.FriendTargetEntityID
				if entityID == "" {
					l.Debug().Msgf("Entity has no current friend target, not applying attribute effect")
					continue
				}

				targetEnt, err := f.getEntity(entityID)
				if err != nil {
					l.Warn().Msgf("Failed to get entity >%v<", err)
					return nil, err
				}

				l.Debug().Msgf("Apply skill effect target entity Name >%s< ID >%s< Attribute >%s<",
					targetEnt.Name, targetEnt.AppEntityID, effectAttribute.AttributeName)

				effectEvents, err := targetEnt.ApplySkillEffect(ent.AppEntityID, record.TargetCurrentFriend, skillEffect)
				if err != nil {
					l.Warn().Msgf("Failed apply skill effect >%v<", err)
					return nil, err
				}

				if len(effectEvents) != 0 {
					fightEvent = &instance.TacticEvent{
						ID:                util.GetUUID(),
						SourceAppEntityID: ent.AppEntityID,
						SourceName:        ent.Name,
						TargetAppEntityID: targetEnt.AppEntityID,
						TargetName:        targetEnt.Name,
						Action:            record.EntityTacticActionUseSkill,
						AppSkillID:        skill.AppSkillID,
						SkillName:         skill.Name,
						EffectEvents:      effectEvents,
					}
				}

			default:
				l.Warn().Msgf("Effect attribute target >%s< is not valid", effectAttribute.Target)
				return nil, fmt.Errorf("Effect attribute target >%s< is not valid", effectAttribute.Target)
			}

			if fightEvent != nil {
				l.Debug().Msgf("Adding fight event >%#v<", fightEvent)
				tacticEvents = append(tacticEvents, fightEvent)
			}
		}
	}

	l.Debug().Msgf("<= Done Apply Entity Skill Effects =>")

	return tacticEvents, nil
}

// ProcessEntityTactics -
func (f *Fight) ProcessEntityTactics(ent *Entity) error {

	l := f.Logger.With().Str("function", "ProcessEntityTactics").Logger()

	l.Debug().Msgf("<===== Process Tactics Entity Name >%s< =====>", ent.Name)

	// get instance data
	data, err := f.fightModel.GetFightInstanceData(f.fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance data >%v<", err)
		return err
	}

	friends, foes, err := f.getEntityFriendsFoes(ent)
	if err != nil {
		l.Warn().Msgf("Failed to get entity friends and foes >%v<", err)
		return err
	}

	// process entity tactics
	tacticResult, err := f.Tactic.Process(ent, friends, foes)

	// TODO: Implement default tactic to execute when no
	// other tactics find a target
	if tacticResult == nil {
		l.Warn().Msgf("No tactic results returned")
		return nil
	}

	if tacticResult.FriendTarget != nil {
		l.Debug().Msgf("Tactics results friendTarget ID >%s< Name >%s<", tacticResult.FriendTarget.AppEntityID, tacticResult.FriendTarget.Name)
		ent.FriendTargetEntityID = tacticResult.FriendTarget.AppEntityID
	}

	if tacticResult.FoeTarget != nil {
		l.Debug().Msgf("Tactics results foeTarget ID >%s< Name >%s<", tacticResult.FoeTarget.AppEntityID, tacticResult.FoeTarget.Name)
		ent.FoeTargetEntityID = tacticResult.FoeTarget.AppEntityID
	}

	l.Debug().Msgf("Tactics results action >%s<", tacticResult.Action)

	tacticEvents := []*instance.TacticEvent{}

	if tacticResult.Action == record.EntityTacticActionUseSkill &&
		tacticResult.AppSkillID != "" {

		l.Debug().Msgf("Tactics results entity skill ID >%s<", tacticResult.AppSkillID)

		// get entity skill
		entitySkill, err := ent.GetEntitySkill(tacticResult.AppSkillID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity skill ID >%s< >%v<", tacticResult.AppSkillID, err)
			return err
		}

		// apply entity skill effects
		tacticEvents, err = f.ApplyEntitySkillEffects(ent, entitySkill, false)
		if err != nil {
			l.Warn().Msgf("Failed to apply entity skill ID >%s< >%v<", tacticResult.AppSkillID, err)
			return err
		}

	} else if tacticResult.Action == record.EntityTacticActionUseItem &&
		tacticResult.AppItemID != "" {

		l.Debug().Msgf("Tactics results entity item ID >%s<", tacticResult.AppItemID)

		// get entity item
		entityItem, err := ent.GetEntityItem(tacticResult.AppItemID)
		if err != nil {
			l.Warn().Msgf("Failed to get entity item ID >%s< >%v<", tacticResult.AppItemID, err)
			return err
		}

		// apply entity item effects
		tacticEvents, err = f.ApplyEntityItemEffects(ent, entityItem, false)
		if err != nil {
			l.Warn().Msgf("Failed to apply entity item ID >%s< >%v<", tacticResult.AppSkillID, err)
			return err
		}
	}

	// fight events
	l.Debug().Msgf("Have fight events >%#v<", tacticEvents)

	// append all events
	for _, tacticEvent := range tacticEvents {
		data.TacticEvents = append(data.TacticEvents, tacticEvent)
	}

	// set instance data
	err = f.fightModel.SetFightInstanceData(f.fightInstanceRec, data)
	if err != nil {
		l.Warn().Msgf("Failed to set fight instance data >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Process Tactics ==>")

	return nil
}

// checkAllFoesDead -
func (f *Fight) checkAllFoesDead(ent *Entity) (bool, error) {

	l := f.Logger.With().Str("function", "checkAllFoesDead").Logger()

	l.Debug().Msgf("<= Check All Foes Dead entity Name >%s< =>", ent.Name)

	_, foes, err := f.getEntityFriendsFoes(ent)
	if err != nil {
		l.Warn().Msgf("Failed to get entity friends and foes >%v<", err)
		return false, err
	}

	allDead := true
	if len(foes) == 0 {
		l.Debug().Msgf("No foes, must all be dead!")
		return allDead, nil
	}

	for _, foe := range foes {

		if foe.Dead != true {
			l.Debug().Msgf("Foe Name >%s< ID >%s< not dead yet", foe.Name, foe.AppEntityID)

			// check dead
			dead, err := f.checkEntityDead(foe)
			if err != nil {
				l.Warn().Msgf("Failed to check entity dead >%v<", err)
				return false, err
			}

			if dead == true {
				l.Debug().Msgf("Entity Name >%s< ID >%s< is now dead", ent.Name, ent.AppEntityID)
				foe.Dead = true
				continue
			}

			allDead = false
			continue
		}

		l.Debug().Msgf("Foe Name >%s< ID >%s< dead", foe.Name, foe.AppEntityID)
	}

	l.Debug().Msgf("<= Done Check All Foes Dead entity Name >%s< =>", ent.Name)

	return allDead, nil
}

func (f *Fight) getEntityFriendsFoes(ent *Entity) (friends []*Entity, foes []*Entity, err error) {

	l := f.Logger.With().Str("function", "getEntityFriendsFoes").Logger()

	l.Debug().Msgf("Getting entity Name >%s< ID >%s< friends and foes", ent.Name, ent.AppEntityID)

	for _, entityGroup := range f.EntityGroups {
		for _, groupMember := range entityGroup.Entities {

			if groupMember.Dead == true {
				continue
			}

			if groupMember.AppEntityID == ent.AppEntityID {
				continue
			}

			// same group then they are friends
			if groupMember.AppEntityGroupID == ent.AppEntityGroupID {
				friends = append(friends, groupMember)
			} else {
				foes = append(foes, groupMember)
			}
		}
	}

	return friends, foes, nil
}

func (f *Fight) getEntity(entityID string) (*Entity, error) {

	l := f.Logger.With().Str("function", "getEntity").Logger()

	for _, entityGroup := range f.EntityGroups {
		for _, ent := range entityGroup.Entities {
			if ent.AppEntityID == entityID {
				return ent, nil
			}
		}
	}

	l.Warn().Msgf("Could not find entity ID >%s<", entityID)

	return nil, fmt.Errorf("Could not find entity ID >%s<", entityID)
}

// GetOrderedEntities - returns list of all entities engaged in the
// fight ordered by initiative
func (f *Fight) GetOrderedEntities() ([]*Entity, error) {

	l := f.Logger.With().Str("function", "getOrderedEntities").Logger()

	l.Debug().Msgf("Ordering entities by initiative")

	initiativeFunc := func(entityParams map[string]int) (int, error) {
		return f.appModel.GetInitiativeValue(f.appRec, entityParams)
	}

	sortEntitiesByInitiative := SortEntitiesByInitiative{
		Logger:         f.Logger,
		InitiativeFunc: initiativeFunc,
	}

	// collate all entities
	for _, entityGroup := range f.EntityGroups {
		for _, entityGroupEntity := range entityGroup.Entities {

			// skip if dead
			if entityGroupEntity.Dead {
				l.Debug().Msgf("Entity Name >-15%s< ID >%10s< is dead, not adding", entityGroupEntity.Name, entityGroupEntity.AppEntityID)
				continue
			}

			sortEntitiesByInitiative.Entities = append(sortEntitiesByInitiative.Entities, entityGroupEntity)
		}
	}

	// sort entities
	sort.Sort(sortEntitiesByInitiative)

	return sortEntitiesByInitiative.Entities, nil
}

func (f *Fight) checkEntityDead(ent *Entity) (bool, error) {

	l := f.Logger.With().Str("function", "checkEntityDead").Logger()

	adjustedValues := make(map[string]int)

	// TODO: Perhaps roll this loop into an entity instance function
	for _, entityAttr := range ent.Attributes {
		l.Debug().Msgf("Adding attribute name >%s< adjusted valued >%d<", entityAttr.Name, entityAttr.AdjustedValue)
		adjustedValues[entityAttr.Name] = entityAttr.AdjustedValue
	}

	return f.appModel.GetDeathValue(f.appRec, adjustedValues)
}

// Save - saves fight instance data
func (f *Fight) Save() (err error) {

	l := f.Logger.With().Str("function", "Save").Logger()

	l.Debug().Msgf("<= Save fight ID >%s< =>", f.FightID)

	// get instance data
	data, err := f.fightModel.GetFightInstanceData(f.fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance data >%v<", err)
		return err
	}

	// entity instance data
	data.Entities = make(map[string]*instance.EntityData)

	// entity groups
	for _, entityGroup := range f.EntityGroups {

		err := entityGroup.Save()
		if err != nil {
			l.Warn().Msgf("Failed entity group save >%v<", err)
			return err
		}

		// entity instance data
		for _, ent := range entityGroup.Entities {

			l.Debug().Msgf("Save entity Name >%s< ID >%s< state data", ent.Name, ent.AppEntityID)

			entityStateData, err := ent.GetState()
			if err != nil {
				l.Warn().Msgf("Failed to get entity state data >%v<", err)
				return err
			}

			data.Entities[ent.AppEntityID] = entityStateData
		}
	}

	// TODO: Update fight instance data with whatever additional
	// data we need here which will be the results of each entity
	// tactics and skill/item execution.

	// set instance data
	err = f.fightModel.SetFightInstanceData(f.fightInstanceRec, data)
	if err != nil {
		l.Warn().Msgf("Failed to set fight instance data >%v<", err)
		return err
	}

	l.Debug().Msgf("Have fight state data >%s<", f.fightInstanceRec.Data)

	// turn
	f.fightInstanceRec.Turn = f.Turn

	// done
	if f.Status == record.AppFightStatusDone {
		f.fightInstanceRec.Status = record.AppFightInstanceStatusDone
	}

	// create fight instance turn
	turnRec := &record.AppFightInstanceTurnRecord{
		AppFightID:         f.fightInstanceRec.AppFightID,
		AppFightInstanceID: f.fightInstanceRec.ID,
		Turn:               f.fightInstanceRec.Turn,
		Data:               f.fightInstanceRec.Data,
	}

	err = f.fightModel.CreateFightInstanceTurnRec(turnRec)
	if err != nil {
		l.Warn().Msgf("Failed to create fight instance turn record >%v<", err)
		return err
	}

	// update fight instance record
	err = f.fightModel.UpdateFightInstanceRec(f.fightInstanceRec)
	if err != nil {
		l.Warn().Msgf("Failed to update fight instance data >%v<", err)
		return err
	}

	// update fight record
	f.fightRec.Status = f.Status

	err = f.fightModel.UpdateFightRec(f.fightRec)
	if err != nil {
		l.Warn().Msgf("Failed to update fight record >%v<", err)
		return err
	}

	l.Debug().Msgf("<= Save fight ID >%s< done", f.FightID)

	return nil
}

// Cleanup - Removes all fight instance data which isn't a good idea
// if there are fights that are not done etc. This function is
// used for testing purposes at this stage.
func (f *Fight) Cleanup() (err error) {

	l := f.Logger.With().Str("function", "Save").Logger()

	l.Debug().Msg("Cleanup fight cleaning")

	// entity groups
	for _, entityGroup := range f.EntityGroups {
		err := entityGroup.Cleanup()
		if err != nil {
			l.Warn().Msgf("Cleanup failed entity group cleanup >%v<", err)
			return err
		}
	}

	if f.fightInstanceRec != nil {
		err = f.fightModel.RemoveFightInstanceRec(f.fightInstanceRec.ID)
		if err != nil {
			l.Warn().Msgf("Cleanup failed to remove fight data >%v<", err)
			return err
		}

		f.fightInstanceRec = nil
	}

	l.Debug().Msg("Cleanup fight done")

	return nil
}

package instance

import (
	"github.com/rs/zerolog"

	// models
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entitygroup"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EntityGroup -
type EntityGroup struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntityGroupID
	AppEntityGroupID string

	// models
	entityGroupModel *entitygroup.Model

	// records
	entityGroupRec        *record.AppEntityGroupRecord
	entityGroupMemberRecs []*record.AppEntityGroupMemberRecord

	// engine instances
	Entities []*Entity
}

// NewEntityGroup -
func NewEntityGroup(e env.Env, l zerolog.Logger, ms *repostore.RepoStore, entityGroupID string) (instance *EntityGroup, err error) {

	// logger
	l = l.With().Str("package", PackageName).Logger()

	instance = &EntityGroup{
		AppEntityGroupID: entityGroupID,
		Env:              e,
		Logger:           l,
		RepoStore:        ms,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init - Initializes all entity attributes, tactics, items and skills
func (e *EntityGroup) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	// models
	e.entityGroupModel, err = entitygroup.NewModel(e.Env, e.Logger, e.RepoStore)

	// init entityGroup
	err = e.InitEntityGroup()
	if err != nil {
		l.Warn().Msgf("Failed init entity group >%v<", err)
		return err
	}

	// init entity items
	err = e.InitEntityGroupMembers()
	if err != nil {
		l.Warn().Msgf("Failed init entity group members >%v<", err)
		return err
	}

	return nil
}

// InitEntityGroup -
func (e *EntityGroup) InitEntityGroup() (err error) {

	l := e.Logger.With().Str("function", "InitEntityGroup").Logger()

	// get entity group record
	entityGroupRec, err := e.entityGroupModel.GetEntityGroupRec(e.AppEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Init failed to get entityGroup rec >%v<", err)
		return err
	}

	e.entityGroupRec = entityGroupRec

	return nil
}

// InitEntityGroupMembers -
func (e *EntityGroup) InitEntityGroupMembers() (err error) {

	l := e.Logger.With().Str("function", "InitEntityGroupMembers").Logger()

	// get entity group member records
	recs, err := e.entityGroupModel.GetEntityGroupMemberRecs(e.AppEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group member records >%v<", err)
		return err
	}

	for _, rec := range recs {

		l.Debug().Msgf("Initialising entity group member ID >%s<", rec.ID)

		// new entity
		entity, err := NewEntity(e.Env, e.Logger, e.RepoStore, rec.AppEntityID)
		if err != nil {
			l.Warn().Msgf("Failed new entity >%v<", err)
			return err
		}

		e.Entities = append(e.Entities, entity)
	}

	e.entityGroupMemberRecs = recs

	return nil
}

// Save - saves fight instance data
func (e *EntityGroup) Save() (err error) {

	// l := e.Logger.With().Str("function", "Save").Logger()

	// l.Debug().Msg("Saving entity group")

	// for _, entity := range e.Entities {
	// 	err := entity.Save()
	// 	if err != nil {
	// 		l.Warn().Msgf("Save failed entity save >%v<", err)
	// 		return err
	// 	}
	// }

	return nil
}

// Cleanup - removes all fight instance data
func (e *EntityGroup) Cleanup() (err error) {

	l := e.Logger.With().Str("function", "Cleanup").Logger()

	l.Debug().Msg("Cleaning up entity group")

	// entities
	// for _, entity := range e.Entities {
	// 	err := entity.Cleanup()
	// 	if err != nil {
	// 		l.Warn().Msgf("Cleanup failed entity cleanup >%v<", err)
	// 		return err
	// 	}
	// }

	e.entityGroupRec = nil
	e.entityGroupMemberRecs = nil
	e.Entities = nil

	return nil
}

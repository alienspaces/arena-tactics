// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"
	"gitlab.com/alienspaces/arena-tactics/server/internal/model/effect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// SkillEffect - effects associated with this skill
type SkillEffect struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppSkillEffectID
	AppSkillEffectID string

	// AppEffectID
	AppEffectID string

	// models
	effectModel *effect.Model

	// records
	skillEffectRec      *record.AppSkillEffectRecord
	effectRec           *record.AppEffectRecord
	effectAttributeRecs []*record.AppEffectAttributeRecord

	// attributes
	Attributes []*EffectAttribute

	// properties
	EffectType      string
	EffectName      string
	EffectDuration  int
	EffectRecurring bool
	EffectPermanent bool
}

// NewSkillEffect -
func NewSkillEffect(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, skillEffectID string) (instance *SkillEffect, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &SkillEffect{
		AppSkillEffectID: skillEffectID,
		Env:              e,
		Logger:           l,
		RepoStore:        rs,
		Attributes:       []*EffectAttribute{},
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *SkillEffect) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Skill Effect ID >%s< =====>", e.AppSkillEffectID)

	// models
	e.effectModel, err = effect.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new effect model >%v<", err)
		return err
	}

	// init skill effect
	err = e.initSkillEffect()
	if err != nil {
		l.Warn().Msgf("Failed init skill effect >%v<", err)
		return err
	}

	// init effect attributes
	err = e.initEffectAttributes()
	if err != nil {
		l.Warn().Msgf("Failed init effect attributes >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Initialise Skill Effect =====>")

	return nil
}

func (e *SkillEffect) initSkillEffect() error {

	l := e.Logger.With().Str("function", "initSkillEffect").Logger()

	skillEffectRec, err := e.effectModel.GetSkillEffectRec(e.AppSkillEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get skill effect rec >%v<", err)
		return err
	}

	effectRec, err := e.effectModel.GetEffectRec(skillEffectRec.AppEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect rec >%v<", err)
		return err
	}

	// set properties
	e.skillEffectRec = skillEffectRec
	e.effectRec = effectRec

	// public properties
	e.AppEffectID = effectRec.ID
	e.EffectType = effectRec.EffectType
	e.EffectName = effectRec.Name
	e.EffectDuration = effectRec.Duration
	e.EffectRecurring = effectRec.Recurring
	e.EffectPermanent = effectRec.Permanent

	return nil
}

func (e *SkillEffect) initEffectAttributes() error {

	l := e.Logger.With().Str("function", "initEffectAttributes").Logger()

	l.Debug().Msgf("<===== Initialise Skill Effect Attributes =====>")

	effectAttributeRecs, err := e.effectModel.GetEffectAttributeRecs(e.effectRec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute recs >%v<", err)
		return err
	}

	e.effectAttributeRecs = effectAttributeRecs

	for _, effectAttributeRec := range effectAttributeRecs {

		l.Debug().Msgf("Adding effect attributes")

		attr, err := NewEffectAttribute(e.Env, e.Logger, e.RepoStore, effectAttributeRec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new effect attribute >%v<", err)
			return err
		}

		e.Attributes = append(e.Attributes, attr)
	}

	l.Debug().Msgf("<===== Done Initialise Skill Effect Attributes >%d< =====>", len(e.Attributes))

	return nil
}

// GetEffectAttributes - returns a list of effect attributes
func (e *SkillEffect) GetEffectAttributes(target string) ([]*EffectAttribute, error) {

	l := e.Logger.With().Str("function", "GetEffectAttributes").Logger()

	l.Debug().Msgf("Getting effect attributes")

	effectAttrs := []*EffectAttribute{}

	for _, effectAttr := range e.Attributes {
		if effectAttr.Target == target {
			l.Debug().Msgf("Including attribute >%s<", effectAttr.AttributeName)
			effectAttrs = append(effectAttrs, effectAttr)
		}
	}

	l.Debug().Msgf("Returning attributes >%d<", len(effectAttrs))

	return effectAttrs, nil
}

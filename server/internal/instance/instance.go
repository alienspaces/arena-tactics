// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

const (
	// PackageName -
	PackageName string = "instance"
)

package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

func TestNewTactic(t *testing.T) {

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	tct, err := NewTactic(e, l, rs)
	if assert.NoError(t, err, "NewTactic created without error") {
		assert.NotNil(t, tct, "Tactic is not nil")
	}

	tx.Rollback()
}

func TestGetEntityWithGreatest(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// setup entities and derive expected result
	var expected *Entity
	greatest := 0
	entities := []*Entity{}

	for _, rec := range td.AppEntityRecs {
		entity, err := NewEntity(e, l, rs, rec.ID)
		if err != nil {
			t.Fatalf("Failed to create entity instance >%v<", err)
		}
		err = entity.CalculateAttributeValues()
		if err != nil {
			t.Fatalf("Failed to calculate attribute values>%v<", err)
		}
		entAttr, err := entity.GetAttribute(testingdata.AttributeStrength)
		if err != nil {
			t.Fatalf("Failed to get entity attribute >%v<", err)
		}
		if entAttr.AdjustedValue > greatest {
			expected = entity
			greatest = entAttr.AdjustedValue
		}
		t.Logf("Adding entity >%s< strength >%d<", entity.Name, entAttr.AdjustedValue)
		entities = append(entities, entity)
	}

	t.Logf("Expecting result >%s<", expected.Name)

	tct, err := NewTactic(e, l, rs)
	if assert.NoError(t, err, "NewTactic created without error") {
		assert.NotNil(t, tct, "Tactic is not nil")

		result, err := tct.GetEntityWithGreatest(entities, testingdata.AttributeStrength)
		if assert.NoError(t, err, "GetEntityWithGreatest does not return error") {
			t.Logf("Have result >%s<", result.Name)
			assert.Equal(t, expected.Name, result.Name, "Resulting entity equals expected entity")
		}
	}

	tx.Rollback()
}

func TestGetEntityWithLeast(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// setup entities and derive expected result
	var expected *Entity
	least := 1000000
	entities := []*Entity{}

	for _, rec := range td.AppEntityRecs {
		entity, err := NewEntity(e, l, rs, rec.ID)
		if err != nil {
			t.Fatalf("Failed to create entity instance >%v<", err)
		}
		err = entity.CalculateAttributeValues()
		if err != nil {
			t.Fatalf("Failed to calculate attribute values>%v<", err)
		}
		entAttr, err := entity.GetAttribute(testingdata.AttributeHealth)
		if err != nil {
			t.Fatalf("Failed to get entity attribute >%v<", err)
		}
		if entAttr.AdjustedValue < least {
			expected = entity
			least = entAttr.AdjustedValue
		}
		t.Logf("Adding entity >%s< health >%d<", entity.Name, entAttr.AdjustedValue)
		entities = append(entities, entity)
	}

	t.Logf("Expecting result >%s<", expected.Name)

	tct, err := NewTactic(e, l, rs)
	if assert.NoError(t, err, "NewTactic created without error") {
		assert.NotNil(t, tct, "Tactic is not nil")

		result, err := tct.GetEntityWithLeast(entities, testingdata.AttributeHealth)
		if assert.NoError(t, err, "GetEntityWithLeast does not return error") {
			t.Logf("Have result >%s<", result.Name)
			assert.Equal(t, expected.Name, result.Name, "Resulting entity equals expected entity")
		}
	}

	tx.Rollback()
}

// Package instance provides all functionality for creating
// and managing fight, entity group and entity instances
package instance

import (
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/item"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// EntityItem -
type EntityItem struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// AppEntityItemID
	AppEntityItemID string

	// AppItemID
	AppItemID string

	// models
	itemModel *item.Model

	// records
	itemRec       *record.AppItemRecord
	entityItemRec *record.AppEntityItemRecord

	// appliable item effects
	Effects []*ItemEffect

	// properties
	Name string
}

// NewEntityItem -
func NewEntityItem(e env.Env, l zerolog.Logger, rs *repostore.RepoStore, entityItemID string) (instance *EntityItem, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &EntityItem{
		AppEntityItemID: entityItemID,
		Env:             e,
		Logger:          l,
		RepoStore:       rs,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EntityItem) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Entity Item ID >%s< =====>", e.AppEntityItemID)

	// models
	e.itemModel, err = item.NewModel(e.Env, e.Logger, e.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new item model >%v<", err)
		return err
	}

	// init entity item
	err = e.initEntityItem()
	if err != nil {
		l.Warn().Msgf("Failed to init entity item >%v<", err)
		return err
	}

	// init item effects
	err = e.initItemEffects()
	if err != nil {
		l.Warn().Msgf("Failed to init item effects >%v<", err)
		return err
	}

	l.Debug().Msgf("<===== Done Initialise Entity Item =====>")

	return nil
}

// initEntityItem -
func (e *EntityItem) initEntityItem() (err error) {

	l := e.Logger.With().Str("function", "initEntityItem").Logger()

	// get entity item record
	entityItemRec, err := e.itemModel.GetEntityItemRec(e.AppEntityItemID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity item rec >%v<", err)
		return err
	}

	// get item record
	itemRec, err := e.itemModel.GetItemRec(entityItemRec.AppItemID)
	if err != nil {
		l.Warn().Msgf("Failed to get item rec >%v<", err)
		return err
	}

	// properties
	e.entityItemRec = entityItemRec
	e.itemRec = itemRec

	// public properties
	e.AppItemID = itemRec.ID
	e.Name = itemRec.Name

	return nil
}

// initItemEffects -
func (e *EntityItem) initItemEffects() (err error) {

	l := e.Logger.With().Str("function", "initItemEffects").Logger()

	// get item effect records
	recs, err := e.itemModel.GetItemEffectRecs(e.itemRec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get item effect recs >%v<", err)
		return err
	}

	for _, rec := range recs {

		// new entity item
		item, err := NewItemEffect(e.Env, e.Logger, e.RepoStore, rec.ID)
		if err != nil {
			l.Warn().Msgf("Failed new item >%v<", err)
			return err
		}

		e.Effects = append(e.Effects, item)
	}

	l.Debug().Msgf("Entity item has >%d< effects", len(e.Effects))

	return nil
}

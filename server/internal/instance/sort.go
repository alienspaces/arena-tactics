package instance

import (
	"github.com/rs/zerolog"
)

// SortEntitiesByInitiative - Sortable list of fight entities
type SortEntitiesByInitiative struct {
	Logger         zerolog.Logger
	InitiativeFunc InitiativeFunc
	Entities       []*Entity
}

// Len -
func (e SortEntitiesByInitiative) Len() int {

	l := e.Logger.With().Str("function", "Len").Logger()

	l.Debug().Msgf("Sort entities len >%d<", len(e.Entities))

	return len(e.Entities)
}

// Swap -
func (e SortEntitiesByInitiative) Swap(i, j int) {

	l := e.Logger.With().Str("function", "Swap").Logger()

	l.Debug().Msgf("Swap entities i >%d< j >%d<", i, j)

	e.Entities[i], e.Entities[j] = e.Entities[j], e.Entities[i]
}

// Less -
func (e SortEntitiesByInitiative) Less(i, j int) bool {

	l := e.Logger.With().Str("function", "Less").Logger()

	l.Debug().Msgf("")

	// TODO: Perhaps roll this loop into an entity instance function
	iValues := make(map[string]int)
	for _, entityAttr := range e.Entities[i].Attributes {
		iValues[entityAttr.Name] = entityAttr.AdjustedValue
	}

	iVal, err := e.InitiativeFunc(iValues)
	if err != nil {
		l.Warn().Msgf("Failed get attribute value >%v<", err)
		iVal = 0
	}

	// TODO: Perhaps roll this loop into an entity instance function
	jValues := make(map[string]int)
	for _, entityAttr := range e.Entities[j].Attributes {
		jValues[entityAttr.Name] = entityAttr.AdjustedValue
	}

	jVal, err := e.InitiativeFunc(jValues)
	if err != nil {
		l.Warn().Msgf("Failed get attribute value >%v<", err)
		jVal = 0
	}

	return jVal < iVal
}

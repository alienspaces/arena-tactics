package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

func TestNewEntityGroup(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	tct, err := NewEntityGroup(e, l, rs, td.AppEntityGroupRecs[0].ID)
	if assert.NoError(t, err, "NewEntityGroup created without error") {
		assert.NotNil(t, tct, "EntityGroup is not nil")
	}

	tx.Rollback()
}

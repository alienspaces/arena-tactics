package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

func TestNewEntity(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity
	ent, err := NewEntity(e, l, rs, td.AppEntityRecs[0].ID)
	if assert.NoError(t, err, "NewEntity does not return an error") {

		assert.NotNil(t, ent, "NewEntity returns a new entity object")

		assert.NotNil(t, ent.entityModel, "Entity entityModel is not nil")
		assert.NotNil(t, ent.attributeModel, "Entity attributeModel is not nil")

		t.Logf("Entity has >%d< skills", len(ent.Skills))
		t.Logf("Entity has >%d< items", len(ent.Items))
		t.Logf("Entity has >%d< applied effects", len(ent.Effects))

		// expecting there to be entity items and skill
		assert.NotEmpty(t, ent.Items, "Entity Items are not empty")
		assert.NotEmpty(t, ent.Skills, "Entity Skills are not empty")
		assert.NotEmpty(t, ent.Tactics, "Entity Tactics are not empty")

		// not expecting any effects to be applied yet
		assert.Empty(t, ent.Effects, "Entity Effects are empty")
	}

	tx.Rollback()
}

func TestEntityAttributes(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity
	ent, err := NewEntity(e, l, rs, td.AppEntityRecs[0].ID)
	if assert.NoError(t, err, "NewEntity does not return an error") {

		// calculate attribute values
		err := ent.CalculateAttributeValues()
		if assert.NoError(t, err, "CalculatedAttributeValues returns without error") {

			// get attribute values
			for _, entAttr := range ent.Attributes {
				t.Logf("Attribute calculated value >%d<", entAttr.CalculatedValue)
			}
		}
	}

	tx.Rollback()
}

func TestApplyItemEffect(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// source entity record
	sourceEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameAdam)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameAdam, err)
	}

	// source entity instance
	sourceEntity, err := NewEntity(e, l, rs, sourceEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// source entity calculate attribute values
	err = sourceEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// get attribute values
	for _, attr := range sourceEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
	}

	for _, entityItem := range sourceEntity.Items {
		for _, itemEffect := range entityItem.Effects {

			effectEvents, err := sourceEntity.ApplyItemEffect(sourceEntity.AppEntityID, record.TargetSelf, itemEffect)
			if assert.NoError(t, err, "ApplyItemEffect does not return an error") {
				if len(effectEvents) != 0 {
					t.Logf("Applied item effect >%s<", itemEffect.EffectName)
				}
			}
		}
	}

	// get attribute values
	for _, attr := range sourceEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
		if attr.Name == testingdata.AttributeDefence {
			assert.NotEqual(t, attr.CalculatedValue, attr.AdjustedValue,
				"Defence calculated value does not equal adjusted value")
		}
	}

	tx.Rollback()
}

func TestApplySkillEffect(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// source entity record
	sourceEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameCarol)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameCarol, err)
	}

	// source entity instance
	sourceEntity, err := NewEntity(e, l, rs, sourceEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// target entity record
	targetEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameBrian)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameBrian, err)
	}

	// target entity instance
	targetEntity, err := NewEntity(e, l, rs, targetEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// source entity calculate attribute values
	err = sourceEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// target entity calculate attribute values
	err = targetEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// log target attribute values
	for _, attr := range targetEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
	}

	// apply all skills from source entity to target entity where target is current foe
	for _, entitySkill := range sourceEntity.Skills {
		for _, skillEffect := range entitySkill.Effects {

			effectEvents, err := targetEntity.ApplySkillEffect(
				sourceEntity.AppEntityID,
				record.TargetCurrentFoe,
				skillEffect,
			)
			if assert.NoError(t, err, "ApplySkillEffect does not return an error") {
				if len(effectEvents) != 0 {
					t.Logf("Applied skill effect >%s<", skillEffect.EffectName)
				}
			}
		}
	}

	if !assert.Equal(t, 1, len(targetEntity.Effects), "Target entity has expected applied effects") {
		for _, entityEffect := range targetEntity.Effects {
			t.Logf("Target entity has effect >%#v<", entityEffect)
		}
	}

	// log target attribute values
	for _, attr := range targetEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
		if attr.Name == testingdata.AttributeHealth {
			assert.NotEqual(t, attr.CalculatedValue, attr.AdjustedValue,
				"Health calculated value does not equal adjusted value")
		}
	}

	tx.Rollback()
}

func TestUpdateActiveEffectsPermanent(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// source entity record
	sourceEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameCarol)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameCarol, err)
	}

	// source entity instance
	sourceEntity, err := NewEntity(e, l, rs, sourceEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// target entity record
	targetEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameBrian)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameBrian, err)
	}

	// target entity instance
	targetEntity, err := NewEntity(e, l, rs, targetEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// source entity calculate attribute values
	err = sourceEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// target entity calculate attribute values
	err = targetEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// apply all skills from source entity to target entity where target is current foe
	for _, entitySkill := range sourceEntity.Skills {

		// looking for repeater punch
		if entitySkill.Name != testingdata.SkillRepeaterPunch {
			continue
		}

		for _, skillEffect := range entitySkill.Effects {

			effectEvents, err := targetEntity.ApplySkillEffect(
				sourceEntity.AppEntityID,
				record.TargetCurrentFoe,
				skillEffect,
			)
			if assert.NoError(t, err, "ApplySkillEffect does not return an error") {
				if len(effectEvents) != 0 {
					t.Logf("Applied skill effect >%s<", skillEffect.EffectName)
				}
			}
		}
	}

	if !assert.Equal(t, 1, len(targetEntity.Effects), "Target entity has expected applied effects") {
		for _, entityEffect := range targetEntity.Effects {
			t.Logf("Target entity has effect >%#v<", entityEffect)
		}
	}

	expectedDuration := 0
	for _, entityEffect := range targetEntity.Effects {
		expectedDuration = entityEffect.Duration
		break
	}

	t.Logf("Expected effect duration >%d<", expectedDuration)

	// update active effects, expect effect events until expected duration
	for i := 0; i < expectedDuration; i++ {
		effectEvents, err := targetEntity.UpdateActiveEffects()
		if !assert.NoError(t, err, "UpdateActiveEffects returns without error") {
			t.Fatalf("Failed UpdateActiveEffects >%v<", err)
		}
		if !assert.NotEmpty(t, effectEvents, "UpdateActiveEffects returns effect events") {
			t.Fatalf("Failed UpdateActiveEffects effect event count >%d<", len(effectEvents))
		}
	}

	// log target attribute values
	for _, attr := range targetEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
		if attr.Name == testingdata.AttributeHealth {
			// resulting health is 30 points less than initial calculated value
			assert.Equal(t, attr.CalculatedValue-30, attr.AdjustedValue,
				"Health calculated value does not equal adjusted value")
		}
	}

	tx.Rollback()
}

func TestUpdateActiveEffectsNonPermanent(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// source entity record
	sourceEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameBetty)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameCarol, err)
	}

	// source entity instance
	sourceEntity, err := NewEntity(e, l, rs, sourceEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// target entity record
	targetEntityRec, err := td.GetEntityRecByName(testingdata.EntityNameCharles)
	if !assert.NoError(t, err, "GetEntityRecByName does not return an error") {
		t.Fatalf("Failed to get test entity >%s< >%v<", testingdata.EntityNameBrian, err)
	}

	// target entity instance
	targetEntity, err := NewEntity(e, l, rs, targetEntityRec.ID)
	if !assert.NoError(t, err, "NewEntity does not return an error") {
		t.Fatalf("Failed new entity >%v<", err)
	}

	// source entity calculate attribute values
	err = sourceEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// target entity calculate attribute values
	err = targetEntity.CalculateAttributeValues()
	if !assert.NoError(t, err, "CalculateAttributeValues does not return an error") {
		t.Fatalf("Failed calculate attribute values >%v<", err)
	}

	// apply all skills from source entity to target entity where target is current foe
	for _, entitySkill := range sourceEntity.Skills {

		// looking for weaken
		if entitySkill.Name != testingdata.SkillWeaken {
			continue
		}

		for _, skillEffect := range entitySkill.Effects {

			effectEvents, err := targetEntity.ApplySkillEffect(
				sourceEntity.AppEntityID,
				record.TargetCurrentFoe,
				skillEffect,
			)
			if assert.NoError(t, err, "ApplySkillEffect does not return an error") {
				if len(effectEvents) != 0 {
					t.Logf("Applied skill effect >%s<", skillEffect.EffectName)
				}
			}
		}
	}

	if !assert.Equal(t, 1, len(targetEntity.Effects), "Target entity has expected applied effects") {
		for _, entityEffect := range targetEntity.Effects {
			t.Logf("Target entity has effect >%#v<", entityEffect)
		}
	}

	expectedDuration := 0
	for _, entityEffect := range targetEntity.Effects {
		expectedDuration = entityEffect.Duration
		break
	}

	t.Logf("Expected effect duration >%d<", expectedDuration)

	// update active effects, expect effect events until expected duration
	for i := 0; i < expectedDuration; i++ {
		effectEvents, err := targetEntity.UpdateActiveEffects()
		if !assert.NoError(t, err, "UpdateActiveEffects returns without error") {
			t.Fatalf("Failed UpdateActiveEffects >%v<", err)
		}
		if !assert.NotEmpty(t, effectEvents, "UpdateActiveEffects returns effect events") {
			t.Fatalf("Failed UpdateActiveEffects effect event count >%d<", len(effectEvents))
		}
		for _, attr := range targetEntity.Attributes {
			if attr.Name == testingdata.AttributeStrength {
				t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
					attr.Name,
					attr.CalculatedValue,
					attr.AdjustedValue,
				)
				// resulting strength x points less than initial value
				// NOTE: Add 2 to what we expect the adjusted value should be as the
				// effect was applied once when the skill was applied and out index here
				// starts at 0, makes sense huh?
				assert.Equal(t, attr.CalculatedValue-(i+2), attr.AdjustedValue,
					"Strength calculated value does not equal adjusted value")
			}
		}
	}

	// one final update to remove the effect as the duration should now be expired
	effectEvents, err := targetEntity.UpdateActiveEffects()
	if !assert.NoError(t, err, "UpdateActiveEffects returns without error") {
		t.Fatalf("Failed UpdateActiveEffects >%v<", err)
	}
	if !assert.Empty(t, effectEvents, "UpdateActiveEffects returns no effect") {
		t.Fatalf("Failed UpdateActiveEffects effect event count >%d<", len(effectEvents))
	}

	// log target attribute values
	for _, attr := range targetEntity.Attributes {
		t.Logf("Attribute name >%15s< calc value >%3d< adj value >%3d<",
			attr.Name,
			attr.CalculatedValue,
			attr.AdjustedValue,
		)
		if attr.Name == testingdata.AttributeStrength {
			// resulting strength is equal to initial calculated value
			assert.Equal(t, attr.CalculatedValue, attr.AdjustedValue,
				"Health calculated value does not equal adjusted value")
		}
	}

	tx.Rollback()
}

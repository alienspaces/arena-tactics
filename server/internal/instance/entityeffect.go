package instance

// EntityEffect -
type EntityEffect struct {

	// id - item or skill effect id
	ID string

	// name -
	Name string

	// type - passive or active
	Type string

	// duration - the duration remaining for this effect
	Duration int

	// recurring - does the adjustment value get applied again each turn
	Recurring bool

	// permanent - does the total adjustment value get removed when the effect is removed
	Permanent bool

	// adjustment - the adjustment applied for this effect each turn
	Adjustments []*EntityEffectAdjustment
}

// NewEntityEffect - returns a new entity effect
func NewEntityEffect(
	id, name string,
	effectType string,
	effectDuration int,
	effectRecurring,
	effectPermanent bool) (instance *EntityEffect, err error) {

	instance = &EntityEffect{
		ID:        id,
		Name:      name,
		Type:      effectType,
		Duration:  effectDuration,
		Recurring: effectRecurring,
		Permanent: effectPermanent,
	}

	instance.Adjustments = []*EntityEffectAdjustment{}

	return instance, nil
}

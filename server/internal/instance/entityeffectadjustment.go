package instance

import (
	"github.com/rs/zerolog"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// EntityEffectAdjustment -
type EntityEffectAdjustment struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// EffectAttribute -
	AppEffectAttributeID string

	// Value - value to apply
	Value int

	// AppliedValue - the total value applied so far
	AppliedValue int

	// EffectAttribute
	EffectAttribute *EffectAttribute
}

// NewEntityEffectAdjustment -
func NewEntityEffectAdjustment(
	e env.Env, l zerolog.Logger, rs *repostore.RepoStore,
	effectAttributeID string, value int) (*EntityEffectAdjustment, error) {

	instance := &EntityEffectAdjustment{
		Env:                  e,
		Logger:               l,
		RepoStore:            rs,
		AppEffectAttributeID: effectAttributeID,
		Value:                value,
	}

	err := instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (e *EntityEffectAdjustment) Init() (err error) {

	l := e.Logger.With().Str("function", "Init").Logger()

	l.Debug().Msgf("<===== Initialise Effect Attribute ID >%s< =====>", e.AppEffectAttributeID)

	effectAttr, err := NewEffectAttribute(e.Env, e.Logger, e.RepoStore, e.AppEffectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed effect attribute >%v<", err)
		return err
	}

	e.EffectAttribute = effectAttr

	l.Debug().Msgf("<===== Done Initialise Effect Attribute =====>")

	return nil
}

// Apply - Apply adjustment value
func (e *EntityEffectAdjustment) Apply() {
	e.AppliedValue += e.Value
}

package instance

import (
	"fmt"

	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/model/entity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Tactic -
type Tactic struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore

	// models
	entityModel *entity.Model
}

// Result -
type Result struct {
	FriendTarget *Entity
	FoeTarget    *Entity
	Action       string
	AppItemID    string
	AppSkillID   string
}

// NewTactic -
func NewTactic(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (instance *Tactic, err error) {

	// logger hook
	l = l.With().Str("package", PackageName).Logger()

	instance = &Tactic{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	err = instance.Init()
	if err != nil {
		l.Warn().Msgf("Failed to initialize >%v<", err)
		return nil, err
	}

	return instance, nil
}

// Init -
func (t *Tactic) Init() (err error) {

	l := t.Logger.With().Str("function", "Init").Logger()

	// models
	t.entityModel, err = entity.NewModel(t.Env, t.Logger, t.RepoStore)
	if err != nil {
		l.Warn().Msgf("Failed new entity model >%v<", err)
		return err
	}

	return nil
}

// TODO: Refactor the following function

// Process -
func (t *Tactic) Process(ent *Entity, friends []*Entity, foes []*Entity) (
	tctcResult *Result,
	err error) {

	l := t.Logger.With().Str("function", "Process").Logger()

	l.Debug().Msgf("Process tactics entity Name >%s< ID >%s<", ent.Name, ent.AppEntityID)

	tctcResult = &Result{}

	// Cycle through entity tactics evaluating each one until
	// we have a resulting action
	for tctcIdx, tctc := range ent.Tactics {

		l.Debug().Msgf("Tactic >%d< Target >%s< Comparator >%s< Attribute >%s< Value >%d< percent",
			tctcIdx,
			tctc.Target,
			tctc.Comparator,
			tctc.Attribute,
			tctc.Value,
		)

		// target is foe with no current foe, skip tactic
		if tctc.Target == record.TargetCurrentFoe &&
			ent.FoeTargetEntityID == "" {
			l.Debug().Msgf("Skip tactic ID >%s<, no current foe target", tctc.AppEntityTacticID)
			continue
		}

		// target is friend with no current friend, skip tactic
		if tctc.Target == record.TargetCurrentFriend &&
			ent.FriendTargetEntityID == "" {
			l.Debug().Msgf("Skip tactic ID >%s<, no current friend target", tctc.AppEntityTacticID)
			continue
		}

		// tactic is self, check attribute comparator against self
		if tctc.Target == record.TargetSelf {

			var (
				result bool
				err    error
			)

			switch tctc.Comparator {
			case record.EntityTacticComparatorLeast:
				result, err = t.EntityHasLeast(ent, friends, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreatest:
				result, err = t.EntityHasGreatest(ent, friends, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorLessThan:
				result, err = t.EntityHasLessThan(ent, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreaterThan:
				result, err = t.EntityHasGreaterThan(ent, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			}

			if result == true {
				switch tctc.Action {
				case record.EntityTacticActionSwitchTarget:
					l.Warn().Msgf("Tactic action switch target is useless as self is self..")
				case record.EntityTacticActionUseItem:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				case record.EntityTacticActionUseSkill:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				}

				l.Debug().Msgf("Tactic identified with target self")
				break
			}
		}

		// target is foe with current foe, check attribute comparator against current foes
		if tctc.Target == record.TargetCurrentFoe &&
			ent.FoeTargetEntityID != "" {

			var (
				result bool
				err    error
				foe    *Entity
			)

			// foe entity
			for _, foe = range foes {
				if foe.AppEntityID == ent.FoeTargetEntityID {
					break
				}
			}

			// missing foe for some reason..
			if foe == nil {
				l.Warn().Msgf("Could not find foe entity ID >%s< in list of foes", ent.FoeTargetEntityID)
				return nil, fmt.Errorf("Could not find foe entity ID >%s< in list of foes", ent.FoeTargetEntityID)
			}

			switch tctc.Comparator {
			case record.EntityTacticComparatorLeast:
				result, err = t.EntityHasLeast(foe, foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreatest:
				result, err = t.EntityHasGreatest(foe, foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorLessThan:
				result, err = t.EntityHasLessThan(foe, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreaterThan:
				result, err = t.EntityHasGreaterThan(foe, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			}

			if result == true {
				switch tctc.Action {
				case record.EntityTacticActionSwitchTarget:
					l.Warn().Msgf("Tactic action switch target is useless as foe is current foe..")
				case record.EntityTacticActionUseItem:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				case record.EntityTacticActionUseSkill:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				}

				l.Debug().Msgf("Tactic identified with target as current foe")
				break
			}
		}

		// target is friend with current friend, check attribute comparator against current friend
		if tctc.Target == record.TargetCurrentFriend &&
			ent.FriendTargetEntityID != "" {

			var (
				result bool
				err    error
				friend *Entity
			)

			// friend entity
			for _, friend = range friends {
				if friend.AppEntityID == ent.FriendTargetEntityID {
					break
				}
			}

			// missing friend for some reason..
			if friend == nil {
				l.Warn().Msgf("Could not find friend entity ID >%s< in list of friends", ent.FriendTargetEntityID)
				return nil, fmt.Errorf("Could not find friend entity ID >%s< in list of friends", ent.FriendTargetEntityID)
			}

			switch tctc.Comparator {
			case record.EntityTacticComparatorLeast:
				result, err = t.EntityHasLeast(friend, friends, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreatest:
				result, err = t.EntityHasGreatest(friend, friends, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorLessThan:
				result, err = t.EntityHasLessThan(friend, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreaterThan:
				result, err = t.EntityHasGreaterThan(friend, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to check if entity has greatest >%v<", err)
					return nil, err
				}
			}

			if result == true {
				switch tctc.Action {
				case record.EntityTacticActionSwitchTarget:
					l.Warn().Msgf("Tactic action switch target is useless as friend is current friend..")
				case record.EntityTacticActionUseItem:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				case record.EntityTacticActionUseSkill:
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				}

				l.Debug().Msgf("Tactic identified with target as current friend")
				break
			}
		}

		// target any foe, with switch target, check attribute comparator against all foes
		if tctc.Target == record.TargetAnyFoe &&
			(tctc.Action == record.EntityTacticActionSwitchTarget ||
				tctc.Action == record.EntityTacticActionUseItem ||
				tctc.Action == record.EntityTacticActionUseSkill) {

			var (
				foe *Entity
				err error
			)

			switch tctc.Comparator {
			case record.EntityTacticComparatorLeast:
				foe, err = t.GetEntityWithLeast(foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreatest:
				foe, err = t.GetEntityWithGreatest(foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreaterThan:
				foe, err = t.GetEntityWithGreaterThan(foes, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with greater than >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorLessThan:
				foe, err = t.GetEntityWithLessThan(foes, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with less than >%v<", err)
					return nil, err
				}
			}

			// switch foe target
			if foe != nil {

				switch tctc.Action {
				case record.EntityTacticActionSwitchTarget:
					tctcResult.FoeTarget = foe
				case record.EntityTacticActionUseItem:
					tctcResult.FoeTarget = foe
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				case record.EntityTacticActionUseSkill:
					tctcResult.FoeTarget = foe
					tctcResult.Action = tctc.Action
					tctcResult.AppSkillID = tctc.AppSkillID
				}

				l.Debug().Msgf("Tactic identified with target as any foe")
				break
			}
		}

		// target any friend, with switch target, check attribute comparator against all friends
		if tctc.Target == record.TargetAnyFriend &&
			(tctc.Action == record.EntityTacticActionSwitchTarget ||
				tctc.Action == record.EntityTacticActionUseItem ||
				tctc.Action == record.EntityTacticActionUseSkill) {

			var (
				friend *Entity
				err    error
			)

			switch tctc.Comparator {
			case record.EntityTacticComparatorLeast:
				friend, err = t.GetEntityWithLeast(foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreatest:
				friend, err = t.GetEntityWithGreatest(foes, tctc.Attribute)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with least >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorGreaterThan:
				friend, err = t.GetEntityWithGreaterThan(friends, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with greater than >%v<", err)
					return nil, err
				}
			case record.EntityTacticComparatorLessThan:
				friend, err = t.GetEntityWithLessThan(friends, tctc.Attribute, tctc.Value)
				if err != nil {
					l.Warn().Msgf("Failed to get entity with less than >%v<", err)
					return nil, err
				}
			}

			// switch friend target
			if friend != nil {

				switch tctc.Action {
				case record.EntityTacticActionSwitchTarget:
					tctcResult.FriendTarget = friend
				case record.EntityTacticActionUseItem:
					tctcResult.FriendTarget = friend
					tctcResult.Action = tctc.Action
					tctcResult.AppItemID = tctc.AppItemID
				case record.EntityTacticActionUseSkill:
					tctcResult.FriendTarget = friend
					tctcResult.Action = tctc.Action
					tctcResult.AppSkillID = tctc.AppSkillID
				}

				l.Debug().Msgf("Tactic identified with target as any friend")
				break
			}
		}
	}

	return tctcResult, nil
}

// GetEntityWithLeast -
func (t *Tactic) GetEntityWithLeast(entities []*Entity, tctcAttr string) (*Entity, error) {

	l := t.Logger.With().Str("function", "GetEntityWithLeast").Logger()

	// current least value is a big number to begin with
	var leastValueEntity *Entity
	var leastValue int

	leastValue = 1000000

	for _, ent := range entities {

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return nil, err
		}

		l.Debug().Msgf("Adjusted value >%d< Least value >%d<", entAttr.AdjustedValue, leastValue)

		if entAttr.AdjustedValue < leastValue {
			leastValueEntity = ent
			leastValue = entAttr.AdjustedValue
		}
	}

	return leastValueEntity, nil
}

// GetEntityWithGreatest -
func (t *Tactic) GetEntityWithGreatest(entities []*Entity, tctcAttr string) (*Entity, error) {

	l := t.Logger.With().Str("function", "GetEntityWithGreatest").Logger()

	// current greatest value is a small number to begin with
	var greatestEntAttrEntity *Entity
	var greatestEntAttr int

	greatestEntAttr = -1000000

	for _, ent := range entities {

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return nil, err
		}

		l.Debug().Msgf("Adjusted value >%d< Greatest value >%d<", entAttr.AdjustedValue, greatestEntAttr)

		if entAttr.AdjustedValue > greatestEntAttr {
			greatestEntAttrEntity = ent
			greatestEntAttr = entAttr.AdjustedValue
		}
	}

	return greatestEntAttrEntity, nil
}

// GetEntityWithGreaterThan -
func (t *Tactic) GetEntityWithGreaterThan(entities []*Entity, tctcAttr string, tctcValue int) (*Entity, error) {

	l := t.Logger.With().Str("function", "GetEntityWithGreaterThan").Logger()

	for _, ent := range entities {

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return nil, err
		}

		l.Debug().Msgf("Entity Name >%s< ID >%s< Attribute >%s< Adjusted >%d< Calculated >%d<",
			ent.Name,
			ent.AppEntityID,
			tctcAttr,
			entAttr.AdjustedValue,
			entAttr.CalculatedValue,
		)

		percentValue := int(tctcValue * entAttr.CalculatedValue / 100)

		if entAttr.AdjustedValue > percentValue {
			return ent, nil
		}
	}

	return nil, nil
}

// GetEntityWithLessThan -
func (t *Tactic) GetEntityWithLessThan(entities []*Entity, tctcAttr string, tctcValue int) (*Entity, error) {

	l := t.Logger.With().Str("function", "GetEntityWithLessThan").Logger()

	for _, ent := range entities {

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return nil, err
		}

		l.Debug().Msgf("Attribute Name >%s< Adjusted >%d< Calculated >%d<",
			tctcAttr,
			entAttr.AdjustedValue,
			entAttr.CalculatedValue,
		)

		percentValue := int(tctcValue * entAttr.CalculatedValue / 100)

		if entAttr.AdjustedValue < percentValue {
			return ent, nil
		}
	}

	return nil, nil
}

// EntityHasLeast -
func (t *Tactic) EntityHasLeast(testEnt *Entity, entities []*Entity, tctcAttr string) (bool, error) {

	l := t.Logger.With().Str("function", "EntityHasLeast").Logger()

	// get test entity attribute value
	testEntAttr, err := testEnt.GetEntityAttribute(tctcAttr)
	if err != nil {
		l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
		return false, err
	}

	for _, ent := range entities {

		// skip test entity
		if ent.AppEntityID == testEnt.AppEntityID {
			continue
		}

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return false, err
		}

		l.Debug().Msgf("Attribute Name >%s< Adjusted >%d< test value >%d<",
			tctcAttr,
			entAttr.AdjustedValue,
			testEntAttr.AdjustedValue,
		)

		if entAttr.AdjustedValue < testEntAttr.AdjustedValue {
			l.Debug().Msgf("Found entity ID >%s< Name >%s< attribute value is less", ent.AppEntityID, ent.Name)
			return false, nil
		}
	}

	l.Debug().Msgf("Entity name >%s< ID >%s< has least Attribute >%s< value",
		testEnt.Name,
		testEnt.AppEntityID,
		tctcAttr,
	)

	return true, nil
}

// EntityHasGreatest -
func (t *Tactic) EntityHasGreatest(testEnt *Entity, entities []*Entity, tctcAttr string) (bool, error) {

	l := t.Logger.With().Str("function", "EntityHasGreatest").Logger()

	// get test entity attribute value
	testEntAttr, err := testEnt.GetEntityAttribute(tctcAttr)
	if err != nil {
		l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
		return false, err
	}

	for _, ent := range entities {

		// skip test entity
		if ent.AppEntityID == testEnt.AppEntityID {
			continue
		}

		entAttr, err := ent.GetEntityAttribute(tctcAttr)
		if err != nil {
			l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
			return false, err
		}

		l.Debug().Msgf("Attribute Name >%s< Adjusted >%d< test value >%d<",
			tctcAttr,
			entAttr.AdjustedValue,
			testEntAttr.AdjustedValue,
		)

		if entAttr.AdjustedValue > testEntAttr.AdjustedValue {
			l.Debug().Msgf("Found entity ID >%s< Name >%s< attribute value is greater", ent.AppEntityID, ent.Name)
			return false, nil
		}
	}

	l.Debug().Msgf("Entity Name >%s< ID >%s< has greatest Attribute >%s< value",
		testEnt.Name,
		testEnt.AppEntityID,
		tctcAttr,
	)

	return true, nil
}

// EntityHasGreaterThan -
func (t *Tactic) EntityHasGreaterThan(ent *Entity, tctcAttr string, tctcValue int) (bool, error) {

	l := t.Logger.With().Str("function", "EntityHasGreaterThan").Logger()

	entAttr, err := ent.GetEntityAttribute(tctcAttr)
	if err != nil {
		l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
		return false, err
	}

	l.Debug().Msgf("Attribute Name >%s< Adjusted >%d< Calculated >%d<",
		tctcAttr,
		entAttr.AdjustedValue,
		entAttr.CalculatedValue,
	)

	percentValue := int(tctcValue * entAttr.CalculatedValue / 100)

	if entAttr.AdjustedValue > percentValue {
		l.Debug().Msgf("Entity Name >%s< ID >%s< has greatest than Attribute >%s< value",
			ent.Name,
			ent.AppEntityID,
			tctcAttr,
		)
		return true, nil
	}

	return false, nil
}

// EntityHasLessThan -
func (t *Tactic) EntityHasLessThan(ent *Entity, tctcAttr string, tctcValue int) (bool, error) {

	l := t.Logger.With().Str("function", "EntityHasLessThan").Logger()

	entAttr, err := ent.GetEntityAttribute(tctcAttr)
	if err != nil {
		l.Warn().Msgf("Failed to get adjusted attribute value >%v<", err)
		return false, err
	}

	l.Debug().Msgf("Attribute Name >%s< Adjusted >%d< Calculated >%d<",
		tctcAttr,
		entAttr.AdjustedValue,
		entAttr.CalculatedValue,
	)

	percentValue := int(tctcValue * entAttr.CalculatedValue / 100)

	if entAttr.AdjustedValue < percentValue {
		l.Debug().Msgf("Entity Name >%s< ID >%s< has less than Attribute >%s< percent value",
			ent.Name,
			ent.AppEntityID,
			tctcAttr,
		)
		return true, nil
	}

	return false, nil
}

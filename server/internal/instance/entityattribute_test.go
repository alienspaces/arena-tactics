package instance

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

func TestNewEntityAttribute(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	entityRec := td.AppEntityRecs[0]
	var entityAttributeRec *record.AppEntityAttributeRecord
	for _, entityAttributeRec = range td.AppEntityAttributeRecs[entityRec.ID] {
		break
	}

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity attribute
	ea, err := NewEntityAttribute(e, l, rs, entityAttributeRec.ID)
	if assert.NoError(t, err, "NewEntityAttribute does not return an error") {
		assert.NotNil(t, ea, "NewEntityAttribute returns a new entity attribute object")
	}

	tx.Rollback()
}

func TestCalculateValues(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	entityAttributeRec, err := td.GetEntityAttributeRecByName(
		td.AppEntityRecs[0].ID,
		testingdata.AttributeStrength,
	)

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity attribute
	ea, err := NewEntityAttribute(e, l, rs, entityAttributeRec.ID)
	if assert.NoError(t, err, "NewEntityAttribute does not return an error") {
		assert.NotNil(t, ea, "NewEntityAttribute returns a new entity attribute object")

		// NOTE: testingdata strength attribute has no parameter dependencies
		err := ea.CalculateValues(map[string]int{})
		if assert.NoError(t, err, "CalculateValues does not return an error") {
			assert.NotEqual(t, ea.MaxValue, "MaxValue is not 0")
			assert.NotEqual(t, ea.CalculatedValue, "Calculated is not 0")
			assert.Equal(t, ea.CalculatedValue, ea.AdjustedValue, "AdjustedValue is equal to CalculatedValue")
		}
	}

	tx.Rollback()
}

func TestAddAdjustment(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	entityAttributeRec, err := td.GetEntityAttributeRecByName(
		td.AppEntityRecs[0].ID,
		testingdata.AttributeStrength,
	)

	// adjustVal - test value
	adjustVal := -2

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity attribute
	ea, err := NewEntityAttribute(e, l, rs, entityAttributeRec.ID)
	if assert.NoError(t, err, "NewEntityAttribute does not return an error") {
		assert.NotNil(t, ea, "NewEntityAttribute returns a new entity attribute object")

		// NOTE: testingdata strength attribute has no parameter dependencies
		err := ea.CalculateValues(map[string]int{})
		if assert.NoError(t, err, "CalculateValues does not return an error") {

			expectedApplyCount := (ea.CalculatedValue / adjustVal) * -1

			t.Logf("Adjustment calculated value >%d<", ea.CalculatedValue)
			t.Logf("            adjusting value >%d<", adjustVal)
			t.Logf("             expected count >%d<", expectedApplyCount)

			// all adjustments should succeed
			for i := 0; i < expectedApplyCount; i++ {
				adjusted, err := ea.AddAdjustment(adjustVal)
				if assert.NoError(t, err, "AddAdjustment does not return an error") {
					assert.Equal(t, true, adjusted, "AddAdjustment returns true")
				}
			}

			// next adjustment should not succeed
			adjusted, err := ea.AddAdjustment(adjustVal)
			if assert.NoError(t, err, "AddAdjustment does not return an error") {
				assert.Equal(t, false, adjusted, "AddAdjustment returns false")
			}
		}
	}

	tx.Rollback()
}

func TestRemoveAdjustment(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	entityAttributeRec, err := td.GetEntityAttributeRecByName(
		td.AppEntityRecs[0].ID,
		testingdata.AttributeStrength,
	)

	// adjustVal - test value
	adjustVal := -2

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new entity attribute
	ea, err := NewEntityAttribute(e, l, rs, entityAttributeRec.ID)
	if assert.NoError(t, err, "NewEntityAttribute does not return an error") {
		assert.NotNil(t, ea, "NewEntityAttribute returns a new entity attribute object")

		// NOTE: testingdata strength attribute has no parameter dependencies
		err := ea.CalculateValues(map[string]int{})
		if assert.NoError(t, err, "CalculateValues does not return an error") {

			// adjust to zero
			adjusted, err := ea.AddAdjustment(ea.CalculatedValue * -1)
			if assert.NoError(t, err, "AddAdjustment does not return an error") {
				assert.Equal(t, true, adjusted, "AddAdjustment returns true")

				expectedApplyCount := ea.MaxValue / adjustVal * -1

				t.Logf("Adjustment calculated value >%d<", ea.MaxValue)
				t.Logf("             adjusted value >%d<", ea.AdjustedValue)
				t.Logf("            adjusting value >%d<", adjustVal)
				t.Logf("             expected count >%d<", expectedApplyCount)

				// all adjustments should succeed
				for i := 0; i < expectedApplyCount; i++ {
					adjusted, err := ea.RemoveAdjustment(adjustVal)
					if assert.NoError(t, err, "RemoveAdjustment does not return an error") {
						assert.Equal(t, true, adjusted, "RemoveAdjustment returns true")
					}
				}

				// next adjustment should not succeed
				adjusted, err = ea.RemoveAdjustment(adjustVal)
				if assert.NoError(t, err, "RemoveAdjustment does not return an error") {
					assert.Equal(t, false, adjusted, "RemoveAdjustment returns false")
				}
			}
		}
	}

	tx.Rollback()
}

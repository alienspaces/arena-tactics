package util

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetUUID(t *testing.T) {

	uuid := GetUUID()
	assert.NotNil(t, uuid, "GetUUID returns something")
}

func TestGetTime(t *testing.T) {

	now := GetTime()
	assert.NotNil(t, now, "GetTime returns something")
}

func TestGetFutureTime(t *testing.T) {

	// Time difference
	hours := 4

	// Get the new time from now
	currentTime := time.Now().UTC()
	futureTimeString := GetFutureTime(time.Duration(hours) * time.Hour)

	// Convert back into a time object
	futureTime, _ := time.Parse(time.RFC3339, futureTimeString)

	// Round both times to the newrest hour
	currentTime = currentTime.Round(time.Hour)
	futureTime = futureTime.Round(time.Hour)

	// Get the difference
	diff := futureTime.Sub(currentTime)

	// Ensure the actual difference is equal to the originally requested  difference
	assert.Equal(t, diff, time.Duration(time.Hour*4), "Time has increased by expected duration")
}

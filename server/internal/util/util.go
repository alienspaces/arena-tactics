package util

import (
	"math/rand"
	"time"

	"github.com/google/uuid"
)

func init() {

	// seed rand
	rand.Seed(time.Now().UTC().UnixNano())
}

// GetUUID returns a unique identifier
func GetUUID() string {
	uuidByte, _ := uuid.NewUUID()
	uuidString := uuidByte.String()
	return uuidString
}

// GetTime returns the current UTC time
func GetTime() string {

	t := time.Now()

	// format UTC
	tf := t.UTC().Format(time.RFC3339)

	return tf
}

// GetFutureTime returns the current UTC time
func GetFutureTime(d time.Duration) string {

	t := time.Now()

	// add duration
	if d != 0 {
		t = t.Add(d)
	}

	// format UTC
	tf := t.UTC().Format(time.RFC3339)

	return tf
}

// RandInt -
func RandInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

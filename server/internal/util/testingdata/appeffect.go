package testingdata

// Create appeffect records for testing purposes

import (
	"github.com/jmoiron/sqlx"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

const (
	// EffectDamage -
	EffectDamage string = "Damage"
	// EffectDamageOverTime -
	EffectDamageOverTime string = "Damage Over Time"
	// EffectHeal -
	EffectHeal string = "Heal"
	// EffectHealOverTime -
	EffectHealOverTime string = "Heal Over Time"
	// EffectDefence -
	EffectDefence string = "Defence"
	// EffectRegenerate -
	EffectRegenerate string = "Regenerate"
	// EffectDrainStrength -
	EffectDrainStrength string = "Drain Strength"
	// EffectHealSelf -
	EffectHealSelf string = "Heal Self"
)

// Effect -
type Effect struct {
	Name             string
	NarrativeText    string
	Type             string
	Duration         int
	Recurring        bool
	Permanent        bool
	EffectAttributes []*EffectAttribute
}

// EffectAttribute -
type EffectAttribute struct {
	Name            string
	Target          string
	ApplyConstraint string
	ValueFormula    string
}

var effects []*Effect

func init() {
	effects = []*Effect{
		&Effect{
			Name:          EffectDamage,
			NarrativeText: "damages",
			Type:          record.EffectTypeActive,
			Duration:      1,
			Recurring:     false,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetCurrentFoe,
					ApplyConstraint: "calculated",
					ValueFormula:    "-20 + defence",
				},
			},
		},
		&Effect{
			Name:          EffectDamageOverTime,
			NarrativeText: "damages",
			Type:          record.EffectTypeActive,
			Duration:      3,
			Recurring:     true,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetCurrentFoe,
					ApplyConstraint: "calculated",
					ValueFormula:    "-10 + defence",
				},
			},
		},
		&Effect{
			Name:          EffectHeal,
			NarrativeText: "heals",
			Type:          record.EffectTypeActive,
			Duration:      1,
			Recurring:     false,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetCurrentFriend,
					ApplyConstraint: "calculated",
					ValueFormula:    "4",
				},
			},
		},
		&Effect{
			Name:          EffectHealOverTime,
			NarrativeText: "heals",
			Type:          record.EffectTypeActive,
			Duration:      3,
			Recurring:     true,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetCurrentFriend,
					ApplyConstraint: "calculated",
					ValueFormula:    "2",
				},
			},
		},
		&Effect{
			Name:          EffectDefence,
			NarrativeText: "deflects",
			Type:          record.EffectTypePassive,
			Duration:      1,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeDefence,
					Target:          record.TargetSelf,
					ApplyConstraint: "maximum",
					ValueFormula:    "1",
				},
			},
		},
		&Effect{
			Name:          EffectRegenerate,
			NarrativeText: "regenerates",
			Type:          record.EffectTypePassive,
			Duration:      1,
			Recurring:     false,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetSelf,
					ApplyConstraint: "calculated",
					ValueFormula:    "2",
				},
			},
		},
		&Effect{
			Name:          EffectDrainStrength,
			NarrativeText: "drains",
			Type:          record.EffectTypeActive,
			Duration:      5,
			Recurring:     true,
			Permanent:     false,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeStrength,
					Target:          record.TargetCurrentFoe,
					ApplyConstraint: "calculated",
					ValueFormula:    "-1",
				},
			},
		},
		&Effect{
			Name:          EffectHealSelf,
			NarrativeText: "heals",
			Type:          record.EffectTypeActive,
			Duration:      1,
			Recurring:     false,
			Permanent:     true,
			EffectAttributes: []*EffectAttribute{
				&EffectAttribute{
					Name:            AttributeHealth,
					Target:          record.TargetSelf,
					ApplyConstraint: "calculated",
					ValueFormula:    "5",
				},
			},
		},
	}
}

// AddAppEffectData -
func (td *Data) AddAppEffectData(tx *sqlx.Tx) (err error) {

	l := td.Logger.With().Str("function", "AddAppEffectData").Logger()

	// effect
	for _, effect := range effects {

		l.Debug().Msgf("Adding app effect name >%s< type >%s<", effect.Name, effect.Type)

		effectRec, err := td.AddAppEffect(
			tx,
			effect.Name,
			effect.NarrativeText,
			effect.Type,
			effect.Duration,
			effect.Recurring,
			effect.Permanent,
		)
		if err != nil {
			l.Error().Msgf("Error creating app effect record >%v<", err)
			return err
		}

		// effect attributes
		for _, effectAttribute := range effect.EffectAttributes {

			l.Debug().Msgf("Adding app effect attribute name >%s<", effectAttribute.Name)

			attributeRec := td.GetAttributeRecByName(effectAttribute.Name)
			if attributeRec == nil {
				l.Error().Msgf("Attribute record name >%s< is nil", effectAttribute.Name)
				continue
			}

			_, err = td.AddAppEffectAttribute(
				tx,
				effectRec.ID,
				attributeRec.ID,
				effectAttribute.Target,
				effectAttribute.ApplyConstraint,
				effectAttribute.ValueFormula,
			)
			if err != nil {
				l.Error().Msgf("Error creating app effect attribute record >%v<", err)
				return err
			}
		}
	}

	return nil
}

// GetEffectRecByName - Finds and returns a test effect record
func (td *Data) GetEffectRecByName(effectName string) *record.AppEffectRecord {

	l := td.Logger.With().Str("function", "GetEffectRecByName").Logger()

	l.Debug().Msgf("Get effect Name >%s<", effectName)

	for _, rec := range td.AppEffectRecs {
		if rec.Name == effectName {
			return rec
		}
	}

	return nil
}

package testingdata

// Create appentity records for testing purposes

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Entity -
type Entity struct {
	Name          string
	EntitySkills  []*EntitySkill
	EntityItems   []*EntityItem
	EntityTactics []*EntityTactic
}

// EntitySkill -
type EntitySkill struct {
	Name string
}

// EntityItem -
type EntityItem struct {
	Name string
}

// EntityTactic -
type EntityTactic struct {
	Target     string
	Attribute  string
	Comparator string
	Value      int
	Action     string
	SkillName  string
	ItemName   string
	Order      int
}

var entitys []*Entity

const (
	// EntityNameAdam -
	EntityNameAdam string = "Adam"
	// EntityNameBrian -
	EntityNameBrian string = "Brian"
	// EntityNameCharles -
	EntityNameCharles string = "Charles"
	// EntityNameAnnie =
	EntityNameAnnie string = "Annie"
	// EntityNameBetty -
	EntityNameBetty string = "Betty"
	// EntityNameCarol -
	EntityNameCarol string = "Carol"
)

func init() {
	entitys = []*Entity{
		// adam - group 1
		&Entity{
			Name: EntityNameAdam,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemShortSword,
				},
				&EntityItem{
					Name: ItemShield,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      0,
					Action:     record.EntityTacticActionUseItem,
					ItemName:   ItemShortSword,
					Order:      1,
				},
			},
		},
		// brian - group 1
		&Entity{
			Name: EntityNameBrian,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillMagicPunch,
				},
				&EntitySkill{
					Name: SkillHealingTouch,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetAnyFriend,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorLessThan,
					Value:      25,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillHealingTouch,
					Order:      1,
				},
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreatest,
					Value:      0,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillMagicPunch,
					Order:      2,
				},
			},
		},
		// charles - group 1
		&Entity{
			Name: EntityNameCharles,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillRepeaterPunch,
				},
				&EntitySkill{
					Name: SkillHealingThoughts,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetSelf,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorLessThan,
					Value:      50,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillHealingThoughts,
					Order:      1,
				},
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      0,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillRepeaterPunch,
					Order:      2,
				},
			},
		},
		// annie - group 2
		&Entity{
			Name: EntityNameAnnie,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillMagicPunch,
				},
				&EntitySkill{
					Name: SkillHealingTouch,
				},
				&EntitySkill{
					Name: SkillHealingThoughts,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetSelf,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorLessThan,
					Value:      50,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillHealingThoughts,
					Order:      1,
				},
				&EntityTactic{
					Target:     record.TargetAnyFriend,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorLessThan,
					Value:      25,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillHealingTouch,
					Order:      2,
				},
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      0,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillMagicPunch,
					Order:      3,
				},
			},
		},
		// betty - group 2
		&Entity{
			Name: EntityNameBetty,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillWeaken,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemShortSword,
				},
				&EntityItem{
					Name: ItemShield,
				},
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeStrength,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      99,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillWeaken,
					Order:      1,
				},
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreatest,
					Value:      0,
					Action:     record.EntityTacticActionUseItem,
					ItemName:   ItemShortSword,
					Order:      1,
				},
			},
		},
		// carol - group 2
		&Entity{
			Name: EntityNameCarol,
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillRepeaterPunch,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      0,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillRepeaterPunch,
					Order:      1,
				},
			},
		},
		&Entity{
			Name: "Orc Warrior",
			EntitySkills: []*EntitySkill{
				&EntitySkill{
					Name: SkillRegenerate,
				},
				&EntitySkill{
					Name: SkillRepeaterPunch,
				},
			},
			EntityItems: []*EntityItem{
				&EntityItem{
					Name: ItemBreastPlate,
				},
			},
			EntityTactics: []*EntityTactic{
				&EntityTactic{
					Target:     record.TargetAnyFoe,
					Attribute:  AttributeHealth,
					Comparator: record.EntityTacticComparatorGreaterThan,
					Value:      0,
					Action:     record.EntityTacticActionUseSkill,
					SkillName:  SkillRepeaterPunch,
					Order:      1,
				},
			},
		},
	}
}

// AddAppEntityData -
func (td *Data) AddAppEntityData(tx *sqlx.Tx) (err error) {

	l := td.Logger.With().Str("function", "AddAppEntityData").Logger()

	// entity
	for _, entity := range entitys {

		entityRec, err := td.AddAppEntity(tx, entity.Name)
		if err != nil {
			l.Error().Msgf("Error creating app entity record >%v<", err)
			return err
		}

		// give all entities all attributes
		for _, attribute := range td.AppAttributeRecs {

			_, err = td.AddAppEntityAttribute(tx, entityRec.ID,
				attribute.ID,
				attribute.Assignable,
			)
			if err != nil {
				l.Error().Msgf("Error creating app entity attribute record >%v<", err)
				return err
			}
		}

		// app entity skills
		for _, entitySkill := range entity.EntitySkills {

			l.Debug().Msgf("Adding entity skill Name >%s<", entitySkill.Name)

			skillRec := td.GetSkillRecByName(entitySkill.Name)
			if skillRec == nil {
				l.Error().Msgf("Skill record name >%s< is nil", entitySkill.Name)
				return fmt.Errorf("Skill record name >%s< is nil", entitySkill.Name)
			}

			err = td.AddAppEntitySkill(
				tx,
				entityRec.ID,
				skillRec.ID,
			)
			if err != nil {
				l.Error().Msgf("Error creating app entity skill record >%v<", err)
				return err
			}
		}

		// app entity items
		for _, entityItem := range entity.EntityItems {

			l.Debug().Msgf("Adding entity item Name >%s<", entityItem.Name)

			itemRec := td.GetItemRecByName(entityItem.Name)
			if itemRec == nil {
				l.Error().Msgf("Item record name >%s< is nil", entityItem.Name)
				return fmt.Errorf("Item record name >%s< is nil", entityItem.Name)
			}

			err = td.AddAppEntityItem(
				tx,
				entityRec.ID,
				itemRec.ID,
			)
			if err != nil {
				l.Error().Msgf("Error creating app entity item record >%v<", err)
				return err
			}
		}

		// app entity tactics
		for _, entityTactic := range entity.EntityTactics {

			itemRec := &record.AppItemRecord{}
			skillRec := &record.AppSkillRecord{}
			attributeRec := td.GetAttributeRecByName(entityTactic.Attribute)

			if entityTactic.Action == record.EntityTacticActionUseItem {

				l.Debug().Msgf("Adding entity tactic item Name >%s<", entityTactic.ItemName)

				itemRec = td.GetItemRecByName(entityTactic.ItemName)
				if itemRec == nil {
					l.Error().Msgf("Item record name >%s< is nil", entityTactic.ItemName)
					return fmt.Errorf("Item record name >%s< is nil", entityTactic.ItemName)
				}
			} else if entityTactic.Action == record.EntityTacticActionUseSkill {

				l.Debug().Msgf("Adding entity tactic skill Name >%s<", entityTactic.SkillName)

				skillRec = td.GetSkillRecByName(entityTactic.SkillName)
				if skillRec == nil {
					l.Error().Msgf("Skill record name >%s< is nil", entityTactic.SkillName)
					return fmt.Errorf("Skill record name >%s< is nil", entityTactic.SkillName)
				}
			}

			err = td.AddAppEntityTactic(
				tx,
				entityRec.ID,
				entityTactic.Target,
				attributeRec.ID,
				entityTactic.Comparator,
				entityTactic.Value,
				entityTactic.Action,
				skillRec.ID,
				itemRec.ID,
				entityTactic.Order,
			)
			if err != nil {
				l.Error().Msgf("Error creating app entity tactic record >%v<", err)
				return err
			}
		}
	}

	return nil
}

// GetEntityRecByName -
func (td *Data) GetEntityRecByName(entityName string) (*record.AppEntityRecord, error) {

	l := td.Logger.With().Str("function", "GetEntityRecByName").Logger()

	l.Debug().Msgf("Get entity  Name >%s<", entityName)

	for _, entityRec := range td.AppEntityRecs {
		if entityRec.Name == entityName {
			return entityRec, nil
		}
	}

	return nil, nil
}

// GetEntitySkillRecByName -
func (td *Data) GetEntitySkillRecByName(entityID, skillName string) (*record.AppSkillRecord, error) {

	l := td.Logger.With().Str("function", "GetEntitySkillRecByName").Logger()

	l.Debug().Msgf("Get entity ID >%s< skill Name >%s<", entityID, skillName)

	for _, entitySkillRec := range td.AppEntitySkillRecs[entityID] {
		for _, skillRec := range td.AppSkillRecs {
			if skillRec.ID == entitySkillRec.AppSkillID &&
				skillRec.Name == skillName {
				return skillRec, nil
			}
		}
	}

	return nil, nil
}

// GetEntityItemRecByName -
func (td *Data) GetEntityItemRecByName(entityID, itemName string) (*record.AppItemRecord, error) {

	l := td.Logger.With().Str("function", "GetEntityItemRecByName").Logger()

	l.Debug().Msgf("Get entity ID >%s< item Name >%s<", entityID, itemName)

	for _, entityItemRec := range td.AppEntityItemRecs[entityID] {
		for _, itemRec := range td.AppItemRecs {
			if itemRec.ID == entityItemRec.AppItemID &&
				itemRec.Name == itemName {
				return itemRec, nil
			}
		}
	}

	return nil, nil
}

// GetEntityAttributeRecByName -
func (td *Data) GetEntityAttributeRecByName(entityID, attributeName string) (*record.AppEntityAttributeRecord, error) {

	l := td.Logger.With().Str("function", "GetEntityAttributeRecByName").Logger()

	l.Debug().Msgf("Get entity ID >%s< attribute Name >%s<", entityID, attributeName)

	for _, entityAttributeRec := range td.AppEntityAttributeRecs[entityID] {
		for _, attributeRec := range td.AppAttributeRecs {
			if attributeRec.ID == entityAttributeRec.AppAttributeID &&
				attributeRec.Name == attributeName {
				return entityAttributeRec, nil
			}
		}
	}

	return nil, nil
}

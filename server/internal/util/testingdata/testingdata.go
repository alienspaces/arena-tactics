package testingdata

import (
	"fmt"

	"github.com/icrowley/fake"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/template"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Data -
type Data struct {
	Env    env.Env
	Logger zerolog.Logger

	// template record
	TemplateRec *record.TemplateRecord

	// app data records
	AppRec         *record.AppRecord
	AppInstanceRec *record.AppInstanceRecord

	AppAttributeRecs       []*record.AppAttributeRecord
	AppEffectAttributeRecs []*record.AppEffectAttributeRecord

	AppEffectRecs []*record.AppEffectRecord

	AppSkillRecs       []*record.AppSkillRecord
	AppSkillEffectRecs map[string][]*record.AppSkillEffectRecord
	AppItemRecs        []*record.AppItemRecord
	AppItemEffectRecs  map[string][]*record.AppItemEffectRecord

	AppEntityRecs []*record.AppEntityRecord

	AppEntityAttributeRecs map[string]map[string]*record.AppEntityAttributeRecord
	AppEntitySkillRecs     map[string]map[string]*record.AppEntitySkillRecord
	AppEntityItemRecs      map[string]map[string]*record.AppEntityItemRecord
	AppEntityTacticRecs    map[string][]*record.AppEntityTacticRecord

	AppEntityGroupRecs       []*record.AppEntityGroupRecord
	AppEntityGroupMemberRecs []*record.AppEntityGroupMemberRecord

	AppFightRecs []*record.AppFightRecord

	AppFightEntityGroupRecs []*record.AppFightEntityGroupRecord
}

// NewData - Creates empty set of test data
func NewData(e env.Env, l zerolog.Logger) (*Data, error) {

	l = l.With().Str("package", "testingdata").Logger()

	// test data
	td := Data{
		Env:    e,
		Logger: l,
	}

	td.AppSkillEffectRecs = make(map[string][]*record.AppSkillEffectRecord)
	td.AppItemEffectRecs = make(map[string][]*record.AppItemEffectRecord)
	td.AppEntityAttributeRecs = make(map[string]map[string]*record.AppEntityAttributeRecord)
	td.AppEntitySkillRecs = make(map[string]map[string]*record.AppEntitySkillRecord)
	td.AppEntityItemRecs = make(map[string]map[string]*record.AppEntityItemRecord)
	td.AppEntityTacticRecs = make(map[string][]*record.AppEntityTacticRecord)

	return &td, nil
}

// AddAppData - Creates set of app test data
func (td *Data) AddAppData(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "AddAppData").Logger()

	// template - for testing basic repo, model and handler functionality
	err := td.AddTemplate(tx)
	if err != nil {
		l.Error().Msgf("Error creating template >%v<", err)
		return err
	}

	// app
	err = td.AddApp(tx)
	if err != nil {
		l.Error().Msgf("Error creating app record >%v<", err)
		return err
	}

	// app attribute data
	err = td.AddAppAttributeData(tx)
	if err != nil {
		l.Error().Msgf("Error creating app attribute data >%v<", err)
		return err
	}

	// app effect data
	err = td.AddAppEffectData(tx)
	if err != nil {
		l.Error().Msgf("Error creating app effect data >%v<", err)
		return err
	}

	// app items
	err = td.AddAppItemData(tx)
	if err != nil {
		l.Error().Msgf("Error creating app item data >%v<", err)
		return err
	}

	// app skills
	err = td.AddAppSkillData(tx)
	if err != nil {
		l.Error().Msgf("Error creating app skill data >%v<", err)
		return err
	}

	// app entities
	err = td.AddAppEntityData(tx)
	if err != nil {
		l.Error().Msgf("Error creating app entity data >%v<", err)
		return err
	}

	// three entity groups
	for i := 0; i < 3; i++ {
		err = td.AddAppEntityGroup(tx)
		if err != nil {
			l.Error().Msgf("Error creating app entity group record >%v<", err)
			return err
		}
	}

	// three entities in group one
	for i := 0; i < 3; i++ {
		err = td.AddAppEntityGroupMember(tx, td.AppEntityGroupRecs[0].ID, td.AppEntityRecs[i].ID)
		if err != nil {
			l.Error().Msgf("Error creating app entity group record >%v<", err)
			return err
		}
	}

	// three entities in group two
	for i := 3; i < 6; i++ {
		err = td.AddAppEntityGroupMember(tx, td.AppEntityGroupRecs[1].ID, td.AppEntityRecs[i].ID)
		if err != nil {
			l.Error().Msgf("Error creating app entity group record >%v<", err)
			return err
		}
	}

	// one app fight
	err = td.AddAppFight(tx)
	if err != nil {
		l.Error().Msgf("Error creating app fight record >%v<", err)
		return err
	}

	// both groups in fight one
	for i := 0; i < 2; i++ {
		err = td.AddAppFightEntityGroup(tx, td.AppFightRecs[0].ID, td.AppEntityGroupRecs[i].ID)
		if err != nil {
			l.Error().Msgf("Error creating app fight entity group record >%v<", err)
			return err
		}
	}

	return nil
}

// RemoveAppData -
func (td *Data) RemoveAppData(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppData").Logger()

	// remove app fight entity group records
	l.Debug().Msgf("Removing app fight entity records")

	err := td.RemoveAppFightEntityGroups(tx)
	if err != nil {
		l.Error().Msgf("Error removing app fight entity group record >%v<", err)
		return err
	}

	// remove app fight instance records
	l.Debug().Msgf("Removing app fight instance records")

	err = td.RemoveAppFightInstances(tx)
	if err != nil {
		l.Error().Msgf("Error removing app fight instance record >%v<", err)
		return err
	}

	// remove app fight record
	l.Debug().Msgf("Removing app fight records")

	err = td.RemoveAppFights(tx)
	if err != nil {
		l.Error().Msgf("Error removing app fight record >%v<", err)
		return err
	}

	// remove app entity group member records
	l.Debug().Msgf("Removing app entity group members records")

	err = td.RemoveAppEntityGroupMembers(tx)
	if err != nil {
		l.Error().Msgf("Error removing app entity group member record >%v<", err)
		return err
	}

	// remove app entity group records
	l.Debug().Msgf("Removing app entity group records")

	err = td.RemoveAppEntityGroups(tx)
	if err != nil {
		l.Error().Msgf("Error removing app entity group record >%v<", err)
		return err
	}

	// remove app entity records
	l.Debug().Msgf("Removing app entity records")

	err = td.RemoveAppEntities(tx)
	if err != nil {
		l.Error().Msgf("Error removing app entity record >%v<", err)
		return err
	}

	// remove app skill records
	l.Debug().Msgf("Removing app skill records")

	err = td.RemoveAppSkills(tx)
	if err != nil {
		l.Error().Msgf("Error removing app skill record >%v<", err)
		return err
	}

	// remove app item records
	l.Debug().Msgf("Removing app item records")

	err = td.RemoveAppItems(tx)
	if err != nil {
		l.Error().Msgf("Error removing app item record >%v<", err)
		return err
	}

	// remove app instance record
	l.Debug().Msgf("Removing app instance records")

	err = td.RemoveAppInstances(tx)
	if err != nil {
		l.Error().Msgf("Error removing app instance record >%v<", err)
		return err
	}

	// remove app effects records
	l.Debug().Msgf("Removing app effect records")

	err = td.RemoveAppEffects(tx)
	if err != nil {
		l.Error().Msgf("Error removing app reffect ecord >%v<", err)
		return err
	}

	// remove app attribute records
	l.Debug().Msgf("Removing app attribute records")

	err = td.RemoveAppAttributes(tx)
	if err != nil {
		l.Error().Msgf("Error removing app attribute record >%v<", err)
		return err
	}

	// remove app record
	l.Debug().Msgf("Removing app record")

	err = td.RemoveApp(tx)
	if err != nil {
		l.Error().Msgf("Error removing app record >%v<", err)
		return err
	}

	// remove app record
	l.Debug().Msgf("Removing template records")

	err = td.RemoveTemplate(tx)
	if err != nil {
		l.Error().Msgf("Error removing template record >%v<", err)
		return err
	}

	// check no data
	err = td.CheckNoData(tx)
	if err != nil {
		l.Error().Msgf("Failed check no data >%v<", err)
		return err
	}

	return nil
}

// AddTemplateData - Creates template test data
func (td *Data) AddTemplateData(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "AddTemplateData").Logger()

	// create template record
	r, _ := template.NewRepo(td.Env, td.Logger, tx)

	rec := r.NewRecord()
	rec.Name = fake.Word()

	// create
	err := r.Create(rec)
	if err != nil {
		l.Error().Msgf("Error creating test template record >%v<", err)
		return err
	}

	td.TemplateRec = rec

	return nil
}

// RemoveTemplateData -
func (td *Data) RemoveTemplateData(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveTemplateData").Logger()

	r, _ := template.NewRepo(td.Env, td.Logger, tx)

	err := r.Remove(td.TemplateRec.ID)
	if err != nil {
		l.Error().Msgf("Error removing test template record >%v<", err)
		return err
	}

	td.TemplateRec = nil

	return nil
}

// CheckNoData - checks that there is no test data left in master data tables
func (td *Data) CheckNoData(tx *sqlx.Tx) error {

	// app entity
	aer, _ := appentity.NewRepo(td.Env, td.Logger, tx)
	count, _ := aer.Count()
	if count != 0 {
		return fmt.Errorf("Repo app entity reports >%d< records", count)
	}

	// app
	ar, _ := app.NewRepo(td.Env, td.Logger, tx)
	count, _ = ar.Count()
	if count != 0 {
		return fmt.Errorf("Repo app reports >%d< records", count)
	}

	return nil
}

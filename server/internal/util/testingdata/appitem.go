package testingdata

// Create appitem records for testing purposes

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

const (
	// ItemShortSword -
	ItemShortSword string = "Short Sword"
	// ItemShield -
	ItemShield string = "Shield"
	// ItemBreastPlate -
	ItemBreastPlate string = "Breast Plate"
)

// Item -
type Item struct {
	Name          string
	NarrativeText string
	Type          string
	Location      string
	ItemEffects   []*ItemEffect
}

// ItemEffect -
type ItemEffect struct {
	Name string
}

var items []*Item

func init() {
	items = []*Item{
		&Item{
			Name:          ItemShortSword,
			NarrativeText: "slices open",
			Type:          record.ItemTypeWeapon,
			Location:      record.ItemLocationHand,
			ItemEffects: []*ItemEffect{
				&ItemEffect{
					Name: EffectDamage,
				},
			},
		},
		&Item{
			Name:          ItemShield,
			NarrativeText: "", // no active effects, cannot use
			Type:          record.ItemTypeArmour,
			Location:      record.ItemLocationHand,
			ItemEffects: []*ItemEffect{
				&ItemEffect{
					Name: EffectDefence,
				},
			},
		},
		&Item{
			Name:          ItemBreastPlate,
			NarrativeText: "", // no active effects, cannot use
			Type:          record.ItemTypeArmour,
			Location:      record.ItemLocationHand,
			ItemEffects: []*ItemEffect{
				&ItemEffect{
					Name: EffectDefence,
				},
			},
		},
	}
}

// AddAppItemData -
func (td *Data) AddAppItemData(tx *sqlx.Tx) (err error) {

	l := td.Logger.With().Str("function", "AddAppItemData").Logger()

	// item
	for _, item := range items {

		itemRec, err := td.AddAppItem(tx, item.Name, item.NarrativeText, item.Type, item.Location)
		if err != nil {
			l.Error().Msgf("Error creating app item record >%v<", err)
			return err
		}

		// item effects
		for _, itemEffect := range item.ItemEffects {

			effectRec := td.GetEffectRecByName(itemEffect.Name)
			if effectRec == nil {
				l.Error().Msgf("Effect record name >%s< is nil", itemEffect.Name)
				continue
			}

			_, err = td.AddAppItemEffect(tx, itemRec.ID, effectRec.ID)
			if err != nil {
				l.Error().Msgf("Error creating app item effect record >%v<", err)
				return err
			}
		}
	}

	return nil
}

// GetItemRecByName - Finds and returns a test item record
func (td *Data) GetItemRecByName(itemName string) *record.AppItemRecord {

	l := td.Logger.With().Str("function", "GetItemRecByName").Logger()

	l.Debug().Msgf("Get item Name >%s<", itemName)

	for _, rec := range td.AppItemRecs {
		if rec.Name == itemName {
			return rec
		}
	}

	return nil
}

package testingdata

// Create appattribute records for testing purposes

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

const (
	// AttributeStrength -
	AttributeStrength string = "strength"
	// AttributeDexterity -
	AttributeDexterity string = "dexterity"
	// AttributeIntelligence -
	AttributeIntelligence string = "intelligence"
	// AttributeHealth -
	AttributeHealth string = "health"
	// AttributeDodge -
	AttributeDodge string = "dodge"
	// AttributeDefence -
	AttributeDefence string = "defence"
)

// Attribute -
type Attribute struct {
	Name            string
	ValueFormula    string
	MinValueFormula string
	MaxValueFormula string
	Assignable      bool
}

var attributes []*Attribute

func init() {
	attributes = []*Attribute{
		&Attribute{
			Name:            AttributeStrength,
			ValueFormula:    "strength",
			MinValueFormula: "0",
			MaxValueFormula: "strength * 2",
			Assignable:      true,
		},
		&Attribute{
			Name:            AttributeDodge,
			ValueFormula:    "dexterity * 2",
			MinValueFormula: "0",
			MaxValueFormula: "dexterity * 2",
			Assignable:      false,
		},
		&Attribute{
			Name:            AttributeDexterity,
			ValueFormula:    "dexterity",
			MinValueFormula: "0",
			MaxValueFormula: "dexterity * 2",
			Assignable:      true,
		},
		&Attribute{
			Name:            AttributeIntelligence,
			ValueFormula:    "intelligence",
			MinValueFormula: "0",
			MaxValueFormula: "intelligence * 2",
			Assignable:      true,
		},
		&Attribute{
			Name:            AttributeHealth,
			ValueFormula:    "strength * 10",
			MinValueFormula: "0",
			MaxValueFormula: "strength * 10",
			Assignable:      false,
		},
		&Attribute{
			Name:            AttributeDefence,
			ValueFormula:    "0",
			MinValueFormula: "0",
			MaxValueFormula: "10",
			Assignable:      false,
		},
	}
}

// AddAppAttributeData -
func (td *Data) AddAppAttributeData(tx *sqlx.Tx) (err error) {

	l := td.Logger.With().Str("function", "AddAppAttributeData").Logger()

	// attributes
	for _, attribute := range attributes {

		err := td.AddAppAttribute(tx,
			attribute.Name,
			attribute.ValueFormula,
			attribute.MinValueFormula,
			attribute.MaxValueFormula,
			attribute.Assignable,
		)
		if err != nil {
			l.Error().Msgf("Error creating app attribute record >%v<", err)
			return err
		}
	}

	return nil
}

// GetAttributeRecByName - Finds and returns a test attribute record
func (td *Data) GetAttributeRecByName(attrName string) *record.AppAttributeRecord {

	l := td.Logger.With().Str("function", "GetAttributeRecByName").Logger()

	l.Debug().Msgf("Get attribute Name >%s<", attrName)

	for _, rec := range td.AppAttributeRecs {
		if rec.Name == attrName {
			return rec
		}
	}

	return nil
}

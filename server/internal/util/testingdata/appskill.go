package testingdata

// Create appskill records for testing purposes

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

const (
	// SkillMagicPunch -
	SkillMagicPunch string = "Magic Punch"
	// SkillRepeaterPunch -
	SkillRepeaterPunch string = "Repeater Punch"
	// SkillRegenerate -
	SkillRegenerate string = "Regenerate"
	// SkillHealingTouch -
	SkillHealingTouch string = "Healing Touch"
	// SkillHealingThoughts -
	SkillHealingThoughts string = "Healing Thoughts"
	// SkillWeaken -
	SkillWeaken string = "Weaken"
)

// Skill -
type Skill struct {
	Name          string
	NarrativeText string
	Type          string
	SkillEffects  []*SkillEffect
}

// SkillEffect -
type SkillEffect struct {
	Name string
}

var skills []*Skill

func init() {
	skills = []*Skill{
		&Skill{
			Name:          SkillMagicPunch,
			NarrativeText: "punches",
			Type:          record.SkillTypeActive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectDamage,
				},
			},
		},
		&Skill{
			Name:          SkillRepeaterPunch,
			NarrativeText: "repeatedly punches",
			Type:          record.SkillTypeActive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectDamageOverTime,
				},
			},
		},
		&Skill{
			Name:          SkillRegenerate,
			NarrativeText: "regenerates",
			Type:          record.SkillTypePassive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectRegenerate,
				},
			},
		},
		&Skill{
			Name:          SkillHealingTouch,
			NarrativeText: "heals",
			Type:          record.SkillTypeActive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectHeal,
				},
			},
		},
		&Skill{
			Name:          SkillHealingThoughts,
			NarrativeText: "heals",
			Type:          record.SkillTypeActive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectHealSelf,
				},
			},
		},
		&Skill{
			Name:          SkillWeaken,
			NarrativeText: "weakens",
			Type:          record.SkillTypeActive,
			SkillEffects: []*SkillEffect{
				&SkillEffect{
					Name: EffectDrainStrength,
				},
			},
		},
	}
}

// AddAppSkillData -
func (td *Data) AddAppSkillData(tx *sqlx.Tx) (err error) {

	l := td.Logger.With().Str("function", "AddAppSkillData").Logger()

	// skill
	for _, skill := range skills {

		skillRec, err := td.AddAppSkill(tx, skill.Name, skill.NarrativeText, skill.Type)
		if err != nil {
			l.Error().Msgf("Error creating app skill record >%v<", err)
			return err
		}

		// skill effects
		for _, skillEffect := range skill.SkillEffects {

			effectRec := td.GetEffectRecByName(skillEffect.Name)
			if effectRec == nil {
				l.Error().Msgf("Effect record name >%s< is nil", skillEffect.Name)
				continue
			}

			_, err = td.AddAppSkillEffect(tx, skillRec.ID, effectRec.ID)
			if err != nil {
				l.Error().Msgf("Error creating app skill effect record >%v<", err)
				return err
			}
		}
	}

	return nil
}

// GetSkillRecByName - Finds and returns a test skill record
func (td *Data) GetSkillRecByName(skillName string) *record.AppSkillRecord {

	l := td.Logger.With().Str("function", "GetSkillRecByName").Logger()

	l.Debug().Msgf("Get skill Name >%s<", skillName)

	for _, rec := range td.AppSkillRecs {
		if rec.Name == skillName {
			return rec
		}
	}

	return nil
}

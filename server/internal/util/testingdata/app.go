package testingdata

import (
	"github.com/jmoiron/sqlx"

	// repo
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/app"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appeffectattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentity"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityattribute"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitygroupmember"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentityskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appentitytactic"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfight"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightentitygroup"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightinstance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appfightinstanceturn"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appinstance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitem"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appitemeffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskill"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/appskilleffect"
	"gitlab.com/alienspaces/arena-tactics/server/internal/repo/template"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// AddApp - Add an app record to test data
func (td *Data) AddApp(tx *sqlx.Tx) error {

	// create app record
	m, _ := app.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord()

	td.AppRec = rec

	return nil
}

// RemoveApp - Removes an app record from test data
func (td *Data) RemoveApp(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveApp").Logger()

	m, _ := app.NewRepo(td.Env, td.Logger, tx)

	err := m.Remove(td.AppRec.ID)
	if err != nil {
		l.Error().Msgf("Error removing test app record >%v<", err)
		return err
	}

	td.AppRec = nil

	return nil
}

// AddTemplate - Add a template record to test data
func (td *Data) AddTemplate(tx *sqlx.Tx) error {

	// create template record
	m, _ := template.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord()

	td.TemplateRec = rec

	return nil
}

// RemoveTemplate - Removes a template record from test data
func (td *Data) RemoveTemplate(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveTemplate").Logger()

	m, _ := template.NewRepo(td.Env, td.Logger, tx)

	err := m.Remove(td.TemplateRec.ID)
	if err != nil {
		l.Error().Msgf("Error removing test template record >%v<", err)
		return err
	}

	td.TemplateRec = nil

	return nil
}

// AddAppAttribute - Add an app attribute record to test data
func (td *Data) AddAppAttribute(tx *sqlx.Tx, name, valueFormula, minValueFormula, maxValueFormula string, assignable bool) error {

	// create app attribute record
	m, _ := appattribute.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID, name, valueFormula, minValueFormula, maxValueFormula, assignable)

	td.AppAttributeRecs = append(td.AppAttributeRecs, rec)

	return nil
}

// RemoveAppAttributes - Removes an app attribute records from test data
func (td *Data) RemoveAppAttributes(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppAttributes").Logger()

	m, _ := appattribute.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppAttributeRecs {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app attribute record >%v<", err)
			return err
		}
	}

	td.AppAttributeRecs = nil

	return nil
}

// AddAppEffect - Add an app effect record to test data
func (td *Data) AddAppEffect(
	tx *sqlx.Tx,
	effectName,
	narrativeText,
	effectType string,
	duration int,
	recurring bool,
	permanent bool) (*record.AppEffectRecord, error) {

	// create app effect record
	m, _ := appeffect.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID, effectName, narrativeText, effectType, duration, recurring, permanent)

	td.AppEffectRecs = append(td.AppEffectRecs, rec)

	return rec, nil
}

// RemoveAppEffects - Removes app effect records from test data
func (td *Data) RemoveAppEffects(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppEffects").Logger()

	em, _ := appeffect.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppEffectRecs {

		err := td.RemoveAppEffectAttributes(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app effect attribute records >%v<", err)
			return err
		}

		err = em.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app effect record >%v<", err)
			return err
		}
	}

	td.AppEffectRecs = nil

	return nil
}

// AddAppEffectAttribute -
func (td *Data) AddAppEffectAttribute(
	tx *sqlx.Tx,
	appEffectID,
	appAttributeID string,
	effectTarget,
	applyConstraint,
	effectFormula string) (*record.AppEffectAttributeRecord, error) {

	m, _ := appeffectattribute.NewRepo(td.Env, td.Logger, tx)

	rec, err := m.CreateTestRecord(appEffectID, appAttributeID, effectTarget, applyConstraint, effectFormula)

	td.AppEffectAttributeRecs = append(td.AppEffectAttributeRecs, rec)

	return rec, err
}

// RemoveAppEffectAttributes -
func (td *Data) RemoveAppEffectAttributes(tx *sqlx.Tx, appEffectID string) (err error) {

	l := td.Logger.With().Str("function", "RemoveAppEffectAttributes").Logger()

	m, _ := appeffectattribute.NewRepo(td.Env, td.Logger, tx)

	for _, effectAttributeRec := range td.AppEffectAttributeRecs {

		if effectAttributeRec.AppEffectID != appEffectID {
			continue
		}

		err := m.Remove(effectAttributeRec.ID)
		if err != nil {
			l.Error().Msgf("Failed to remove effect attribute record >%v<", err)
			return err
		}
	}

	return nil
}

// AddAppInstance - Add an app instance record to test data
func (td *Data) AddAppInstance(tx *sqlx.Tx) error {

	// create app instance record
	m, _ := appinstance.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID)

	td.AppInstanceRec = rec

	return nil
}

// RemoveAppInstances - Removes an app record from test data
func (td *Data) RemoveAppInstances(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppInstances").Logger()

	m, _ := appinstance.NewRepo(td.Env, td.Logger, tx)

	recs, _ := m.GetByParam(map[string]interface{}{"app_id": td.AppRec.ID})
	for _, rec := range recs {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app instance record >%v<", err)
			return err
		}
	}

	td.AppInstanceRec = nil

	return nil
}

// AddAppEntity - Add an app entity record to test data
func (td *Data) AddAppEntity(tx *sqlx.Tx, entityName string) (*record.AppEntityRecord, error) {

	l := td.Logger.With().Str("function", "AddAppEntity").Logger()

	l.Debug().Msgf("Adding entity Name >%s<", entityName)

	// create app entity record
	m, _ := appentity.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID, entityName)

	td.AppEntityRecs = append(td.AppEntityRecs, rec)

	return rec, nil
}

// RemoveAppEntities - Removes all app entity records
func (td *Data) RemoveAppEntities(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppEntities").Logger()

	m, _ := appentity.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppEntityRecs {

		l.Debug().Msgf("Removing entity Name >%s<", rec.Name)

		// remove attributes
		err := td.RemoveAppEntityAttributes(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity attribute records >%v<", err)
			return err
		}

		// remove skills
		err = td.RemoveAppEntitySkills(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity skill records >%v<", err)
			return err
		}

		// remove items
		err = td.RemoveAppEntityItems(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity item records >%v<", err)
			return err
		}

		// remove tactics
		err = td.RemoveAppEntityTactics(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity tactic records >%v<", err)
			return err
		}

		err = m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity record >%v<", err)
			return err
		}
	}

	td.AppEntityRecs = nil

	return nil
}

// AddAppSkill - Add an app skill record to test data
func (td *Data) AddAppSkill(tx *sqlx.Tx, skillName, narrativeText, skillType string) (*record.AppSkillRecord, error) {

	m, _ := appskill.NewRepo(td.Env, td.Logger, tx)

	// create app skill record
	rec, _ := m.CreateTestRecord(td.AppRec.ID, skillName, narrativeText, skillType)

	td.AppSkillRecs = append(td.AppSkillRecs, rec)

	return rec, nil
}

// RemoveAppSkills - Removes an app skill record from test data
func (td *Data) RemoveAppSkills(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppSkills").Logger()

	m, _ := appskill.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppSkillRecs {

		err := td.RemoveAppSkillEffects(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app skill effect records >%v<", err)
			return err
		}

		err = m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app skill record >%v<", err)
			return err
		}
	}

	td.AppSkillRecs = nil

	return nil
}

// AddAppItem - Add an app item record to test data
func (td *Data) AddAppItem(tx *sqlx.Tx, itemName, narrativeText, itemType, itemLocation string) (*record.AppItemRecord, error) {

	// create app item record
	m, _ := appitem.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID, itemName, narrativeText, itemType, itemLocation)

	td.AppItemRecs = append(td.AppItemRecs, rec)

	return rec, nil
}

// RemoveAppItems - Removes an app item record from test data
func (td *Data) RemoveAppItems(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppItems").Logger()

	m, _ := appitem.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppItemRecs {

		err := td.RemoveAppItemEffects(tx, rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app item effect records >%v<", err)
			return err
		}

		err = m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app item record >%v<", err)
			return err
		}
	}

	td.AppItemRecs = nil

	return nil
}

// AddAppItemEffect -
func (td *Data) AddAppItemEffect(tx *sqlx.Tx, appItemID, appEffectID string) (*record.AppItemEffectRecord, error) {

	m, _ := appitemeffect.NewRepo(td.Env, td.Logger, tx)

	rec, err := m.CreateTestRecord(appItemID, appEffectID)

	td.AppItemEffectRecs[appItemID] = append(td.AppItemEffectRecs[appItemID], rec)

	return rec, err
}

// RemoveAppItemEffects -
func (td *Data) RemoveAppItemEffects(tx *sqlx.Tx, appItemID string) (err error) {

	l := td.Logger.With().Str("function", "RemoveAppItemEffects").Logger()

	m, _ := appitemeffect.NewRepo(td.Env, td.Logger, tx)

	for _, effectRec := range td.AppItemEffectRecs[appItemID] {
		err := m.Remove(effectRec.ID)
		if err != nil {
			l.Error().Msgf("Failed to remove item effect record >%v<", err)
			return err
		}
	}

	return nil
}

// AddAppSkillEffect -
func (td *Data) AddAppSkillEffect(tx *sqlx.Tx, appSkillID, appEffectID string) (*record.AppSkillEffectRecord, error) {

	m, _ := appskilleffect.NewRepo(td.Env, td.Logger, tx)

	rec, err := m.CreateTestRecord(appSkillID, appEffectID)

	td.AppSkillEffectRecs[appSkillID] = append(td.AppSkillEffectRecs[appSkillID], rec)

	return rec, err
}

// RemoveAppSkillEffects -
func (td *Data) RemoveAppSkillEffects(tx *sqlx.Tx, appSkillID string) (err error) {

	l := td.Logger.With().Str("function", "RemoveAppSkillEffects").Logger()

	m, _ := appskilleffect.NewRepo(td.Env, td.Logger, tx)

	for _, effectRec := range td.AppSkillEffectRecs[appSkillID] {
		err := m.Remove(effectRec.ID)
		if err != nil {
			l.Error().Msgf("Failed to remove skill effect record >%v<", err)
			return err
		}
	}

	return nil
}

// AddAppEntityAttribute - Add an app entity attribute record to test data
func (td *Data) AddAppEntityAttribute(tx *sqlx.Tx, entityID, attributeID string, assignable bool) (*record.AppEntityAttributeRecord, error) {

	// create app entity attribute record
	m, _ := appentityattribute.NewRepo(td.Env, td.Logger, tx)

	var rec *record.AppEntityAttributeRecord

	// An assignable attribute allows the entity to have a chosen
	// value and can add further attribute points to the attribute when
	// leveling up, is set by its formula and is able to be adjusted by
	// effects.  A non-assignable attribute has a value of 0, is calculated
	// by its formula alone and is only ever able to be adjusted by effects.
	if assignable {
		rec, _ = m.CreateTestRecord(entityID, attributeID, nil)
	} else {
		value := 0
		rec, _ = m.CreateTestRecord(entityID, attributeID, &value)
	}

	if td.AppEntityAttributeRecs[entityID] == nil {
		td.AppEntityAttributeRecs[entityID] = make(map[string]*record.AppEntityAttributeRecord)
	}

	td.AppEntityAttributeRecs[entityID][rec.ID] = rec

	return rec, nil
}

// RemoveAppEntityAttributes - Removes all app entity attribute records
func (td *Data) RemoveAppEntityAttributes(tx *sqlx.Tx, entityID string) error {

	l := td.Logger.With().Str("function", "RemoveAppEntityAttributes").Logger()

	m, _ := appentityattribute.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppEntityAttributeRecs[entityID] {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Failed to remove entity attribute record >%v<", err)
			return err
		}

		delete(td.AppEntityAttributeRecs[entityID], rec.ID)
	}

	delete(td.AppEntityAttributeRecs, entityID)

	return nil
}

// AddAppEntitySkill - Add an app entity skill record to test data
func (td *Data) AddAppEntitySkill(tx *sqlx.Tx, entityID, skillID string) error {

	// create app entity skill record
	m, _ := appentityskill.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(entityID, skillID)

	if td.AppEntitySkillRecs[entityID] == nil {
		td.AppEntitySkillRecs[entityID] = make(map[string]*record.AppEntitySkillRecord)
	}

	td.AppEntitySkillRecs[entityID][rec.ID] = rec

	return nil
}

// RemoveAppEntitySkills - Removes an app entity skill record from test data
func (td *Data) RemoveAppEntitySkills(tx *sqlx.Tx, entityID string) error {

	l := td.Logger.With().Str("function", "RemoveAppEntitySkills").Logger()

	m, _ := appentityskill.NewRepo(td.Env, td.Logger, tx)

	// remove entity skills
	for _, rec := range td.AppEntitySkillRecs[entityID] {

		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity skill record >%v<", err)
			return err
		}

		delete(td.AppEntitySkillRecs[entityID], rec.ID)
	}

	delete(td.AppEntitySkillRecs, entityID)

	return nil
}

// AddAppEntityItem - Add an app entity item record to test data
func (td *Data) AddAppEntityItem(tx *sqlx.Tx, entityID, itemID string) error {

	// create app entity item record
	m, _ := appentityitem.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(entityID, itemID)

	if td.AppEntityItemRecs[entityID] == nil {
		td.AppEntityItemRecs[entityID] = make(map[string]*record.AppEntityItemRecord)
	}

	td.AppEntityItemRecs[entityID][rec.ID] = rec

	return nil
}

// RemoveAppEntityItems - Removes an app entity item record from test data
func (td *Data) RemoveAppEntityItems(tx *sqlx.Tx, entityID string) error {

	l := td.Logger.With().Str("function", "RemoveAppEntityItems").Logger()

	m, _ := appentityitem.NewRepo(td.Env, td.Logger, tx)

	// remove entity items
	for _, rec := range td.AppEntityItemRecs[entityID] {

		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity item record >%v<", err)
			return err
		}

		delete(td.AppEntityItemRecs[entityID], rec.ID)
	}

	delete(td.AppEntityItemRecs, entityID)

	return nil
}

// AddAppEntityTactic - Add an app entity tactic record to test data
func (td *Data) AddAppEntityTactic(
	tx *sqlx.Tx,
	entityID,
	target,
	attributeID,
	comparator string,
	value int,
	action,
	skillID,
	itemID string,
	order int) error {

	// create app entity tactic record
	m, _ := appentitytactic.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(
		entityID,
		target,
		attributeID,
		comparator,
		value,
		action,
		skillID,
		itemID,
		order,
	)

	if td.AppEntityTacticRecs[entityID] == nil {
		td.AppEntityTacticRecs[entityID] = []*record.AppEntityTacticRecord{}
	}

	td.AppEntityTacticRecs[entityID] = append(td.AppEntityTacticRecs[entityID], rec)

	return nil
}

// RemoveAppEntityTactics - Removes an app entity tactic records from test data
func (td *Data) RemoveAppEntityTactics(tx *sqlx.Tx, entityID string) error {

	l := td.Logger.With().Str("function", "RemoveAppEntityTactics").Logger()

	m, _ := appentitytactic.NewRepo(td.Env, td.Logger, tx)

	// remove entity items
	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err := m.GetByParam(params)
	if err != nil {
		l.Error().Msgf("Failed to get app entity tactic records >%v<", err)
		return err
	}

	for _, rec := range recs {

		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity tactic record >%v<", err)
			return err
		}
	}

	delete(td.AppEntityTacticRecs, entityID)

	return nil
}

// AddAppEntityGroup - Add an app entity group record to test data
func (td *Data) AddAppEntityGroup(tx *sqlx.Tx) error {

	// create app entity group record
	m, _ := appentitygroup.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(td.AppRec.ID)

	td.AppEntityGroupRecs = append(td.AppEntityGroupRecs, rec)

	return nil
}

// RemoveAppEntityGroups - Removes all app entity group records
func (td *Data) RemoveAppEntityGroups(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppEntityGroups").Logger()

	m, _ := appentitygroup.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppEntityGroupRecs {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity group record >%v<", err)
			return err
		}
	}

	td.AppEntityGroupRecs = nil

	return nil
}

// AddAppEntityGroupMember - Add an app entity group record to test data
func (td *Data) AddAppEntityGroupMember(tx *sqlx.Tx, appEntityGroupID, appEntityID string) error {

	// create app entity group record
	m, _ := appentitygroupmember.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(appEntityGroupID, appEntityID)

	td.AppEntityGroupMemberRecs = append(td.AppEntityGroupMemberRecs, rec)

	return nil
}

// RemoveAppEntityGroupMembers - Removes all app entity group records
func (td *Data) RemoveAppEntityGroupMembers(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppEntityGroupMembers").Logger()

	m, _ := appentitygroupmember.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppEntityGroupMemberRecs {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app entity group member record >%v<", err)
			return err
		}
	}

	td.AppEntityGroupMemberRecs = nil

	return nil
}

// AddAppFight - Add an app fight record to test data
func (td *Data) AddAppFight(tx *sqlx.Tx) error {

	// create app fight record
	r, _ := appfight.NewRepo(td.Env, td.Logger, tx)

	rec, _ := r.CreateTestRecord(td.AppRec.ID)

	td.AppFightRecs = append(td.AppFightRecs, rec)

	return nil
}

// RemoveAppFights - Removes an app fight record from test data
func (td *Data) RemoveAppFights(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppFight").Logger()

	r, _ := appfight.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppFightRecs {

		err := r.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app fight record >%v<", err)
			return err
		}
	}

	td.AppFightRecs = nil

	return nil
}

// RemoveAppFightInstances - Removes an app fight record from test data
func (td *Data) RemoveAppFightInstances(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppFightInstances").Logger()

	m, _ := appfightinstance.NewRepo(td.Env, td.Logger, tx)
	hm, _ := appfightinstanceturn.NewRepo(td.Env, td.Logger, tx)

	for _, fightRec := range td.AppFightRecs {

		recs, _ := m.GetByParam(map[string]interface{}{"app_fight_id": fightRec.ID})
		for _, rec := range recs {

			// fight instance history
			hRecs, _ := hm.GetByParam(map[string]interface{}{"app_fight_instance_id": rec.ID})
			for _, hRec := range hRecs {
				err := hm.Remove(hRec.ID)
				if err != nil {
					l.Error().Msgf("Error removing test app fight instance history record >%v<", err)
					return err
				}
			}

			// fight instance
			err := m.Remove(rec.ID)
			if err != nil {
				l.Error().Msgf("Error removing test app fight instance record >%v<", err)
				return err
			}
		}
	}

	return nil
}

// AddAppFightEntityGroup - Add an app fight entity group record
func (td *Data) AddAppFightEntityGroup(tx *sqlx.Tx, appFightRecID, appEntityGroupID string) error {

	// create app fight entity group record
	m, _ := appfightentitygroup.NewRepo(td.Env, td.Logger, tx)

	rec, _ := m.CreateTestRecord(appFightRecID, appEntityGroupID)

	td.AppFightEntityGroupRecs = append(td.AppFightEntityGroupRecs, rec)

	return nil
}

// RemoveAppFightEntityGroups - Removes all app fight entity group records
func (td *Data) RemoveAppFightEntityGroups(tx *sqlx.Tx) error {

	l := td.Logger.With().Str("function", "RemoveAppFightEntityGroups").Logger()

	m, _ := appfightentitygroup.NewRepo(td.Env, td.Logger, tx)

	for _, rec := range td.AppFightEntityGroupRecs {
		err := m.Remove(rec.ID)
		if err != nil {
			l.Error().Msgf("Error removing test app fight entity group record >%v<", err)
			return err
		}
	}

	td.AppFightEntityGroupRecs = nil

	return nil
}

package testingdata

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

// environment
var e, _ = env.NewEnv()

// logger
var log, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, log)

func TestAddRemoveAppData(t *testing.T) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new transaction %v", err)
	}

	td, err := NewData(e, log)
	assert.NoError(t, err, "New empty data did not return an error")
	assert.NotNil(t, td, "Test empty data is not nil")

	// test app data
	err = td.AddAppData(tx)
	if assert.NoError(t, err, "New app data did not return an error") {

		assert.NotNil(t, td.AppRec, "Test data AppRec is not nil")
		assert.Equal(t, 6, len(td.AppAttributeRecs), "Test data AppAttributeRecs length equals expected")
		assert.Equal(t, 8, len(td.AppEffectRecs), "Test data AppEffectRecs length equals expected")

		assert.NotEmpty(t, td.AppItemRecs, "Test data AppItemRec is not empty")
		assert.NotEmpty(t, td.AppSkillRecs, "Test data AppSkillRec is not empty")

		assert.NotEmpty(t, td.AppEntityRecs, "Test data AppEntityRecs is not empty")

		assert.NotEmpty(t, td.AppEntityAttributeRecs, "Test data AppEntityAttributeRecs is not empty")
		assert.NotEmpty(t, td.AppEntityItemRecs, "Test data AppEntityItemRecs is not empty")
		assert.NotEmpty(t, td.AppEntitySkillRecs, "Test data AppEntitySkillRecs is not empty")

		assert.NotEmpty(t, td.AppEntityGroupRecs, "Test data AppEntityGroupRecs is not empty")
		assert.NotEmpty(t, td.AppEntityGroupMemberRecs, "Test data AppEntityGroupMemberRecs is not empty")

		assert.NotEmpty(t, td.AppFightRecs, "Test data AppFightRecs is not empty")
		assert.NotEmpty(t, td.AppFightEntityGroupRecs, "Test data AppFightEntityGroupRecs is not empty")

		// remove app data
		err = td.RemoveAppData(tx)
		if assert.NoError(t, err, "Removing app data did not return an error") {

			assert.Nil(t, td.AppRec, "Test data AppRec is nil")
			assert.Nil(t, td.AppAttributeRecs, "Test data AppAttributeRecs is nil")
			assert.Nil(t, td.AppEffectRecs, "Test data AppEffectRecs is nil")

			assert.Nil(t, td.AppInstanceRec, "Test data AppInstanceRec is nil")

			assert.Nil(t, td.AppItemRecs, "Test data AppItemRec is nil")
			assert.Nil(t, td.AppSkillRecs, "Test data AppSkillRec is nil")

			assert.Empty(t, td.AppEntityRecs, "Test data AppEntityRecs is empty")

			assert.Empty(t, td.AppEntityAttributeRecs, "Test data AppEntityAttributeRecs is empty")
			assert.Empty(t, td.AppEntityItemRecs, "Test data AppEntityItemRecs is empty")
			assert.Empty(t, td.AppEntitySkillRecs, "Test data AppEntitySkillRecs is empty")

			assert.Empty(t, td.AppEntityGroupRecs, "Test data AppEntityGroupRecs is empty")
			assert.Empty(t, td.AppEntityGroupMemberRecs, "Test data AppEntityGroupMemberRecs is empty")

			assert.Empty(t, td.AppFightRecs, "Test data AppFightRecs is empty")
			assert.Empty(t, td.AppFightEntityGroupRecs, "Test data AppFightEntityGroupRecs is empty")
		}
	}

	// rollback
	tx.Rollback()
}

package template

import (
	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetTemplateRecs -
func (m *Model) GetTemplateRecs(params map[string]interface{}) (recs []*record.TemplateRecord, err error) {

	l := m.Logger.With().Str("function", "GetTemplateRecs").Logger()

	// repos
	r := m.RepoStore.TemplateRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// GetTemplateRec -
func (m *Model) GetTemplateRec(templateID string) (rec *record.TemplateRecord, err error) {

	l := m.Logger.With().Str("function", "GetTemplateRec").Logger()

	// repos
	r := m.RepoStore.TemplateRepo

	rec, err = r.GetByID(templateID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// CreateTemplateRec -
func (m *Model) CreateTemplateRec(rec *record.TemplateRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateTemplateRec").Logger()

	// repos
	r := m.RepoStore.TemplateRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateTemplateRec -
func (m *Model) UpdateTemplateRec(rec *record.TemplateRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateTemplateRec").Logger()

	// repos
	r := m.RepoStore.TemplateRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteTemplateRec -
func (m *Model) DeleteTemplateRec(templateID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteTemplateRec").Logger()

	// repos
	r := m.RepoStore.TemplateRepo

	err = r.Delete(templateID)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

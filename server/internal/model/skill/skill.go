package skill

import (
	"github.com/rs/zerolog"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "skill"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, rs *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: rs,
	}

	return m, err
}

package skill

import (
	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetSkillRec -
func (m *Model) GetSkillRec(skillID string) (rec *record.AppSkillRecord, err error) {

	l := m.Logger.With().Str("function", "GetSkillRec").Logger()

	// repos
	r := m.RepoStore.AppSkillRepo

	rec, err = r.GetByID(skillID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetSkillRecs -
func (m *Model) GetSkillRecs(params map[string]interface{}) (recs []*record.AppSkillRecord, err error) {

	l := m.Logger.With().Str("function", "GetSkillRecs").Logger()

	// repos
	r := m.RepoStore.AppSkillRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateSkillRec -
func (m *Model) CreateSkillRec(rec *record.AppSkillRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateSkillRec").Logger()

	// repos
	r := m.RepoStore.AppSkillRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateSkillRec -
func (m *Model) UpdateSkillRec(rec *record.AppSkillRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateSkillRec").Logger()

	// repos
	r := m.RepoStore.AppSkillRepo

	currentRec, err := m.GetSkillRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.Name == "" {
		rec.Name = currentRec.Name
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteSkillRec -
func (m *Model) DeleteSkillRec(appSkillID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteSkillRec").Logger()

	// repos
	r := m.RepoStore.AppSkillRepo
	er := m.RepoStore.AppEntityRepo
	esr := m.RepoStore.AppEntitySkillRepo

	skillRec, err := r.GetByID(appSkillID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return err
	}

	err = r.Delete(appSkillID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	// delete skill from all entities
	params := make(map[string]interface{})
	params["app_id"] = skillRec.AppID

	entRecs, err := er.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity records >%v<", err)
		return err
	}

	for _, entRec := range entRecs {
		params = make(map[string]interface{})
		params["app_skill_id"] = appSkillID
		params["app_entity_id"] = entRec.ID

		entSkillRecs, err := esr.GetByParam(params)
		if err != nil {
			l.Warn().Msgf("Failed to get entity skill records >%v<", err)
			return err
		}

		if len(entSkillRecs) == 0 {
			l.Info().Msgf("Entity ID >%s< does not have skill ID >%s<", entRec.ID, appSkillID)
			continue
		}

		err = esr.Delete(entSkillRecs[0].ID)
		if err != nil {
			l.Warn().Msgf("Failed to delete entity skill record >%v<", err)
			return err
		}
	}

	return nil
}

// GetEntitySkillRec -
func (m *Model) GetEntitySkillRec(entitySkillID string) (rec *record.AppEntitySkillRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntitySkillRec").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	rec, err = r.GetByID(entitySkillID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetSkillEffectRec -
func (m *Model) GetSkillEffectRec(skillEffectID string) (rec *record.AppSkillEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetSkillEffectRecs").Logger()

	// repos
	r := m.RepoStore.AppSkillEffectRepo

	rec, err = r.GetByID(skillEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetSkillEffectRecs -
func (m *Model) GetSkillEffectRecs(skillID string) (recs []*record.AppSkillEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetSkillEffectRecs").Logger()

	// repos
	r := m.RepoStore.AppSkillEffectRepo

	params := make(map[string]interface{})
	params["app_skill_id"] = skillID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateSkillEffectRec -
func (m *Model) CreateSkillEffectRec(rec *record.AppSkillEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateSkillEffectRec").Logger()

	// repos
	r := m.RepoStore.AppSkillEffectRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateSkillEffectRec -
func (m *Model) UpdateSkillEffectRec(rec *record.AppSkillEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateSkillEffectRec").Logger()

	// repos
	r := m.RepoStore.AppSkillEffectRepo

	currentRec, err := m.GetSkillEffectRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.AppSkillID == "" {
		rec.AppSkillID = currentRec.AppSkillID
	}

	if rec.AppEffectID == "" {
		rec.AppEffectID = currentRec.AppEffectID
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteSkillEffectRec -
func (m *Model) DeleteSkillEffectRec(skillEffectID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteSkillEffectRec").Logger()

	// repos
	r := m.RepoStore.AppSkillEffectRepo

	err = r.Delete(skillEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

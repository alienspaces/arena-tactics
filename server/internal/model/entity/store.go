package entity

import (
	// types
	"fmt"

	"github.com/bradfitz/slice"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetEntityRec -
func (m *Model) GetEntityRec(entityID string) (rec *record.AppEntityRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityRec").Logger()

	// repos
	r := m.RepoStore.AppEntityRepo

	rec, err = r.GetByID(entityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityRecs -
func (m *Model) GetEntityRecs(params map[string]interface{}) (recs []*record.AppEntityRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityRecs").Logger()

	// repos
	r := m.RepoStore.AppEntityRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entities >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEntityRec -
func (m *Model) CreateEntityRec(rec *record.AppEntityRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityRec").Logger()

	// repos
	r := m.RepoStore.AppEntityRepo
	ar := m.RepoStore.AppAttributeRepo
	ear := m.RepoStore.AppEntityAttributeRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity >%v<", err)
		return err
	}

	// create entity attribute records
	params := make(map[string]interface{})
	params["app_id"] = rec.AppID
	attRecs, err := ar.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute records >%v<", err)
		return err
	}

	for _, attRec := range attRecs {
		entAttrRec := &record.AppEntityAttributeRecord{
			AppEntityID:    rec.ID,
			AppAttributeID: attRec.ID,
		}
		err := ear.Create(entAttrRec)
		if err != nil {
			l.Warn().Msgf("Failed to create entity attribute record >%v<", err)
			return err
		}
	}

	return nil
}

// UpdateEntityRec -
func (m *Model) UpdateEntityRec(rec *record.AppEntityRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityRec").Logger()

	// repos
	r := m.RepoStore.AppEntityRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityRec -
func (m *Model) DeleteEntityRec(entityID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityRec").Logger()

	// repos
	r := m.RepoStore.AppEntityRepo

	// delete entity group member record if it exists
	entityGroupMemberRec, err := m.GetEntityGroupMemberRec(entityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group member record >%v<", err)
		return err
	}

	if entityGroupMemberRec != nil {
		err := m.DeleteEntityGroupMemberRec(entityGroupMemberRec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to delete entity group record >%v<", err)
			return err
		}
	}

	// delete entity
	err = r.Delete(entityID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity >%v<", err)
		return err
	}

	return nil
}

// ENTITY GROUP

// GetEntityGroupRec -
func (m *Model) GetEntityGroupRec(entityID string) (*record.AppEntityGroupRecord, error) {

	l := m.Logger.With().Str("function", "GetEntityGroupRec").Logger()

	// repos
	egr := m.RepoStore.AppEntityGroupRepo

	entityGroupMemberRec, err := m.GetEntityGroupMemberRec(entityID)
	if err != nil {
		l.Warn().Msgf("Failed to get app entity group member rec >%v<", err)
		return nil, err
	}

	if entityGroupMemberRec == nil {
		// NOTE: not an error, an entity may not currently be a group member
		l.Debug().Msgf("Entity ID >%s< does not currently belong to a group", entityID)
		return nil, nil
	}

	rec, err := egr.GetByID(entityGroupMemberRec.AppEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to get app entity group >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityGroupMemberRec -
func (m *Model) GetEntityGroupMemberRec(entityID string) (*record.AppEntityGroupMemberRecord, error) {

	l := m.Logger.With().Str("function", "GetEntityGroupMemberRec").Logger()

	// repos
	egmr := m.RepoStore.AppEntityGroupMemberRepo

	// get the group this entity is in
	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err := egmr.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get group members >%v<", err)
		return nil, err
	}

	if len(recs) != 1 {
		// NOTE: not an error, an entity may not currently be a group member
		l.Debug().Msgf("Entity ID >%s< does not currently belong to a group", entityID)
		return nil, nil
	}

	return recs[0], nil
}

// DeleteEntityGroupMemberRec -
func (m *Model) DeleteEntityGroupMemberRec(entityGroupMemberID string) error {

	l := m.Logger.With().Str("function", "DeleteEntityGroupMemberRec").Logger()

	// repos
	egmr := m.RepoStore.AppEntityGroupMemberRepo

	err := egmr.Delete(entityGroupMemberID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity group member record >%v<", err)
		return err
	}

	return nil
}

// ATTRIBUTE

// GetEntityAttributeRec -
func (m *Model) GetEntityAttributeRec(entityAttributeID string) (rec *record.AppEntityAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEntityAttributeRepo

	rec, err = r.GetByID(entityAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity attribute >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityAttributeRecs -
func (m *Model) GetEntityAttributeRecs(entityID string) (recs []*record.AppEntityAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityAttributeRecs").Logger()

	// repos
	r := m.RepoStore.AppEntityAttributeRepo

	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed get entity attributes >%v<", err)
		return nil, err
	}

	l.Debug().Msgf("Returning >%d< attribute recs", len(recs))

	return recs, nil
}

// CreateEntityAttributeRec -
func (m *Model) CreateEntityAttributeRec(rec *record.AppEntityAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEntityAttributeRepo
	ar := m.RepoStore.AppAttributeRepo

	// validate
	attrRec, err := ar.GetByID(rec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute record >%v<", err)
		return err
	}

	// only assignable can have a non 0 value
	if attrRec.Assignable == false && rec.Value != 0 {
		l.Warn().Msgf("Only assignable attributes may have a value other than 0 >%v<", rec)
		return fmt.Errorf("Only assignable attributes may have a value other than 0 >%v<", rec)
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity attribute >%v<", err)
		return err
	}

	return nil
}

// UpdateEntityAttributeRec -
func (m *Model) UpdateEntityAttributeRec(rec *record.AppEntityAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEntityAttributeRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity attribute >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityAttributeRec -
func (m *Model) DeleteEntityAttributeRec(entityAttributeID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEntityAttributeRepo

	err = r.Delete(entityAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity attribute >%v<", err)
		return err
	}

	return nil
}

// ITEM

// GetEntityItemRec -
func (m *Model) GetEntityItemRec(entityItemID string) (rec *record.AppEntityItemRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityItemRec").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	rec, err = r.GetByID(entityItemID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity item >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityItemRecs -
func (m *Model) GetEntityItemRecs(entityID string) (recs []*record.AppEntityItemRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityItemRecs").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed get entity items >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEntityItemRec -
func (m *Model) CreateEntityItemRec(rec *record.AppEntityItemRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityItemRec").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity item >%v<", err)
		return err
	}

	return nil
}

// UpdateEntityItemRec -
func (m *Model) UpdateEntityItemRec(rec *record.AppEntityItemRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityItemRec").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity item >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityItemRec -
func (m *Model) DeleteEntityItemRec(entityItemID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityItemRec").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	err = r.Delete(entityItemID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity item >%v<", err)
		return err
	}

	return nil
}

// SKILL

// GetEntitySkillRec -
func (m *Model) GetEntitySkillRec(entitySkillID string) (rec *record.AppEntitySkillRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntitySkillRec").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	rec, err = r.GetByID(entitySkillID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity skill >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntitySkillRecs -
func (m *Model) GetEntitySkillRecs(entityID string) (recs []*record.AppEntitySkillRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntitySkillRecs").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed get entity skills >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEntitySkillRec -
func (m *Model) CreateEntitySkillRec(rec *record.AppEntitySkillRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntitySkillRec").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity skill >%v<", err)
		return err
	}

	return nil
}

// UpdateEntitySkillRec -
func (m *Model) UpdateEntitySkillRec(rec *record.AppEntitySkillRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntitySkillRec").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity skill >%v<", err)
		return err
	}

	return nil
}

// DeleteEntitySkillRec -
func (m *Model) DeleteEntitySkillRec(entitySkillID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntitySkillRec").Logger()

	// repos
	r := m.RepoStore.AppEntitySkillRepo

	err = r.Delete(entitySkillID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity skill >%v<", err)
		return err
	}

	return nil
}

// Tactic

// GetEntityTacticRec -
func (m *Model) GetEntityTacticRec(entityTacticID string) (rec *record.AppEntityTacticRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityTacticRec").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo

	rec, err = r.GetByID(entityTacticID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity tactic record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityTacticRecs -
func (m *Model) GetEntityTacticRecs(entityID string) (recs []*record.AppEntityTacticRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityTacticRecs").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo

	params := make(map[string]interface{})
	params["app_entity_id"] = entityID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed get entity tactic records >%v<", err)
		return nil, err
	}

	// sort by "Order"
	slice.Sort(recs[:], func(i, j int) bool {
		return recs[i].Order < recs[j].Order
	})

	l.Debug().Msgf("Returning >%d< attribute recs", len(recs))

	return recs, nil
}

// CreateEntityTacticRec -
func (m *Model) CreateEntityTacticRec(rec *record.AppEntityTacticRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityTacticRec").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo
	ar := m.RepoStore.AppAttributeRepo
	esr := m.RepoStore.AppEntitySkillRepo
	eir := m.RepoStore.AppEntityItemRepo

	// validate attribute
	_, err = ar.GetByID(rec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute record >%v<", err)
		return err
	}

	// validate skill
	if rec.AppSkillID.Valid {
		recs, err := esr.GetByParam(map[string]interface{}{
			"app_entity_id": rec.AppEntityID,
			"app_skill_id":  rec.AppSkillID.String,
		})
		if err != nil {
			l.Warn().Msgf("Failed to get skill record >%v<", err)
			return err
		}
		if len(recs) != 1 {
			l.Warn().Msgf("Entity does not have skill ID >%s<", rec.AppSkillID.String)
			return fmt.Errorf("Entity does not have skill ID >%s<", rec.AppSkillID.String)
		}
	}

	// validate item
	if rec.AppItemID.Valid {
		recs, err := eir.GetByParam(map[string]interface{}{
			"app_entity_id": rec.AppEntityID,
			"app_item_id":   rec.AppItemID.String,
		})
		if err != nil {
			l.Warn().Msgf("Failed to get item record >%v<", err)
			return err
		}
		if len(recs) != 1 {
			l.Warn().Msgf("Entity does not have item ID >%s<", rec.AppItemID.String)
			return fmt.Errorf("Entity does not have item ID >%s<", rec.AppItemID.String)
		}
	}

	rec.Status = record.EntityTacticStatusValid

	// set record order
	err = m.setEntityTacticRecOrder(rec)
	if err != nil {
		l.Warn().Msgf("Failed to set record order >%v<", err)
		return err
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity tactic >%v<", err)
		return err
	}

	return nil
}

// UpdateEntityTacticRec -
func (m *Model) UpdateEntityTacticRec(rec *record.AppEntityTacticRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityTacticRec").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo
	ar := m.RepoStore.AppAttributeRepo
	esr := m.RepoStore.AppEntitySkillRepo
	eir := m.RepoStore.AppEntityItemRepo

	// validate attribute
	_, err = ar.GetByID(rec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute record >%v<", err)
		return err
	}

	// validate skill
	if rec.AppSkillID.Valid {
		recs, err := esr.GetByParam(map[string]interface{}{
			"app_entity_id": rec.AppEntityID,
			"app_skill_id":  rec.AppSkillID.String,
		})
		if err != nil {
			l.Warn().Msgf("Failed to get skill record >%v<", err)
			return err
		}
		if len(recs) != 1 {
			l.Warn().Msgf("Entity does not have skill ID >%s<", rec.AppSkillID.String)
			return fmt.Errorf("Entity does not have skill ID >%s<", rec.AppSkillID.String)
		}
	}

	// validate item
	if rec.AppItemID.Valid {
		recs, err := eir.GetByParam(map[string]interface{}{
			"app_entity_id": rec.AppEntityID,
			"app_item_id":   rec.AppItemID.String,
		})
		if err != nil {
			l.Warn().Msgf("Failed to get item record >%v<", err)
			return err
		}
		if len(recs) != 1 {
			l.Warn().Msgf("Entity does not have item ID >%s<", rec.AppItemID.String)
			return fmt.Errorf("Entity does not have item ID >%s<", rec.AppItemID.String)
		}
	}

	// set record order
	err = m.setEntityTacticRecOrder(rec)
	if err != nil {
		l.Warn().Msgf("Failed to set record order >%v<", err)
		return err
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity tactic >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityTacticRec -
func (m *Model) DeleteEntityTacticRec(entityTacticID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityTacticRec").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo

	err = r.Delete(entityTacticID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity tactic >%v<", err)
		return err
	}

	return nil
}

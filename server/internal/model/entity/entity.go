package entity

import (
	"github.com/bradfitz/slice"
	"github.com/rs/zerolog"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "entity"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	return m, nil
}

func (m *Model) setEntityTacticRecOrder(rec *record.AppEntityTacticRecord) error {

	l := m.Logger.With().Str("function", "CreateEntityTacticRec").Logger()

	// repos
	r := m.RepoStore.AppEntityTacticRepo

	recs, err := m.GetEntityTacticRecs(rec.AppEntityID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity tactic records >%v<", err)
		return err
	}

	// sort by order
	slice.Sort(recs[:], func(i, j int) bool {
		return recs[i].Order < recs[j].Order
	})

	// verify proposed record order is valid and adjust to last in order if it isn't
	if rec.Order <= 0 ||
		rec.Order > recs[len(recs)-1].Order+1 {
		l.Warn().Msgf("Setting tactic rec >%+v< order to >%d<", rec, recs[len(recs)-1].Order+1)
		rec.Order = recs[len(recs)-1].Order + 1
	}

	// when the provided record has no ID we are inserting so
	// need to increment all existing records from the provided
	// record order index
	if rec.ID == "" {

		// increment all record orders that fall above provided new record order
		for _, eRec := range recs {
			if eRec.Order >= rec.Order {
				eRec.Order++
				err := r.Update(eRec)
				if err != nil {
					l.Warn().Msgf("Failed to update tactic record >%v<", err)
					return err
				}
			}
		}

		// all done
		return nil
	}

	// when the provided record already exists we need to move
	// the record from one order position to another
	cOrder := 0
	for _, eRec := range recs {
		if eRec.ID == rec.ID {
			cOrder = eRec.Order
			break
		}
	}

	// moving record down in order
	// - if x <= (new order) decrement && > (current order) decrement
	if cOrder < rec.Order {
		for _, eRec := range recs {
			// skip record we are moving
			if eRec.ID == rec.ID {
				continue
			}
			if eRec.Order <= rec.Order && eRec.Order > cOrder {
				eRec.Order--
				err := r.Update(eRec)
				if err != nil {
					l.Warn().Msgf("Failed to update tactic record >%v<", err)
					return err
				}
			}
		}
	}

	// moving record up in order
	//  - if x >= (new order) && < (current order) increment
	if cOrder > rec.Order {
		for _, eRec := range recs {
			// skip record we are moving
			if eRec.ID == rec.ID {
				continue
			}
			if eRec.Order >= rec.Order && eRec.Order < cOrder {
				eRec.Order++
				err := r.Update(eRec)
				if err != nil {
					l.Warn().Msgf("Failed to update tactic record >%v<", err)
					return err
				}
			}
		}
	}

	return nil
}

package entity

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		// remove app test data
		td.RemoveAppData(tx)

		tx.Commit()
	}

	return td, teardown
}

func TestNewEntity(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	assert.NoError(t, err, "NewModel returns without error")
	assert.NotNil(t, m, "NewModel returns a new model")

	tx.Rollback()
}

func TestGetEntityRec(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// test data
	entityID := td.AppEntityRecs[0].ID

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, _ := NewModel(e, l, rs)

	rec, err := m.GetEntityRec(entityID)
	if assert.NoError(t, err, "GetEntityRec returns without error") {
		assert.NotNil(t, rec, "GetEntityRec returns an entity record")
	}

	tx.Rollback()
}

func TestGetEntityRecs(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// test data
	appID := td.AppRec.ID

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, _ := NewModel(e, l, rs)

	params := make(map[string]interface{})
	params["app_id"] = appID

	recs, err := m.GetEntityRecs(params)
	if assert.NoError(t, err, "GetEntityRecs returns without error") {
		assert.NotEmpty(t, recs, "GetEntityRecs returns entity records")
	}

	tx.Rollback()
}

func TestGetEntityGroupRec(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// test data
	entityID := td.AppEntityRecs[0].ID

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, _ := NewModel(e, l, rs)

	rec, err := m.GetEntityGroupRec(entityID)
	if assert.NoError(t, err, "GetEntityGroupRec returns without error") {
		assert.NotNil(t, rec, "GetEntityGroupRec returns an entity record")
	}

	tx.Rollback()
}

func TestGetEntityAttributeRecs(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// test data
	entityID := td.AppEntityRecs[0].ID

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, _ := NewModel(e, l, rs)

	recs, err := m.GetEntityAttributeRecs(entityID)
	if assert.NoError(t, err, "GetEntityAttributeRecs returns without error") {
		assert.NotEmpty(t, recs, "GetEntityAttributeRecs returns an entity record")
	}

	tx.Rollback()
}

func TestGetEntityAttributeRec(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	// test data
	entityID := td.AppEntityRecs[0].ID

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, _ := NewModel(e, l, rs)

	// get all records
	recs, _ := m.GetEntityAttributeRecs(entityID)

	// get single record
	rec, err := m.GetEntityAttributeRec(recs[0].ID)
	if assert.NoError(t, err, "GetEntityAttributeRec returns without error") {
		assert.NotEmpty(t, rec, "GetEntityAttributeRec returns an entity record")
	}

	tx.Rollback()
}

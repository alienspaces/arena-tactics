package fight

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "fight"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	return m, nil
}

// GetFightInstanceData - unmarshal state data from the entity instance record
func (m *Model) GetFightInstanceData(rec *record.AppFightInstanceRecord) (data *instance.FightData, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceData").Logger()

	data = &instance.FightData{}
	err = json.Unmarshal(rec.Data, &data)
	if err != nil {
		l.Warn().Msgf("Failed unmarshalling data >%v<", err)
		return nil, err
	}

	return data, nil
}

// SetFightInstanceData - marshals new state data assigning to the app instance record
func (m *Model) SetFightInstanceData(rec *record.AppFightInstanceRecord, data *instance.FightData) (err error) {

	l := m.Logger.With().Str("function", "SetFightInstanceData").Logger()

	rec.Data, err = json.Marshal(&data)
	if err != nil {
		l.Warn().Msgf("Failed marshalling data >%v<", err)
		return err
	}

	return nil
}

// GetFightInstanceTurnData - unmarshal state data from the entity instance record
func (m *Model) GetFightInstanceTurnData(rec *record.AppFightInstanceTurnRecord) (data *instance.FightData, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceTurnData").Logger()

	data = &instance.FightData{}
	err = json.Unmarshal(rec.Data, &data)
	if err != nil {
		l.Warn().Msgf("Failed unmarshalling data >%v<", err)
		return nil, err
	}

	return data, nil
}

// SetFightInstanceTurnData - marshals new state data assigning to the app instance record
func (m *Model) SetFightInstanceTurnData(rec *record.AppFightInstanceTurnRecord, data *instance.FightData) (err error) {

	l := m.Logger.With().Str("function", "SetFightInstanceTurnData").Logger()

	rec.Data, err = json.Marshal(&data)
	if err != nil {
		l.Warn().Msgf("Failed marshalling data >%v<", err)
		return err
	}

	return nil
}

// ClearFightInstanceEvents -
func (m *Model) ClearFightInstanceEvents(rec *record.AppFightInstanceRecord) error {

	l := m.Logger.With().Str("function", "ClearFightInstanceEvents").Logger()

	data, err := m.GetFightInstanceData(rec)
	if err != nil {
		l.Warn().Msgf("Failed getting fight instance data >%v<", err)
		return err
	}

	data.TacticEvents = []*instance.TacticEvent{}

	err = m.SetFightInstanceData(rec, data)
	if err != nil {
		l.Warn().Msgf("Failed setting fight instance data >%v<", err)
		return err
	}

	return nil
}

// CanModifyFight - check rules to see whether the group can be removed
// or group members can be added or removed
func (m *Model) CanModifyFight(fightID string) (bool, error) {

	l := m.Logger.With().Str("function", "CanModifyFight").Logger()

	// RULES:
	// - Cannot modify a fight that has a status of starting or fighting

	fr := m.RepoStore.AppFightRepo

	fRec, err := fr.GetByID(fightID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight >%v<", err)
		return false, err
	}

	if fRec.Status == record.AppFightStatusStarting || fRec.Status == record.AppFightStatusFighting {
		l.Warn().Msgf("Fight is starting or fighting, cannot modify fight")
		return false, fmt.Errorf("Fight is starting or fighting, cannot modify fight")
	}

	return true, nil
}

// CanAddGroup - check rules to see whether a group can be added
func (m *Model) CanAddGroup(entityGroupID string) (bool, error) {

	l := m.Logger.With().Str("function", "CanAddGroup").Logger()

	// RULES:
	// - Cannot add a group to a fight if the group has no members

	gmr := m.RepoStore.AppEntityGroupMemberRepo

	params := make(map[string]interface{})
	params["app_entity_group_id"] = entityGroupID

	gmRecs, err := gmr.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group members >%v<", err)
		return false, err
	}

	if len(gmRecs) == 0 {
		l.Warn().Msgf("Entity group has no members, cannot add to fight")
		return false, fmt.Errorf("Entity group has no members, cannot add to fight")
	}

	return true, nil
}

// CanCreateFightInstance - check rules to see whether a new fight instance can be created
func (m *Model) CanCreateFightInstance(fightID string) (bool, error) {

	l := m.Logger.With().Str("function", "CanCreateFightInstance").Logger()

	// RULES:
	// - Cannot create a new fight instance if there is already a `fighting` instance

	fir := m.RepoStore.AppFightInstanceRepo

	params := make(map[string]interface{})
	params["app_fight_id"] = fightID
	params["status"] = record.AppFightInstanceStatusFighting

	fiRecs, err := fir.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance records >%v<", err)
		return false, err
	}

	if len(fiRecs) != 0 {
		l.Warn().Msgf("Fight instance fighting, cannot create a new fight instance")
		return false, fmt.Errorf("Fight instance fighting, cannot create a new fight instance")
	}

	return true, nil
}

func (m *Model) addInstanceTurnNarrative(rec *record.AppFightInstanceTurnRecord) error {

	l := m.Logger.With().Str("function", "addInstanceTurnNarrative").Logger()

	// repos
	iRepo := m.RepoStore.AppItemRepo
	sRepo := m.RepoStore.AppSkillRepo
	eRepo := m.RepoStore.AppEffectRepo

	data, err := m.GetFightInstanceTurnData(rec)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance data >%v<", err)
		return err
	}

	// add tactic event narrative
	for _, tacticEvent := range data.TacticEvents {

		// skill
		if tacticEvent.Action == record.EntityTacticActionUseSkill && tacticEvent.AppSkillID != "" {
			sRec, err := sRepo.GetByID(tacticEvent.AppSkillID)
			if err != nil {
				l.Warn().Msgf("Failed to get skill record >%v<", err)
				return err
			}
			tacticEvent.NarrativeText = sRec.NarrativeText
		}

		// item
		if tacticEvent.Action == record.EntityTacticActionUseItem && tacticEvent.AppItemID != "" {
			iRec, err := iRepo.GetByID(tacticEvent.AppItemID)
			if err != nil {
				l.Warn().Msgf("Failed to get item record >%v<", err)
				return err
			}
			tacticEvent.NarrativeText = iRec.NarrativeText
		}

		// add tactic event effect events narrative
		for _, effectEvent := range tacticEvent.EffectEvents {
			l.Warn().Msgf("Adding tactic event / effect event narrative >%+v<", effectEvent)
			eRec, err := eRepo.GetByID(effectEvent.AppEffectID)
			if err != nil {
				l.Warn().Msgf("Failed to get tactic event / effect event record >%v<", err)
				return err
			}
			effectEvent.NarrativeText = eRec.NarrativeText
		}
	}

	// add effect event narrative
	for _, effectEvent := range data.EffectEvents {
		eRec, err := eRepo.GetByID(effectEvent.AppEffectID)
		if err != nil {
			l.Warn().Msgf("Failed to get effect event record >%v<", err)
			return err
		}
		effectEvent.NarrativeText = eRec.NarrativeText
	}

	err = m.SetFightInstanceTurnData(rec, data)
	if err != nil {
		l.Warn().Msgf("Failed to set fight instance data >%v<", err)
		return err
	}

	return nil
}

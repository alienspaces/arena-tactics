package fight

import (
	"encoding/json"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/instance"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetFightRec -
func (m *Model) GetFightRec(fightID string) (rec *record.AppFightRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightRec").Logger()

	r := m.RepoStore.AppFightRepo

	rec, err = r.GetByID(fightID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetFightRecs -
func (m *Model) GetFightRecs(params map[string]interface{}) (recs []*record.AppFightRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightRecs").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight records by param >%v<", err)
		return nil, err
	}

	return recs, nil
}

// GetFightingFightRecs -
func (m *Model) GetFightingFightRecs(appID string) (recs []*record.AppFightRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightingFightRecs").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	params := make(map[string]interface{})
	params["app_id"] = appID
	params["status"] = record.AppFightStatusFighting

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// GetStartingFightRecs -
func (m *Model) GetStartingFightRecs(appID string) (recs []*record.AppFightRecord, err error) {

	l := m.Logger.With().Str("function", "GetStartingFightRecs").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	params := make(map[string]interface{})
	params["app_id"] = appID
	params["status"] = record.AppFightStatusStarting

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateFightRec -
func (m *Model) CreateFightRec(rec *record.AppFightRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateFightRec").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create fight record >%v<", err)
		return err
	}

	return nil
}

// UpdateFightRec -
func (m *Model) UpdateFightRec(rec *record.AppFightRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateFightRec").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	// TODO: Maybe rules here around fight status progression

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update fight record >%v<", err)
		return err
	}

	return nil
}

// DeleteFightRec -
func (m *Model) DeleteFightRec(fightID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteFightRec").Logger()

	// repos
	r := m.RepoStore.AppFightRepo

	// can modify fight
	canModify, err := m.CanModifyFight(fightID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify fight >%v<", err)
		return err
	}

	err = r.Delete(fightID)
	if err != nil {
		l.Warn().Msgf("Failed to delete fight record >%v<", err)
		return err
	}

	return nil
}

// FIGHT INSTANCE

// GetFightInstanceRec -
func (m *Model) GetFightInstanceRec(fightInstanceID string) (rec *record.AppFightInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceRec").Logger()

	r := m.RepoStore.AppFightInstanceRepo

	rec, err = r.GetByID(fightInstanceID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetFightInstanceRecs -
func (m *Model) GetFightInstanceRecs(params map[string]interface{}) (recs []*record.AppFightInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceRecs").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance records by param >%v<", err)
		return nil, err
	}

	return recs, nil
}

// GetFightingFightInstanceRec -
func (m *Model) GetFightingFightInstanceRec(fightID string) (rec *record.AppFightInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceRepo

	params := make(map[string]interface{})
	params["app_fight_id"] = fightID
	params["status"] = record.AppFightInstanceStatusFighting

	recs, err := r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance record >%v<", err)
		return nil, err
	}

	// not an error
	if len(recs) != 1 {
		l.Debug().Msgf("Fetched %d records", len(recs))
		return nil, nil
	}

	rec = recs[0]

	return rec, nil
}

// CreateFightInstanceRec -
func (m *Model) CreateFightInstanceRec(fightID string) (rec *record.AppFightInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "CreateFightInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceRepo

	rec = r.NewRecord()
	rec.AppFightID = fightID
	rec.Status = record.AppFightInstanceStatusFighting

	// can only create a new fight instance if all existing
	// fight instances are 'done'
	// can modify fight
	canCreate, err := m.CanCreateFightInstance(fightID)
	if canCreate == false || err != nil {
		l.Warn().Msgf("Cannot create fight instance >%v<", err)
		return nil, err
	}

	// data
	data := instance.FightData{
		AppFightID: fightID,
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		l.Warn().Msgf("Failed marshalling json data >%v<", err)
		return nil, err
	}

	rec.Data = jsonData

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create fight instance record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// UpdateFightInstanceRec - updates the current fight instance record and also
// inserts a fight instance turn record for fight playback
func (m *Model) UpdateFightInstanceRec(rec *record.AppFightInstanceRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateFightInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update fight instance record >%v<", err)
		return err
	}

	return nil
}

// RemoveFightInstanceRec -
func (m *Model) RemoveFightInstanceRec(fightInstanceID string) (err error) {

	l := m.Logger.With().Str("function", "RemoveFightInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceRepo
	hr := m.RepoStore.AppFightInstanceTurnRepo

	l.Debug().Msgf("Removing record ID >%s<", fightInstanceID)

	// remove all fight instance turn records first
	params := make(map[string]interface{})
	params["app_fight_instance_id"] = fightInstanceID

	turnRecs, err := hr.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get turn records >%v<", err)
		return err
	}

	for _, turnRec := range turnRecs {
		err := hr.Remove(turnRec.ID)
		if err != nil {
			l.Warn().Msgf("Failed to remove fight instance turn record >%v<", err)
			return err
		}
	}

	// remove fight instance record
	err = r.Remove(fightInstanceID)
	if err != nil {
		l.Warn().Msgf("Failed to remove fight instance record >%v<", err)
		return err
	}

	return nil
}

// FIGHT INSTANCE TURNS

// CreateFightInstanceTurnRec -
func (m *Model) CreateFightInstanceTurnRec(rec *record.AppFightInstanceTurnRecord) error {

	l := m.Logger.With().Str("function", "GetFightInstanceTurnRec").Logger()

	// repos
	hr := m.RepoStore.AppFightInstanceTurnRepo

	err := hr.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create fight instance turn record >%v<", err)
		return err
	}

	return nil
}

// GetFightInstanceTurnRec -
func (m *Model) GetFightInstanceTurnRec(fightInstanceTurnID string) (rec *record.AppFightInstanceTurnRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceTurnRec").Logger()

	r := m.RepoStore.AppFightInstanceTurnRepo

	rec, err = r.GetByID(fightInstanceTurnID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance turn record >%v<", err)
		return nil, err
	}

	// fill in related narrative data for all items, skills and effects
	err = m.addInstanceTurnNarrative(rec)
	if err != nil {
		l.Warn().Msgf("Failed to add instance turn narrative >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetFightInstanceTurnRecs -
func (m *Model) GetFightInstanceTurnRecs(params map[string]interface{}) (recs []*record.AppFightInstanceTurnRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightInstanceTurnRecs").Logger()

	// repos
	r := m.RepoStore.AppFightInstanceTurnRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight instance turn records >%v<", err)
		return nil, err
	}

	// fill in related narrative data for all items, skills and effects
	for _, rec := range recs {
		err := m.addInstanceTurnNarrative(rec)
		if err != nil {
			l.Warn().Msgf("Failed to add instance turn narrative >%v<", err)
			return nil, err
		}
	}

	return recs, nil
}

// ENTITY GROUPS

// GetFightEntityGroupRec -
func (m *Model) GetFightEntityGroupRec(fightEntityGroupID string) (rec *record.AppFightEntityGroupRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightEntityGroupRec").Logger()

	r := m.RepoStore.AppFightEntityGroupRepo

	rec, err = r.GetByID(fightEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight entity group >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetFightEntityGroupRecs -
func (m *Model) GetFightEntityGroupRecs(fightID string) (recs []*record.AppFightEntityGroupRecord, err error) {

	l := m.Logger.With().Str("function", "GetFightEntityGroupRecs").Logger()

	// repos
	r := m.RepoStore.AppFightEntityGroupRepo

	recs, err = r.GetByParam(map[string]interface{}{"app_fight_id": fightID})
	if err != nil {
		l.Warn().Msgf("Failed to by param >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateFightEntityGroupRec -
func (m *Model) CreateFightEntityGroupRec(rec *record.AppFightEntityGroupRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateFightEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppFightEntityGroupRepo

	// can modify fight
	canModify, err := m.CanModifyFight(rec.AppFightID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify fight >%v<", err)
		return err
	}

	canAddGroup, err := m.CanAddGroup(rec.AppEntityGroupID)
	if canAddGroup == false || err != nil {
		l.Warn().Msgf("Cannot add group to fight >%v<", err)
		return err
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateFightEntityGroupRec -
func (m *Model) UpdateFightEntityGroupRec(rec *record.AppFightEntityGroupRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateFightEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppFightEntityGroupRepo

	// can modify fight
	canModify, err := m.CanModifyFight(rec.AppFightID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify fight >%v<", err)
		return err
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update >%v<", err)
		return err
	}

	return nil
}

// DeleteFightEntityGroupRec -
func (m *Model) DeleteFightEntityGroupRec(fightEntityGroupID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteFightEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppFightEntityGroupRepo

	fegRec, err := r.GetByID(fightEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight entity group >%v<", err)
		return err
	}

	// can modify fight
	canModify, err := m.CanModifyFight(fegRec.AppFightID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify fight >%v<", err)
		return err
	}

	err = r.Delete(fightEntityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

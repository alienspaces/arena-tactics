package app

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx %v", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data %v", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data %v", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx %v", err)
		}

		// remove app test data
		td.RemoveAppData(tx)

		tx.Commit()
	}

	return td, teardown
}

func TestNewApp(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	rs, _ := repostore.NewRepoStore(e, l, nil)

	a, err := NewModel(e, l, rs)

	assert.NoError(t, err, "NewApp new app returns without error")

	assert.NotNil(t, a, "NewApp new app returns a new app")
}

func TestAppInstanceData(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	// new app test record
	r := rs.AppRepo
	ar, _ := r.CreateTestRecord()

	a, err := NewModel(e, l, rs)
	assert.NoError(t, err, "NewModel returns without error")

	if assert.NotNil(t, a, "NewModel returns a new app") {

		rec, err := a.CreateAppInstanceRec(ar.ID)
		assert.NoError(t, err, "CreateAppInstanceRec returns without error")

		if assert.NotNil(t, rec, "CreateAppInstanceRec record is not nil") {

			rec, err := a.GetAppInstanceRec(ar.ID)

			assert.NoError(t, err, "GetAppInstanceRec returns without error")
			assert.NotNil(t, rec, "GetAppInstanceRec record is not nil")
		}
	}

	tx.Rollback()
}

func TestGetInitiativeValue(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)
	assert.NoError(t, err, "NewModel returns without error")

	if assert.NotNil(t, m, "NewModel returns a new app") {

		// test initiative formula params
		formulaParams, err := m.GetInitiativeFormulaParams(td.AppRec)
		if assert.NoError(t, err, "GetInitiativeFormulaParams returns without error") {
			t.Logf("Initiative formula params >%v<", formulaParams)
			assert.NotEmpty(t, formulaParams, "GetInitiativeFormulaParams value is not empty")
		}

		// test initiative value
		params := make(map[string]int)
		for _, attr := range td.AppAttributeRecs {
			params[attr.Name] = 10
		}

		value, err := m.GetInitiativeValue(td.AppRec, params)
		if assert.NoError(t, err, "GetInitiativeValue returns without error") {
			t.Logf("Initiative value >%d<", value)
			assert.NotNil(t, value, "GetInitiativeValue value is not nil")
		}
	}

	tx.Rollback()
}

func TestGetDeathValue(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)
	assert.NoError(t, err, "NewModel returns without error")

	if assert.NotNil(t, m, "NewModel returns a new app") {

		// test death formula params
		formulaParams, err := m.GetDeathFormulaParams(td.AppRec)
		if assert.NoError(t, err, "GetDeathFormulaParams returns without error") {
			t.Logf("Death formula params >%v<", formulaParams)
			assert.NotEmpty(t, formulaParams, "GetDeathFormulaParams value is not empty")
		}

		// test expected death false value
		params := make(map[string]int)
		for _, attr := range td.AppAttributeRecs {
			params[attr.Name] = 10
		}

		value, err := m.GetDeathValue(td.AppRec, params)
		if assert.NoError(t, err, "GetDeathValue returns without error") {
			t.Logf("Death value >%t<", value)
			assert.Equal(t, false, value, "GetDeathValue value is false")
		}

		// test expected death true value
		for _, attr := range td.AppAttributeRecs {
			params[attr.Name] = 0
		}

		value, err = m.GetDeathValue(td.AppRec, params)
		if assert.NoError(t, err, "GetDeathValue returns without error") {
			t.Logf("Death value >%t<", value)
			assert.Equal(t, true, value, "GetDeathValue value is true")
		}

	}

	tx.Rollback()
}

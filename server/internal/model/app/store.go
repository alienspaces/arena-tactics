package app

import (
	"encoding/json"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetAppRec -
func (m *Model) GetAppRec(appID string) (rec *record.AppRecord, err error) {

	l := m.Logger.With().Str("function", "GetAppRec").Logger()

	// repos
	r := m.RepoStore.AppRepo

	rec, err = r.GetByID(appID)
	if err != nil {
		l.Warn().Msgf("Failed to get app record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetAppRecs -
func (m *Model) GetAppRecs(params map[string]interface{}) (recs []*record.AppRecord, err error) {

	l := m.Logger.With().Str("function", "GetAppRecs").Logger()

	// repos
	r := m.RepoStore.AppRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get app records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateAppRec -
func (m *Model) CreateAppRec(rec *record.AppRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateAppRec").Logger()

	// repos
	r := m.RepoStore.AppRepo

	// apply required values
	err = m.applyCreateAppValues(rec)
	if err != nil {
		l.Warn().Msgf("Failed to apply app defaults >%v<", err)
		return err
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// applyCreateAppValues - Applies required values for a new app
func (m *Model) applyCreateAppValues(rec *record.AppRecord) (err error) {

	l := m.Logger.With().Str("function", "ApplyCreateAppValues").Logger()

	l.Info().Msgf("Applying defaults")

	//
	rec.Status = record.AppStatusStopped

	return nil
}

// UpdateAppRec -
func (m *Model) UpdateAppRec(rec *record.AppRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateAppRec").Logger()

	// repos
	r := m.RepoStore.AppRepo

	currentRec, err := m.GetAppRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.Name == "" {
		rec.Name = currentRec.Name
	}
	if rec.Description == "" {
		rec.Description = currentRec.Description
	}
	if rec.DeathFormula == "" {
		rec.DeathFormula = currentRec.DeathFormula
	}
	if rec.Status == "" {
		rec.Status = currentRec.Status
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update app record >%v<", err)
		return err
	}

	return nil
}

// DeleteAppRec -
func (m *Model) DeleteAppRec(appID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteAppRec").Logger()

	// repos
	r := m.RepoStore.AppRepo

	err = r.Delete(appID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

// CreateAppInstanceRec -
func (m *Model) CreateAppInstanceRec(appID string) (rec *record.AppInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "CreateAppInstanceRec").Logger()

	// repo
	r := m.RepoStore.AppInstanceRepo

	rec = r.NewRecord()
	rec.AppID = appID

	// data
	data := record.AppInstanceData{
		AppID: appID,
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		l.Error().Msgf("Failed marshalling json data >%v<", err)
		return nil, err
	}

	rec.Data = jsonData

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetAppInstanceRec -
func (m *Model) GetAppInstanceRec(appID string) (rec *record.AppInstanceRecord, err error) {

	l := m.Logger.With().Str("function", "GetAppInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppInstanceRepo

	params := make(map[string]interface{})
	params["app_id"] = appID

	recs, err := r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get app instance >%v<", err)
		return nil, err
	}

	// not an error
	if len(recs) != 1 {
		l.Warn().Msgf("Fetched %d records", len(recs))
		return nil, nil
	}

	rec = recs[0]

	return rec, nil
}

// UpdateAppInstanceRec -
func (m *Model) UpdateAppInstanceRec(rec *record.AppInstanceRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateAppInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppInstanceRepo

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update >%v<", err)
		return err
	}

	return nil
}

// RemoveAppInstanceRec -
func (m *Model) RemoveAppInstanceRec(appInstanceID string) (err error) {

	l := m.Logger.With().Str("function", "RemoveAppInstanceRec").Logger()

	// repos
	r := m.RepoStore.AppInstanceRepo

	l.Info().Msgf("Removing record ID >%s<", appInstanceID)

	err = r.Remove(appInstanceID)
	if err != nil {
		l.Warn().Msgf("Failed removing record >%v<", err)
		return err
	}

	return nil
}

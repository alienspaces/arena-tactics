package app

import (
	"encoding/json"

	"github.com/Knetic/govaluate"
	"github.com/rs/zerolog"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "app"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, rs *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: rs,
	}

	return m, nil
}

// GetInitiativeFormulaParams - returns array of parameter names from the
// formula that are required to calculate initiative
func (m *Model) GetInitiativeFormulaParams(rec *record.AppRecord) (params []string, err error) {

	l := m.Logger.With().Str("function", "GetInitiativeFormulaParams").Logger()

	params, err = getParams(rec.InitiativeFormula)

	l.Debug().Msgf("Initiative formula params >%v<", params)

	return params, nil
}

// GetDeathFormulaParams - returns array of parameter names from the
// formula that are required to calculate initiative
func (m *Model) GetDeathFormulaParams(rec *record.AppRecord) (params []string, err error) {

	l := m.Logger.With().Str("function", "GetDeathFormulaParams").Logger()

	params, err = getParams(rec.DeathFormula)

	l.Debug().Msgf("Death formula params >%v<", params)

	return params, nil
}

// GetInitiativeValue -
func (m *Model) GetInitiativeValue(rec *record.AppRecord, params map[string]int) (value int, err error) {

	l := m.Logger.With().Str("function", "GetInitiativeValue").Logger()

	expression, err := govaluate.NewEvaluableExpression(rec.InitiativeFormula)
	if err != nil {
		l.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return 0, err
	}

	evalParams := make(map[string]interface{})
	for pName, pValue := range params {
		evalParams[pName] = pValue
	}

	result, err := expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate >%v<", err)
		return 0, err
	}

	value = int(result.(float64))

	l.Debug().Msgf("Application name >%s< initiative value >%d<", rec.Name, value)

	return value, nil
}

// GetDeathValue -
func (m *Model) GetDeathValue(rec *record.AppRecord, params map[string]int) (value bool, err error) {

	l := m.Logger.With().Str("function", "GetDeathValue").Logger()

	expression, err := govaluate.NewEvaluableExpression(rec.DeathFormula)
	if err != nil {
		l.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return false, err
	}

	evalParams := make(map[string]interface{})
	for pName, pValue := range params {
		evalParams[pName] = pValue
	}

	result, err := expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate >%v<", err)
		return false, err
	}

	value = result.(bool)

	l.Debug().Msgf("Death value >%t<", value)

	return value, nil
}

// GetInstanceData - unmarshal state data from the app instance record
func (m *Model) GetInstanceData(rec *record.AppInstanceRecord) (data *record.AppInstanceData, err error) {

	l := m.Logger.With().Str("function", "GetInstanceData").Logger()

	data = &record.AppInstanceData{}
	err = json.Unmarshal(rec.Data, &data)
	if err != nil {
		l.Warn().Msgf("Failed unmarshalling data >%v<", err)
		return nil, err
	}

	return data, nil
}

// SetAppInstanceData - marshal new state data assigning to the app instance record
func (m *Model) SetAppInstanceData(rec *record.AppInstanceRecord, data *record.AppInstanceData) (err error) {

	l := m.Logger.With().Str("function", "SetAppInstanceData").Logger()

	rec.Data, err = json.Marshal(&data)
	if err != nil {
		l.Warn().Msgf("Failed marshalling data >%v<", err)
		return err
	}

	return nil
}

func getParams(formula string) (params []string, err error) {

	expression, err := govaluate.NewEvaluableExpression(formula)
	if err != nil {
		return nil, err
	}

	tokens := expression.Tokens()
	for _, token := range tokens {
		if token.Kind == govaluate.VARIABLE {
			params = append(params, token.Value.(string))
		}
	}

	return params, nil
}

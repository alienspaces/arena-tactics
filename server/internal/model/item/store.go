package item

import (
	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetItemRec -
func (m *Model) GetItemRec(itemID string) (rec *record.AppItemRecord, err error) {

	l := m.Logger.With().Str("function", "GetItemRec").Logger()

	// repos
	r := m.RepoStore.AppItemRepo

	rec, err = r.GetByID(itemID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetItemRecs -
func (m *Model) GetItemRecs(params map[string]interface{}) (recs []*record.AppItemRecord, err error) {

	l := m.Logger.With().Str("function", "GetItemRecs").Logger()

	// repos
	r := m.RepoStore.AppItemRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateItemRec -
func (m *Model) CreateItemRec(rec *record.AppItemRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateItemRec").Logger()

	// repos
	r := m.RepoStore.AppItemRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateItemRec -
func (m *Model) UpdateItemRec(rec *record.AppItemRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateItemRec").Logger()

	// repos
	r := m.RepoStore.AppItemRepo

	currentRec, err := m.GetItemRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.Name == "" {
		rec.Name = currentRec.Name
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteItemRec -
func (m *Model) DeleteItemRec(appItemID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteItemRec").Logger()

	// repos
	r := m.RepoStore.AppItemRepo
	er := m.RepoStore.AppEntityRepo
	eir := m.RepoStore.AppEntityItemRepo

	itemRec, err := r.GetByID(appItemID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return err
	}

	err = r.Delete(appItemID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	// delete item from all entities
	params := make(map[string]interface{})
	params["app_id"] = itemRec.AppID

	entRecs, err := er.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity records >%v<", err)
		return err
	}

	for _, entRec := range entRecs {
		params = make(map[string]interface{})
		params["app_item_id"] = appItemID
		params["app_entity_id"] = entRec.ID

		entItemRecs, err := eir.GetByParam(params)
		if err != nil {
			l.Warn().Msgf("Failed to get entity item records >%v<", err)
			return err
		}

		if len(entItemRecs) == 0 {
			l.Info().Msgf("Entity ID >%s< does not have item ID >%s<", entRec.ID, appItemID)
			continue
		}

		err = eir.Delete(entItemRecs[0].ID)
		if err != nil {
			l.Warn().Msgf("Failed to delete entity item record >%v<", err)
			return err
		}
	}

	return nil
}

// GetEntityItemRec -
func (m *Model) GetEntityItemRec(entityItemID string) (rec *record.AppEntityItemRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityItemRec").Logger()

	// repos
	r := m.RepoStore.AppEntityItemRepo

	rec, err = r.GetByID(entityItemID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetItemEffectRec -
func (m *Model) GetItemEffectRec(itemEffectID string) (rec *record.AppItemEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetItemEffectRecs").Logger()

	// repos
	r := m.RepoStore.AppItemEffectRepo

	rec, err = r.GetByID(itemEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetItemEffectRecs -
func (m *Model) GetItemEffectRecs(itemID string) (recs []*record.AppItemEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetItemEffectRecs").Logger()

	// repos
	r := m.RepoStore.AppItemEffectRepo

	params := make(map[string]interface{})
	params["app_item_id"] = itemID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateItemEffectRec -
func (m *Model) CreateItemEffectRec(rec *record.AppItemEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateItemEffectRec").Logger()

	// repos
	r := m.RepoStore.AppItemEffectRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateItemEffectRec -
func (m *Model) UpdateItemEffectRec(rec *record.AppItemEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateItemEffectRec").Logger()

	// repos
	r := m.RepoStore.AppItemEffectRepo

	currentRec, err := m.GetItemEffectRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.AppItemID == "" {
		rec.AppItemID = currentRec.AppItemID
	}

	if rec.AppEffectID == "" {
		rec.AppEffectID = currentRec.AppEffectID
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteItemEffectRec -
func (m *Model) DeleteItemEffectRec(itemEffectID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteItemEffectRec").Logger()

	// repos
	r := m.RepoStore.AppItemEffectRepo

	err = r.Delete(itemEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

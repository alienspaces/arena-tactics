package item

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		// remove app test data
		td.RemoveAppData(tx)

		tx.Commit()
	}

	return td, teardown
}

func TestNewItem(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)
	if assert.NoError(t, err, "NewItem returns without error") {
		assert.NotNil(t, m, "NewItem returns a new model")
	}

	tx.Rollback()
}

func TestGetItemRec(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)
	if assert.NoError(t, err, "NewItem returns without error") {
		assert.NotNil(t, m, "NewItem returns a new model")

		rec, err := m.GetItemRec(td.AppItemRecs[0].ID)
		if assert.NoError(t, err, "GetItemRec returns without error") {
			assert.NotNil(t, rec, "GetItemRec returns an item record")
		}
	}

	tx.Rollback()
}

func TestGetItemEffectRecs(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)
	if assert.NoError(t, err, "NewItem returns without error") {
		assert.NotNil(t, m, "NewItem returns a new model")

		recs, err := m.GetItemEffectRecs(td.AppItemRecs[0].ID)
		if assert.NoError(t, err, "GetItemEffectRecs returns without error") {
			assert.NotEmpty(t, recs, "GetItemEffectRecs returns a item effect records")
		}
	}

	tx.Rollback()
}

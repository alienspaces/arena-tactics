package entitygroup

import (
	"fmt"

	"github.com/rs/zerolog"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "entitygroup"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	return m, nil
}

// CanModifyGroup - check rules to see whether the group can be removed
// or group members can be added or removed
func (m *Model) CanModifyGroup(entityGroupID string) (bool, error) {

	l := m.Logger.With().Str("function", "CanModifyGroupMembers").Logger()

	// RULES:
	// - Cannot add or remove members from a group if the group is in a fight
	// that has a status of starting or fighting

	fegr := m.RepoStore.AppFightEntityGroupRepo
	fr := m.RepoStore.AppFightRepo

	// find fights with this entity group ID
	params := make(map[string]interface{})
	params["app_entity_group_id"] = entityGroupID

	fgRecs, err := fegr.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get fight entity groups >%v<", err)
		return false, err
	}

	// check fights for starting or fighting
	for _, fgRec := range fgRecs {
		fRec, err := fr.GetByID(fgRec.AppFightID)
		if err != nil {
			l.Warn().Msgf("Failed to get fight >%v<", err)
			return false, err
		}

		if fRec.Status == record.AppFightStatusStarting || fRec.Status == record.AppFightStatusFighting {
			l.Warn().Msgf("Entity group is currently in a fight that is starting or fighting, cannot modify entity group")
			return false, fmt.Errorf("Entity group is currently in a fight that is starting or fighting, cannot modify entity group")
		}
	}

	return true, nil
}

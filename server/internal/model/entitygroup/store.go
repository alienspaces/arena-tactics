package entitygroup

import (
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetEntityGroupRec -
func (m *Model) GetEntityGroupRec(entityGroupID string) (rec *record.AppEntityGroupRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityGroupRec").Logger()

	r := m.RepoStore.AppEntityGroupRepo

	rec, err = r.GetByID(entityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityGroupRecs -
func (m *Model) GetEntityGroupRecs(params map[string]interface{}) (recs []*record.AppEntityGroupRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityGroupRecs").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entities >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEntityGroupRec -
func (m *Model) CreateEntityGroupRec(rec *record.AppEntityGroupRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupRepo

	// apply required values
	err = m.applyCreateEntityGroupValues(rec)
	if err != nil {
		l.Warn().Msgf("Failed to apply entity group defaults >%v<", err)
		return err
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity group member group >%v<", err)
		return err
	}

	return nil
}

// applyCreateEntityGroupValues - Applies required values for a new entity group
func (m *Model) applyCreateEntityGroupValues(rec *record.AppEntityGroupRecord) (err error) {

	l := m.Logger.With().Str("function", "applyCreateEntityGroupValues").Logger()

	l.Info().Msgf("Applying defaults")

	return nil
}

// UpdateEntityGroupRec -
func (m *Model) UpdateEntityGroupRec(rec *record.AppEntityGroupRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupRepo

	// can modify entity group members
	canModify, err := m.CanModifyGroup(rec.ID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify entity group members >%v<", err)
		return err
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity group member group >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityGroupRec -
func (m *Model) DeleteEntityGroupRec(entityGroupID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityGroupRec").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupRepo

	// can modify entity group members
	canModify, err := m.CanModifyGroup(entityGroupID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify entity group members >%v<", err)
		return err
	}

	err = r.Delete(entityGroupID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity group member group >%v<", err)
		return err
	}

	return nil
}

// GROUP MEMBERS

// GetEntityGroupMemberRec -
func (m *Model) GetEntityGroupMemberRec(entityGroupMemberID string) (rec *record.AppEntityGroupMemberRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityGroupMemberRec").Logger()

	r := m.RepoStore.AppEntityGroupMemberRepo

	rec, err = r.GetByID(entityGroupMemberID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group member record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEntityGroupMemberRecs -
func (m *Model) GetEntityGroupMemberRecs(entityGroupID string) (recs []*record.AppEntityGroupMemberRecord, err error) {

	r := m.RepoStore.AppEntityGroupMemberRepo

	params := make(map[string]interface{})
	params["app_entity_group_id"] = entityGroupID

	recs, err = r.GetByParam(params)
	if err != nil {
		m.Logger.Warn().Msgf("getEntityGroupMemberRecs failed to get entity group member group group members >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEntityGroupMemberRec -
func (m *Model) CreateEntityGroupMemberRec(rec *record.AppEntityGroupMemberRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEntityGroupMemberRec").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupMemberRepo

	// can modify entity group members
	canModify, err := m.CanModifyGroup(rec.AppEntityGroupID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify entity group members >%v<", err)
		return err
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create entity group member >%v<", err)
		return err
	}

	return nil
}

// UpdateEntityGroupMemberRec -
func (m *Model) UpdateEntityGroupMemberRec(rec *record.AppEntityGroupMemberRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEntityGroupMemberRec").Logger()

	// repos
	r := m.RepoStore.AppEntityGroupMemberRepo

	// can modify entity group members
	canModify, err := m.CanModifyGroup(rec.AppEntityGroupID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify entity group members >%v<", err)
		return err
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update entity group member >%v<", err)
		return err
	}

	return nil
}

// DeleteEntityGroupMemberRec -
func (m *Model) DeleteEntityGroupMemberRec(entityGroupMemberID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEntityGroupMemberRec").Logger()

	// repos
	egmr := m.RepoStore.AppEntityGroupMemberRepo

	// get entity group ID
	egmRec, err := egmr.GetByID(entityGroupMemberID)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group member >%v<", err)
		return err
	}

	// can modify entity group members
	canModify, err := m.CanModifyGroup(egmRec.AppEntityGroupID)
	if canModify == false || err != nil {
		l.Warn().Msgf("Cannot modify entity group members >%v<", err)
		return err
	}

	err = egmr.Delete(entityGroupMemberID)
	if err != nil {
		l.Warn().Msgf("Failed to delete entity group member >%v<", err)
		return err
	}

	return nil
}

// FIGHT

// GetEntityGroupFightRec -
func (m *Model) GetEntityGroupFightRec(entityGroupID string) (rec *record.AppFightRecord, err error) {

	l := m.Logger.With().Str("function", "GetEntityGroupFightRec").Logger()

	r := m.RepoStore.AppFightEntityGroupRepo
	fr := m.RepoStore.AppFightRepo

	params := make(map[string]interface{})
	params["app_entity_group_id"] = entityGroupID

	recs, err := r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity group fight records >%v<", err)
		return nil, err
	}

	if len(recs) != 1 {
		l.Info().Msgf("Entity group ID >%s< does not currently belong to a fight", entityGroupID)
		return nil, nil
	}

	rec, err = fr.GetByID(recs[0].AppFightID)
	if err != nil {
		l.Warn().Msgf("Failed to get fight record >%v<", err)
		return nil, err
	}

	return rec, nil
}

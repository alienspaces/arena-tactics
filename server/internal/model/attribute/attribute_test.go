package attribute

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"
)

// environment
var e, _ = env.NewEnv()

// logger
var l, _ = logger.NewLogger(e)

// database
var db, _ = database.NewDatabase(e, l)

func setup(t *testing.T) (
	*testingdata.Data, func()) {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		t.Fatalf("Failed to start new tx >%v<", err)
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		t.Fatalf("Failed to create test data >%v<", err)
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		t.Fatalf("Failed to create app test data >%v<", err)
	}

	tx.Commit()

	teardown := func() {

		// tx
		tx, err := db.Beginx()
		if err != nil {
			t.Fatalf("Failed to start new tx >%v<", err)
		}

		// remove app test data
		td.RemoveAppData(tx)

		tx.Commit()
	}

	return td, teardown
}

func TestNewAttribute(t *testing.T) {

	_, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	if assert.NoError(t, err, "NewModel returns without error") {
		assert.NotNil(t, m, "NewModel returns a new model")
	}

	tx.Rollback()
}

func TestGetParams(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	if assert.NoError(t, err, "NewModel returns without error") {
		assert.NotNil(t, m, "NewModel returns a new model")

		params, err := m.GetFormulaParams(td.AppAttributeRecs[0])
		if assert.NoError(t, err, "GetParams returns without error") {
			assert.NotEmpty(t, params, "GetParams returns params")
		}
	}

	tx.Rollback()
}

func TestGetEntityAttributeValue(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	if assert.NoError(t, err, "NewModel returns without error") {
		assert.NotNil(t, m, "NewModel returns a new model")

		for _, entityAttrRecsMap := range td.AppEntityAttributeRecs {

			t.Logf("Testing >%d< attributes", len(entityAttrRecsMap))

			entityAttrRecs := []*record.AppEntityAttributeRecord{}

			// create slice of entity attribute recs
			for _, entityAttrRec := range entityAttrRecsMap {
				entityAttrRecs = append(entityAttrRecs, entityAttrRec)
			}

			// order entity attribute recs
			err := m.OrderEntityAttributeRecs(entityAttrRecs)

			if assert.NoError(t, err, "OrderEntityAttributeRecs returns without error") {

				params := make(map[string]int)

				for _, entityAttrRec := range entityAttrRecs {

					calculatedValue, _, _, err := m.GetEntityAttributeValue(entityAttrRec, params)

					if assert.NoError(t, err, "GetEntityAttributeValue returns without error") {

						assert.NotNil(t, calculatedValue, "GetEntityAttributeValue returns a value")

						// add value to params map
						attrRec, _ := m.GetAttributeRec(entityAttrRec.AppAttributeID)
						params[attrRec.Name] = calculatedValue
					}
				}
			}

			// test one set of entity attributes only
			break
		}
	}

	tx.Rollback()
}

func TestOrderEntityAttributeRecs(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	if assert.NoError(t, err, "NewModel returns without error") {
		assert.NotNil(t, m, "NewModel returns a new model")

		for _, entityAttrRecsMap := range td.AppEntityAttributeRecs {

			t.Logf("Testing >%d< attributes", len(entityAttrRecsMap))

			entityAttrRecs := []*record.AppEntityAttributeRecord{}

			// create slice of entity attribute recs
			for _, entityAttrRec := range entityAttrRecsMap {
				entityAttrRecs = append(entityAttrRecs, entityAttrRec)
				attrRec, _ := m.GetAttributeRec(entityAttrRec.AppAttributeID)
				t.Logf("Before sort have attribute >%s<", attrRec.Name)
			}

			// order entity attribute recs
			err := m.OrderEntityAttributeRecs(entityAttrRecs)

			if assert.NoError(t, err, "OrderEntityAttributes returns without error") {

				for _, entityAttrRec := range entityAttrRecs {

					attrRec, _ := m.GetAttributeRec(entityAttrRec.AppAttributeID)

					t.Logf("After sort have attribute >%s<", attrRec.Name)
				}
			}

			// test one set of entity attributes only
			break
		}
	}

	tx.Rollback()
}

func TestValidateEntityAttributeRecs(t *testing.T) {

	td, teardown := setup(t)
	defer teardown()

	tx, _ := db.Beginx()

	rs, _ := repostore.NewRepoStore(e, l, tx)

	m, err := NewModel(e, l, rs)

	if assert.NoError(t, err, "NewModel returns without error") {
		assert.NotNil(t, m, "NewModel returns a new model")

		for _, entityAttrRecsMap := range td.AppEntityAttributeRecs {

			t.Logf("Testing >%d< attributes", len(entityAttrRecsMap))

			entityAttrRecs := []*record.AppEntityAttributeRecord{}

			// create slice of entity attribute recs
			for _, entityAttrRec := range entityAttrRecsMap {
				entityAttrRecs = append(entityAttrRecs, entityAttrRec)
			}

			// order entity attribute recs
			err := m.OrderEntityAttributeRecs(entityAttrRecs)
			if assert.NoError(t, err, "OrderEntityAttributes returns without error") {

				// validate entity attribute recs
				err := m.ValidateEntityAttributeRecs(entityAttrRecs)
				assert.NoError(t, err, "ValidateEntityAttributes returns without error")
			}

			// test one set of entity attributes only
			break

		}
	}

	tx.Rollback()
}

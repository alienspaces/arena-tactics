package attribute

import (
	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// GetAttributeRec -
func (m *Model) GetAttributeRec(ID string) (rec *record.AppAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppAttributeRepo

	rec, err = r.GetByID(ID)
	if err != nil {
		l.Warn().Msgf("Failed to get attribute >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetAttributeRecs -
func (m *Model) GetAttributeRecs(params map[string]interface{}) (recs []*record.AppAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetAttributeRecs").Logger()

	// repos
	r := m.RepoStore.AppAttributeRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateAttributeRec -
func (m *Model) CreateAttributeRec(rec *record.AppAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppAttributeRepo
	er := m.RepoStore.AppEntityRepo
	ear := m.RepoStore.AppEntityAttributeRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	// add new attribute to all entities
	params := make(map[string]interface{})
	params["app_id"] = rec.AppID
	entRecs, err := er.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity records >%v<", err)
		return err
	}

	for _, entRec := range entRecs {
		entAttrRec := &record.AppEntityAttributeRecord{
			AppEntityID:    entRec.ID,
			AppAttributeID: rec.ID,
		}
		err := ear.Create(entAttrRec)
		if err != nil {
			l.Warn().Msgf("Failed to create entity attribute record >%v<", err)
			return err
		}
	}

	return nil
}

// UpdateAttributeRec -
func (m *Model) UpdateAttributeRec(rec *record.AppAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppAttributeRepo

	currentRec, err := m.GetAttributeRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.Name == "" {
		rec.Name = currentRec.Name
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteAttributeRec -
func (m *Model) DeleteAttributeRec(appAttributeID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppAttributeRepo
	er := m.RepoStore.AppEntityRepo
	ear := m.RepoStore.AppEntityAttributeRepo

	attrRec, err := r.GetByID(appAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get record >%v<", err)
		return err
	}

	err = r.Delete(appAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	// delete attribute from all entities
	params := make(map[string]interface{})
	params["app_id"] = attrRec.AppID
	entRecs, err := er.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get entity records >%v<", err)
		return err
	}

	for _, entRec := range entRecs {
		params = make(map[string]interface{})
		params["app_attribute_id"] = appAttributeID
		params["app_entity_id"] = entRec.ID
		entAttrRecs, err := ear.GetByParam(params)
		if err != nil {
			l.Warn().Msgf("Failed to get entity atttribute records >%v<", err)
			return err
		}
		if len(entAttrRecs) == 0 {
			l.Warn().Msgf("Entity ID >%s< does not have attribute ID >%s<", entRec.ID, appAttributeID)
			continue
		}
		err = ear.Delete(entAttrRecs[0].ID)
		if err != nil {
			l.Warn().Msgf("Failed to delete entity attribute record >%v<", err)
			return err
		}
	}

	return nil
}

package attribute

import (
	"fmt"

	"github.com/Knetic/govaluate"
	"github.com/rs/zerolog"

	// types

	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "attribute"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	err = m.Init()
	if err != nil {
		l.Warn().Msgf("Failed init >%v<", err)
		return nil, err
	}

	return m, nil
}

// Init -
func (m *Model) Init() error {

	return nil
}

// GetFormulaParams - returns array of parameter names from the formula that are required
// to calculate an attribute value.
func (m *Model) GetFormulaParams(rec *record.AppAttributeRecord) (params []string, err error) {

	l := m.Logger.With().Str("function", "GetParams").Logger()

	params, err = getParams(rec.ValueFormula)

	l.Debug().Msgf("Record name >%s< has params >%v<", rec.Name, params)

	return params, nil
}

// GetMinValueFormulaParams -
func (m *Model) GetMinValueFormulaParams(rec *record.AppAttributeRecord) (params []string, err error) {

	l := m.Logger.With().Str("function", "GetMinValueFormulaParams").Logger()

	params, err = getParams(rec.MinValueFormula)

	l.Debug().Msgf("Record name >%s< has min value formula params >%v<", rec.Name, params)

	return params, nil
}

// GetMaxValueFormulaParams -
func (m *Model) GetMaxValueFormulaParams(rec *record.AppAttributeRecord) (params []string, err error) {

	l := m.Logger.With().Str("function", "GetMaxValueFormulaParams").Logger()

	params, err = getParams(rec.MaxValueFormula)

	l.Debug().Msgf("Record name >%s< has max value formula params >%v<", rec.Name, params)

	return params, nil
}

func getParams(formula string) (params []string, err error) {

	expression, err := govaluate.NewEvaluableExpression(formula)
	if err != nil {
		return nil, err
	}

	tokens := expression.Tokens()
	for _, token := range tokens {
		if token.Kind == govaluate.VARIABLE {
			params = append(params, token.Value.(string))
		}
	}

	return params, nil
}

// GetEntityAttributeValues - Sorts, calculates and returns resulting values
func (m *Model) GetEntityAttributeValues(recs []*record.AppEntityAttributeRecord) (map[string]int, error) {

	l := m.Logger.With().Str("function", "GetEntityAttributeValues").Logger()

	// order attributes first
	err := m.OrderEntityAttributeRecs(recs)
	if err != nil {
		l.Warn().Msgf("Failed order entity attribute records >%v<", err)
		return nil, err
	}

	// calculated
	values := make(map[string]int)

	for _, rec := range recs {

		attrRec, err := m.GetAttributeRec(rec.AppAttributeID)
		if err != nil {
			l.Warn().Msgf("Failed get attribute record >%v<", err)
			return nil, err
		}

		// calculate attribute value
		value, _, _, err := m.GetEntityAttributeValue(rec, values)
		if err != nil {
			l.Warn().Msgf("Failed get set attribute calculated value >%v<", err)
			return nil, err
		}

		// assign adjusted value to be used for additional
		// attribute calculations
		values[attrRec.Name] = value

		l.Debug().Msgf("Attribute Name >%s< Calculated >%d<", attrRec.Name, value)
	}

	return values, nil
}

// GetEntityAttributeValue - See GetAttributeValue
func (m *Model) GetEntityAttributeValue(rec *record.AppEntityAttributeRecord, params map[string]int) (
	calculatedValue, minValue, maxValue int,
	err error) {

	l := m.Logger.With().Str("function", "GetEntityAttributeValue").Logger()

	attrRec, err := m.GetAttributeRec(rec.AppAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed get attribute record >%v<", err)
		return 0, 0, 0, err
	}

	// add entity base attribute value to params
	params[attrRec.Name] = rec.Value

	return m.GetAttributeValues(attrRec, params)
}

// GetAttributeValues - returns the calculated, min and max values of an attribute
// based on its formula, params is expected to contain calculated values for existing
// attributes that the formula depends on. Be sure to OrderEntityAttributes before
// attempting to cycle calculating their values.
func (m *Model) GetAttributeValues(rec *record.AppAttributeRecord, params map[string]int) (
	calculatedValue, minValue, maxValue int,
	err error) {

	l := m.Logger.With().Str("function", "GetAttributeValue").Logger()

	evalParams := make(map[string]interface{})
	for pName, pValue := range params {
		evalParams[pName] = pValue
	}

	// calculated value
	expression, err := govaluate.NewEvaluableExpression(rec.ValueFormula)
	if err != nil {
		l.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< value formula error >%v<", rec.Name, err)
	}

	result, err := expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate params >%+v< formula >%s< >%v<", evalParams, rec.ValueFormula, err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< value formula error >%v<", rec.Name, err)
	}

	calculatedValue = int(result.(float64))

	// minimum value
	expression, err = govaluate.NewEvaluableExpression(rec.MinValueFormula)
	if err != nil {
		l.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< min value formula error >%v<", rec.Name, err)
	}

	result, err = expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate >%v<", err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< min value formula error >%v<", rec.Name, err)
	}

	minValue = int(result.(float64))

	// maximum value
	expression, err = govaluate.NewEvaluableExpression(rec.MaxValueFormula)
	if err != nil {
		l.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< max value formula error >%v<", rec.Name, err)
	}

	result, err = expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate >%v<", err)
		return 0, 0, 0, fmt.Errorf("Attribute >%s< max value formula error >%v<", rec.Name, err)
	}

	maxValue = int(result.(float64))

	l.Debug().Msgf("Attribute name >%s< has values calculated >%d< min >%d< max >%d<",
		rec.Name,
		calculatedValue,
		minValue,
		maxValue,
	)

	return calculatedValue, minValue, maxValue, nil
}

// OrderEntityAttributeRecs - see OrderAttributes
func (m *Model) OrderEntityAttributeRecs(attrs []*record.AppEntityAttributeRecord) error {

	l := m.Logger.With().Str("function", "OrderEntityAttributeRecs").Logger()

	l.Debug().Msgf("Ordering entity attribute recordss")

	// build sortable attributes array
	sortable := []*SortableAttribute{}

	for _, rec := range attrs {

		attrRec, err := m.GetAttributeRec(rec.AppAttributeID)
		if err != nil {
			l.Warn().Msgf("Failed to get attribute record >%v<", err)
			return err
		}

		sortable = append(sortable, &SortableAttribute{
			EntityAttributeRec: rec,
			AttributeRec:       attrRec,
		})
	}

	sorter, err := NewSorter(m.Logger, getParams)
	if err != nil {
		l.Warn().Msgf("Failed new sorter >%v<", err)
		return err
	}

	err = sorter.Sort(sortable)
	if err != nil {
		l.Warn().Msgf("Failed sort >%v<", err)
		return err
	}

	// assign sorted attributes back
	attrs = attrs[:0]
	for _, rec := range sortable {
		attrs = append(attrs, rec.EntityAttributeRec)
	}

	return nil
}

// OrderAttributeRecs - orders attributes based on formula parameter dependencies, to
// be used for ordering attributes prior to calculating attribute values.
func (m *Model) OrderAttributeRecs(attrs []*record.AppAttributeRecord) error {

	l := m.Logger.With().Str("function", "OrderAttributeRecs").Logger()

	l.Debug().Msgf("Ordering attribute records")

	// build sortable attributes array
	sortable := []*SortableAttribute{}

	for _, rec := range attrs {
		sortable = append(sortable, &SortableAttribute{
			AttributeRec: rec,
		})
	}

	sorter, err := NewSorter(m.Logger, getParams)
	if err != nil {
		l.Warn().Msgf("Failed new sorter >%v<", err)
		return err
	}

	err = sorter.Sort(sortable)
	if err != nil {
		l.Warn().Msgf("Failed sort >%v<", err)
		return err
	}

	// assign sorted attributes back
	attrs = attrs[:0]
	for _, rec := range sortable {
		attrs = append(attrs, rec.AttributeRec)
	}

	return nil
}

// ValidateAttributes -
func (m *Model) ValidateAttributes(appID string) error {

	l := m.Logger.With().Str("function", "ValidateEntityAttributeRecs").Logger()

	recs, err := m.GetAttributeRecs(map[string]interface{}{
		"app_id": appID,
	})
	if err != nil {
		l.Warn().Msgf("Failed getting attribute records >%v<", err)
		return err
	}

	// order attributes first
	err = m.OrderAttributeRecs(recs)
	if err != nil {
		l.Warn().Msgf("Failed getting attribute records >%v<", err)
		return err
	}

	// validate attributes
	err = m.ValidateAttributeRecs(recs)
	if err != nil {
		l.Warn().Msgf("Failed to validate attribute records >%v<", err)
		return err
	}

	return nil
}

// ValidateEntityAttributeRecs - checks attributes to ensure there are no cyclic
// dependencies in formula params, that is, attribute X cannot depend on
// attribute Y that also depends on attribute X. Also checks that all attribute
// formula are correct. This function assumes attribute records are ordered
// before being passed in.
func (m *Model) ValidateEntityAttributeRecs(attrs []*record.AppEntityAttributeRecord) error {

	l := m.Logger.With().Str("function", "ValidateEntityAttributeRecs").Logger()

	l.Debug().Msgf("Validating entity attributes")

	var recs []*record.AppAttributeRecord

	for _, rec := range attrs {
		attrRec, err := m.GetAttributeRec(rec.AppAttributeID)
		if err != nil {
			l.Warn().Msgf("Failed get attribute rec >%v<", err)
			return err
		}

		recs = append(recs, attrRec)
	}

	return m.ValidateAttributeRecs(recs)
}

// ValidateAttributeRecs - checks attributes to ensure there are no cyclic
// dependencies in formula params, that is, attribute X cannot depend on
// attribute Y that also depends on attribute X. Also checks that all attribute
// formula are correct. This function assumes attribute records are ordered
// before being passed in.
func (m *Model) ValidateAttributeRecs(attrs []*record.AppAttributeRecord) error {

	l := m.Logger.With().Str("function", "ValidateAttributeRecs").Logger()

	l.Debug().Msgf("Validating >%d< attribute records", len(attrs))

	// calculated
	values := make(map[string]int)

	// value
	for _, testAttr := range attrs {

		l.Debug().Msgf("Validating attribute record >%s<", testAttr.Name)

		// add dummy 0 value for current attribute value
		values[testAttr.Name] = 0

		// calculate attribute value
		calcVal, minVal, maxVal, err := m.GetAttributeValues(testAttr, values)
		if err != nil {
			l.Warn().Msgf("Failed get get attribute values >%v<", err)
			return err
		}

		l.Debug().Msgf("Attribute Name >%s< Calculated >%d< Min >%d< Max >%d<", testAttr.Name, calcVal, minVal, maxVal)

		// assign actual calculated value
		values[testAttr.Name] = calcVal

		// list of params the current attribute formula depends on
		testParams, err := m.GetFormulaParams(testAttr)
		if err != nil {
			return fmt.Errorf("Failed get params >%v<", err)
		}

		for _, testParam := range testParams {
			for _, checkAttr := range attrs {

				// skip where the name of the check attribute is not
				// one of the current test attribute param names
				if checkAttr.Name != testParam {
					continue
				}

				// skip if the check attribute is the test attribute
				if checkAttr.Name == testAttr.Name {
					continue
				}

				// list of params the check attribute formula depends on
				checkParams, err := m.GetFormulaParams(checkAttr)
				if err != nil {
					return fmt.Errorf("Failed get params >%v<", err)
				}

				for _, checkParam := range checkParams {

					// check param is not me
					if testAttr.Name != checkParam {
						continue
					}

					return fmt.Errorf("Failed cyclic dependency detected >%s< depends on >%s< which depends on >%s<", testAttr.Name, checkAttr.Name, testAttr.Name)
				}
			}
		}

		l.Debug().Msgf("All good >%s<", testAttr.Name)
	}

	return nil
}

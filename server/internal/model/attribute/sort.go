package attribute

import (
	"fmt"

	"github.com/rs/zerolog"

	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// Sorter -
type Sorter struct {
	Logger       zerolog.Logger
	SwapA        int
	SwapB        int
	GetParamFunc GetParamFunc
	Attributes   []*SortableAttribute
}

// SortableAttribute -
type SortableAttribute struct {
	AttributeRec       *record.AppAttributeRecord
	EntityAttributeRec *record.AppEntityAttributeRecord
}

// GetParamFunc - Function that returns formula parameters from
// an attribute record
type GetParamFunc func(formula string) (params []string, err error)

// NewSorter - Returns a new sorter
func NewSorter(l zerolog.Logger, f GetParamFunc) (*Sorter, error) {

	sorter := &Sorter{
		Logger:       l,
		GetParamFunc: f,
	}

	return sorter, nil
}

// Sort -
func (s *Sorter) Sort(sortAttributes []*SortableAttribute) error {

	l := s.Logger.With().Str("function", "Sort").Logger()

	s.Attributes = sortAttributes

	cycles := 0
	for c := 0; c < s.Len(); c++ {

		l.Debug().Msgf("Sort c >%d<", c)

		if s.Less(c, c+1) {
			s.Swap(s.SwapA, s.SwapB)
			c--
		}

		cycles++
		if cycles == s.Len()*5 {
			l.Debug().Msgf("Cannot sort")
			return fmt.Errorf("Cannot sort")
		}
	}

	sortAttributes = s.Attributes

	return nil
}

func (s *Sorter) Len() int {
	return len(s.Attributes)
}

func (s *Sorter) Swap(i, j int) {

	l := s.Logger.With().Str("function", "Swap").Logger()

	l.Debug().Msgf("Swapping i >%d< j >%d<", i, j)

	s.Attributes[i], s.Attributes[j] = s.Attributes[j], s.Attributes[i]
}

func (s *Sorter) Less(i, j int) bool {

	l := s.Logger.With().Str("function", "Less").Logger()

	l.Debug().Msgf("i >%d< j >%d< length things >%d<", i, j, len(s.Attributes))

	checkAttribute := s.Attributes[i].AttributeRec

	l.Debug().Msgf("checkAttribute name >%s<", checkAttribute.Name)

	for c := 0; c < s.Len(); c++ {

		if c == i {
			continue
		}

		testAttribute := s.Attributes[c].AttributeRec

		variables, err := s.GetParamFunc(testAttribute.ValueFormula)
		if err != nil {
			l.Warn().Msgf("Failed to get params for attribute name >%s<", testAttribute.Name)
		}

		for _, variable := range variables {

			l.Debug().Msgf("-> checkAttribute name >%s< against testAttribute name >%s< var >%s<", checkAttribute.Name, testAttribute.Name, variable)

			if checkAttribute.Name == variable && c < i {

				l.Debug().Msgf("-> Matched, returning true")

				s.SwapA = i
				s.SwapB = c

				return true
			}
		}
	}

	l.Debug().Msgf("-> Did not matched, returning false")

	return false
}

package effect

import (
	"github.com/Knetic/govaluate"
	"github.com/rs/zerolog"

	// services
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/repostore"
)

// Model -
type Model struct {
	Env       env.Env
	Logger    zerolog.Logger
	RepoStore *repostore.RepoStore
}

const (
	// ModelName -
	ModelName string = "effect"
)

// NewModel -
func NewModel(e env.Env, l zerolog.Logger, ms *repostore.RepoStore) (m *Model, err error) {

	// logger
	l = l.With().Str("package", "model/"+ModelName).Logger()

	m = &Model{
		Env:       e,
		Logger:    l,
		RepoStore: ms,
	}

	return m, nil
}

// TODO: Decide whether we are passing records or id's into these model functions

// GetAttributeValue - Returns calculated adjustment for an effect attribute
func (m *Model) GetAttributeValue(appEffectAttributeID string, params map[string]int) (value int, err error) {

	l := m.Logger.With().Str("function", "GetAttributeValue").Logger()

	l.Debug().Msgf("Get effect attribute value ID >%s<", appEffectAttributeID)

	rec, err := m.GetEffectAttributeRec(appEffectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute rec >%v<", err)
		return 0, err
	}

	expression, err := govaluate.NewEvaluableExpression(rec.ValueFormula)
	if err != nil {
		m.Logger.Warn().Msgf("Failed new evaluable expression >%v<", err)
		return 0, err
	}

	evalParams := make(map[string]interface{})
	for pName, pValue := range params {
		evalParams[pName] = pValue
	}

	result, err := expression.Evaluate(evalParams)
	if err != nil {
		l.Warn().Msgf("Failed formula evaluate >%v<", err)
		return 0, err
	}

	value = int(result.(float64))

	l.Debug().Msgf("Effect attribute value >%d<", value)

	return value, nil
}

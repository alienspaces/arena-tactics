package effect

import (
	// types
	"gitlab.com/alienspaces/arena-tactics/server/internal/types/record"
)

// constants
const (
	minDuration int = 0
	maxDuration int = 100
)

// GetEffectRec -
func (m *Model) GetEffectRec(appEffectID string) (rec *record.AppEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetEffectRec").Logger()

	r := m.RepoStore.AppEffectRepo

	rec, err = r.GetByID(appEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEffectRecs -
func (m *Model) GetEffectRecs(params map[string]interface{}) (recs []*record.AppEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetEffectRecs").Logger()

	// repos
	r := m.RepoStore.AppEffectRepo

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get effect records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEffectRec -
func (m *Model) CreateEffectRec(rec *record.AppEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEffectRec").Logger()

	// repos
	r := m.RepoStore.AppEffectRepo

	// minimum duration
	if rec.Duration < minDuration {
		rec.Duration = minDuration
	}

	// maximum duration
	if rec.Duration > maxDuration {
		rec.Duration = maxDuration
	}

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateEffectRec -
func (m *Model) UpdateEffectRec(rec *record.AppEffectRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEffectRec").Logger()

	// repos
	r := m.RepoStore.AppEffectRepo

	currentRec, err := m.GetEffectRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	// fill empty fields
	if rec.Name == "" {
		rec.Name = currentRec.Name
	}

	// minimum duration
	if rec.Duration < minDuration {
		rec.Duration = minDuration
	}

	// maximum duration
	if rec.Duration > maxDuration {
		rec.Duration = maxDuration
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteEffectRec -
func (m *Model) DeleteEffectRec(appEffectID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEffectRec").Logger()

	// repos
	r := m.RepoStore.AppEffectRepo

	err = r.Delete(appEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

// GetItemEffectRec -
func (m *Model) GetItemEffectRec(appItemEffectID string) (rec *record.AppItemEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetItemEffectRec").Logger()

	r := m.RepoStore.AppItemEffectRepo

	rec, err = r.GetByID(appItemEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get item effect rec >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetSkillEffectRec -
func (m *Model) GetSkillEffectRec(appSkillEffectID string) (rec *record.AppSkillEffectRecord, err error) {

	l := m.Logger.With().Str("function", "GetSkillEffectRec").Logger()

	r := m.RepoStore.AppSkillEffectRepo

	rec, err = r.GetByID(appSkillEffectID)
	if err != nil {
		l.Warn().Msgf("Failed to get skill effect rec >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEffectAttributeRec -
func (m *Model) GetEffectAttributeRec(appEffectAttributeID string) (recs *record.AppEffectAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetEffectAttributeRecs").Logger()

	r := m.RepoStore.AppEffectAttributeRepo

	rec, err := r.GetByID(appEffectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute record >%v<", err)
		return nil, err
	}

	return rec, nil
}

// GetEffectAttributeRecs -
func (m *Model) GetEffectAttributeRecs(appEffectID string) (recs []*record.AppEffectAttributeRecord, err error) {

	l := m.Logger.With().Str("function", "GetEffectAttributeRecs").Logger()

	r := m.RepoStore.AppEffectAttributeRepo

	params := make(map[string]interface{})
	params["app_effect_id"] = appEffectID

	recs, err = r.GetByParam(params)
	if err != nil {
		l.Warn().Msgf("Failed to get effect attribute records >%v<", err)
		return nil, err
	}

	return recs, nil
}

// CreateEffectAttributeRec -
func (m *Model) CreateEffectAttributeRec(rec *record.AppEffectAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "CreateEffectAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEffectAttributeRepo

	err = r.Create(rec)
	if err != nil {
		l.Warn().Msgf("Failed to create record >%v<", err)
		return err
	}

	return nil
}

// UpdateEffectAttributeRec -
func (m *Model) UpdateEffectAttributeRec(rec *record.AppEffectAttributeRecord) (err error) {

	l := m.Logger.With().Str("function", "UpdateEffectAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEffectAttributeRepo

	_, err = m.GetEffectAttributeRec(rec.ID)
	if err != nil {
		l.Warn().Msgf("Failed to get current record for update >%v<", err)
		return err
	}

	err = r.Update(rec)
	if err != nil {
		l.Warn().Msgf("Failed to update record >%v<", err)
		return err
	}

	return nil
}

// DeleteEffectAttributeRec -
func (m *Model) DeleteEffectAttributeRec(appEffectAttributeID string) (err error) {

	l := m.Logger.With().Str("function", "DeleteEffectAttributeRec").Logger()

	// repos
	r := m.RepoStore.AppEffectAttributeRepo

	err = r.Delete(appEffectAttributeID)
	if err != nil {
		l.Warn().Msgf("Failed to delete record >%v<", err)
		return err
	}

	return nil
}

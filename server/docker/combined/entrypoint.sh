#!/usr/bin/env bash

echo "=> PWD      : $PWD"
echo "=> APP_HOME : $APP_HOME"

cd $APP_HOME

# include env
source ./dev-bin/environment

load_env
debug_env

# include retry
source ./dev-bin/retry

# migrate
if [ "$1" == "migrate" ]; then

    echo "=> Running 'migrate' command"

    echo "=> Database migrations start"
    retry_cmd ./dev-bin/db-migrate-up
    echo "=> Database migrations done"

# api
elif [ "$1" == "api" ]; then

    echo "=> Running 'api' command"

    echo "=> Starting API"
    arena-tactics-api || exit $?
    echo "=> API shutdown"

# engine
elif [ "$1" == "engine" ]; then

    echo "=> Running 'engine' command"

    echo "=> Starting engine"
    arena-tactics-engine || exit $?
    echo "=> Engine shutdown"

# command line
else
    echo "=> Command: $@"

    exec "$@"
fi

# change to original directory
cd -


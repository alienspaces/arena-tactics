module gitlab.com/alienspaces/arena-tactics/server

require (
	github.com/Knetic/govaluate v3.0.0+incompatible
	github.com/bradfitz/slice v0.0.0-20180809154707-2b758aa73013
	github.com/corpix/uarand v0.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/google/uuid v0.0.0-20161128191214-064e2069ce9c
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.2
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/rs/cors v1.6.0
	github.com/rs/zerolog v1.14.3
	github.com/stretchr/testify v1.3.0
	go4.org v0.0.0-20190313082347-94abd6928b1d // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.0
)

package main

// NOTES:
// Main entry point for running applications
// Polls looking for applications to start / shut down
// Args
//   test  - Starts with test data loaded
// Opts
//   -app_id - Application ID to run
//   -runtimes - Cycles to execute
//   -runspeed - Time between cycles

import (
	"flag"
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	// cmd
	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/engine"

	// testing data
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

const (
	defaultAppID  string = ""
	defaultCycles int    = 0
	argTest       string = "test"
)

func main() {

	// flags
	var appID = flag.String("app-id", defaultAppID, "Optional application ID")
	runTimes := flag.Int("run-times", 0, "Number of engine loop iterations to run")
	runSpeed := flag.Int("run-speed", 0, "Speed in milliseconds engine loop should pause between cycles")
	flag.Parse()

	// environment
	e, err := env.NewEnv()
	if err != nil {
		panic(fmt.Sprintf("Failed new env >%v<", err))
	}

	// logger
	l, err := logger.NewLogger(e)
	if err != nil {
		panic(fmt.Sprintf("Failed new logger >%v<", err))
	}

	// database
	db, err := database.NewDatabase(e, l)
	if err != nil {
		panic(fmt.Sprintf("Failed new database >%v<", err))
	}

	// args
	args := flag.Args()
	for _, arg := range args {
		if arg == argTest {
			l.Info().Msgf("Loading test data")
			err := loadTestData(e, l, db)
			if err != nil {
				panic(fmt.Sprintf("Failed loading test data >%v<", err))
			}
		}
	}

	l.Info().Msgf("Executing with application ID >%s< runtimes >%d<", *appID, *runTimes)

	// engine
	eng, err := engine.NewEngine(e, l, db)
	if err != nil {
		panic(fmt.Sprintf("Failed new server >%v<", err))
	}

	// run
	err = eng.Run(*appID, *runTimes, *runSpeed)
	if err != nil {
		l.Error().Msgf("Failed server run >%v<", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func loadTestData(e env.Env, l zerolog.Logger, db *sqlx.DB) error {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		l.Warn().Msgf("Failed to start new tx >%v<", err)
		return err
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		l.Warn().Msgf("Failed to create test data >%v<", err)
		return err
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		l.Warn().Msgf("Failed to create app test data >%v<", err)
		return err
	}

	tx.Commit()

	return nil
}

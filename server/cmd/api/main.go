// REST API for the game arena-tactics
package main

import (
	"flag"
	"fmt"
	"net/http"

	// cmd
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"

	"gitlab.com/alienspaces/arena-tactics/server/internal/cmd/api/router"
	"gitlab.com/alienspaces/arena-tactics/server/internal/util/testingdata"

	// service
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/database"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/env"
	"gitlab.com/alienspaces/arena-tactics/server/internal/service/logger"
)

const (
	argTest string = "test"
)

func main() {

	// flags
	flag.Parse()

	// environment
	e, err := env.NewEnv()
	if err != nil {
		panic(fmt.Sprintf("Env error: %v", err))
	}

	// logger
	l, err := logger.NewLogger(e)
	if err != nil {
		panic(fmt.Sprintf("Database error: %v", err))
	}

	// database
	db, err := database.NewDatabase(e, l)
	if err != nil {
		panic(fmt.Sprintf("Database error: %v", err))
	}

	// args
	args := flag.Args()
	for _, arg := range args {
		if arg == argTest {
			l.Info().Msgf("Loading test data")
			err := loadTestData(e, l, db)
			if err != nil {
				panic(fmt.Sprintf("Failed loading test data >%v<", err))
			}
		}
	}

	// router
	r, err := router.NewRouter(e, l, db)
	if err != nil {
		panic(fmt.Sprintf("Router error: %v", err))
	}

	// server
	sp := e.Get("APP_SERVER_PORT")
	l.Info().Msgf("Listing on http://0.0.0.0:%s", sp)

	l.Error().Msgf(fmt.Sprintf("%v", http.ListenAndServe(fmt.Sprintf(":%s", sp), r)))
}

func loadTestData(e env.Env, l zerolog.Logger, db *sqlx.DB) error {

	// tx
	tx, err := db.Beginx()
	if err != nil {
		l.Warn().Msgf("Failed to start new tx >%v<", err)
		return err
	}

	// test data
	td, err := testingdata.NewData(e, l)
	if err != nil {
		l.Warn().Msgf("Failed to create test data >%v<", err)
		return err
	}

	// app test data
	err = td.AddAppData(tx)
	if err != nil {
		l.Warn().Msgf("Failed to create app test data >%v<", err)
		return err
	}

	tx.Commit()

	return nil
}

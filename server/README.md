# Server

Arena Tactics engine and API server.

## Development

This is a work in progress, incomplete and prone to inconceivable levels of refactoring.

### Global Dependencies

#### Database Migrations

From outside of an existing Go module

[golang-migrate/migrate](https://github.com/golang-migrate/migrate/tree/master/cli)

```bash
go get -u -d github.com/golang-migrate/migrate/cmd/migrate github.com/lib/pq
go get -u -d github.com/hashicorp/go-multierror
go build -tags 'postgres' -o $GOPATH/bin/migrate github.com/golang-migrate/migrate/cmd/migrate
```

#### Dependency Management

[golang/dep](https://golang.github.io/dep/docs/installation.html)

```bash
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

### Setup

Clone repository

```bash
${GOPATH}/src/gitlab.com/alienspaces/arena-tactics
```

Project dependencies

```bash
cd ${GOPATH}/src/gitlab.com/alienspaces/arena-tactics/server
dep ensure
```

Create `.env` environment file in `$APP_HOME`

```bash
# app home - local
APP_HOME=${GOPATH}/src/gitlab.com/alienspaces/arena-tactics/server

# database
APP_DATABASE_NAME=arena
APP_DATABASE_USER=arena-user
APP_DATABASE_PASSWORD=arena-1234
APP_DATABASE_OWNER_USER=arena-user
APP_DATABASE_OWNER_PASSWORD=arena-1234
APP_DATABASE_HOST=0.0.0.0
APP_DATABASE_PORT=5432

# logger
APP_LOG_LEVEL=debug
APP_LOG_PRETTY=1
```

Start services (requires docker)

- Starts services in Docker
- Executes database migrations
- Compiles executables

```bash
./dev-bin/start-services
```

### Build

```bash
./dev-bin/build
```

### Run

```bash
arena-tactics-api --help
Usage of ./arena-tactics-api:
  -log-level string
      The level of log output
  -server-port string
      The port the server will listen on

arena-tactics-engine --help
  -app-id string
        Optional application ID
  -run-speed int
        Speed in milliseconds engine loop should pause between cycles
  -run-times int
        Number of engine loop iterations to run
```

#### Run server with test data

To run the server and begin with a canned set of application test data.

```bash
go run ./cmd/engine/main.go -run-times=10 test
```

To continue running the server with existing application data.

```bash
go run ./cmd/engine/main.go -run-times=10
```

### Test

Run all package tests

```bash
./dev-bin/test
```

### Dev Bin

See `./dev-bin` directory for more scripts of variable usefulness.
